import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

import '../lib/serial_number.dart';

void main() {
  const MethodChannel channel = MethodChannel('serial_number');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await SerialNumber.firstHddSerialNumber, '42');
  });
}
