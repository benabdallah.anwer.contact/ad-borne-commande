import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:serial_number/serial_number.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _firstHddSerialNumber = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String firstHddSerialNumber;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      firstHddSerialNumber = await SerialNumber.firstHddSerialNumber;
    } on PlatformException {
      firstHddSerialNumber = 'Failed to get first HDD Serial Number.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _firstHddSerialNumber = firstHddSerialNumber;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('First HDD Serial Number: $_firstHddSerialNumber\n'),
        ),
      ),
    );
  }
}
