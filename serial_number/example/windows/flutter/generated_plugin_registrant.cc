//
//  Generated file. Do not edit.
//

#include "generated_plugin_registrant.h"

#include <serial_number/serial_number_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  SerialNumberPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("SerialNumberPlugin"));
}
