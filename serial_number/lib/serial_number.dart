
import 'dart:async';

import 'package:flutter/services.dart';

class SerialNumber {
  static const MethodChannel _channel =
      const MethodChannel('serial_number');

  static Future<String> get firstHddSerialNumber async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
