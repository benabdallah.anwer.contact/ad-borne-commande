class UserAuth {
  final int isvalidated;

  final String dateCreation;

  final String idPointVente;

  final String matricule;

  final String identifiant;

  final String idUtilisateur;

  final String idTypeUtilisateur;

  final String login;

  final String nom;

  final String idPartenaire;

  final String password;

  final int fActif;

  final String prenom;

  final String email;

  final String idProfil;

  UserAuth(
      {this.isvalidated,
      this.dateCreation,
      this.matricule,
      this.identifiant,
      this.fActif,
      this.idUtilisateur,
      this.idPointVente,
      this.idPartenaire,
      this.idProfil,
      this.idTypeUtilisateur,
      this.nom,
      this.prenom,
      this.login,
      this.password,
      this.email});

  factory UserAuth.fromJson(Map<String, dynamic> json) {
    return UserAuth(
      idUtilisateur: json['idUtilisateur'],
      idPointVente: json['idPointVente'],
      idPartenaire: json['idPartenaire'],
      idProfil: json['idProfil'],
      idTypeUtilisateur: json['idTypeUtilisateur'],
      nom: json['nom'],
      prenom: json['prenom'],
      login: json['login'],
      password: json['password'],
      email: json['email'],
      matricule: json['matricule'],
      identifiant: json['identifiant'],
      dateCreation: json['dateCreation'],
      fActif: json['fActif'],
      isvalidated: json['isvalidated'],
    );
  }
}
