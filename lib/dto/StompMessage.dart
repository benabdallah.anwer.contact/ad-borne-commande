class StompMessage {
  String idMessageCommande;
  String idPointVente;
  String idPartenaire;
  DateTime dateEnvoie;
  DateTime dateTraite;
  String body;
  String idClientPartenaire;
  String nom;
  String prenom;
  String ntel;
  int ftraite;
  List<Details> details;

  StompMessage(
      {this.idPointVente,
      this.idPartenaire,
      this.dateEnvoie,
      this.body,
      this.idClientPartenaire,
      this.nom,
      this.prenom,
      this.ntel,
      this.ftraite,
      this.details});

  StompMessage.fromJson(Map<String, dynamic> json) {
    idPointVente = json['idPointVente'];
    idMessageCommande = json['idMessage'];
    idPartenaire = json['idPartenaire'];
    dateEnvoie =
        json['dateEnvoie'] != null ? DateTime.parse(json['dateEnvoie']) : null;
    dateTraite =
        json['dateTraite'] != null ? DateTime.parse(json['dateTraite']) : null;
    body = json['body'];
    idClientPartenaire = json['idClientPartenaire'];
    nom = json['nom'];
    prenom = json['prenom'];
    ntel = json['ntel'];
    ftraite = json['ftraite'];
    if (json['details'] != null) {
      details = new List<Details>();
      json['details'].forEach((v) {
        details.add(new Details.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idPointVente'] = this.idPointVente;
    data['idPartenaire'] = this.idPartenaire;
    data['dateEnvoie'] = this.dateEnvoie.toIso8601String();
    data['dateTraite'] = this.dateTraite.toIso8601String();
    data['body'] = this.body;
    data['idClientPartenaire'] = this.idClientPartenaire;
    data['nom'] = this.nom;
    data['prenom'] = this.prenom;
    data['ntel'] = this.ntel;
    data['ftraite'] = this.ftraite;
    data['idMessage'] = this.idMessageCommande;
    if (this.details != null) {
      data['details'] = this.details.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Details {
  String idProduit;
  double prix;
  double quantite;
  String designation;
  String ingredient;

  Details(
      {this.idProduit,
      this.prix,
      this.quantite,
      this.designation,
      this.ingredient});

  Details.fromJson(Map<String, dynamic> json) {
    idProduit = json['idProduit'];
    prix = json['prix'];
    quantite = json['quantite'];
    designation = json['designation'];
    ingredient = json['ingredient'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idProduit'] = this.idProduit;
    data['prix'] = this.prix;
    data['quantite'] = this.quantite;
    data['designation'] = this.designation;
    data['ingredient'] = this.ingredient;
    return data;
  }
}
