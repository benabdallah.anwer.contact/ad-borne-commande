import 'package:ad_caisse/dto/Clients.dart';
import 'package:ad_caisse/transaction/model/commande_details.dart';

class CommandeClient {
  String idCommande;
  String idSession;
  double montant;
  DateTime dateGeneration;
  DateTime dateCreation;
  int fPaye;
  int numCommande;
  String idPointVente;
  int isForClient;
  double reste;
  String idTable;
  int numTable;
  List<Clients> clients;
  List<CommandeDetailsTransaction> commandeDetailsTransactions = [];

  CommandeClient(
      {this.idCommande,
      this.idSession,
      this.montant,
      this.dateGeneration,
      this.dateCreation,
      this.fPaye,
      this.numCommande,
      this.idPointVente,
      this.isForClient,
      this.reste,
      this.idTable,
      this.numTable,
      this.clients});

  CommandeClient.fromJson(Map<String, dynamic> json) {
    idCommande = json['idCommande'];
    idSession = json['idSession'];
    montant = json['montant'];
    dateGeneration = json['dateGeneration'] != null
        ? DateTime.parse(json['dateGeneration'])
        : null;
    dateCreation = json['dateCreation'] != null
        ? DateTime.parse(json['dateCreation'])
        : null;
    fPaye = json['fPaye'];
    numCommande = json['numCommande'];
    idPointVente = json['idPointVente'];
    isForClient = json['isForClient'];
    reste = json['reste'];
    idTable = json['idTable'];
    numTable = json['numTable'];
    if (json['clients'] != null) {
      clients = new List<Clients>();
      json['clients'].forEach((v) {
        clients.add(new Clients.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idCommande'] = this.idCommande;
    data['idSession'] = this.idSession;
    data['montant'] = this.montant;
    data['dateGeneration'] = this.dateGeneration;
    data['dateCreation'] = this.dateCreation;
    data['fPaye'] = this.fPaye;
    data['numCommande'] = this.numCommande;
    data['idPointVente'] = this.idPointVente;
    data['isForClient'] = this.isForClient;
    data['reste'] = this.reste;
    data['idTable'] = this.idTable;
    data['numTable'] = this.numTable;
    if (this.clients != null) {
      data['clients'] = this.clients.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
