class Produitpointvente {
  final String idproduitPointVente;

  final String idProduit;

  final String idPointVente;

  final double stockReel;

  final double prix;

  Produitpointvente(
      {this.idproduitPointVente,
      this.idProduit,
      this.idPointVente,
      this.stockReel,
      this.prix});

  factory Produitpointvente.fromJson(Map<String, dynamic> json) {
    return Produitpointvente(
      idproduitPointVente: json['idproduitPointVente'],
      idProduit: json['idProduit'],
      idPointVente: json['idPointVente'],
      stockReel: json['stockReel'],
      prix: json['prix'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idproduitPointVente'] = this.idproduitPointVente;
    data['idProduit'] = this.idProduit;
    data['idPointVente'] = this.idPointVente;
    data['prix'] = this.prix;
    data['stockReel'] = this.stockReel;
    return data;
  }
}
