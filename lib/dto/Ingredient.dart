class Ingredient {
  String idIngredient;
  String couleur;
  String designation;
  String idPartenaire;
  int isComps;

  Ingredient(
      {this.idIngredient, this.couleur, this.designation, this.idPartenaire, this.isComps});

  Ingredient.fromJson(Map<String, dynamic> json) {
    idIngredient = json['idIngredient'];
    couleur = json['couleur'];
    designation = json['designation'];
    idPartenaire = json['idPartenaire'];
    isComps = json['isComps'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idIngredient'] = this.idIngredient;
    data['couleur'] = this.couleur;
    data['designation'] = this.designation;
    data['idPartenaire'] = this.idPartenaire;
    data['isComps'] = this.isComps;
    return data;
  }
}
