class Clients {
  String idClient;
  String nom;
  String prenom;
  String cin;
  String dateNaissance;
  String genre;
  String email;
  String nTel;
  int fActive;
  String dateCreation;
  String qrCodeBprice;
  double soldeBprice;

  Clients(
      {this.idClient,
        this.nom,
        this.prenom,
        this.cin,
        this.dateNaissance,
        this.genre,
        this.email,
        this.nTel,
        this.fActive,
        this.dateCreation,
        this.qrCodeBprice,
        this.soldeBprice});

  Clients.fromJson(Map<String, dynamic> json) {
    idClient = json['idClient'];
    nom = json['nom'];
    prenom = json['prenom'];
    cin = json['cin'];
    dateNaissance = json['dateNaissance'];
    genre = json['genre'];
    email = json['email'];
    nTel = json['nTel'];
    fActive = json['fActive'];
    dateCreation = json['dateCreation'];
    qrCodeBprice = json['qrCodeBprice'];
    soldeBprice = json['soldeBprice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idClient'] = this.idClient;
    data['nom'] = this.nom;
    data['prenom'] = this.prenom;
    data['cin'] = this.cin;
    data['dateNaissance'] = this.dateNaissance;
    data['genre'] = this.genre;
    data['email'] = this.email;
    data['nTel'] = this.nTel;
    data['fActive'] = this.fActive;
    data['dateCreation'] = this.dateCreation;
    data['qrCodeBprice'] = this.qrCodeBprice;
    data['soldeBprice'] = this.soldeBprice;
    return data;
  }
}