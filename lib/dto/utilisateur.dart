class Utilisateur {
  DateTime dateCreation = null;

  String email = null;

  int fActif = null;

  String idPartenaire = null;

  String idPointVente = null;

  String idProfil = null;

  String idTypeUtilisateur = null;

  String idUtilisateur = null;

  String identifiant = null;

  int isvalidated = null;

  String login = null;

  String matricule = null;

  String nom = null;

  String password = null;

  String prenom = null;

  Utilisateur();

  @override
  String toString() {
    return 'Utilisateur[dateCreation=$dateCreation, email=$email, fActif=$fActif, idPartenaire=$idPartenaire, idPointVente=$idPointVente, idProfil=$idProfil, idTypeUtilisateur=$idTypeUtilisateur, idUtilisateur=$idUtilisateur, identifiant=$identifiant, isvalidated=$isvalidated, login=$login, matricule=$matricule, nom=$nom, password=$password, prenom=$prenom, ]';
  }

  Utilisateur.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    email = json['email'];
    fActif = json['fActif'];
    idPartenaire = json['idPartenaire'];
    idPointVente = json['idPointVente'];
    idProfil = json['idProfil'];
    idTypeUtilisateur = json['idTypeUtilisateur'];
    idUtilisateur = json['idUtilisateur'];
    identifiant = json['identifiant'];
    isvalidated = json['isvalidated'];
    login = json['login'];
    matricule = json['matricule'];
    nom = json['nom'];
    password = json['password'];
    prenom = json['prenom'];
  }

  Map<String, dynamic> toJson() {
    return {
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'email': email,
      'fActif': fActif,
      'idPartenaire': idPartenaire,
      'idPointVente': idPointVente,
      'idProfil': idProfil,
      'idTypeUtilisateur': idTypeUtilisateur,
      'idUtilisateur': idUtilisateur,
      'identifiant': identifiant,
      'isvalidated': isvalidated,
      'login': login,
      'matricule': matricule,
      'nom': nom,
      'password': password,
      'prenom': prenom
    };
  }

  static List<Utilisateur> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Utilisateur>()
        : json.map((value) => new Utilisateur.fromJson(value)).toList();
  }

  static Map<String, Utilisateur> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Utilisateur>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Utilisateur.fromJson(value));
    }
    return map;
  }
}
