class UpdateDto {
  String id;
  int version;
  int appVersion;
  String url;
  String os;

  UpdateDto({this.id, this.version, this.appVersion, this.url, this.os});

  UpdateDto.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    version = json['version'];
    appVersion = json['appVersion'];
    url = json['url'];
    os = json['os'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['version'] = this.version;
    data['appVersion'] = this.appVersion;
    data['url'] = this.url;
    data['os'] = this.os;
    return data;
  }
}