class VersionDto {
  String id;
  String code;
  String fileName ;
  double latestVersion ;
  DateTime dateVersion;
  String pltaform ;
  String releaseNote;
  String releaseNoteToDisplay ;

  VersionDto({this.id, this.code, this.fileName, this.latestVersion, this.dateVersion,this.pltaform,this.releaseNote,this.releaseNoteToDisplay});

  VersionDto.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code=json['code'];
    fileName=json['fileName'];
    latestVersion=json['latestVersion'];
    dateVersion=json['dateVersion'] != null
        ? DateTime.parse(json['dateVersion'])
        : null;
    pltaform=json['pltaform'];
    releaseNote=json['releaseNote'];
    releaseNoteToDisplay=json['releaseNoteToDisplay'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['fileName'] = this.fileName;
    data['latestVersion'] = this.latestVersion;
    data['dateVersion'] = this.dateVersion;
    data['pltaform'] = this.pltaform;
    data['releaseNote'] = this.releaseNote;
    data['releaseNoteToDisplay'] = this.releaseNoteToDisplay;


    return data;
  }
}