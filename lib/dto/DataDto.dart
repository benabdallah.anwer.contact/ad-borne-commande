import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/admin/model/utilisateur_type.dart';
import 'package:ad_caisse/dto/StompMessage.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/otp/PayementDto.dart';
import 'package:ad_caisse/payement/model/reglm_details_dto.dart';
import 'package:ad_caisse/pos/model/caisse.dart';
import 'package:ad_caisse/pos/model/caisse_type.dart';
import 'package:ad_caisse/pos/model/journee.dart';
import 'package:ad_caisse/pos/model/mode_reglement.dart';
import 'package:ad_caisse/pos/model/operation.dart';
import 'package:ad_caisse/pos/model/point_vente.dart';
import 'package:ad_caisse/pos/model/session.dart';
import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/product/model/categorie_article.dart';
import 'package:ad_caisse/reservation/model/ReservationResponse.dart';
import 'package:ad_caisse/reservation/model/produit_and_qte.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/transaction/model/commande.dart';
import 'package:flutter/material.dart';

class DataDto {
  String commandeType;
  bool isSwitchedRetour = false;
  FocusNode focusNode = FocusNode();
  static bool isConnected = true;
  int lastTicketNumber = null;
  StompMessage stompMessage;
  String ingredients = null;
  Utilisateur selectedUser;
  bool saved = false;
  bool isNumericMode = false;
  UtilisateurType utilisateurType;
  String hint = "Chercher";
  BuildContext buildContext;
  String reservationId;
  Function callbackKeyboard;
  Function searchClient;
  PointVente pointVente;
  Utilisateur utilisateur;
  CaisseType caisseType;
  Caisse caisse;
  Journee journee;
  Session session;
  Function callbackCommande;
  Function setSearchController;
  Function callbackNumbers;
  Function setCurrentPage;
  Function setClientBPrice;
  List<CategorieArticle> listCat;
  List<ModeReglement> modeReg;
  ClientBPrice clientBPrice;
  String currentPage;
  List<ReglmDetailsDto> payementList = [];
  List<PayementDto> payementDtos = [];
  String total;
  double rendu = 0;
  int focused;
  bool hideKeyboard = true;
  List<CommandeDetails> commandeDetails = [];
  List<CommandeDetails> commandeDetailsBeforePack = [];
  List<CommandeDetails> commandeDelete = [];
  CommandeTransaction commande = CommandeTransaction();
  CommandesDtoTransaction commandeDto = CommandesDtoTransaction();
  List<ClientBPrice> listClient = [];
  List<ClientBPrice> listClientFiltred = [];
  String search = "";
  TextEditingController searchController = new TextEditingController();
  TextEditingController textController = new TextEditingController();
  List<CommandesDtoTransaction> listCommande = [];
  List<CommandesDtoTransaction> listTickets;
  DateTime reservationDate = DateTime.now();
  List<ReservationResponse> reservations = [];
  TableCaisse tableCaisse;
  bool isRecharge = false;
  TextEditingController montant = new TextEditingController();
  TextEditingController remise = new TextEditingController();
  TextEditingController comment = new TextEditingController();
  TextEditingController pax = new TextEditingController();
  TimeOfDay finalTime;
  List<String> dropDownList = ["1"];
  List<ProduitAndQte> produitsQtes = [];
  bool isModified = false;
  List<Operation> operationList = [];
  int numberNotifications = 0;
  List<String> idsIngrdComp = [];
  bool loadingTable = false;

  DataDto(
      this.utilisateur,
      this.caisse,
      this.journee,
      this.session,
      this.callbackCommande,
      this.setCurrentPage,
      this.setClientBPrice,
      this.listCat,
      this.modeReg);
}
