//import 'Produitpointvente.dart';

class Product {
  final String dateCreation;

  final String idProduit;

  final String code;

  final int stockReel;

  final int fRacourci;

  final String codeBarre;

  final String description;

  final String typeProduit;

  final double prixHt;

  final String urlImg;

  final Map<String, dynamic> produitpointvente;

  final String idCateg;

  final String idPartenaire;

  final String reference;

  final double prixTtc;

  final int fVendu;

  final String composition;

  final String ajoutePar;

  final String designation;

  final int fAchete;

  final String referenceInterne;

  int quantity;

  double totalPrice;

  Product(
      {this.dateCreation,
      this.idProduit,
      this.code,
      this.stockReel,
      this.fRacourci,
      this.codeBarre,
      this.description,
      this.typeProduit,
      this.prixHt,
      this.urlImg,
      this.produitpointvente,
      this.idCateg,
      this.idPartenaire,
      this.reference,
      this.prixTtc,
      this.fVendu,
      this.composition,
      this.ajoutePar,
      this.designation,
      this.fAchete,
      this.referenceInterne,
      this.quantity,
      this.totalPrice});

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      dateCreation: json['dateCreation'],
      idProduit: json['idProduit'],
      code: json['code'],
      stockReel: json['stockReel'],
      fRacourci: json['fRacourci'],
      codeBarre: json['codeBarre'],
      description: json['description'],
      typeProduit: json['typeProduit'],
      prixHt: json['prixHt'],
      urlImg: json['urlImg'],
      produitpointvente: json['produitpointvente'],
      idCateg: json['idCateg'],
      idPartenaire: json['idPartenaire'],
      reference: json['reference'],
      prixTtc: json['prixTtc'],
      fVendu: json['fVendu'],
      composition: json['composition'],
      ajoutePar: json['ajoutePar'],
      designation: json['designation'],
      fAchete: json['fAchete'],
      referenceInterne: json['referenceInterne'],
    );
  }
}
