class CommandeLivreur {
  DateTime dateAffect;
  String idCommande;
  String idUtilisateur;

  CommandeLivreur({this.dateAffect, this.idCommande, this.idUtilisateur});

  CommandeLivreur.fromJson(Map<String, dynamic> json) {
    dateAffect = json['dateAffect'];
    idCommande = json['idCommande'];
    idUtilisateur = json['idUtilisateur'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dateAffect'] = this.dateAffect.toIso8601String();
    data['idCommande'] = this.idCommande;
    data['idUtilisateur'] = this.idUtilisateur;
    return data;
  }
}