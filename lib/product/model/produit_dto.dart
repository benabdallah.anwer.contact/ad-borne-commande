import 'package:ad_caisse/dto/Produitpointvente.dart';

import 'produit.dart';

class ProduitDto {
  Produit produit = null;
  List<Produitpointvente> produitPointVentes;

  ProduitDto();

  @override
  String toString() {
    return 'ProduitDto[produit=$produit, ]';
  }

  ProduitDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    produit = new Produit.fromJson(json['produit']);
    if (json['produitPointVentes'] != null) {
      produitPointVentes = new List<Produitpointvente>();
      json['produitPointVentes'].forEach((v) {
        produitPointVentes.add(new Produitpointvente.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.produit != null) {
      data['produit'] = this.produit.toJson();
    }
    if (this.produitPointVentes != null) {
      data['produitPointVentes'] =
          this.produitPointVentes.map((v) => v.toJson()).toList();
    }
    return data;
  }

  static List<ProduitDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ProduitDto>()
        : json.map((value) => new ProduitDto.fromJson(value)).toList();
  }

  static Map<String, ProduitDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ProduitDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ProduitDto.fromJson(value));
    }
    return map;
  }
}
