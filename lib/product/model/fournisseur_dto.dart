
class FournisseurDto {
  
  String email = null;
  

  int fAlerte = null;
  

  int fdefaut = null;
  

  String idFournisseur = null;
  

  String idPatenaireBprice = null;
  

  String nom = null;
  

  String societe = null;
  

  String tel = null;
  
  FournisseurDto();

  @override
  String toString() {
    return 'FournisseurDto[email=$email, fAlerte=$fAlerte, fdefaut=$fdefaut, idFournisseur=$idFournisseur, idPatenaireBprice=$idPatenaireBprice, nom=$nom, societe=$societe, tel=$tel, ]';
  }

  FournisseurDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    email =
        json['email']
    ;
    fAlerte =
        json['fAlerte']
    ;
    fdefaut =
        json['fdefaut']
    ;
    idFournisseur =
        json['idFournisseur']
    ;
    idPatenaireBprice =
        json['idPatenaireBprice']
    ;
    nom =
        json['nom']
    ;
    societe =
        json['societe']
    ;
    tel =
        json['tel']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'fAlerte': fAlerte,
      'fdefaut': fdefaut,
      'idFournisseur': idFournisseur,
      'idPatenaireBprice': idPatenaireBprice,
      'nom': nom,
      'societe': societe,
      'tel': tel
     };
  }

  static List<FournisseurDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<FournisseurDto>() : json.map((value) => new FournisseurDto.fromJson(value)).toList();
  }

  static Map<String, FournisseurDto> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, FournisseurDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new FournisseurDto.fromJson(value));
    }
    return map;
  }
}

