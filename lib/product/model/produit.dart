import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/dto/Ingredient.dart';
import 'package:ad_caisse/dto/Produitpointvente.dart';

class Produit {
  String ajoutePar = null;

  String code = null;

  String codeBarre = null;

  String composition = null;

  String couleur = null;

  DateTime dateCreation = null;

  String description = null;

  String designation = null;

  int fAchete = null;

  int fRacourci = null;

  int fVendu = null;

  int fautorisGerant = null;

  String idCateg = null;

  String idPartenaire = null;

  String idProduit = null;

  String idPack = null;

  String mesure = null;

  double prixHt = null;

  double prixTtc = null;

  String reference = null;

  String referenceInterne = null;

  int stockAlert = null;

  int stockQteDepart = null;

  int stockReel = null;

  double tva = null;

  String typeProduit = null;

  String urlImg = null;

  Produitpointvente produitpointvente;

  Utilisateur employe;

  int orderproduct;

  List<Ingredient> ingredients;

  Produit();

  @override
  String toString() {
    return 'Produit[ajoutePar=$ajoutePar, code=$code, codeBarre=$codeBarre, composition=$composition, couleur=$couleur, dateCreation=$dateCreation, description=$description, designation=$designation, fAchete=$fAchete, fRacourci=$fRacourci, fVendu=$fVendu, fautorisGerant=$fautorisGerant, idCateg=$idCateg, idPartenaire=$idPartenaire, idProduit=$idProduit, mesure=$mesure, prixHt=$prixHt, prixTtc=$prixTtc, reference=$reference, referenceInterne=$referenceInterne, stockAlert=$stockAlert, stockQteDepart=$stockQteDepart, stockReel=$stockReel, tva=$tva, typeProduit=$typeProduit, urlImg=$urlImg, ]';
  }

  Produit.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    ajoutePar = json['ajoutePar'];
    code = json['code'];
    codeBarre = json['codeBarre'];
    composition = json['composition'];
    couleur = json['couleur'];
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    description = json['description'];
    designation = json['designation'];
    fAchete = json['fAchete'];
    fRacourci = json['fRacourci'];
    fVendu = json['fVendu'];
    fautorisGerant = json['fautorisGerant'];
    idCateg = json['idCateg'];
    idPartenaire = json['idPartenaire'];
    idProduit = json['idProduit'];
    mesure = json['mesure'];
    prixHt = json['prixHt'];
    prixTtc = json['prixTtc'];
    reference = json['reference'];
    referenceInterne = json['referenceInterne'];
    stockAlert = json['stockAlert'];
    stockQteDepart = json['stockQteDepart'];
    stockReel = json['stockReel'];
    tva = json['tva'];
    typeProduit = json['typeProduit'];
    orderproduct = json['orderproduct'];
    urlImg = json['urlImg'];
    ingredients = json['ingredients'] != null
        ? List.from(json['ingredients'])
            .map((e) => Ingredient.fromJson(e))
            .toList()
        : null;
    produitpointvente = json['produitpointvente'] != null
        ? new Produitpointvente.fromJson(json['produitpointvente'])
        : null;
  }

  Map<String, dynamic> toJson() {
    return {
      'ajoutePar': ajoutePar,
      'code': code,
      'codeBarre': codeBarre,
      'composition': composition,
      'couleur': couleur,
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'description': description,
      'designation': designation,
      'fAchete': fAchete,
      'fRacourci': fRacourci,
      'fVendu': fVendu,
      'fautorisGerant': fautorisGerant,
      'idCateg': idCateg,
      'idPartenaire': idPartenaire,
      'idProduit': idProduit,
      'mesure': mesure,
      'prixHt': prixHt,
      'prixTtc': prixTtc,
      'reference': reference,
      'referenceInterne': referenceInterne,
      'stockAlert': stockAlert,
      'stockQteDepart': stockQteDepart,
      'stockReel': stockReel,
      'tva': tva,
      'typeProduit': typeProduit,
      'urlImg': urlImg,
      'orderproduct': orderproduct,
      'ingredients': ingredients,
      'produitpointvente': this.produitpointvente != null
          ? this.produitpointvente.toJson()
          : null
    };
  }

  static List<Produit> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Produit>()
        : json.map((value) => new Produit.fromJson(value)).toList();
  }

  static Map<String, Produit> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Produit>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Produit.fromJson(value));
    }
    return map;
  }
}
