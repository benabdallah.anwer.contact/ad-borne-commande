
class PointVenteVisibile {
  
  int fvisible = null;
  

  String idPointvente = null;
  
  PointVenteVisibile();

  @override
  String toString() {
    return 'PointVenteVisibile[fvisible=$fvisible, idPointvente=$idPointvente, ]';
  }

  PointVenteVisibile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    fvisible =
        json['fvisible']
    ;
    idPointvente =
        json['idPointvente']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'fvisible': fvisible,
      'idPointvente': idPointvente
     };
  }

  static List<PointVenteVisibile> listFromJson(List<dynamic> json) {
    return json == null ? new List<PointVenteVisibile>() : json.map((value) => new PointVenteVisibile.fromJson(value)).toList();
  }

  static Map<String, PointVenteVisibile> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, PointVenteVisibile>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new PointVenteVisibile.fromJson(value));
    }
    return map;
  }
}

