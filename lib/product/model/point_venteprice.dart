
class PointVenteprice {
  
  String idpointvente = null;
  

  double price = null;
  

  int visible = null;
  
  PointVenteprice();

  @override
  String toString() {
    return 'PointVenteprice[idpointvente=$idpointvente, price=$price, visible=$visible, ]';
  }

  PointVenteprice.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idpointvente =
        json['idpointvente']
    ;
    price =
        json['price']
    ;
    visible =
        json['visible']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'idpointvente': idpointvente,
      'price': price,
      'visible': visible
     };
  }

  static List<PointVenteprice> listFromJson(List<dynamic> json) {
    return json == null ? new List<PointVenteprice>() : json.map((value) => new PointVenteprice.fromJson(value)).toList();
  }

  static Map<String, PointVenteprice> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, PointVenteprice>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new PointVenteprice.fromJson(value));
    }
    return map;
  }
}

