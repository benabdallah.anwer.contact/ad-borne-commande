import 'package:ad_caisse/product/model/pack.dart';
import 'package:ad_caisse/product/model/produit.dart';

class CategorieArticle {
  String couleur = null;

  List<Produit> produits = null;

  List<CategorieArticle> fils = null;

  List<Pack> packs = null;

  DateTime dateCreation = null;

  String description = null;

  String designation = null;

  int fActif = null;

  String idCategorie = null;

  String idCetgorieMere = null;

  String idPartenaire = null;

  String photo = null;

  int order;

  CategorieArticle();

  @override
  String toString() {
    return 'CategorieArticle[couleur=$couleur, dateCreation=$dateCreation, description=$description, designation=$designation, fActif=$fActif, idCategorie=$idCategorie, idCetgorieMere=$idCetgorieMere, idPartenaire=$idPartenaire, photo=$photo, ]';
  }

  CategorieArticle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    couleur = json['couleur'];
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    description = json['description'];
    designation = json['designation'];
    fActif = json['fActif'];
    idCategorie = json['idCategorie'];
    idCetgorieMere = json['idCetgorieMere'];
    idPartenaire = json['idPartenaire'];
    photo = json['photo'];
    produits =
        List.from(json['produits']).map((e) => Produit.fromJson(e)).toList();
    fils = List.from(json['fils'])
        .map((e) => CategorieArticle.fromJson(e))
        .toList();
    if (json['packs'] != null) {
      packs = new List<Pack>();
      json['packs'].forEach((v) {
        packs.add(new Pack.fromJson(v));
      });
    }
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    return {
      'couleur': couleur,
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'description': description,
      'designation': designation,
      'fActif': fActif,
      'idCategorie': idCategorie,
      'idCetgorieMere': idCetgorieMere,
      'idPartenaire': idPartenaire,
      'photo': photo,
      'produits': produits,
      'fils': fils,
      'order': order
    };
  }

  static List<CategorieArticle> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<CategorieArticle>()
        : json.map((value) => new CategorieArticle.fromJson(value)).toList();
  }

  static Map<String, CategorieArticle> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CategorieArticle>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new CategorieArticle.fromJson(value));
    }
    return map;
  }
}
