
class ProduitPointVente {
  
  String idPointVente = null;
  

  String idProduit = null;
  

  String idproduitPointVente = null;
  

  double prix = null;
  

  int stockReel = null;
  
  ProduitPointVente();

  @override
  String toString() {
    return 'ProduitPointVente[idPointVente=$idPointVente, idProduit=$idProduit, idproduitPointVente=$idproduitPointVente, prix=$prix, stockReel=$stockReel, ]';
  }

  ProduitPointVente.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idPointVente =
        json['idPointVente']
    ;
    idProduit =
        json['idProduit']
    ;
    idproduitPointVente =
        json['idproduitPointVente']
    ;
    prix =
        json['prix']
    ;
    stockReel =
        json['stockReel']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'idPointVente': idPointVente,
      'idProduit': idProduit,
      'idproduitPointVente': idproduitPointVente,
      'prix': prix,
      'stockReel': stockReel
     };
  }

  static List<ProduitPointVente> listFromJson(List<dynamic> json) {
    return json == null ? new List<ProduitPointVente>() : json.map((value) => new ProduitPointVente.fromJson(value)).toList();
  }

  static Map<String, ProduitPointVente> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ProduitPointVente>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new ProduitPointVente.fromJson(value));
    }
    return map;
  }
}

