class Produits {
  String idProduit;
  double quantite;

  Produits({this.idProduit, this.quantite});

  Produits.fromJson(Map<String, dynamic> json) {
    idProduit = json['idProduit'];
    quantite = json['quantite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idProduit'] = this.idProduit;
    data['quantite'] = this.quantite;
    return data;
  }
}