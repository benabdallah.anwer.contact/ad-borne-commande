import 'categorie_article.dart';
import 'point_vente_visibile.dart';

class CategoryProductDto {
  CategorieArticle categorieArticle = null;

  List<PointVenteVisibile> idPointVentevisible = [];

  List<String> productIds = [];

  CategoryProductDto();

  @override
  String toString() {
    return 'CategoryProductDto[categorieArticle=$categorieArticle, idPointVentevisible=$idPointVentevisible, productIds=$productIds, ]';
  }

  CategoryProductDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    categorieArticle = new CategorieArticle.fromJson(json['categorieArticle']);
    idPointVentevisible =
        PointVenteVisibile.listFromJson(json['idPointVentevisible']);
    productIds =
        (json['productIds'] as List).map((item) => item as String).toList();
  }

  Map<String, dynamic> toJson() {
    return {
      'categorieArticle': categorieArticle,
      'idPointVentevisible': idPointVentevisible,
      'productIds': productIds
    };
  }

  static List<CategoryProductDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<CategoryProductDto>()
        : json.map((value) => new CategoryProductDto.fromJson(value)).toList();
  }

  static Map<String, CategoryProductDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CategoryProductDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new CategoryProductDto.fromJson(value));
    }
    return map;
  }
}
