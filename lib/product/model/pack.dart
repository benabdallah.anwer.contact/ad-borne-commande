import 'package:ad_caisse/admin/model/utilisateur.dart';

import 'Produits.dart';

class Pack {
  String idPack;
  String idPartenaire;
  String designation;
  String dateCreation;
  double prix;
  int fautoriseGerant;
  double taux;
  int fRacourci;
  double prixTtc = null;
  String idProduit = null;
  String typeProduit = null;
  List<Produits> produits;
  Utilisateur employe;
  String mesure = "";
  dynamic ingredients = null;

  Pack(
      {this.idPack,
      this.idPartenaire,
      this.designation,
      this.dateCreation,
      this.prix,
      this.fautoriseGerant,
      this.taux,
      this.fRacourci});

  Pack.fromJson(Map<String, dynamic> json) {
    idPack = json['idPack'];
    idPartenaire = json['idPartenaire'];
    designation = json['designation'];
    dateCreation = json['dateCreation'];
    prix = json['prix'];
    fautoriseGerant = json['fautoriseGerant'];
    taux = json['taux'];
    fRacourci = json['fRacourci'];
    if (json['produits'] != null) {
      produits = new List<Produits>();
      json['produits'].forEach((v) {
        produits.add(new Produits.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idPack'] = this.idPack;
    data['idPartenaire'] = this.idPartenaire;
    data['designation'] = this.designation;
    data['dateCreation'] = this.dateCreation;
    data['prix'] = this.prix;
    data['fautoriseGerant'] = this.fautoriseGerant;
    data['taux'] = this.taux;
    data['fRacourci'] = this.fRacourci;
    if (this.produits != null) {
      data['produits'] = this.produits.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
