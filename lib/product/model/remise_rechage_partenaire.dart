class RemiseRechagePartenaire {
  DateTime dateCreation = null;

  String idPartenaire = null;

  String idRemiseRechagePartenaire = null;

  int isActif = null;

  double max = null;

  double min = null;

  double valeurRemise = null;

  RemiseRechagePartenaire();

  @override
  String toString() {
    return 'RemiseRechagePartenaire[dateCreation=$dateCreation, idPartenaire=$idPartenaire, idRemiseRechagePartenaire=$idRemiseRechagePartenaire, isActif=$isActif, max=$max, min=$min, valeurRemise=$valeurRemise, ]';
  }

  RemiseRechagePartenaire.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    idPartenaire = json['idPartenaire'];
    idRemiseRechagePartenaire = json['idRemiseRechagePartenaire'];
    isActif = json['isActif'];
    max = json['max'];
    min = json['min'];
    valeurRemise = json['valeurRemise'];
  }

  Map<String, dynamic> toJson() {
    return {
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'idPartenaire': idPartenaire,
      'idRemiseRechagePartenaire': idRemiseRechagePartenaire,
      'isActif': isActif,
      'max': max,
      'min': min,
      'valeurRemise': valeurRemise
    };
  }

  static List<RemiseRechagePartenaire> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<RemiseRechagePartenaire>()
        : json
            .map((value) => new RemiseRechagePartenaire.fromJson(value))
            .toList();
  }

  static Map<String, RemiseRechagePartenaire> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, RemiseRechagePartenaire>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new RemiseRechagePartenaire.fromJson(value));
    }
    return map;
  }
}
