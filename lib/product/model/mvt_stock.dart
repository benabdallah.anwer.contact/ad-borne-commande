
class MvtStock {
  
  DateTime dateMvt = null;
  

  String idCaisseEn = null;
  

  String idMvt = null;
  

  String idProduitProintVente = null;
  

  int quantite = null;
  

  String sens = null;
  

  double valeur = null;
  
  MvtStock();

  @override
  String toString() {
    return 'MvtStock[dateMvt=$dateMvt, idCaisseEn=$idCaisseEn, idMvt=$idMvt, idProduitProintVente=$idProduitProintVente, quantite=$quantite, sens=$sens, valeur=$valeur, ]';
  }

  MvtStock.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateMvt = json['dateMvt'] == null ? null : DateTime.parse(json['dateMvt']);
    idCaisseEn =
        json['idCaisseEn']
    ;
    idMvt =
        json['idMvt']
    ;
    idProduitProintVente =
        json['idProduitProintVente']
    ;
    quantite =
        json['quantite']
    ;
    sens =
        json['sens']
    ;
    valeur =
        json['valeur']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'dateMvt': dateMvt == null ? '' : dateMvt.toIso8601String(),
      'idCaisseEn': idCaisseEn,
      'idMvt': idMvt,
      'idProduitProintVente': idProduitProintVente,
      'quantite': quantite,
      'sens': sens,
      'valeur': valeur
     };
  }

  static List<MvtStock> listFromJson(List<dynamic> json) {
    return json == null ? new List<MvtStock>() : json.map((value) => new MvtStock.fromJson(value)).toList();
  }

  static Map<String, MvtStock> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, MvtStock>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new MvtStock.fromJson(value));
    }
    return map;
  }
}

