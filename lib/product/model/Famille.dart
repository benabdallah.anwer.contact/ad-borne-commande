class Famille {
  String idFamille;
  String designation;
  String idPartenaire;

  Famille({String idFamille, String designation, String idPartenaire}) {
    this.idFamille = idFamille;
    this.designation = designation;
    this.idPartenaire = idPartenaire;
  }



  Famille.fromJson(Map<String, dynamic> json) {
    idFamille = json['idFamille'];
    designation = json['designation'];
    idPartenaire = json['idPartenaire'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idFamille'] = this.idFamille;
    data['designation'] = this.designation;
    data['idPartenaire'] = this.idPartenaire;
    return data;
  }

  static List<Famille> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Famille>()
        : json.map((value) => new Famille.fromJson(value)).toList();
  }
}