class ProduitPV {
  String idProduit;
  String idCateg;
  String idPartenaire;
  String code;
  String designation;
  String description;
  String composition;
  int stockReel;
  int stockAlert;
  int stockQteDepart;
  String dateCreation;
  String ajoutePar;
  String reference;
  double prixHt;
  double prixTtc;
  String urlImg;
  int fVendu;
  int fAchete;
  String typeProduit;
  String codeBarre;
  String referenceInterne;
  int fRacourci;
  String couleur;
  Produitpointvente produitpointvente;
  int fautorisGerant;
  bool recharge = true;
  ProduitPV(
      {this.idProduit,
        this.idCateg,
        this.idPartenaire,
        this.code,
        this.designation,
        this.description,
        this.composition,
        this.stockReel,
        this.stockAlert,
        this.stockQteDepart,
        this.dateCreation,
        this.ajoutePar,
        this.reference,
        this.prixHt,
        this.prixTtc,
        this.urlImg,
        this.fVendu,
        this.fAchete,
        this.typeProduit,
        this.codeBarre,
        this.referenceInterne,
        this.fRacourci,
        this.couleur,
        this.produitpointvente,
        this.fautorisGerant});

  ProduitPV.fromJson(Map<String, dynamic> json) {
    idProduit = json['idProduit'];
    idCateg = json['idCateg'];
    idPartenaire = json['idPartenaire'];
    code = json['code'];
    designation = json['designation'];
    description = json['description'];
    composition = json['composition'];
    stockReel = json['stockReel'];
    stockAlert = json['stockAlert'];
    stockQteDepart = json['stockQteDepart'];
    dateCreation = json['dateCreation'];
    ajoutePar = json['ajoutePar'];
    reference = json['reference'];
    prixHt = json['prixHt'];
    prixTtc = json['prixTtc'];
    urlImg = json['urlImg'];
    fVendu = json['fVendu'];
    fAchete = json['fAchete'];
    typeProduit = json['typeProduit'];
    codeBarre = json['codeBarre'];
    referenceInterne = json['referenceInterne'];
    fRacourci = json['fRacourci'];
    couleur = json['couleur'];
    produitpointvente = json['produitpointvente'] != null
        ? new Produitpointvente.fromJson(json['produitpointvente'])
        : null;
    fautorisGerant = json['fautorisGerant'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idProduit'] = this.idProduit;
    data['idCateg'] = this.idCateg;
    data['idPartenaire'] = this.idPartenaire;
    data['code'] = this.code;
    data['designation'] = this.designation;
    data['description'] = this.description;
    data['composition'] = this.composition;
    data['stockReel'] = this.stockReel;
    data['stockAlert'] = this.stockAlert;
    data['stockQteDepart'] = this.stockQteDepart;
    data['dateCreation'] = this.dateCreation;
    data['ajoutePar'] = this.ajoutePar;
    data['reference'] = this.reference;
    data['prixHt'] = this.prixHt;
    data['prixTtc'] = this.prixTtc;
    data['urlImg'] = this.urlImg;
    data['fVendu'] = this.fVendu;
    data['fAchete'] = this.fAchete;
    data['typeProduit'] = this.typeProduit;
    data['codeBarre'] = this.codeBarre;
    data['referenceInterne'] = this.referenceInterne;
    data['fRacourci'] = this.fRacourci;
    data['couleur'] = this.couleur;
    if (this.produitpointvente != null) {
      data['produitpointvente'] = this.produitpointvente.toJson();
    }
    data['fautorisGerant'] = this.fautorisGerant;
    return data;
  }
}

class Produitpointvente {
  String idproduitPointVente;
  String idProduit;
  String idPointVente;
  double prix;
  double stockReel;

  Produitpointvente(
      {this.idproduitPointVente,
        this.idProduit,
        this.idPointVente,
        this.prix,
        this.stockReel});

  Produitpointvente.fromJson(Map<String, dynamic> json) {
    idproduitPointVente = json['idproduitPointVente'];
    idProduit = json['idProduit'];
    idPointVente = json['idPointVente'];
    prix = json['prix'];
    stockReel = json['stockReel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idproduitPointVente'] = this.idproduitPointVente;
    data['idProduit'] = this.idProduit;
    data['idPointVente'] = this.idPointVente;
    data['prix'] = this.prix;
    data['stockReel'] = this.stockReel;
    return data;
  }
}