import 'package:ad_caisse/product/model/pack.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class PackEndPointApi {
  final ApiClient apiClient;

  PackEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer un pack avec l&#39;ensemble de ses produits et ses points de vente
  ///
  /// Retourner le pack créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;La liste des produits est  vide   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;le partenaire n&#39;existe pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;List PointVente pour ce pack est vide   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; Le produit n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createPackUsingPOST(Pack pack) async {
    Object postBody = pack;

    // verify required params are set
    if (pack == null) {
      throw new ApiException(400, "Missing required param: pack");
    }

    // create path and map variables
    String path = "/v1/createPack".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une Pack
  ///
  /// Retourner une valeur boolean qui indique si la Pack a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -10:&lt;/b&gt;le pack n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteMvtStockUsingDELETE1(String idPack) async {
    Object postBody = null;

    // verify required params are set
    if (idPack == null) {
      throw new ApiException(400, "Missing required param: idPack");
    }

    // create path and map variables
    String path = "/v1/DeletePack/{idPack}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPack" + "}", idPack.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des packs en fonction du idPartenaire
  ///
  /// Retourner list de packs.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des packs est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste packs est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllPacktByIdpartnerUsingGET(String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllPacktByIdpartner/{idPartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer le pack par id
  ///
  /// Retourner le pack par id.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; le pack existe   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -10 :&lt;/b&gt;le pack n&#39;existe pas   &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> findByIdPacktUsingGET(String idPack) async {
    Object postBody = null;

    // verify required params are set
    if (idPack == null) {
      throw new ApiException(400, "Missing required param: idPack");
    }

    // create path and map variables
    String path = "/v1/findByIdPackt/{idPack}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPack" + "}", idPack.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }
}
