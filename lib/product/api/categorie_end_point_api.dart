import 'package:ad_caisse/request/ResponseList.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/category_product_dto.dart';

class CategorieEndPointApi {
  final ApiClient apiClient;

  CategorieEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer  une catégorie d&#39;article
  ///
  /// Retourner la categorie d&#39;article créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object  est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; La catégorie Mére de l&#39;article n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> affecteProduitToCateogrieUsingPOST(
      String idCategoriearticle, List<String> idProduits) async {
    Object postBody = idProduits;

    // verify required params are set
    if (idCategoriearticle == null) {
      throw new ApiException(400, "Missing required param: idCategoriearticle");
    }
    if (idProduits == null) {
      throw new ApiException(400, "Missing required param: idProduits");
    }

    // create path and map variables
    String path = "/v1/affecteProduitToCateogrie/{idCategoriearticle}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idCategoriearticle" + "}", idCategoriearticle.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// créer  une catégorie d&#39;article
  ///
  /// Retourner la categorie d&#39;article créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object  est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; La catégorie Mére de l&#39;article n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createCategorieArticleUsingPOST(
      CategoryProductDto categoryProductDto) async {
    Object postBody = categoryProductDto;

    // verify required params are set
    if (categoryProductDto == null) {
      throw new ApiException(400, "Missing required param: categoryProductDto");
    }

    // create path and map variables
    String path = "/v1/createCategorieArticle".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente
  ///
  /// Retourner la catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; la Categorie article existe&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;la Categorie article n&#39;existe pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdpartenaireUsingGET(String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllCategorieByIdpartenaire/{idPartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des  catégories d&#39;article avec les sous listes pour un point de vente
  ///
  /// Retourner list des categories d&#39;article.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des catégories d&#39;article  est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste catégories d&#39;article  est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllCategorieArticleByIdPVUsingGET(
      String idPointVente) async {
    Object postBody = null;
    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/listcategorieArticleForPointVente/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      debugPrint(response.body);
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// recuperer la liste des catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente
  ///
  /// Retourner la catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; la Categorie article existe&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;la Categorie article n&#39;existe pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllCategoriesbyidcategorieUsingGET(
      String idCategoriearticle) async {
    Object postBody = null;

    // verify required params are set
    if (idCategoriearticle == null) {
      throw new ApiException(400, "Missing required param: idCategoriearticle");
    }

    // create path and map variables
    String path = "/v1/findAllCategoriesbyidcategorie/{idCategoriearticle}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idCategoriearticle" + "}", idCategoriearticle.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des sous catégories d&#39;article
  ///
  /// Retourner la catégories d&#39;article.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; la Categorie article existe&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;la Categorie article n&#39;existe pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findbyidCategorieMererUsingGET(
      String idCategoriearticle) async {
    Object postBody = null;

    // verify required params are set
    if (idCategoriearticle == null) {
      throw new ApiException(400, "Missing required param: idCategoriearticle");
    }

    // create path and map variables
    String path = "/v1/findbyidCategorieMerer/{idCategoriearticle}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idCategoriearticle" + "}", idCategoriearticle.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente
  ///
  /// Retourner la catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; la Categorie article existe&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;la Categorie article n&#39;existe pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findbyidCategorieUsingGET(
      String idCategoriearticle, String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idCategoriearticle == null) {
      throw new ApiException(400, "Missing required param: idCategoriearticle");
    }
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path =
        "/v1/findbyidCategorieAndIdpointVente/{idCategoriearticle}/{idPointVente}"
            .replaceAll("{format}", "json")
            .replaceAll(
                "{" + "idCategoriearticle" + "}", idCategoriearticle.toString())
            .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente
  ///
  /// Retourner la catégories d&#39;article avec les listes des produits pour une categorie article et un point de vente.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; la Categorie article existe&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;la Categorie article n&#39;existe pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findchildbyidCategorieAndIdpointVenteUsingGET(
      String idCategoriearticle, String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idCategoriearticle == null) {
      throw new ApiException(400, "Missing required param: idCategoriearticle");
    }
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path =
        "/v1/findchildbyidCategorieAndIdpointVente/{idCategoriearticle}/{idPointVente}"
            .replaceAll("{format}", "json")
            .replaceAll(
                "{" + "idCategoriearticle" + "}", idCategoriearticle.toString())
            .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier une catégorie d&#39;article
  ///
  /// Retourner la categorie d&#39;article modifiée.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; La catégorie Mére de l&#39;article n&#39;existe pas  &lt;b&gt;result &#x3D; -10 :&lt;/b&gt; La catégorie de l&#39;article n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateCategorieArticleUsingPOST(
      CategoryProductDto categoryProductDto) async {
    Object postBody = categoryProductDto;

    // verify required params are set
    if (categoryProductDto == null) {
      throw new ApiException(400, "Missing required param: categoryProductDto");
    }

    // create path and map variables
    String path = "/v1/updateCategorieArticle".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
