import 'package:ad_caisse/request/ResponseList.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class FournisseurEndPointApi {
  final ApiClient apiClient;

  FournisseurEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// recuperer la liste des fournisseurs
  ///
  /// Retourner list des fournisseurs.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;List Fourinsseur n&#39;est pas vide  &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; List Fourinsseur est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllByIdPatenaireBpriceUsingGET(
      String idpartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idpartenaire == null) {
      throw new ApiException(400, "Missing required param: idpartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllByIdPatenaireBprice/{idpartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idpartenaire" + "}", idpartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// recuperer la liste des fournisseurs
  ///
  /// Retourner list des fournisseurs.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;List Fourinsseur n&#39;est pas vide  &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; List Fourinsseur est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPointVenteUsingGET() async {
    Object postBody = null;

    // verify required params are set

    // create path and map variables
    String path = "/v1/findAllfourinsseur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
