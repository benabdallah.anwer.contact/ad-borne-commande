import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/produit.dart';
import '../model/produit_dto.dart';

class ProductEndPointApi {
  final ApiClient apiClient;

  ProductEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer un produit avec la possibilté de l&#39;affecter à une catégorie d&#39;article
  ///
  /// Retourner le produit créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object product est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt; La catégorie produit n&#39;existe pas  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; La catégorie de l&#39;article n&#39;existe pas  &lt;b&gt;result &#x3D; -10 :&lt;/b&gt; Le type de produit est null   &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createProductforPVUsingPOST(Produit produit) async {
    Object postBody = produit;

    // verify required params are set
    if (produit == null) {
      throw new ApiException(400, "Missing required param: produit");
    }

    // create path and map variables
    String path = "/v1/createProduct".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// créer un produit avec la possibilté de l&#39;affecter à une catégorie d&#39;article
  ///
  /// Retourner le produit créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object product est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; list pointvente est vide  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;Produit est vide  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt; La catégorie produit n&#39;existe pas  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; La catégorie de l&#39;article n&#39;existe pas  &lt;b&gt;result &#x3D; -10 :&lt;/b&gt; Le type de produit est null   &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createProductwithaffectationUsingPOST(
      ProduitDto produitdto) async {
    Object postBody = produitdto;

    // verify required params are set
    if (produitdto == null) {
      throw new ApiException(400, "Missing required param: produitdto");
    }

    // create path and map variables
    String path =
        "/v1/createProductwithaffectation".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// supprimer un produit
  ///
  ///  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre product Id envoyer est null  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; Le produit n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteProductUsingDELETE(String productId) async {
    Object postBody = null;

    // verify required params are set
    if (productId == null) {
      throw new ApiException(400, "Missing required param: productId");
    }

    // create path and map variables
    String path = "/v1/deleteProduct/{productId}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "productId" + "}", productId.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des produits en fonction du idPartenaire
  ///
  /// Retourner list de produit.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des produits est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste produit est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllByPartenaireUsingGET(String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllByPartenaire/{idPartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// recuperer la liste des produits pour une catégorie d&#39;article et un point de vente
  ///
  /// Retourner list de produit et un point de vente.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des produits est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste produit est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt; La catégorie article n&#39;existe pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt; l&#39;id categorie artcile est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllProductByIdCategorieArticleUsingGET(
      String idCategArticle, String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idCategArticle == null) {
      throw new ApiException(400, "Missing required param: idCategArticle");
    }
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path =
        "/v1/findAllProductByIdCategorieArticleAndIdPointVente/{idCategArticle}/{idPointVente}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idCategArticle" + "}", idCategArticle.toString())
            .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des produits qui seront affichées comme raccourci au niveau de la caisse
  ///
  /// Retourner list de produit.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des produits est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste produit est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllProductShortCutByIdPointVenteUsingGET(
      String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/findAllProductShortCutByIdPointVente/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// recuperer le produit par idproduit du produit
  ///
  /// Retourner le produit.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; le produit exist&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt;Le produit n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> findproduitByIdProduitUsingGET(
      String idproduit) async {
    Object postBody = null;

    // verify required params are set
    if (idproduit == null) {
      throw new ApiException(400, "Missing required param: idproduit");
    }

    // create path and map variables
    String path = "/v1/findproduitByIdProduit/{idproduit}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idproduit" + "}", idproduit.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// recuperer la liste des produits en fonction du idPointVente et soit par le code ou la destination
  ///
  /// Retourner list de produit.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des produits est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste produit est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> searchProduitPointVenteByIdPointVenteUsingGET(
      String idPointVente, String value) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }
    if (value == null) {
      throw new ApiException(400, "Missing required param: value");
    }

    // create path and map variables
    String path =
        "/v1/searchProduitPointVenteByIdPointVente/{idPointVente}/{value}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idPointVente" + "}", idPointVente.toString())
            .replaceAll("{" + "value" + "}", value.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// mise à jour d&#39;un produit
  ///
  /// Retourner le produit modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object product est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt; La catégorie produit n&#39;existe pas  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; Le produit n&#39;existe pas  &lt;b&gt;result &#x3D; -10 :&lt;/b&gt; Le type de produit est null   &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateProductUsingPOST(Produit produit) async {
    Object postBody = produit;

    // verify required params are set
    if (produit == null) {
      throw new ApiException(400, "Missing required param: produit");
    }

    // create path and map variables
    String path = "/v1/updateProduct".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des produits en fonction du type produit
  ///
  /// Retourner list de produit.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des produits est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste produit est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllProductByIdPointVenteUsingGET(
      String type, String idparentaire) async {
    Object postBody = null;

    // verify required params are set
    if (type == null) {
      throw new ApiException(400, "Missing required param: type");
    }
    if (idparentaire == null) {
      throw new ApiException(400, "Missing required param: idparentaire");
    }

    // create path and map variables
    String path = "/v1/findAllByTypeProduit/{type}/{idparentaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "type" + "}", type.toString())
        .replaceAll("{" + "idparentaire" + "}", idparentaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  Future<ResponseList> findAllFamilleByPartenaire(
      String idparentaire) async {
    Object postBody = null;

    // verify required params are set

    if (idparentaire == null) {
      throw new ApiException(400, "Missing required param: idparentaire");
    }

    // create path and map variables
    String path = "/v1/findAllFamilleByPartenaire/{idPartenaire}"
        .replaceAll("{format}", "json")

        .replaceAll("{" + "idPartenaire" + "}", idparentaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
    contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }
}
