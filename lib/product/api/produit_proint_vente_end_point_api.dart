import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/produit_point_vente.dart';

class ProduitProintVenteEndPointApi {
  final ApiClient apiClient;

  ProduitProintVenteEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// recuperer la liste des produitsPointVentes en fonction du idPointVente
  ///
  /// Retourner list des produitsPointVentes.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la liste est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  /// recuperer la produit de recharge pour un point de vente
  ///
  /// Retourner un objet produit point vente.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; produit de type recharge exist&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;le point de vente n&#39;existe pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le point de vente n&#39;autorise pas le recharge   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt;Aucun produit de recharge est trouvé  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;input idpointVente est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> findProduitRechargeForPVUsingGET(
      String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/findProduitRechargeForPV/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  Future<ResponseList> findAllByIdPointVenteUsingGET2(
      String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/findAllProduitPointVenteByIdPointVente/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// mise à jour d&#39;un produit
  ///
  /// Retourner le produit modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;objet est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;produit pointvente n&#39;exit pas  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; veuillez verifier l&#39;objet envoyer, il y&#39;a des champs manquants  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateProduitProintVenteUsingPOST(
      ProduitPointVente produitPointVente) async {
    Object postBody = produitPointVente;

    // verify required params are set
    if (produitPointVente == null) {
      throw new ApiException(400, "Missing required param: produitPointVente");
    }

    // create path and map variables
    String path = "/v1/updateProduitProintVente".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
