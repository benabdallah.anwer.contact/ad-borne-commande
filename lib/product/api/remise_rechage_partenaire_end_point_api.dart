import 'package:ad_caisse/product/model/remise_rechage_partenaire.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class RemiseRechagePartenaireEndPointApi {
  final ApiClient apiClient;

  RemiseRechagePartenaireEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer un remiseRechagePartenaire
  ///
  /// Retourner le remiseRechagePartenaire créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;objet est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;veuillez vérifier le chevauchement des remises  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt;partenaire n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createremiseRechagePartenaireUsingPOST(
      RemiseRechagePartenaire remiseRechagePartenaire) async {
    Object postBody = remiseRechagePartenaire;

    // verify required params are set
    if (remiseRechagePartenaire == null) {
      throw new ApiException(
          400, "Missing required param: remiseRechagePartenaire");
    }

    // create path and map variables
    String path =
        "/v1/CreateremiseRechagePartenaire".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une remiseRechagePartenaire
  ///
  /// Retourner une valeur boolean qui indique si la remiseRechagePartenaire a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt; le remise n&#39;exite pas&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteremiseRechagePartenaireUsingDELETE(
      String idremiseRechagePartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idremiseRechagePartenaire == null) {
      throw new ApiException(
          400, "Missing required param: idremiseRechagePartenaire");
    }

    // create path and map variables
    String path =
        "/v1/DeleteremiseRechagePartenaire/{idremiseRechagePartenaire}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idremiseRechagePartenaire" + "}",
                idremiseRechagePartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des remises par partenaire
  ///
  /// Retourner list des remises.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La list n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;La list est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt;partenaire n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllAtifRemiseByIdpartnerUsingGET(
      String idpartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idpartenaire == null) {
      throw new ApiException(400, "Missing required param: idpartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllAtifRemiseByIdpartner/{idpartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idpartenaire" + "}", idpartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      // print("FindAllAtifRemiseByIdpartner: " + response.body);
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// recuperer la liste des remises par partenaire
  ///
  /// Retourner list des remises.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La list n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;La list est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt;partenaire n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPointVenteUsingGET3(String idpartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idpartenaire == null) {
      throw new ApiException(400, "Missing required param: idpartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllRemiseByIdpartner/{idpartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idpartenaire" + "}", idpartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la remiseRechagePartenaire selon l&#39;idremiseRechagePartenaire envoyer
  ///
  /// Retourner la remiseRechagePartenaire selon l&#39;idremiseRechagePartenaire envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; la remise exite&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;la remise n&#39;exite pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdremiseRechagePartenaireUsingGET(
      String idremiseRechagePartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idremiseRechagePartenaire == null) {
      throw new ApiException(
          400, "Missing required param: idremiseRechagePartenaire");
    }

    // create path and map variables
    String path =
        "/v1/findByIdremiseRechagePartenaire/{idremiseRechagePartenaire}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idremiseRechagePartenaire" + "}",
                idremiseRechagePartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
