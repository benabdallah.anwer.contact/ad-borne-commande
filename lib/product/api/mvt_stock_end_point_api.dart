import '../api.dart';

import '../model/mvt_stock.dart';
import 'package:http/http.dart';

import '../api_client.dart';
import '../api_exception.dart';

class MvtStockEndPointApi {
  final ApiClient apiClient;

  MvtStockEndPointApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// créer un MvtStock
  ///
  /// Retourner le MvtStock créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Produit est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;l&#39;objet mouvement stock est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt;Le produit n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createMvtStockUsingPOST(MvtStock mvtStock) async {
    Object postBody = mvtStock;

    // verify required params are set
    if (mvtStock == null) {
      throw new ApiException(400, "Missing required param: mvtStock");
    }

    // create path and map variables
    String path = "/v1/CreateMvtStock".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une MvtStock
  ///
  /// Retourner une valeur boolean qui indique si la MvtStock a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -10 :&lt;/b&gt;le mouvement stock n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteMvtStockUsingDELETE(String idMvtStock) async {
    Object postBody = null;

    // verify required params are set
    if (idMvtStock == null) {
      throw new ApiException(400, "Missing required param: idMvtStock");
    }

    // create path and map variables
    String path = "/v1/DeleteMvtStock/{idMvtStock}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idMvtStock" + "}", idMvtStock.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// recuperer la liste des produits qui seront affichées comme raccourci au niveau de la caisse
  ///
  /// Retourner list de produit.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La liste des mouvements stocks est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;La liste des mouvements stocks est  vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPointVenteUsingGET1(String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/findAllMctByIdPointVente/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la MvtStock selon l&#39;idMvtStock envoyer
  ///
  /// Retourner la MvtStock selon l&#39;idMvtStock envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; MvtStock exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -10 :&lt;/ble mouvement stock n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdMvtStockUsingGET(String idMvtStock) async {
    Object postBody = null;

    // verify required params are set
    if (idMvtStock == null) {
      throw new ApiException(400, "Missing required param: idMvtStock");
    }

    // create path and map variables
    String path = "/v1/findByIdMvtStock/{idMvtStock}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idMvtStock" + "}", idMvtStock.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher le dernièr MvtStock  selon l&#39;idPointVente envoyer
  ///
  /// Retourner le dernièr MvtStock selon l&#39;idMvtStock envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; MvtStock exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -10 :&lt;/ble mouvement stock n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findTop5ByIdProduitProintVenteOrderByDateMvtUsingGET(
      String idProduitProintVente) async {
    Object postBody = null;

    // verify required params are set
    if (idProduitProintVente == null) {
      throw new ApiException(
          400, "Missing required param: idProduitProintVente");
    }

    // create path and map variables
    String path =
        "/v1/findTop5ByIdProduitProintVenteOrderByDateMvt/{idProduitProintVente}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idProduitProintVente" + "}",
                idProduitProintVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher le dernièr MvtStock  selon l&#39;idPointVente envoyer
  ///
  /// Retourner le dernièr MvtStock selon l&#39;idMvtStock envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; MvtStock exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -10 :&lt;/ble mouvement stock n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findTopByIdProduitProintVenteOrderByDateMvtUsingGET(
      String idProduitProintVente) async {
    Object postBody = null;

    // verify required params are set
    if (idProduitProintVente == null) {
      throw new ApiException(
          400, "Missing required param: idProduitProintVente");
    }

    // create path and map variables
    String path =
        "/v1/findTopByIdProduitProintVenteOrderByDateMvt/{idProduitProintVente}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idProduitProintVente" + "}",
                idProduitProintVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier un MvtStock
  ///
  /// Retourner le MvtStock modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Produit est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;l&#39;objet mouvement stock est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id categorie article est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt;Le produit n&#39;existe pas  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt;le mouvement stock n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateMvtStockUsingPUT(MvtStock mvtStock) async {
    Object postBody = mvtStock;

    // verify required params are set
    if (mvtStock == null) {
      throw new ApiException(400, "Missing required param: mvtStock");
    }

    // create path and map variables
    String path = "/v1/UpdateMvtStock".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
