import 'package:ad_caisse/customer/model/mvt_solde_dto.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class MvtSoldeEndPointApi {
  final ApiClient apiClient;

  MvtSoldeEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// Créer un nouveau mouvement du solde
  ///
  /// Retourner Le mouvement du solde créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object crée est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createMouvementSoldeUsingPOST(MvtSoldeDto mvtSoldeDto) async {
    Object postBody = mvtSoldeDto;

    // verify required params are set
    if (mvtSoldeDto == null) {
      throw new ApiException(400, "Missing required param: mvtSoldeDto");
    }

    // create path and map variables
    String path = "/v1/createMouvementSolde".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Retourner tous les mouvements du solde d&#39;un client donné
  ///
  /// Retourner tous les mouvements par IdClientPartner .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Liste des  mouvements retournée avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;La liste est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllMvtSoldeByCustomerUsingPOST(
      String idClientPartner) async {
    Object postBody = null;

    // verify required params are set
    if (idClientPartner == null) {
      throw new ApiException(400, "Missing required param: idClientPartner");
    }

    // create path and map variables
    String path = "/v1/findAllMvtSoldeByCustomer/{idClientPartner}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idClientPartner" + "}", idClientPartner.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Retourner le mouvements du solde par Id donnée
  ///
  /// Retourner le mouvement par IdMouvement .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Le mouvement retourné avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Pas de mouvement  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findMvtSoldeByIdUsingPOST(String idClientPartner) async {
    Object postBody = null;

    // verify required params are set
    if (idClientPartner == null) {
      throw new ApiException(400, "Missing required param: idClientPartner");
    }

    // create path and map variables
    String path = "/v1/findMvtSoldeById/{idMvtSolde}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idClientPartner" + "}", idClientPartner.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
