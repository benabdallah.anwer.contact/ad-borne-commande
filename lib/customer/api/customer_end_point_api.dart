import 'package:ad_caisse/customer/model/client_dto.dart';
import 'package:ad_caisse/customer/model/client_dto_for_update.dart';
import 'package:ad_caisse/customer/model/client_partner_dto.dart';
import 'package:ad_caisse/customer/model/input_client_partner_dto.dart';
import 'package:ad_caisse/customer/model/tel_client_dto_for_update.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class CustomerEndPointApi {
  final ApiClient apiClient;

  CustomerEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// affecter un QrCode à un client du partenaire
  ///
  /// affecter un QrCode à un client : passer les paramètres suivants :  String QrCode, String idClient .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Mise à jour avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Client inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> affectQrCodeToClientPartnerUsingPOST(
      InputClientPartnerDto inputClientPartnerDto) async {
    Object postBody = inputClientPartnerDto;

    // verify required params are set
    if (inputClientPartnerDto == null) {
      throw new ApiException(
          400, "Missing required param: inputClientPartnerDto");
    }

    // create path and map variables
    String path =
        "/v1/affectQrCodeToClientPartner".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Bloquer un client Bprice, son statut est désactivé
  ///
  /// Retourner Le client bloqué et mis à jour.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet Mis à jour succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> blockClientBpriceUsingPOST(String idClient) async {
    Object postBody = idClient;

    // verify required params are set
    if (idClient == null) {
      throw new ApiException(400, "Missing required param: idClient");
    }

    // create path and map variables
    String path = "/v1/blockClientBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Bloquer un client chez le Partenaire, son statut sera désactivé
  ///
  /// Retourner Le client bloqué et mis à jour.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet Mis à jour succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> blockClientPartnerUsingPOST(String idClient) async {
    Object postBody = idClient;

    // verify required params are set
    if (idClient == null) {
      throw new ApiException(400, "Missing required param: idClient");
    }

    // create path and map variables
    String path = "/v1/blockClientPartner".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Créer un nouveau client Bprice avec son QrCode généré
  ///
  /// Retourner Le client créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> createClientBpriceUsingPOST(
      ClientDto clientDto) async {
    Object postBody = clientDto;

    // verify required params are set
    if (clientDto == null) {
      throw new ApiException(400, "Missing required param: clientDto");
    }

    // create path and map variables
    String path = "/v1/createClientBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Créer un nouveau client pour un Partenaire Donnée
  ///
  /// Retourner Le client créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> createClientPartnerUsingPOST(
      ClientPartnerDto clientPartnerDto) async {
    Object postBody = clientPartnerDto;

    // verify required params are set
    if (clientPartnerDto == null) {
      throw new ApiException(400, "Missing required param: clientPartnerDto");
    }

    // create path and map variables
    String path = "/v1/createClientPartner".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Afficher la liste des clients Atifs d&#39;un partenaire donné
  ///
  /// Retourner les clients Actifs selon le IdPartenaire envoyé  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; list client n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;list client est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;un ou plusieurs parametres envoyés sont null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllActiveClientByPartenaireUsingGET(
      String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllActiveClientByPartenaire/{idPartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}
    ApiClient client = ApiClient();
    var response = apiClient != null
        ? await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
            headerParams, formParams, contentType, authNames)
        : await client.invokeAPI(path, 'GET', queryParams, postBody,
            headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      //print("findAllActiveClientByPartenaire: " + response.body);
      return apiClient != null
          ? apiClient.deserialize(response.body, 'Object') as ResponseList
          : client.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Afficher le client selon l&#39;IdPartenaire envoyé
  ///
  /// Retourner la Devise selon l&#39;IdPartenaire envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; list client n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;list client est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPartenaireAndFActiveUsingGET() async {
    Object postBody = null;

    // verify required params are set

    // create path and map variables
    String path = "/v1/findAllClientBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste des clients Bprice  par statut
  ///
  /// Retourner les clients selon le statut envoyé  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; list client n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;list client est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;un ou plusieurs parametres envoyés sont null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllClientBpriceByStatusUsingGET(int fActif) async {
    Object postBody = null;

    // verify required params are set
    if (fActif == null) {
      throw new ApiException(400, "Missing required param: fActif");
    }

    // create path and map variables
    String path = "/v1/findAllClientBpriceByStatus/{fActif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "fActif" + "}", fActif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Retourner tous les clients d&#39;un Partenaire donné
  ///
  /// Retourner tous les clients par IdPartner .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Liste des  Clients retournée avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;La liste est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllClientByIdPartenerUsingPOST(String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path = "/v1/findAllClientByIdPartener/{idPartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Retourner un client Bprice par son IdClient
  ///
  /// Retourner un client Unique .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet Client retourné avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdClientBpriceUsingPOST(String idClient) async {
    Object postBody = idClient;

    // verify required params are set
    if (idClient == null) {
      throw new ApiException(400, "Missing required param: idClient");
    }

    // create path and map variables
    String path = "/v1/findByIdClientBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Rechercher les Clients ajoutés par Journée et par partenaire donnée
  ///
  /// Rechercher les Clients ajoutés par Journée comptable: passer les paramètres suivants : String idPartenaire, String idJournee .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Liste Non vide &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Liste vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findClientByIdJourneeForSpecificPartnerUsingPOST(
      InputClientPartnerDto inputClientPartnerDto) async {
    Object postBody = inputClientPartnerDto;

    // verify required params are set
    if (inputClientPartnerDto == null) {
      throw new ApiException(
          400, "Missing required param: inputClientPartnerDto");
    }

    // create path and map variables
    String path = "/v1/findClientByIdJourneeForSpecificPartner"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Rechercher un client par son QrCode principal
  ///
  /// Retourner le client selon le paramètre qrCodeBprice envoyé  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; client retourné avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;client inexistant  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;un ou plusieurs parametres envoyés sont null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findClientByQrCodeBpriceUsingGET(String qrCodeBprice) async {
    Object postBody = null;

    // verify required params are set
    if (qrCodeBprice == null) {
      throw new ApiException(400, "Missing required param: qrCodeBprice");
    }

    // create path and map variables
    String path = "/v1/findClientByQrCodeBprice/{qrCodeBprice}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "qrCodeBprice" + "}", qrCodeBprice.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Rechercher un client par son QrCode principal et son IdPartenaire
  ///
  /// Retourner le client selon le paramètre qrCodePartner et idPartner envoyé  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; client retourné avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;client inexistant  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;un ou plusieurs parametres envoyés sont null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findClientByQrCodePartnerUsingGET(
      String qrCodePartner, String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (qrCodePartner == null) {
      throw new ApiException(400, "Missing required param: qrCodePartner");
    }
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path = "/v1/findClientByQrCodePartner/{qrCodePartner}/{idPartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "qrCodePartner" + "}", qrCodePartner.toString())
        .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Rechercher Client par téléphone pour un partenaire donnée
  ///
  /// Recherche Client par téléhone pour un partenaire : passer les paramètres suivants :  String nTel, String idPartner .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Client existant et retourné &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Client inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findClientByTelephoneForSpecificPartnerUsingPOST(
      InputClientPartnerDto inputClientPartnerDto) async {
    Object postBody = inputClientPartnerDto;

    // verify required params are set
    if (inputClientPartnerDto == null) {
      throw new ApiException(
          400, "Missing required param: inputClientPartnerDto");
    }

    // create path and map variables
    String path = "/v1/findClientByTelephoneForSpecificPartner"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Rechercher un client d&#39;un partenaire donnée par son IdClient
  ///
  /// Passer uniquement les params suivants :  idClient ET  idPartner .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet Client retourné avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findClientPartnerByIdClientUsingPOST(
      ClientPartnerDto clientPartnerDto) async {
    Object postBody = clientPartnerDto;

    // verify required params are set
    if (clientPartnerDto == null) {
      throw new ApiException(400, "Missing required param: clientPartnerDto");
    }

    // create path and map variables
    String path =
        "/v1/findClientPartnerByIdClient".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Retourner un client Bprice par son Téléphone
  ///
  /// Retourner un client Unique .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet Client retourné avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findCustomerByTelephoneUsingPOST(String numTel) async {
    Object postBody = numTel;

    // verify required params are set
    if (numTel == null) {
      throw new ApiException(400, "Missing required param: numTel");
    }

    // create path and map variables
    String path = "/v1/findCustomerByTelephone".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Rechercher une liste des clients par Tél ou Nom ou prénom ou mail - pour un partenaire donnée
  ///
  /// Passer uniquement Le paramètre rechercheValue ET  idPartner .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; list client n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;list client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> searchClientbyIdPartenaireUsingPOST(
      InputClientPartnerDto inputClientPartnerDto) async {
    Object postBody = inputClientPartnerDto;

    // verify required params are set
    if (inputClientPartnerDto == null) {
      throw new ApiException(
          400, "Missing required param: inputClientPartnerDto");
    }

    // create path and map variables
    String path =
        "/v1/searchClientbyIdPartenaire".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Mise à jour du client par un Admin Bprice
  ///
  /// Mettre à jour Le client indépendament du partenaire : passer l&#39;objet :  nom, Prenom, email, dateNaissance, genre .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Mise à jour avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Client inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateClientBpriceByAdminUsingPOST(
      ClientDtoForUpdate clientDtoForUpdate) async {
    Object postBody = clientDtoForUpdate;

    // verify required params are set
    if (clientDtoForUpdate == null) {
      throw new ApiException(400, "Missing required param: clientDtoForUpdate");
    }

    // create path and map variables
    String path =
        "/v1/updateClientBpriceByAdmin".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Mise à jour d&#39;un client par un partenaire
  ///
  /// Mettre à jour le nom et le prénom d&#39;un client : passer l&#39;objet :ClientPartnerDto et renseigner éventuellement (le nom, prénom) en option, idClient, et id partenaire  .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Mise à jour avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Client inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateClientBpriceByPartnerUsingPOST(
      ClientPartnerDto clientPartnerDto) async {
    Object postBody = clientPartnerDto;

    // verify required params are set
    if (clientPartnerDto == null) {
      throw new ApiException(400, "Missing required param: clientPartnerDto");
    }

    // create path and map variables
    String path =
        "/v1/updateClientBpriceByPartner".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Mise à jour du solde principal du client
  ///
  /// Mettre à jour le solde du client : passer les paramètres suivants :  String idClient, Double amount, String operation .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Mise à jour avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Client inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateSoldeForClientBpriceUsingPOST(
      InputClientPartnerDto inputClientPartnerDto) async {
    Object postBody = inputClientPartnerDto;

    // verify required params are set
    if (inputClientPartnerDto == null) {
      throw new ApiException(
          400, "Missing required param: inputClientPartnerDto");
    }

    // create path and map variables
    String path =
        "/v1/updateSoldeForClientBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Mise à jour du solde client chez le partenaire
  ///
  /// Mettre à jour le solde du client chez le partenaire : passer les paramètres suivants :  String idClient, Double amount, String operation .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Mise à jour avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Client inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateSoldeForClientPartnerUsingPOST(
      InputClientPartnerDto inputClientPartnerDto) async {
    Object postBody = inputClientPartnerDto;

    // verify required params are set
    if (inputClientPartnerDto == null) {
      throw new ApiException(
          400, "Missing required param: inputClientPartnerDto");
    }

    // create path and map variables
    String path =
        "/v1/updateSoldeForClientPartner".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Mise à jour du Téléphone d&#39;un client Bprice
  ///
  /// Mettre à jour Le téléphone d&#39;un client : passer l&#39;objet :  idClient, nTel, et le token envoyé par SMS .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Mise à jour avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Client inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateTelephoneClientUsingPOST(
      TelClientDtoForUpdate telClientDtoForUpdate) async {
    Object postBody = telClientDtoForUpdate;

    // verify required params are set
    if (telClientDtoForUpdate == null) {
      throw new ApiException(
          400, "Missing required param: telClientDtoForUpdate");
    }

    // create path and map variables
    String path = "/v1/updateTelephoneClient".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
