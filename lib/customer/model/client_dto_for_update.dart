class ClientDtoForUpdate {
  DateTime dateNaissance = null;

  String email = null;

  String genre = null;

  String idClient = null;

  String nom = null;

  String prenom = null;

  ClientDtoForUpdate();

  @override
  String toString() {
    return 'ClientDtoForUpdate[dateNaissance=$dateNaissance, email=$email, genre=$genre, idClient=$idClient, nom=$nom, prenom=$prenom, ]';
  }

  ClientDtoForUpdate.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateNaissance = json['dateNaissance'] == null
        ? null
        : DateTime.parse(json['dateNaissance']);
    email = json['email'];
    genre = json['genre'];
    idClient = json['idClient'];
    nom = json['nom'];
    prenom = json['prenom'];
  }

  Map<String, dynamic> toJson() {
    return {
      'dateNaissance':
          dateNaissance == null ? '' : dateNaissance.toIso8601String(),
      'email': email,
      'genre': genre,
      'idClient': idClient,
      'nom': nom,
      'prenom': prenom
    };
  }

  static List<ClientDtoForUpdate> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ClientDtoForUpdate>()
        : json.map((value) => new ClientDtoForUpdate.fromJson(value)).toList();
  }

  static Map<String, ClientDtoForUpdate> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ClientDtoForUpdate>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ClientDtoForUpdate.fromJson(value));
    }
    return map;
  }
}
