class InputClientPartnerDto {
  double amount = null;

  String idClient = null;

  String idJournee = null;

  String idPartner = null;

  String nTel = null;

  String operation = null;

  String qrCode = null;

  String rechercheValue = null;

  InputClientPartnerDto();

  @override
  String toString() {
    return 'InputClientPartnerDto[amount=$amount, idClient=$idClient, idJournee=$idJournee, idPartner=$idPartner, nTel=$nTel, operation=$operation, qrCode=$qrCode, rechercheValue=$rechercheValue, ]';
  }

  InputClientPartnerDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    amount = json['amount'];
    idClient = json['idClient'];
    idJournee = json['idJournee'];
    idPartner = json['idPartner'];
    nTel = json['nTel'];
    operation = json['operation'];
    qrCode = json['qrCode'];
    rechercheValue = json['rechercheValue'];
  }

  Map<String, dynamic> toJson() {
    return {
      'amount': amount,
      'idClient': idClient,
      'idJournee': idJournee,
      'idPartner': idPartner,
      'nTel': nTel,
      'operation': operation,
      'qrCode': qrCode,
      'rechercheValue': rechercheValue
    };
  }

  static List<InputClientPartnerDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<InputClientPartnerDto>()
        : json
            .map((value) => new InputClientPartnerDto.fromJson(value))
            .toList();
  }

  static Map<String, InputClientPartnerDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, InputClientPartnerDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new InputClientPartnerDto.fromJson(value));
    }
    return map;
  }
}
