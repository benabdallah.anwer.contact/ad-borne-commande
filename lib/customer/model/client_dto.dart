class ClientDto {
  DateTime dateCreation = null;

  DateTime dateNaissance = null;

  String email = null;

  String genre = null;

  String nTel = null;

  String nom = null;

  String prenom = null;

  String adress = null;

  ClientDto();

  @override
  String toString() {
    return 'ClientDto[dateCreation=$dateCreation, dateNaissance=$dateNaissance, email=$email, genre=$genre, nTel=$nTel, nom=$nom, prenom=$prenom,adress=$adress ]';
  }

  ClientDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    dateNaissance = json['dateNaissance'] == null
        ? null
        : DateTime.parse(json['dateNaissance']);
    email = json['email'];
    genre = json['genre'];
    nTel = json['nTel'];
    nom = json['nom'];
    prenom = json['prenom'];
    adress = json['adress'];
  }

  Map<String, dynamic> toJson() {
    return {
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'dateNaissance':
          dateNaissance == null ? '' : dateNaissance.toIso8601String(),
      'email': email,
      'genre': genre,
      'nTel': nTel,
      'nom': nom,
      'prenom': prenom,
      'adress':adress
    };
  }

  static List<ClientDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ClientDto>()
        : json.map((value) => new ClientDto.fromJson(value)).toList();
  }

  static Map<String, ClientDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ClientDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ClientDto.fromJson(value));
    }
    return map;
  }
}
