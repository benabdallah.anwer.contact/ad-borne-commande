class MvtSoldeDto {
  String idClientPartenaire = null;

  double montant = null;

  String sens = null;

  String source = null;

  MvtSoldeDto();

  @override
  String toString() {
    return 'MvtSoldeDto[idClientPartenaire=$idClientPartenaire, montant=$montant, sens=$sens, source=$source, ]';
  }

  MvtSoldeDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idClientPartenaire = json['idClientPartenaire'];
    montant = json['montant'];
    sens = json['sens'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    return {
      'idClientPartenaire': idClientPartenaire,
      'montant': montant,
      'sens': sens,
      'source': source
    };
  }

  static List<MvtSoldeDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<MvtSoldeDto>()
        : json.map((value) => new MvtSoldeDto.fromJson(value)).toList();
  }

  static Map<String, MvtSoldeDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, MvtSoldeDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new MvtSoldeDto.fromJson(value));
    }
    return map;
  }
}
