class TelClientDtoForUpdate {
  String idClient = null;

  String nTel = null;

  String token = null;

  TelClientDtoForUpdate();

  @override
  String toString() {
    return 'TelClientDtoForUpdate[idClient=$idClient, nTel=$nTel, token=$token, ]';
  }

  TelClientDtoForUpdate.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idClient = json['idClient'];
    nTel = json['nTel'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    return {'idClient': idClient, 'nTel': nTel, 'token': token};
  }

  static List<TelClientDtoForUpdate> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<TelClientDtoForUpdate>()
        : json
            .map((value) => new TelClientDtoForUpdate.fromJson(value))
            .toList();
  }

  static Map<String, TelClientDtoForUpdate> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, TelClientDtoForUpdate>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new TelClientDtoForUpdate.fromJson(value));
    }
    return map;
  }
}
