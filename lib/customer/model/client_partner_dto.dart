class ClientPartnerDto {
  String idClient = null;

  String idJournee = null;

  String idPartenaire = null;

  String nom = null;

  String otp = null;

  String password = null;

  String prenom = null;

  String qrCode = null;

  String sourceCreation = null;

  String tokenNotification = null;

  String adress = null;

  ClientPartnerDto();

  @override
  String toString() {
    return 'ClientPartnerDto[idClient=$idClient, idJournee=$idJournee, idPartenaire=$idPartenaire, nom=$nom, otp=$otp, password=$password, prenom=$prenom, qrCode=$qrCode, sourceCreation=$sourceCreation, tokenNotification=$tokenNotification,adress=$adress ]';
  }

  ClientPartnerDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idClient = json['idClient'];
    idJournee = json['idJournee'];
    idPartenaire = json['idPartenaire'];
    nom = json['nom'];
    otp = json['otp'];
    password = json['password'];
    prenom = json['prenom'];
    qrCode = json['qrCode'];
    sourceCreation = json['sourceCreation'];
    tokenNotification = json['tokenNotification'];
    adress = json['adress'];
  }

  Map<String, dynamic> toJson() {
    return {
      'idClient': idClient,
      'idJournee': idJournee,
      'idPartenaire': idPartenaire,
      'nom': nom,
      'otp': otp,
      'password': password,
      'prenom': prenom,
      'qrCode': qrCode,
      'sourceCreation': sourceCreation,
      'tokenNotification': tokenNotification,
      'adress':adress
    };
  }

  static List<ClientPartnerDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ClientPartnerDto>()
        : json.map((value) => new ClientPartnerDto.fromJson(value)).toList();
  }

  static Map<String, ClientPartnerDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ClientPartnerDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ClientPartnerDto.fromJson(value));
    }
    return map;
  }
}
