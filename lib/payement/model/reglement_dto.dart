import 'package:ad_caisse/payement/model/reglm_details_dto.dart';

class ReglementDto {
  DateTime dateTransaction = null;

  String idCommande = null;

  String idPointVente = null;

  String idSessionServer = null;

  List<ReglmDetailsDto> reglements = [];

  ReglementDto();

  @override
  String toString() {
    return 'ReglementDto[dateTransaction=$dateTransaction, idCommande=$idCommande, idPointVente=$idPointVente, reglements=$reglements, ]';
  }

  ReglementDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateTransaction = json['dateTransaction'] == null
        ? null
        : DateTime.parse(json['dateTransaction']);
    idCommande = json['idCommande'];
    idPointVente = json['idPointVente'];
    reglements = ReglmDetailsDto.listFromJson(json['reglements']);
    idSessionServer = json['idSessionServer'];
  }

  Map<String, dynamic> toJson() {
    return {
      'dateTransaction':
          dateTransaction == null ? '' : dateTransaction.toIso8601String(),
      'idCommande': idCommande,
      'idPointVente': idPointVente,
      'reglements': reglements,
      'idSessionServer':idSessionServer
    };
  }

  static List<ReglementDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ReglementDto>()
        : json.map((value) => new ReglementDto.fromJson(value)).toList();
  }

  static Map<String, ReglementDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ReglementDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ReglementDto.fromJson(value));
    }
    return map;
  }
}
