class ReglmDetailsDto {
  DateTime datePourEncaissement = null;

  DateTime dateReg = null;

  String idClientPartenaire = null;

  String idCommande = null;

  String idModReg = null;

  int isRejete = null;

  double montant = null;

  int num = null;

  String numReg = null;

  ReglmDetailsDto();

  @override
  String toString() {
    return 'ReglmDetailsDto[datePourEncaissement=$datePourEncaissement, dateReg=$dateReg, idClientPartenaire=$idClientPartenaire, idCommande=$idCommande, idModReg=$idModReg, isRejete=$isRejete, montant=$montant, num=$num, numReg=$numReg, ]';
  }

  ReglmDetailsDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    datePourEncaissement = json['datePourEncaissement'] == null || json['datePourEncaissement'] == ''
        ? null
        : DateTime.parse(json['datePourEncaissement']);
    dateReg = json['dateReg'] == null|| json['dateReg'] =='' ? null : DateTime.parse(json['dateReg']);
    idClientPartenaire = json['idClientPartenaire'];
    idCommande = json['idCommande'];
    idModReg = json['idModReg'];
    isRejete = json['isRejete'];
    montant = json['montant'];
    num = json['num'];
    numReg = json['numReg'];
  }

  Map<String, dynamic> toJson() {
    return {
      'datePourEncaissement': datePourEncaissement == null
          ? ''
          : datePourEncaissement.toIso8601String(),
      'dateReg': dateReg == null ? '' : dateReg.toIso8601String(),
      'idClientPartenaire': idClientPartenaire,
      'idCommande': idCommande,
      'idModReg': idModReg,
      'isRejete': isRejete,
      'montant': montant,
      'num': num,
      'numReg': numReg
    };
  }

  static List<ReglmDetailsDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ReglmDetailsDto>()
        : json.map((value) => new ReglmDetailsDto.fromJson(value)).toList();
  }

  static Map<String, ReglmDetailsDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ReglmDetailsDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ReglmDetailsDto.fromJson(value));
    }
    return map;
  }
}
