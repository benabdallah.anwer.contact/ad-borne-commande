import 'package:ad_caisse/payement/model/reglement_dto.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class PaymentEndPointApi {
  final ApiClient apiClient;

  PaymentEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer et verifier les reglements concernant  une transaction de paiement
  ///
  /// Retourner le code 1 si tous les conditions sont remplis.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; paiement créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;La commande n&#39;existe pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt;Le mode de reglement renseigné n&#39;existe pas  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; Le numéro du reglement n&#39;est pas saisi (exp:cheque)  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; La date d&#39;encaissement du reglement n&#39;est pas saisie (exp: date cheque)  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; Le code de la carte est vide  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt;La carte est non reconnue  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt;La carte n&#39;est plus actif  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt;La carte exige un nombre minimale de consommataion et visite  &lt;b&gt;result &#x3D; -10 :&lt;/b&gt;La carte exige un solde minimal accumulé  &lt;b&gt;result &#x3D; -11 :&lt;/b&gt;L&#39;utilisation de la carte de fidélité n&#39;est pas permise en ce moment  &lt;b&gt;result &#x3D; -12 :&lt;/b&gt;Le client n&#39;exist pas  &lt;b&gt;result &#x3D; -17 :&lt;/b&gt;Le solde de la carte est insuffisant  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> checkAndSavePaymentUsingPOST(
      ReglementDto regls) async {
    Object postBody = regls;
    // print("ReglementDtoHamdi: "+jsonEncode(regls));
    // verify required params are set
    if (regls == null) {
      throw new ApiException(400, "Missing required param: regls");
    }
    debugPrint("***************************: $regls");
    // create path and map variables
    String path = "/v1/checkAndSavePayment".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);
    ApiClient client = ApiClient();
    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient != null
          ? apiClient.deserialize(response.body, 'ObjectSingle')
      as ResponseSingle
          : client.deserialize(response.body, 'ObjectSingle') as ResponseSingle;
    }else {
      return null;
    }
  }
}
