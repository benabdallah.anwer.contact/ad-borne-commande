import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/product/api/product_end_point_api.dart';
import 'package:ad_caisse/product/model/produit_dto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/reservation/api/reservation_end_point_api.dart';
import 'package:ad_caisse/reservation/model/ReservationResponse.dart';
import 'package:ad_caisse/reservation/model/produit_and_qte.dart';
import 'package:ad_caisse/reservation/model/reservation_dto.dart';
import 'package:ad_caisse/screens/NewReservation.dart';
import 'package:ad_caisse/screens/Reservation.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:intl/intl.dart' show DateFormat, Intl;

class ReservationCoiff extends StatefulWidget {
  final DataDto dataDto;

  ReservationCoiff(this.dataDto);

  @override
  ReservationState createState() => ReservationState(this.dataDto);
}

class ReservationState extends State<ReservationCoiff> {
  final DataDto dataDto;
  List<Event> events = [];
  String search = "";
  ReservationEndPointApi reservationEndPointApi = ReservationEndPointApi();
  List<Utilisateur> services = [];
  bool light = true;

  ReservationState(this.dataDto);

  final utilisateurEndPointApi = UtilisateurEndPointApi();

  @override
  void initState() {
    dataDto.searchClient("Rechercher");
    dataDto.searchController.addListener(_printLatestValue);
    getAllEmpl();
    getAllReservation(dataDto.reservationDate);
    super.initState();
  }

  @override
  void dispose() {
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  void getAllEmpl() async {
    try {
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1 && responseList.objectResponse != null) {
        setState(() {
          services = responseList.objectResponse
              .map((e) => Utilisateur.fromJson(e))
              .toList();
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("ReservationCoiff getAllEmpl", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getAllReservation(DateTime dateTime) async {
    try {
      ProductEndPointApi productEndPointApi = ProductEndPointApi();
      ReservationDto reservationDto = ReservationDto();
      reservationDto.idPointVente = dataDto.caisse.idPointVente;
      reservationDto.calledfrom = "CAISSE";
      reservationDto.dateReseravation = dateTime;
      ResponseList responseList = await reservationEndPointApi
          .findReservationByDateAndIdPointVenteUsingPOST(reservationDto);
      List<ReservationResponse> res = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        res = responseList.objectResponse
            .map((e) => ReservationResponse.fromJson(e))
            .toList();
      }

      for (ReservationResponse element in res) {
        List<ProduitAndQte> listp = [];
        if (element.vReservationTable != null &&
            element.vReservationTable.produitsQtes != null) {
          listp = element.vReservationTable.produitsQtes;
        }
        for (ProduitAndQte pr in listp) {
          ResponseSingle single = await productEndPointApi
              .findproduitByIdProduitUsingGET(pr.idProduit);
          if (single.result == 1 && single.objectResponse != null) {
            ProduitDto produit = ProduitDto.fromJson(single.objectResponse);
            produit.produit.produitpointvente = produit.produitPointVentes
                        .where((element) =>
                            element.idPointVente ==
                            dataDto.pointVente.idPointVente)
                        .toList()
                        .length >
                    0
                ? produit.produitPointVentes
                    .where((element) =>
                        element.idPointVente == dataDto.pointVente.idPointVente)
                    .toList()[0]
                : null;
            pr.produit = produit.produit;
          }
        }
      }
      setState(() {
        dataDto.reservations = res;
      });
    } catch (err) {
      Crashlytics()
          .sendEmail("ReservationCoiff getAllReservation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void callDatePicker() async {
    DateTime order = await getDate();
    if (order != null) {
      DateTime dateTime = order;
      setState(() {
        dataDto.reservationDate = dateTime;
      });
      getAllReservation(dateTime);
    }
  }

  Future<DateTime> getDate() {
    // Imagine that this function is
    // more complex and slow.
    return showDatePicker(
      context: context,
      helpText: "Date de réservation",
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(2050),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: Color(0xff352C60),
          ),
          child: child,
        );
      },
    );
  }

  ReservationResponse getReservation(int time, Utilisateur u) {
    var list = dataDto.reservations
        .where((element) =>
            element.vReservationTable != null &&
            element.vReservationTable.dateReseravation.hour == time &&
            element.vReservationTable.employee != null &&
            element.vReservationTable.employee.idUtilisateur == u.idUtilisateur)
        .toList();
    if (list.length > 0) {
      return list[0];
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Intl.defaultLocale = 'fr';

    var dayLight = TableRow(
        decoration: BoxDecoration(color: Color(0xffCF5FFE)),
        children: [
          TableCell(
            child: Container(
              child: Text(
                '00H-01H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
            child: Container(
              child: Text(
                '01H-02H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
              child: Container(
            child: Text(
              '02H-03H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '03H-04H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '04H-05H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '05H-06H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '06H-07H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '07H-08H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '08H-09H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '09H-10H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '10H-11H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '11H-12H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
        ]);

    var dayNight = TableRow(
        decoration: BoxDecoration(color: Color(0xffCF5FFE)),
        children: [
          TableCell(
            child: Container(
              child: Text(
                '12H-13H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
            child: Container(
              child: Text(
                '13H-14H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
              child: Container(
            child: Text(
              '14H-15H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '15H-16H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '16H-17H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '17H-18H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '18H-19H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '19H-20H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '20H-21H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '21H-22H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '22H-23H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '23H-00H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
        ]);

    var tableLight = Table(
      border: TableBorder.all(
          color: Colors.white, width: 1, style: BorderStyle.solid),
      children: [
        dayLight,
        for (Utilisateur u in services)
          TableRow(children: [
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(0, u) != null &&
                        getReservation(0, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(0, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(0, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 0, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                    dataDto.pointVente.fGestionTable != 1
                        ? getReservation(0, u) != null &&
                                getReservation(0, u)
                                        .vReservationTable
                                        .produitsQtes[0]
                                        .produit !=
                                    null
                            ? getReservation(0, u).prenomClient +
                                " " +
                                getReservation(0, u).nomClient +
                                '\n\n' +
                                getReservation(0, u)
                                    .vReservationTable
                                    .produitsQtes[0]
                                    .produit
                                    .designation
                            : ""
                        : "",
                    style: TextStyle(fontSize: width / 120),
                    textAlign: TextAlign.center,
                  ),
                  color: getReservation(0, u) != null &&
                          getReservation(0, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(0, u) != null &&
                              getReservation(0, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(0, u) != null &&
                                  getReservation(0, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(1, u) != null &&
                        getReservation(1, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(1, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(1, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 1, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(1, u) != null &&
                                  getReservation(1, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(1, u).prenomClient +
                                  " " +
                                  getReservation(1, u).nomClient +
                                  '\n\n' +
                                  getReservation(1, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(1, u) != null &&
                          getReservation(1, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(1, u) != null &&
                              getReservation(1, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(1, u) != null &&
                                  getReservation(1, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(2, u) != null &&
                        getReservation(2, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(2, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(2, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 2, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(2, u) != null &&
                                  getReservation(2, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(2, u).prenomClient +
                                  " " +
                                  getReservation(2, u).nomClient +
                                  '\n\n' +
                                  getReservation(2, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(2, u) != null &&
                          getReservation(2, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(2, u) != null &&
                              getReservation(2, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(2, u) != null &&
                                  getReservation(2, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(3, u) != null &&
                        getReservation(3, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(3, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(3, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 3, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(3, u) != null &&
                                  getReservation(3, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(3, u).prenomClient +
                                  " " +
                                  getReservation(3, u).nomClient +
                                  '\n\n' +
                                  getReservation(3, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(3, u) != null &&
                          getReservation(3, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(3, u) != null &&
                              getReservation(3, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(3, u) != null &&
                                  getReservation(3, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(4, u) != null &&
                        getReservation(4, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(4, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(4, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 4, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(4, u) != null &&
                                  getReservation(4, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(4, u).prenomClient +
                                  " " +
                                  getReservation(4, u).nomClient +
                                  '\n\n' +
                                  getReservation(4, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(4, u) != null &&
                          getReservation(4, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(4, u) != null &&
                              getReservation(4, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(4, u) != null &&
                                  getReservation(4, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(5, u) != null &&
                        getReservation(5, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(5, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(5, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 5, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(5, u) != null &&
                                  getReservation(5, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(5, u).prenomClient +
                                  " " +
                                  getReservation(5, u).nomClient +
                                  '\n\n' +
                                  getReservation(5, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(5, u) != null &&
                          getReservation(5, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(5, u) != null &&
                              getReservation(5, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(5, u) != null &&
                                  getReservation(5, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(6, u) != null &&
                        getReservation(6, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(6, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(6, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 6, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(6, u) != null &&
                                  getReservation(6, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(6, u).prenomClient +
                                  " " +
                                  getReservation(6, u).nomClient +
                                  '\n\n' +
                                  getReservation(6, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(6, u) != null &&
                          getReservation(6, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(6, u) != null &&
                              getReservation(6, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(6, u) != null &&
                                  getReservation(6, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(7, u) != null &&
                        getReservation(7, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(7, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(7, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 7, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(7, u) != null &&
                                  getReservation(7, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(7, u).prenomClient +
                                  " " +
                                  getReservation(7, u).nomClient +
                                  '\n\n' +
                                  getReservation(7, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(7, u) != null &&
                          getReservation(7, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(7, u) != null &&
                              getReservation(7, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(7, u) != null &&
                                  getReservation(7, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(8, u) != null &&
                        getReservation(8, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(8, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(8, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 8, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(8, u) != null &&
                                  getReservation(8, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(8, u).prenomClient +
                                  " " +
                                  getReservation(8, u).nomClient +
                                  '\n\n' +
                                  getReservation(8, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(8, u) != null &&
                          getReservation(8, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(8, u) != null &&
                              getReservation(8, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(8, u) != null &&
                                  getReservation(8, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(9, u) != null &&
                        getReservation(9, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(9, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(9, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 9, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(9, u) != null &&
                                  getReservation(9, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(9, u).prenomClient +
                                  " " +
                                  getReservation(9, u).nomClient +
                                  '\n\n' +
                                  getReservation(9, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(9, u) != null &&
                          getReservation(9, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(9, u) != null &&
                              getReservation(9, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(9, u) != null &&
                                  getReservation(9, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(10, u) != null &&
                        getReservation(10, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(10, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(10, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 10, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(10, u) != null &&
                                  getReservation(10, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(10, u).prenomClient +
                                  " " +
                                  getReservation(10, u).nomClient +
                                  '\n\n' +
                                  getReservation(10, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(10, u) != null &&
                          getReservation(10, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(10, u) != null &&
                              getReservation(10, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(10, u) != null &&
                                  getReservation(10, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(11, u) != null &&
                        getReservation(11, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(11, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(11, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 11, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(11, u) != null &&
                                  getReservation(11, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(11, u).prenomClient +
                                  " " +
                                  getReservation(11, u).nomClient +
                                  '\n\n' +
                                  getReservation(11, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(11, u) != null &&
                          getReservation(11, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(11, u) != null &&
                              getReservation(11, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(11, u) != null &&
                                  getReservation(11, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
          ]),
      ],
    );

    var tableNight = Table(
      border: TableBorder.all(
          color: Colors.white, width: 1, style: BorderStyle.solid),
      children: [
        dayNight,
        for (Utilisateur u in services)
          TableRow(children: [
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(12, u) != null &&
                        getReservation(12, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(12, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(12, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 12, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                    dataDto.pointVente.fGestionTable != 1
                        ? getReservation(12, u) != null &&
                                getReservation(12, u)
                                        .vReservationTable
                                        .produitsQtes[0]
                                        .produit !=
                                    null
                            ? getReservation(12, u).prenomClient +
                                " " +
                                getReservation(12, u).nomClient +
                                '\n\n' +
                                getReservation(12, u)
                                    .vReservationTable
                                    .produitsQtes[0]
                                    .produit
                                    .designation
                            : ""
                        : "",
                    style: TextStyle(fontSize: width / 120),
                    textAlign: TextAlign.center,
                  ),
                  color: getReservation(12, u) != null &&
                          getReservation(12, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(12, u) != null &&
                              getReservation(12, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(12, u) != null &&
                                  getReservation(12, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(13, u) != null &&
                        getReservation(13, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(13, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(13, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 13, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(13, u) != null &&
                                  getReservation(13, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(13, u).prenomClient +
                                  " " +
                                  getReservation(13, u).nomClient +
                                  '\n\n' +
                                  getReservation(13, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(13, u) != null &&
                          getReservation(13, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(13, u) != null &&
                              getReservation(13, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(13, u) != null &&
                                  getReservation(13, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    dataDto.selectedUser = u;
                    if (getReservation(14, u) != null &&
                        getReservation(14, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservation(14, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(14, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 14, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(14, u) != null &&
                                  getReservation(14, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(14, u).prenomClient +
                                  " " +
                                  getReservation(14, u).nomClient +
                                  '\n\n' +
                                  getReservation(14, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(14, u) != null &&
                          getReservation(14, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(14, u) != null &&
                              getReservation(14, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(14, u) != null &&
                                  getReservation(14, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(15, u) != null &&
                        getReservation(15, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(15, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(15, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 15, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(15, u) != null &&
                                  getReservation(15, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(15, u).prenomClient +
                                  " " +
                                  getReservation(15, u).nomClient +
                                  '\n\n' +
                                  getReservation(15, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(15, u) != null &&
                          getReservation(15, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(15, u) != null &&
                              getReservation(15, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(15, u) != null &&
                                  getReservation(15, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(16, u) != null &&
                        getReservation(16, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(16, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(16, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 16, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(16, u) != null &&
                                  getReservation(16, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(16, u).prenomClient +
                                  " " +
                                  getReservation(16, u).nomClient +
                                  '\n\n' +
                                  getReservation(16, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(16, u) != null &&
                          getReservation(16, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(16, u) != null &&
                              getReservation(16, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(16, u) != null &&
                                  getReservation(16, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(17, u) != null &&
                        getReservation(17, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(17, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(17, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 17, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(17, u) != null &&
                                  getReservation(17, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(17, u).prenomClient +
                                  " " +
                                  getReservation(17, u).nomClient +
                                  '\n\n' +
                                  getReservation(17, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(17, u) != null &&
                          getReservation(17, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(17, u) != null &&
                              getReservation(17, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(17, u) != null &&
                                  getReservation(17, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(18, u) != null &&
                        getReservation(18, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(18, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(18, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 18, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(18, u) != null &&
                                  getReservation(18, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(18, u).prenomClient +
                                  " " +
                                  getReservation(18, u).nomClient +
                                  '\n\n' +
                                  getReservation(18, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(18, u) != null &&
                          getReservation(18, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(18, u) != null &&
                              getReservation(18, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(18, u) != null &&
                                  getReservation(18, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(19, u) != null &&
                        getReservation(19, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(19, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(19, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 19, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(19, u) != null &&
                                  getReservation(19, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(19, u).prenomClient +
                                  " " +
                                  getReservation(19, u).nomClient +
                                  '\n\n' +
                                  getReservation(19, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(19, u) != null &&
                          getReservation(19, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(19, u) != null &&
                              getReservation(19, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(19, u) != null &&
                                  getReservation(19, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(20, u) != null &&
                        getReservation(20, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(20, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(20, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 20, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(20, u) != null &&
                                  getReservation(20, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(20, u).prenomClient +
                                  " " +
                                  getReservation(20, u).nomClient +
                                  '\n\n' +
                                  getReservation(20, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(20, u) != null &&
                          getReservation(20, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(20, u) != null &&
                              getReservation(20, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(20, u) != null &&
                                  getReservation(20, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(21, u) != null &&
                        getReservation(21, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(21, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(21, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 21, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(21, u) != null &&
                                  getReservation(21, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(21, u).prenomClient +
                                  " " +
                                  getReservation(21, u).nomClient +
                                  '\n\n' +
                                  getReservation(21, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(21, u) != null &&
                          getReservation(21, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(21, u) != null &&
                              getReservation(21, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(21, u) != null &&
                                  getReservation(21, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(22, u) != null &&
                        getReservation(22, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(22, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(22, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 22, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(22, u) != null &&
                                  getReservation(22, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(22, u).prenomClient +
                                  " " +
                                  getReservation(22, u).nomClient +
                                  '\n\n' +
                                  getReservation(22, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(22, u) != null &&
                          getReservation(22, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(22, u) != null &&
                              getReservation(22, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(22, u) != null &&
                                  getReservation(22, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservation(23, u) != null &&
                        getReservation(23, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    dataDto.selectedUser = u;
                    if (getReservation(23, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservation(23, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 23, minute: 0);
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      dataDto.pointVente.fGestionTable != 1
                          ? getReservation(23, u) != null &&
                                  getReservation(23, u)
                                          .vReservationTable
                                          .produitsQtes[0]
                                          .produit !=
                                      null
                              ? getReservation(23, u).prenomClient +
                                  " " +
                                  getReservation(23, u).nomClient +
                                  '\n\n' +
                                  getReservation(23, u)
                                      .vReservationTable
                                      .produitsQtes[0]
                                      .produit
                                      .designation
                              : ""
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservation(23, u) != null &&
                          getReservation(23, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservation(23, u) != null &&
                              getReservation(23, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservation(23, u) != null &&
                                  getReservation(23, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
          ]),
      ],
    );

    return Stack(
      fit: StackFit.loose,
      children: [
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Table Réservations",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width > height ? width / 50 : width / 30),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            FlatButton(
                                onPressed: () {
                                  callDatePicker();
                                },
                                child: Text(
                                  DateFormat("dd-MM-yyyy")
                                      .format(dataDto.reservationDate),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: width > height
                                          ? width / 60
                                          : width / 40),
                                )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "Après midi",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: width / 100),
                                ),
                                Switch(
                                  value: light,
                                  onChanged: (value) {
                                    setState(() {
                                      light = value;
                                    });
                                  },
                                  inactiveThumbColor: Colors.purpleAccent,
                                  activeColor: Colors.purpleAccent,
                                  activeTrackColor: Colors.grey,
                                ),
                                Text(
                                  "Matin",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: width / 100),
                                )
                              ],
                            ),
                          ],
                        )),
                  ),
                  SizedBox(
                    height: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    width: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    child: FlatButton(
                        color: Color(0xff32C533),
                        onPressed: () => {
                              dataDto.setCurrentPage(
                                  Reservation(dataDto), "Reservation")
                            },
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          'assets/back/back.png',
                          color: Colors.white,
                          fit: BoxFit.contain,
                          height: width > height ? width / 40 : width / 20,
                          width: width > height ? width / 40 : width / 20,
                        )),
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 32,
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.blueGrey,
                                border: Border(
                                  left: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  top: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  right: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                )),
                            child: FlatButton(
                                padding: EdgeInsets.all(0),
                                onPressed: () {
                                  setState(() {
                                    light = !light;
                                  });
                                },
                                child: Text(
                                  light ? "Matin" : "Après midi",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: width / 100),
                                )),
                            alignment: Alignment.center,
                          ),
                        ),
                        for (Utilisateur user in services)
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xffCFC5FF),
                                border: Border(
                                  left: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  top: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  right: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                )),
                            child: Text(
                              user.nom,
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            height: height / 5,
                            alignment: Alignment.center,
                          ),
                      ],
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    child: light ? tableLight : tableNight,
                    flex: 10,
                  ),
                ],
              )
            ],
          ),
        ),
        dataDto.pointVente.fAffectEmployetoservice == 1 && search != ""
            ? Positioned(
                child: Container(
                  color: Colors.white,
                  child: Container(
                      child: SingleChildScrollView(
                          scrollDirection:
                              width > height ? Axis.vertical : Axis.horizontal,
                          child: DataTable(
                            columns: [
                              DataColumn(
                                label: Text('Nom'),
                              ),
                              DataColumn(label: Text('Email')),
                            ],
                            rows: services
                                .where((c) =>
                                    c.prenom != null &&
                                        c.prenom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nom != null &&
                                        c.nom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.email != null &&
                                        c.email
                                            .toLowerCase()
                                            .contains(search.toLowerCase()))
                                .toList()
                                .map(
                                  (empl) => DataRow(cells: [
                                    DataCell(
                                      Text(
                                        empl.nom + " " + empl.prenom,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        getAllReservation(
                                            dataDto.reservationDate);
                                        setState(() {
                                          // selectedUser = empl;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        empl.email != null ? empl.email : "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        getAllReservation(
                                            dataDto.reservationDate);
                                        setState(() {
                                          // selectedUser = empl;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                  ]),
                                )
                                .toList(),
                          ))),
                ),
                top: 0,
                left: 0,
                right: 0,
              )
            : SizedBox()
      ],
    );
  }
}
