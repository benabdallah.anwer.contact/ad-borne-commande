import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/pos/api/table_caisse_end_point_api.dart';
import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/pos/model/transfert_detail.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/Animator.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/transaction/api/commande_details_end_point_api.dart';
import 'package:ad_caisse/transaction/model/CommandeDetailsDto.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/transaction/model/commande.dart';
import 'package:ad_caisse/transaction/model/commande_details.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Tables extends StatefulWidget {
  final DataDto dataDto;

  Tables(this.dataDto);

  @override
  State<StatefulWidget> createState() => new TablesState(this.dataDto);
}

class TablesState extends State<Tables> {
  DataDto dataDto;
  String search = "";
  final api_instance = new TableCaisseEndPointApi();
  final commandeDetailsEndPointApi = CommandeDetailsEndPointApi();
  bool _loading = false;
  bool _transfertDetail = false;

  TablesState(this.dataDto);
  TableCaisse tableSrc;
  TableCaisse tableDes;
  List<TableCaisse> tables = [];

  @override
  void initState() {
    dataDto.searchClient(dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"Rechercher Bipeur":"Rechercher Table");
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    getTables();
    super.initState();
  }
  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  void _showToast(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(  backgroundColor: Colors.green,
        content: const Text('Transfert effectué avec succés'),

      ),
    );
  }

  void _showToastWarning(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(  backgroundColor: Colors.orange,
        content: const Text('Aucun détails séléctionné!'),

      ),
    );
  }

  void getTables() async {
    setState(() {
      _loading = true;
    });
    try {
      ResponseList responseList =
          await api_instance.findAllByIdPointVenteAndFDefautUsingGET1(
              dataDto.pointVente.idPointVente);
      if (responseList.objectResponse != null) {
        List<TableCaisse> tables = responseList.objectResponse
            .map((e) => TableCaisse.fromJson(e))
            .toList();
        setState(() {
          this.tables = tables;
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("Tables getTables", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
    setState(() {
      _loading = false;
    });
  }

  void setCurrentCommnde(CommandeTransaction commande,
      List<CommandeDetails> commandeDetails, BuildContext context) {
    dataDto.commandeDetails = [];
    List<CommandeDetails> list = [];
    commandeDetails.forEach((element) {
      CommandeDetails commandeDetails = CommandeDetails();
      commandeDetails.prix = element.prix;
      commandeDetails.idProduit = element.idProduit;
      commandeDetails.quantite = element.quantite;
      commandeDetails.idCommande = element.idCommande;
      commandeDetails.idDetailComm = element.idDetailComm;
      commandeDetails.isAnnule = element.isAnnule;
      commandeDetails.designation = element.designation;
      list.add(commandeDetails);
    });

    if (commande.clients.length > 0) {
      print("Table: " + commande.clients.length.toString());
      ClientBPrice clientBPrice = ClientBPrice();
      clientBPrice.nom = commande.clients[0].nom;
      clientBPrice.prenom = commande.clients[0].prenom;
      clientBPrice.idClient = commande.clients[0].idClient;
      clientBPrice.idPartenaire = commande.clients[0].idPartenaire;
      clientBPrice.idClientPartenaire = commande.clients[0].idClientPartenaire;
      clientBPrice.nTel = commande.clients[0].nTel;
      clientBPrice.qrCodeBprice = commande.clients[0].qrCodeBprice;
      clientBPrice.dateNaissance = commande.clients[0].dateNaissance;
      clientBPrice.email = commande.clients[0].email;
      clientBPrice.soldePartn = commande.clients[0].soldeBprice;
      dataDto.clientBPrice = clientBPrice;
    }
    dataDto.commande = commande;
    dataDto.commandeDetails = list;
    dataDto.callbackCommande(null, context);
    dataDto.rendu = null;
    dataDto.payementList = [];
    dataDto.setCurrentPage(CommandeScreen(dataDto), "CommandeScreen");
  }

  Future<void> transfererTable(TableCaisse destiniation) async {
    setState(() {
      _loading = true;
    });
    try {
      if(!_transfertDetail) {
        ResponseSingle responseList =
        await api_instance.transfertTableUsingGET(
            tableSrc.idTable, destiniation.idTable);
        if (responseList.result == 1) {
          _showToast(context);
          getTables();
          tableSrc;
        }
      }else{
        List<String> idDetails=[];
        tableSrc.commandeDetails.forEach((element) {if(element.isChecked)
          idDetails.add(element.idDetailComm);
        });


        TransfertDetail dto =TransfertDetail() ;
        dto.ids=idDetails;
        dto.idTableSrc=tableSrc.idTable;
        dto.idTableDes=destiniation.idTable;
        ResponseSingle responseList =
        await api_instance.transfertDetailsTableUsingPOST(
            dto);
        if (responseList.result == 1) {
          _showToast(context);
          getTables();
          tableSrc;
          idDetails=[];
          _transfertDetail=false;
        }
      }
    } catch (err) {
      Crashlytics().sendEmail("Tables Transfert", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
    setState(() {
      _loading = false;
    });


  }

  Future<void> alert(TableCaisse item) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Êtes-vous sûr de vouloir transférer '+(dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"le bipeur ":"la table ")+tableSrc.numTable.toString()+' vers '+(dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"le bipeur ":"la table ")+item.numTable.toString()+' ?',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('ANNULER'),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: Colors.black26,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text('OK'),
              elevation: 0,
              color: Colors.green,
              onPressed: () {
                transfererTable(item);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }




  Future<void> showDetailsDialog(TableCaisse item) async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      item.commandeDetails.forEach((element) {element.isChecked=false;});
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
          builder: (context) {
            return StatefulBuilder(
              builder: (context, setState) {
                return AlertDialog(
                  backgroundColor: Color(0xffEEEEEE),
                  title: Text(
                    'Veuillez séléctionner les détails à Transférer  ' +(dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"du bipeur ":"de la table ")+
                        item.numTable.toString(),
                    textAlign: TextAlign.center,
                  ),
                  titleTextStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                  content: Container(
                      height: height * 0.60,
                      width: width * 0.5,
                      padding: EdgeInsets.all(16),
                      child: item.commandeDetails.length > 0
                          ? ListView(
                        padding: const EdgeInsets.all(8),
                        children: item.commandeDetails
                            .map(
                              (CommandeDetails itemd) =>
                              CheckboxListTile(
                                title: Text(
                                    itemd.quantite.toString() + '  X  ' +
                                        itemd.designation + '           ' +
                                        itemd.prix.toString() + ' TND'),
                                value: itemd.isChecked,
                                onChanged: (bool val) {
                                  print("Table: $val");
                                  setState(() => itemd.isChecked = val);
                                },
                              ),
                        )
                            .toList(),
                      ) : SizedBox()
                  ),
                  actions: <Widget>[
                    FlatButton(
                      color: Colors.red,
                      child: Text('Annuler'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    RaisedButton(
                      child: Text('OK'),
                      elevation: 0,
                      color: Colors.green,
                      onPressed: () {
                        bool check=false;
                        item.commandeDetails.forEach((element) {if(element.isChecked){
                          check=true;
                          return;
                        }

                        });
                        if(!check)
                        _showToastWarning(context);
               else {
                          _transfertDetail=true;
                          showTableDialog(item);
                          Navigator.of(context).pop();
                        }
                      },
                    ),
                  ],
                );
              },

            );
          },
            );
         } catch (err) {
      Crashlytics().sendEmail("Dashboard showTableDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }


  Future<void> showTableDialog(TableCaisse item) async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      final api_instance = new TableCaisseEndPointApi();

      ResponseList responseList =
      await api_instance.findAllByIdPointVenteAndFDefautUsingGET1(
          dataDto.pointVente.idPointVente);
      tableSrc=item;
      List<TableCaisse> tables = [];
      if (responseList.objectResponse != null) {
        tables = responseList.objectResponse
            .map((e) => TableCaisse.fromJson(e))
            .toList();
      }
      tables = tables.where((element) => element.idTable!=item.idTable).toList();
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: Text(
              'Transférer '+(dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"le bipeur ":"la table ")+item.numTable.toString()+' vers :',
              textAlign: TextAlign.center,
            ),
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            content: Container(
              height: height * 0.60,
              width: width * 0.5,
              padding: EdgeInsets.all(16),
              child: tables.length > 0
                  ? GridView.count(
                padding: EdgeInsets.only(top: 8),
                crossAxisCount: width > height ? 6 : 3,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                childAspectRatio: 1,
                children: tables
                    .map(
                      (item) => SizedBox(
                    child: WidgetANimator(FlatButton(
                        color: Color(0xffE7D7EE),
                        padding: EdgeInsets.all(0),
                        onPressed: () => {

                          // createCommande(paye),
                          Navigator.of(context).pop(),
                          alert(item)

                        },
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                            Positioned(
                              top: 0,
                              right: 0,
                              child: Container(
                                color: item.etat == "occupée"
                                    ? Colors.red
                                    : Colors.green,
                                width: 16,
                                height: 16,
                              ),
                            ),
                            Column(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: [
                                Text(
                                  dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"Bipeur N°" :   "Table N°",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: width > height
                                          ? width / 80
                                          : width / 35,
                                      color: Color(0xff352C60)),
                                ),
                                Text(
                                  item.numTable.toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: width > height
                                          ? width / 80
                                          : width / 35,
                                      color: Color(0xff352C60)),
                                ),
                                Text(
                                  item.commande
                                      !=null ? item.commande.montant.toStringAsFixed(2) +" DT ":"",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: width > height
                                          ? width / 130
                                          : width / 35,
                                      color: Color(0xff352C60)),
                                ),
                              ],
                            )
                          ],
                        ))),
                  ),
                )
                    .toList(),
              )
                  : Container(
                alignment: Alignment.center,
                child: Text(
                  (dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"Aucun bipeur n'est disponible pour le moment ":"Aucune table n'est disponible pour le moment ")      ,
                  style: TextStyle(
                      color: Colors.red, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                child: Text('Annuler'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("Dashboard showTableDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }
  Future<void> printNote(TableCaisse item) async {
    CommandesDtoTransaction comm=new CommandesDtoTransaction();
    comm.commande=item.commande;
    item.commandeDetails.forEach((element) {CommandeDetailsDto j=new CommandeDetailsDto();
    j.commandeDetails=CommandeDetailsTransaction.fromJson(element.toJson());
    j.nomProduit=element.designation;
    comm.commandeDetails.add(j);
    });

    debugPrint('print note ') ;
    await Utils().printService(context, comm, dataDto);
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    Color fromHex(String hexString) {
      final buffer = StringBuffer();
      if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
      buffer.write(hexString.replaceFirst('#', ''));
      return Color(int.parse(buffer.toString(), radix: 16));
    }

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          SizedBox(
            height: 8,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"Bipeurs":   "Tables",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: SizeClacule().calcule(context, 25),
                ),
              ),
            ],
          ),
          _loading
              ? Container(
                  margin: EdgeInsets.all(16),
                  height: 64,
                  width: 64,
                  child: CircularProgressIndicator(),
                )
              : Expanded(
                  child: GridView.count(
                  padding: EdgeInsets.only(top: 10),
                  crossAxisCount: width > height ? 7 : 6,
                  mainAxisSpacing: 20.0,
                  crossAxisSpacing: 5.0,
                  childAspectRatio: 1,
                  children:[

                  for (var item in tables    .where((c) =>

                      c.numTable
                          .toString()
                          .toLowerCase()
                          .contains(search.toLowerCase())).toList())

                            SizedBox(
                            //  height: 100,

                          child: FlatButton(
                              color: Color(0xffE7D7EE),
                              padding: EdgeInsets.all(0),
                              onLongPress: ()=>{debugPrint("transfert: " + item.numTable.toString())},
                              onPressed: () => {
                                dataDto.commandeType=null,
                                    if (dataDto.commande != null &&
                                        dataDto.commande.idTable !=
                                            item.idTable)
                                      {
                                        setState(() {
                                          dataDto.commande = null;
                                          dataDto.commandeDetails = [];

                                          dataDto.tableCaisse = item;
                                          dataDto.clientBPrice = null;
                                          dataDto.rendu = null;
                                          dataDto.payementList = [];
                                          dataDto.callbackCommande(
                                              null, context);
                                        }),
                                        dataDto.setCurrentPage(
                                            CommandeScreen(dataDto),
                                            "CommandeScreen")
                                      },
                                    setState(() {
                                      dataDto.tableCaisse = item;
                                      dataDto.commandeType = 'table';
                                    }),
                                    item.etat == "occupée"
                                        ? setCurrentCommnde(item.commande,
                                            item.commandeDetails, context)
                                        : dataDto.setCurrentPage(
                                            CommandeScreen(dataDto),
                                            "CommandeScreen")
                                  },
                              child: Stack(
                                fit: StackFit.expand,
                                children: [
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      color: item.etat == "occupée"
                                          ? Colors.red
                                          : Colors.green,
                                      width: 16,
                                      height: 16,
                                    ),
                                  ),
                                  Positioned(
                                    top: 0,
                                    left: 0,
                                    child:  item.etat == "occupée"?  IconButton(
                                      color: Colors.deepPurple,
                                      iconSize: 25,
                                      icon: const Icon(Icons.sticky_note_2_outlined),
                                      padding: EdgeInsets.all(0),
                                      onPressed: () => {  printNote(item),}

                                    ):SizedBox(),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                   /*   Text(
                                        dataDto.pointVente.fGestionTable!=null  && dataDto.pointVente.fGestionTable ==2?"Bipeur N°"   :  "Table N°",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: width > height
                                                ? width / 80
                                                : width / 35,
                                            color: Color(0xff352C60)),
                                      ),*/
                                      Text(
                                        item.numTable.toString(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: width > height
                                                ? width / 80
                                                : width / 35,
                                            color: Color(0xff352C60)),
                                      ),
                                      item.etat=="occupée"?Text(
                                        timeAgo(DateTime.fromMicrosecondsSinceEpoch(item.commande.dateCreation.microsecondsSinceEpoch)),
                                        // "",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: width > height
                                                ? width / 100
                                                : width / 35,
                                            color: Color(0xff352C60)),
                                      ):SizedBox(),
                                      Text(
                                        item.commande
                                            !=null ? item.commande.montant.toStringAsFixed(3) +" DT ":"",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: width > height
                                                ? width / 130
                                                : width / 35,
                                            color: Color(0xff352C60)),
                                      ),

                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),

                                  Positioned(
                                    bottom: -10,
                                    left: -5,
                                    child:  item.etat=="occupée" ?       IconButton(
                                      color: Colors.deepPurple,
                                      iconSize: 25,
                                      icon: const Icon(Icons.swap_horiz),
                                      padding: EdgeInsets.all(0),
                                      onPressed: () => { showTableDialog(item)},

                                    ):SizedBox(),
                                  ),
                                      Positioned(
                                        bottom: -10,
                                        right: -5,
                                        child:  item.etat=="occupée" && item.commandeDetails.length>1 ?       IconButton(
                                          color: Colors.deepPurple,
                                          iconSize: 25,
                                          icon: const Icon(Icons.add_to_home_screen),
                                          padding: EdgeInsets.all(0),
                                          onPressed: () => { showDetailsDialog(item)},

                                        ):SizedBox(),
                                      ),
                                  Positioned(
                                    bottom: -10,
                                    right: 45,
                                    child:  item.etat=="occupée" && dataDto.pointVente.fGestionTable ==2 ?       IconButton(
                                      color: Colors.deepPurple,
                                      iconSize: 20,
                                      icon: const Icon(Icons.login),
                                      padding: EdgeInsets.all(0),
                                      onPressed: () => {
                                        setState(() {
                                          item.etat="libre";
                                          item.commande=null;
                                          item.commandeDetails=null;
                                        }),
                                        api_instance.updateTableCaisseUsingPUT(item),
                                      },

                                    ):SizedBox(),
                                  ),

                                ],
                              )),
                        ),




                    ]
                )),
        ],
      ),
    );
  }
}
String timeAgo(DateTime d) {
  Duration diff = DateTime.now().difference(d);
  if (diff.inDays > 365)
    return "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? "anneé" : "anneés"}";
  if (diff.inDays > 30)
    return "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? "moin" : "moins"}";
  if (diff.inDays > 7)
    return "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? "semaine" : "semaines"}";
  if (diff.inDays > 0)
    return "${diff.inDays} ${diff.inDays == 1 ? "jour" : "jours"}";
  if (diff.inHours > 0)
    return "${diff.inHours} ${diff.inHours == 1 ? "heure" : "heures"}";
  if (diff.inMinutes > 0)
    return "${diff.inMinutes} ${diff.inMinutes == 1 ? "minute" : "minutes"}";
  return "A l'instant";
}