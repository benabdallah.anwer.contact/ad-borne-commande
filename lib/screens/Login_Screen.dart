import 'dart:convert';
import 'dart:io';

import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/card_dto.dart';
import 'package:ad_caisse/dto/VersionDto.dart';
import 'package:ad_caisse/pos/model/journee.dart';
import 'package:ad_caisse/pos/model/session.dart';
import 'package:ad_caisse/request/AuthRequest.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/Loading.dart';
import 'package:ad_caisse/screens/NumbersLogin.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/TestingPlatform.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'package:process_run/shell.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _State();
}

class _State extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController ip = new TextEditingController();
  TextEditingController ip_synchro = new TextEditingController();
  bool loading = false;
  int focused;
  Journee journee;
  List<Session> sessions = [];
  bool isSwitched = false;
  FocusNode _myFocusNode = FocusNode();
  String _keyCode = '';
  String res = "";
  TextEditingController keyController = new TextEditingController();
  bool isClosed = true ;
  bool _validate = false;
  var shell = Shell();


  @override
  void initState() {
    getInfoVersion();
    _myFocusNode.requestFocus();
    getSharedPreferences();
    getConnectedSessions();
    getJournee();
    super.initState();
  }

  getInfoVersion()async{
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    double versionCurrent = prefs.getDouble('current_version');
    if (versionCurrent==null){
      return null ;
    }
    if (TestingPlatform().windows()){
      getLastUpdate(versionCurrent, "windows");
    }else if (TestingPlatform().android()){
      getLastUpdate(versionCurrent, "android");
    }

  }

  Future<dynamic> getLastUpdate(double version , String os) async {
    try {
      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        return null;
      }

      var url = "http://localhost:8030/v1/checkVersion/$version/$os";
      final response = await http
          .get(Uri.parse(url), headers: {"Content-Type": "application/json"});
      if (response == null) {
        return null;
      }

      if (response != null && response.statusCode == 200) {
        ResponseSingle responseSingle =
        ResponseSingle.fromJson(json.decode(response.body));
        if (responseSingle.result==1 && responseSingle.objectResponse != null){
          VersionDto versionDto ;
          print(responseSingle.objectResponse);
          versionDto = VersionDto.fromJson(responseSingle.objectResponse);
          showUpdateVersionDialog(versionDto);
        }
      } else {
        return null;
      }
    } catch (error) {
      Crashlytics().sendEmail("AuthRequest getLastUpdate", error.toString());
      return null;
    }
  }



  void showUpdateVersionDialog(VersionDto version) async {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder:
              (BuildContext context, void Function(void Function()) setState) {
            return AlertDialog(
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Image.asset('assets/logo/logo.png',
                        width: 50, height: 50, fit: BoxFit.scaleDown),
                    SizedBox(height: 20.0),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: Text("Une nouvelle Mise à jour à télécharger :",style: TextStyle(fontWeight: FontWeight.bold), ),
                        ),
                        SizedBox(height: 15.0),

                        Container(
                          width: width / 3,
                          height: height * 0.18,
                          child:  ListView.builder(
                              itemCount: version.releaseNoteToDisplay.split(",").length,
                              itemBuilder: (BuildContext context,int index){
                                return Row(
                                  children: [
                                    Icon(Icons.check_circle , color: Color(0xff352C60)),
                                    SizedBox(width: 3,),
                                    Container(
                                      width: width/4,
                                      //height: height * 0.18,
                                      child:   Text(version.releaseNoteToDisplay.split(",")[index] ,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,),
                                    )
                                  ],
                                );
                              }
                          ),
                        ),


                      ],
                    )
                  ],
                ),
              ),
              actions: <Widget>[
                RaisedButton(
                  child: Text('Plus tard', style: TextStyle(color: Colors.white)),
                  elevation: 0,
                  padding: EdgeInsets.all(16),
                  color: Color(0xffCF0606),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                RaisedButton(
                  child: Text(
                    'Mettre à jour',
                    style: TextStyle(color: Colors.white),
                  ),
                  elevation: 0,
                  padding: EdgeInsets.all(16),
                  color: Color(0xFF32C533),
                  onPressed: () async {
                    await LocalStorage.getInstance();
                    LocalStorageInterface prefs = await LocalStorage.getInstance();
                    prefs.setDouble('current_version', version.latestVersion);
                    String fileName = version.fileName;

                    await shell.run('''
                    start C:\\versions-ADCaisse\\$fileName
             
                    ''').then((value) => {
                      exit(0)
                    });
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  saveSharedPreferences() async {
    if (ip.text != null && ip.text != "") {
      try {
        final response = await http.get(Uri.parse(ip.text)).timeout(
          Duration(seconds: 2),
          onTimeout: () {
            return null;
          },
        ).onError(
            (error, stackTrace) => new http.Response('', kIsWeb ? 200 : -1));
        if (!kIsWeb && response.statusCode != 200) {
          AlertNotif().alert(context,
              "IP/Domain non valide, Veuillez vérifier votre configuration");
        } else {
          setState(() {
            ServerUrl.url = ip.text;
            ServerUrl.fid = isSwitched;
          });
          LocalStorageInterface prefs = await LocalStorage.getInstance();
          await prefs.setString("my_ip", ip.text);
          await prefs.setBool("my_fid", isSwitched);
          setState(() {});
        }
      } catch (error) {
        AlertNotif()
            .alert(context, "IP/Domain, Veuillez vérifier votre configuration");
        return;
      }
    } else {
      return;
    }

    if (ip_synchro.text != null && ip_synchro.text != "") {
      try {
        final response = await http.get(Uri.parse(ip_synchro.text)).timeout(
          Duration(seconds: 2),
          onTimeout: () {
            return null;
          },
        ).onError(
            (error, stackTrace) => new http.Response('', kIsWeb ? 200 : -1));
        if (!kIsWeb && response.statusCode != 200) {
          AlertNotif().alert(context,
              "IP/Domain Synchro non valide, Veuillez vérifier votre configuration");
          return;
        } else {
          setState(() {
            ServerUrl.urlSynchro = ip_synchro.text;
          });
          LocalStorageInterface prefs = await LocalStorage.getInstance();
          await prefs.setString("my_ip_synchro", ip_synchro.text);
          setState(() {});
        }
      } catch (error) {
        AlertNotif().alert(context,
            "IP/Domain Synchro, Veuillez vérifier votre configuration");
        return;
      }
    } else {
      return;
    }
    Navigator.of(context).pop();

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => Loading(),
        ),
        ModalRoute.withName('/'));
  }

  getSharedPreferences() async {
    try {
      LocalStorageInterface prefs = await LocalStorage.getInstance();
      String savedIpd = prefs.getString('my_ip');
      String synch = prefs.getString('my_ip_synchro');
      bool savedFid = prefs.getBool('my_fid');

      setState(() {
        ip_synchro.text = synch != null ? synch : '';
        ip.text = savedIpd != null ? savedIpd : '';
        isSwitched = savedFid != null ? savedFid : false;
        ServerUrl.url = savedIpd != null ? savedIpd : '';
        ServerUrl.urlSynchro = synch != null ? synch : '';
        ServerUrl.fid = savedFid != null ? savedFid : false;
      });
      setState(() {});

      if (savedIpd == null || savedIpd == '' || synch == null || synch == '') {
        showSettingDialog();
      }
    } catch (err) {
      debugPrint(err.toString());
      Crashlytics().sendEmail("Login getSharedPreferences", err.toString());
    }
  }

  clearSharedPreferences() async {
    try {
      LocalStorageInterface prefs = await LocalStorage.getInstance();
      prefs.clear();
      prefs.remove('my_ip');
      prefs.remove('my_ip_synchro');
      print('teeeeeeeeeest');
      ip_synchro.clear();
      ip.clear();

      await prefs.setString("my_ip", '');
      await prefs.setString("my_ip_synchro", '');
      setState(() {
        ip_synchro.text = null;
        ip.text = null;

        ServerUrl.url = null;
        ServerUrl.urlSynchro = null;
      });
    } catch (err) {
      debugPrint(err.toString());
      Crashlytics().sendEmail("clear getSharedPreferences", err.toString());
    }
  }

  getConnectedSessions() async {
    if (ServerUrl.url.isEmpty && ServerUrl.urlSynchro.isEmpty) {
      return;
    }
    List<Session> list = await AuthRequest().getConnectedSessions(context);
    setState(() {
      sessions = list;
    });
  }

  getJournee() async {
    if (ServerUrl.url.isEmpty || ServerUrl.urlSynchro.isEmpty) {
      return;
    }
    Journee res = await AuthRequest().getJournee(context);
    setState(() {
      journee = res;
    });
  }

  handleKey(RawKeyEvent event) async {
    if (focused == null) {
      if (event.character != null) {
        if (event.physicalKey == PhysicalKeyboardKey.escape) {
          setState(() {
            _keyCode = '';
          });
          return;
        }

        switch (event.character.toString()) {
          // case "à":
          //   _keyCode += '0';
          //   break;
          // case "é":
          //   _keyCode += '2';
          //   break;
          // case "(":
          //   _keyCode += '5';
          //   break;
          // case "è":
          //   _keyCode += '7';
          //   break;
          // case "'":
          //   _keyCode += '4';
          //   break;
          // case "&":
          //   {
          //     _keyCode += '1';
          //   }
          //   break;
          // case "-":
          //   {
          //     _keyCode += '6';
          //   }
          //   break;
          //
          // case "_":
          //   {
          //     _keyCode += '8';
          //   }
          //   break;
          //
          // case "ç":
          //   {
          //     _keyCode += '9';
          //   }
          //   break;
          //
          // case '"':
          //   {
          //     _keyCode += '3';
          //   }
          //   break;

          case '§':
            {
              _keyCode += '!';
            }
            break;

          default:
            {
              _keyCode += event.character.toString();
            }
            break;
        }

        _keyCode.trim();
        if (_keyCode.contains("\r")) {
          debugPrint("espace detecté************************:");
          _keyCode = _keyCode.replaceAll("\r", "");
        }

        if (_keyCode.length > 10) {
          _keyCode = '';
          _myFocusNode.nextFocus();
        }
        res = res + event.character.toString();

        debugPrint("RawKeyEvent: $_keyCode");
        if (_keyCode.contains('!') &&
            _keyCode.length <= 10 &&
            _keyCode.length > 6) {
          emailController.text = '';
          UtilisateurEndPointApi api = UtilisateurEndPointApi();
          ResponseSingle responseSingle =
              await api.findUtilisateurByIdCardUsingGET(_keyCode.trim());
          debugPrint(responseSingle.objectResponse.toString());
          if (responseSingle.result == 1) {
            CardDto inf;
            inf = CardDto.fromJson(responseSingle.objectResponse);
            _myFocusNode.dispose();
            AuthRequest()
                .fetchLogin(context, inf.login, inf.password)
                .then((value) => setState(() {
                      loading = value;
                    }));
          } else {
            _keyCode = '';
            // emailController.clear();
            _myFocusNode.requestFocus();
          }
        }
      } else {
        if (focused == null) {
          if (_myFocusNode != null) _myFocusNode.nextFocus();
        }
      }
    }
  }

  void callbackNumbers(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        emailController.text = "";
        passwordController.text = "";
      });
      return;
    }
    if (focused == 1) {
      setState(() {
        emailController.text += number;
        emailController.selection = TextSelection.fromPosition(
            TextPosition(offset: emailController.text.length));
      });
    } else if (focused == 2) {
      passwordController.text += number;
      passwordController.selection = TextSelection.fromPosition(
          TextPosition(offset: passwordController.text.length));
    }
  }

  void showSettingDialog() async {
    // await getSharedPreferences();
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder:
              (BuildContext context, void Function(void Function()) setState) {
            return AlertDialog(
              content:isClosed? SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Image.asset('assets/logo/logo.png',
                        width: 50, height: 50, fit: BoxFit.scaleDown),
                    SizedBox(height: 30),
                    Center(
                      child: Text('Pour modifier',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),

                    ),
                    Center(
                      child: Text('Veuillez saisir le "SECRET TOKEN"',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),

                    ),
                    SizedBox(height: 30),

                    TextField(
                        cursorColor: Color(0xff352C60),
                        controller: keyController,
                        obscureText: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xfff5f5f5),
                          contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          hintText: "SECRET TOKEN",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),
                        )),
                  ],
                ),
              ):SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Image.asset('assets/logo/logo.png',
                        width: 50, height: 50, fit: BoxFit.scaleDown),
                    SizedBox(height: 16.0),
                    TextField(
                        cursorColor: Color(0xff352C60),
                        controller: ip,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xfff5f5f5),
                          contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          hintText: "IP/Domain:",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),
                        )),
                    SizedBox(height: 16.0),
                    TextField(
                        cursorColor: Color(0xff352C60),
                        controller: ip_synchro,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xfff5f5f5),
                          contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          hintText: "IP/Domain Synchro:",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                          ),
                        )),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text("Activation module FID"),
                        ),
                        SizedBox(
                          width: 0,
                        ),
                        Switch(
                          value: isSwitched,
                          onChanged: (value) {
                            setState(() {
                              isSwitched = value;
                            });
                          },
                          activeColor: Colors.green,
                        ),
                        RaisedButton(
                          child: Text(
                            'Clear shared',
                            style: TextStyle(color: Colors.white),
                          ),
                          elevation: 0,
                          padding: EdgeInsets.all(16),
                          color: Color(0xFF32C533),
                          onPressed: () {
                            clearSharedPreferences();
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
              actions: isClosed? <Widget> [
                RaisedButton(
                  child: Text('Annuler', style: TextStyle(color: Colors.white)),
                  elevation: 0,
                  padding: EdgeInsets.all(16),
                  color:  Color(0xffCF0606),
                  onPressed: () {
                    Navigator.of(context).pop();
                    keyController.clear();

                  },
                ),
                RaisedButton(
                  child:Text(
                    'Valider',
                    style: TextStyle(color: Colors.white),
                  ),
                  elevation: 0,
                  padding: EdgeInsets.all(16),
                  color: Color(0xFF32C533),
                  onPressed: () {
                    // ignore: unnecessary_statements
                    keyController.text.toUpperCase()=='adcaisse'.toUpperCase()? {
                      setState(() {
                        isClosed=false;
                      }),
                      _validate =true,
                       keyController.clear(),
                    } :_validate=false;

                  },
                ),
              ]:<Widget>[
                RaisedButton(
                  child: Text('Annuler', style: TextStyle(color: Colors.white)),
                  elevation: 0,
                  padding: EdgeInsets.all(16),
                  color: Color(0xffCF0606),
                  onPressed: () {
                    Navigator.of(context).pop();
                    setState(() {
                      isClosed=true;
                    });
                    keyController.clear();
                  },
                ),
                RaisedButton(
                  child: Text(
                    'OK',
                    style: TextStyle(color: Colors.white),
                  ),
                  elevation: 0,
                  padding: EdgeInsets.all(16),
                  color: Color(0xFF32C533),
                  onPressed: () {
                    saveSharedPreferences();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final emailField = TextField(
      onTap: () {
        print("dddd:$focused");
        setState(() {
          focused = 1;
        });
      },
      autofocus: true,
      cursorColor: Color(0xff352C60),
      controller: emailController,
      obscureText: false,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        prefixIcon: Icon(
          Icons.person,
          color: Color(0xff352C60),
        ),
        contentPadding: EdgeInsets.all(width / 60),
        hintText: "Login",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(0),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(0),
        ),
      ),
    );
    final passwordField = TextField(
      onTap: () {
        // print('Editing stated password');
        setState(() {
          focused = 2;
        });
      },
      cursorColor: Color(0xff352C60),
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
        prefixIcon: Icon(
          Icons.lock,
          color: Color(0xff352C60),
        ),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.all(width / 60),
        hintText: "Mot de passe",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(0),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(0),
        ),
      ),
    );
    final loginButon = Material(
      elevation: 0,
      borderRadius: BorderRadius.circular(40.0),
      color: Color(0xffCF5FFE),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(width / 55),
        onPressed: () {
          // print(emailController.text);
          //print(passwordController.text);
          setState(() {
            loading = true;
          });
          AuthRequest()
              .fetchLogin(
                  context, emailController.text, passwordController.text)
              .then((value) => setState(() {
                    loading = value;
                  }));
        },
        child: Text(
          "LOGIN",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
        ),
      ),
    );

    final column = Column(
      children: [
        Expanded(
          flex: 1,
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/resto/resto.png"),
                    fit: BoxFit.cover)),
            child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Center(
                child: Container(
                  color: Colors.transparent,
                  width: 400,
                  child: Padding(
                    padding: const EdgeInsets.all(36.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(16),
                          child: Image.asset(
                            'assets/logo/logo.png',
                            height: 100,
                            width: 100,
                          ),
                        ),
                        emailField,
                        SizedBox(height: 25.0),
                        passwordField,
                        SizedBox(
                          height: 35.0,
                        ),
                        !loading
                            ? loginButon
                            : CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Color(0xffCF5FFE)),
                              ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              focused = null;
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 24),
                            child: Image.asset(
                              'assets/qr-code-home/qr-code.png',
                              color: Colors.white,
                              height: 50,
                              width: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );

    final row = Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.all(width / 20),
                  child: Image.asset(
                    'assets/logo/logo.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => Loading(),
                      ),
                      ModalRoute.withName('/'));
                },
                child: Container(
                  margin: EdgeInsets.all(width / 50),
                  child: Image.asset(
                    'assets/qr-code-home/qr-code.png',
                    fit: BoxFit.scaleDown,
                    color: Colors.black,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(width / 50),
                alignment: Alignment.center,
                child: SizedBox(
                  height: 32,
                  width: 32,
                  child: FlatButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      showSettingDialog();
                    },
                    child: Icon(
                      Icons.settings,
                      size: 32,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/resto/resto.png"),
                    fit: BoxFit.cover)),
            child: Column(
              children: [
                sessions.length > 0
                    ? Text(
                        sessions.length.toString() + " Session(s) active(s)",
                        style: TextStyle(color: Colors.white),
                      )
                    : SizedBox(),
                SizedBox(
                  height: sessions.length > 0 ? 8 : 0,
                ),
                SingleChildScrollView(
                  child: Row(
                    children: [
                      for (var item in sessions
                          .where((element) => element.utilisateur != null)
                          .toList())
                        Container(
                          margin: EdgeInsets.only(bottom: 16),
                          child: FlatButton(
                            onPressed: () {
                              setState(() {
                                emailController.text = item.utilisateur.login;
                                emailController.selection =
                                    TextSelection.fromPosition(TextPosition(
                                        offset: emailController.text.length));
                              });
                            },
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: Colors.green,
                                    width: 1,
                                    style: BorderStyle.solid)),
                            child: Text(
                              item.utilisateur.nom != null &&
                                      item.utilisateur.prenom != null
                                  ? item.utilisateur.nom +
                                      " " +
                                      item.utilisateur.prenom
                                  : "",
                              style: TextStyle(color: Colors.white),
                            ),
                            padding: EdgeInsets.all(16),
                          ),
                        )
                    ],
                  ),
                  scrollDirection: Axis.horizontal,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      color: Colors.transparent,
                      width: width * 0.3,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            emailField,
                            SizedBox(height: 25.0),
                            passwordField,
                            SizedBox(
                              height: 35.0,
                            ),
                            !loading
                                ? loginButon
                                : CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Color(0xffCF5FFE)),
                                  ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      color: Colors.transparent,
                      width: width * 0.3,
                      height: width * 0.215,
                      child: NumbersLogin(callbackNumbers),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                journee != null
                    ? FlatButton(
                        onPressed: () {},
                        child: Text(
                          "Journée ouverte le " +
                              DateFormat("dd-MM-yyyy")
                                  .format(journee.dateOuverture),
                          style: TextStyle(color: Colors.white),
                        ),
                        padding: EdgeInsets.all(16),
                      )
                    : SizedBox(),
                SizedBox(height: 10),
                Text(
                  "V 1.0",
                  style: TextStyle(color: Colors.grey),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          ),
        )
      ],
    );

    return Scaffold(
        key: _scaffoldKey,
        body: RawKeyboardListener(
            includeSemantics: true,
            onKey: (RawKeyEvent event) {
              handleKey(event);
            },
            focusNode: _myFocusNode,
            child: row));
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    _myFocusNode.dispose();
    super.dispose();
  }
}
