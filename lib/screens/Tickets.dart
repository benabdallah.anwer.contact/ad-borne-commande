import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/transaction/api/commande_end_point_api.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'PrintTicket.dart';
import 'Transactions.dart';

class Tickets extends StatefulWidget {
  final DataDto dataDto;

  Tickets(this.dataDto);

  @override
  TicketsState createState() => TicketsState(this.dataDto);
}

class TicketsState extends State<Tickets> {
  DataDto dataDto;
  String search = "";
  var commandeEndPointApi = CommandeEndPointApi();

  TicketsState(this.dataDto);

  @override
  void initState() {
    dataDto.searchClient("Rechercher Ticket");
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    super.initState();
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  void getTickets() async {
    try {
      ResponseList responseList =
          await commandeEndPointApi.gettransactionbyjourneeandfpayeUsingGET(
              dataDto.journee.idJournee, 1);
      List<CommandesDtoTransaction> list = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        list = List.from(responseList.objectResponse)
            .map((e) => CommandesDtoTransaction.fromJson(e))
            .toList()
            .where((element) => element.commandeDetails?.isNotEmpty)
            .toList();
      }

      setState(() {
        dataDto.listTickets = list;
      });
    } catch (err) {
      Crashlytics().sendEmail("Transactions getTickets", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> alert(CommandesDtoTransaction commandesDtoTransaction) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Êtes-vous sûr de vouloir supprimer cette commande ?',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('ANNULER'),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: Colors.black26,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text('OK'),
              elevation: 0,
              color: Colors.green,
              onPressed: () {
                http.get(Uri.parse(ServerUrl.url +
                    '/bp-api-transaction/v1/AnnulerCommande/' +
                    commandesDtoTransaction.commande.idCommande));
                getTickets();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Tickets :",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: width > height ? width / 50 : width / 30),
            ),
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () => {
                        dataDto.setCurrentPage(
                            Transactions(dataDto), "Transactions")
                      },
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Expanded(
          child: GridView.count(
            padding: EdgeInsets.only(top: 8),
            crossAxisCount: width > height ? 4 : 2,
            mainAxisSpacing: 8.0,
            crossAxisSpacing: 8.0,
            childAspectRatio: 0.6,
            children: dataDto.listTickets.reversed
                .where((c) =>
                    c.commande.numTable != null &&
                        c.commande.numTable
                            .toString()
                            .toLowerCase()
                            .contains(search.toLowerCase()) ||
                    c.commande.numCommande
                        .toString()
                        .toLowerCase()
                        .contains(search.toLowerCase()) ||
                    c.commandeDetails
                            .where((element) => element.commandeDetails.designation
                                .toLowerCase()
                                .contains(search.toLowerCase()))
                            .toList()
                            .length >
                        0 ||
                    c.commande.clients
                            .where((element) =>
                                element.nom != null &&
                                element.nom
                                    .toLowerCase()
                                    .contains(search.toLowerCase()))
                            .toList()
                            .length >
                        0 ||
                    c.commande.clients.where((element) => element.prenom != null && element.prenom.toLowerCase().contains(search.toLowerCase())).toList().length > 0)
                .map((item) => SizedBox(
                      height: height * 0.5,
                      child: FlatButton(
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                          ),
                          color: Color(0xffE7D7EE),
                          onPressed: () => {
                                dataDto.setCurrentPage(
                                    PrintTicket(dataDto, item), "PrintTicket")
                              },
                          onLongPress: () {
                            alert(item);
                          },
                          child: SingleChildScrollView(
                            child: Column(
                              children: [

                                ///Tag fidilitè
/*                                Align(
                                  alignment: Alignment.topRight,
                                  child:  Container(
                                    width: width > height
                                        ? width / 20
                                        : width / 15,

                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(4),
                                        color: Colors.red
                                    ),
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(left:4.0),
                                          child: Icon(Icons.monetization_on_outlined,color: Colors.white,size: 15,),
                                        ),
                                        Center(child: Text(" FID",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)),
                                      ],
                                    ),
                                  ),
                                ),*/
                                Container(
                                  height: 16,
                                ),

                            dataDto.pointVente.fGestionTable==1?    Text(
                                  item.commande.numTable != null
                                      ? "N° " +
                                          item.commande.numCommande.toString() +
                                          " / Table N° " +
                                          item.commande.numTable.toString()
                                      : "N° " +
                                          item.commande.numCommande.toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: width > height
                                        ? width / 80
                                        : width / 30,
                                  ),
                                ):Text(
                              item.commande.numTable != null
                                  ? "N° " +
                                  item.commande.numCommande.toString() +
                                  " / Bipeur N° " +
                                  item.commande.numTable.toString()
                                  : "N° " +
                                  item.commande.numCommande.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: width > height
                                    ? width / 80
                                    : width / 30,
                              ),
                            ),
                                Container(
                                  height: 1,
                                  width: 100,
                                  color: Colors.black12,
                                  margin: EdgeInsets.only(top: 16, bottom: 16),
                                ),
                                Text(
                                  item.commande.montant.toStringAsFixed(3) +
                                      " DT",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: width > height
                                        ? width / 100
                                        : width / 30,
                                  ),
                                ),
                                Container(
                                  height: 1,
                                  width: 100,
                                  color: Colors.black12,
                                  margin: EdgeInsets.only(top: 16, bottom: 16),
                                ),
                                Text(
                                  DateFormat("dd-MM-yyyy HH:mm")
                                      .format(item.commande.dateCreation),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: width > height
                                        ? width / 100
                                        : width / 30,
                                  ),
                                ),

                                for (var c in item.commande.clients)
                                  Container(
                                    margin: EdgeInsets.only(top: 8),
                                    child: Text(
                                      c.nom != null
                                          ? c.nom + " " + c.prenom
                                          : '',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: width > height
                                            ? width / 100
                                            : width / 30,
                                      ),
                                    ),
                                  ),
                                Container(
                                  height: 1,
                                  width: 100,
                                  color: Colors.black12,
                                  margin: EdgeInsets.only(top: 16, bottom: 16),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    for (var c in item.commandeDetails)
                                      Container(
                                        margin: EdgeInsets.only(bottom: 8),
                                        child: Column (
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text(
                                          c.commandeDetails.quantite
                                                  .toString() +
                                              " X " +
                                              c.commandeDetails.prix
                                                  .toStringAsFixed(3) +
                                              " " +
                                              c.commandeDetails.designation,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: width > height
                                                ? width / 100
                                                : width / 30,
                                          ),
                                        ),
                                          if(c.commandeDetails.remise!=null)
                                          Text("P.init "+
    c.commandeDetails.prixSansRemise.toStringAsFixed(3)+
                                                "-- Remise " +
                                                c.commandeDetails.remise.toString()+
                                                " %"
                                               ,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              color: Colors.red,
                                              fontWeight: FontWeight.w500,
                                              fontSize: width > height
                                                  ? width / 100
                                                  : width / 30,
                                            ),
                                          )
                                        ]),
                                      )
                                  ],
                                ),
                              ],
                            ),
                          )),
                    ))
                .toList(),
          ),
        )
      ],
    );
  }
}
