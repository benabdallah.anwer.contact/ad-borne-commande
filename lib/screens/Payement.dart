import 'dart:convert';

import 'package:ad_caisse/customer/api/customer_end_point_api.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/dto/PayementNumbers.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/otp/PayementDto.dart';
import 'package:ad_caisse/payement/model/reglm_details_dto.dart';
import 'package:ad_caisse/pos/api/mode_reglement_end_point_api.dart';
import 'package:ad_caisse/pos/model/mode_reglement.dart';
import 'package:ad_caisse/request/CommandeRequest.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/screens/Clients.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/Numbers.dart';
import 'package:ad_caisse/screens/NumbersLogin.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:toast/toast.dart';

import 'NumbersPaye.dart';

class Payement extends StatefulWidget {
  final DataDto dataDto;

  Payement(this.dataDto);

  @override
  _MyHomePageState createState() => _MyHomePageState(this.dataDto);

  void logError(String code, String message) =>
      print('Error: $code\nError Message: $message');
}

class _MyHomePageState extends State<Payement> with TickerProviderStateMixin {
  DataDto dataDto;
  var modeReglementEndPointApi = ModeReglementEndPointApi();
  String number = "";
  bool loading = false;
  DateTime finaldate;
  List<ModeReglement> modeReglement = [];
  TextEditingController qrCode = new TextEditingController();
  TextEditingController error = new TextEditingController();
  String search = "";
  FocusNode _focusNodeSelectClient = FocusNode();
  FocusNode _focusNodeQrCode = FocusNode();
  String _keyCode = '';

  _MyHomePageState(this.dataDto);

  TextEditingController chqNumber = new TextEditingController();
  var customerEndPointApi = CustomerEndPointApi();

  TextEditingController test = new TextEditingController();


  @override
  void initState() {
    dataDto.searchClient("Rechercher Client");
    print("List<CommandeDetails>: " +
        this.dataDto.commandeDetails.length.toString());
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    List<CommandeDetails> list = this
        .dataDto
        .commandeDetails
        .where((element) => element.recharge == true)
        .toList();
    if (list.length > 0) {
      this.modeReglement =
          dataDto.modeReg.where((element) => element.ffidelite != 1).toList();
    } else {
      this.modeReglement = dataDto.modeReg;
    }
    super.initState();
  }

  txtListener() {
    switch (dataDto.focused) {
      case -1:
        {
          setState(() {
            dataDto.searchController.text = dataDto.textController.text;
          });
        }
        break;
      case 1:
        {}
        break;
    }
  }

  @override
  void dispose() {
    _focusNodeSelectClient.dispose();
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  void callDatePicker(ModeReglement modeReglement) async {
    DateTime order = await getDate();
    if (order != null) {
      setState(() {
        finaldate = order;
      });
      setChequeNumber(modeReglement);
    }
  }

  void setChequeNumber(ModeReglement modeReglement) {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                TextField(
                    cursorColor: Color(0xff352C60),
                    controller: chqNumber,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xfff5f5f5),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Numéro du chèque:",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    )),
                SizedBox(height: 16.0),
                Container(
                  alignment: Alignment.center,
                  color: Colors.transparent,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.width * 0.215,
                  child: NumbersLogin(callbackNumbersChq),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xffCF0606),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'OK',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.all(16),
              elevation: 0,
              color: Color(0xFF32C533),
              onPressed: () {
                if (chqNumber.text.isNotEmpty) {
                  createReg(modeReglement);
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        );
      },
    );
  }

  Future<DateTime> getDate() {
    // Imagine that this function is
    // more complex and slow.
    return showDatePicker(
      context: context,
      helpText: "Date de versement",
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2050),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: Color(0xff352C60),
          ),
          child: child,
        );
      },
    );
  }

  void callbackNumbers(PayementNumbers number, BuildContext context) {
    if (dataDto.rendu == null) {
      dataDto.rendu = double.parse(dataDto.total);
    }
    if (number.designation == "C") {
      setState(() {
        this.number = "";
      });
      return;
    }
    String n = "";
    if (number.designation == ".") {
      n = ".";
    } else {
      n = number.value.toString();
    }

    String s = this.number.toString() + n.toString();
    setState(() {
      this.number = s;
    });
  }

  void callbackNumbersChq(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        chqNumber.text = "";
      });
      return;
    }
    setState(() {
      chqNumber.text += number;
      chqNumber.selection = TextSelection.fromPosition(
          TextPosition(offset: chqNumber.text.length));
    });
  }

  void payementCheckout() async {
    try {
      if (dataDto.rendu > 0) {
        AlertNotif().alert(
            context, "Reste: " + dataDto.rendu.toStringAsFixed(3) + " DT");
        return;
      }
      if (this.dataDto.commandeDelete.length > 0) {
        this.dataDto.commandeDelete.forEach((element) {
          CommandeRequest().cancelOrderDetails(element.idDetailComm);
        });
      }
      if (dataDto.payementList.length == 0) {
        return;
      }
      setState(() {
        loading = true;
      });
      if (dataDto.payementList.length == 1 && dataDto.clientBPrice != null) {
        dataDto.payementList.forEach((element) {
          if (element.idClientPartenaire == null ||
              element.idClientPartenaire == "") {
            element.idClientPartenaire =
                dataDto.clientBPrice.idClientPartenaire;
          }
        });
      }
      ResponseSingle responseSingle =
          await CommandeRequest().createCommande(1, dataDto, context);
      if (responseSingle.result != 1) {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
      setState(() {
        loading = false;
      });
      if (responseSingle.result == 1)  {

        print(responseSingle);





        setState(() {
          dataDto.commande = null;
          dataDto.commandeDelete = [];
          dataDto.currentPage = "";
          dataDto.payementList = [];
          //dataDto.total = "0.000";
          dataDto.rendu = null;
          dataDto.commandeDetails = [];
          //dataDto.clientBPrice = null;
          dataDto.payementDtos = [];
          dataDto.tableCaisse=null;
          dataDto.listCommande=[];
         dataDto.lastTicketNumber=dataDto.lastTicketNumber+1;
        });
        _showMyDialogAfterPay();
        await  Future.delayed(Duration(seconds: 6), () {
          setState(() {
            dataDto.clientBPrice = null;
            dataDto.total = "0.000";
          });
          dataDto.setCurrentPage(CommandeScreen(dataDto), "CommandeScreen");
        });
        Navigator.of(context).pop();

      }
    } catch (err) {
      setState(() {
        loading = false;
      });
   //   Crashlytics().sendEmail("Payement payementCheckout", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  filterClientList(String qr) {
    if (qr != "" && qr.length == 10) {
      var list = dataDto.listClientFiltred
          .where((element) =>
              element.qrCodePartn != null && element.qrCodePartn == qr)
          .toList();
      ClientBPrice c = list.length > 0 ? list[0] : null;
      c != null
          ? setState(() {
              dataDto.clientBPrice = c;
              error.text = "";
              qrCode.text = "";
              Navigator.of(context).pop();
              payementCheckout();
            })
          : Toast.show("QR Code non valide", context,
              duration: Toast.LENGTH_LONG,
              gravity: Toast.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white);
    }
  }

  handleKey(RawKeyEvent event, BuildContext context) {
    if (event.character != null) {
      if (event.physicalKey == PhysicalKeyboardKey.escape) {
        setState(() {
          _keyCode = '';
          dataDto.searchController.clear();
          dataDto.textController.clear();
          error.text = "";
          qrCode.text = "";
        });
        return;
      }
      switch (event.character.toString()) {
        case "à":
          _keyCode +='0';
          break;
        case "é":
          _keyCode +='2';
          break;
        case "(":
          _keyCode +='5';
          break;
        case "è":
          _keyCode +='7';
          break;
        case "'":
          _keyCode +='4';
          break;
        case "&":
          {
            _keyCode +='1';
          }
          break;
        case "-":
          {
            _keyCode +='6';
          }
          break;

        case "_":
          {
            _keyCode +='8';
          }
          break;

        case "ç":
          {
            _keyCode +='9';
          }
          break;

        case '"':
          {
            _keyCode +='3';
          }
          break;


        default:
          {
            _keyCode += event.character.toString();
          }
          break;
      }
      debugPrint("RawKeyEvent: $_keyCode");
      List<ClientBPrice> list = dataDto.listClientFiltred
          .where((element) =>
              element.qrCodePartn != null &&
              element.qrCodePartn.toLowerCase() == _keyCode.toLowerCase())
          .toList();
      if (list.isNotEmpty) {
        setState(() {
          _keyCode = '';
          dataDto.clientBPrice = list[0];
          error.clear();
          qrCode.clear();
          Navigator.of(context).pop();
          payementCheckout();
        });
      }
    }
  }

  handleKeyQrCode(RawKeyEvent event, BuildContext context,
      ModeReglement modeReglement) async {
    if (event.character != null) {
      if (event.physicalKey == PhysicalKeyboardKey.escape) {
        setState(() {
          _keyCode = '';
          dataDto.searchController.clear();
          dataDto.textController.clear();
          error.text = "";
          qrCode.text = "";
        });
        return;
      }
      setModeReg(ModeReglement modeReglement) {
        Navigator.of(context).pop();
        setState(() {
          error.text = "";
          qrCode.text = "";
        });
        createReg(modeReglement);
      }

      switch (event.character.toString()) {
        case "à":
          _keyCode +='0';
          break;
        case "é":
          _keyCode +='2';
          break;
        case "(":
          _keyCode +='5';
          break;
        case "è":
          _keyCode +='7';
          break;
        case "'":
          _keyCode +='4';
          break;
        case "&":
          {
            _keyCode +='1';
          }
          break;
        case "-":
          {
            _keyCode +='6';
          }
          break;

        case "_":
          {
            _keyCode +='8';
          }
          break;

        case "ç":
          {
            _keyCode +='9';
          }
          break;

        case '"':
          {
            _keyCode +='3';
          }
          break;


        default:
          {
            _keyCode += event.character.toString();
          }
          break;
      }
      debugPrint("RawKeyEvent test: $_keyCode");

      try {
        if(_keyCode.length>=10) {
          ResponseList responseList =
          await customerEndPointApi.findAllActiveClientByPartenaireUsingGET(
              dataDto.utilisateur.idPartenaire);
          List<ClientBPrice> list = responseList.result == 1
              ? responseList.objectResponse
              .map((i) => ClientBPrice.fromJson(i))
              .toList()
              : [];

          setState(() {
            dataDto.listClient = list;
            dataDto.listClientFiltred = list;
            if (_keyCode != "") {
              debugPrint(
                  "***********************************" + list.toString());
              var filtred = list
                  .where((element) =>
              element.qrCodePartn?.toLowerCase() ==
                  _keyCode?.toLowerCase()?.trim())
                  .toList();
              if (filtred.isEmpty) {
                return;
              }
              ClientBPrice c = filtred.length > 0 ? filtred[0] : null;
              double d = dataDto.rendu != null && dataDto.rendu != 0
                  ? dataDto.rendu
                  : dataDto.total;
              if (number != null && number != "") {
                d = double.parse(number);
              }
              print('ccccccccccccccccccccccccc'+c.nom);
              if (c != null && c.soldePartn < d) {
                setState(() {
                  qrCode.clear();
                  error.clear();
                });
                Toast.show(
                    "Le solde de la carte est insuffisant: " +
                        c.soldePartn.toStringAsFixed(int.parse(
                            dataDto.pointVente.chiffrevirgule != null
                                ? dataDto.pointVente.chiffrevirgule
                                : "3")) +
                        " DT",
                    context,
                    duration: Toast.LENGTH_LONG,
                    gravity: Toast.CENTER,
                    backgroundColor: Colors.red,
                    textColor: Colors.white);
                return;
              }
              c != null
                  ? setState(() {
                dataDto.clientBPrice = c;
                setModeReg(modeReglement);
                payementCheckout();
              })
                  : Toast.show("QR Code non valide", context,
                  duration: Toast.LENGTH_SHORT,
                  gravity: Toast.CENTER,
                  backgroundColor: Colors.orange,
                  textColor: Colors.white);
            }
          });
        }
        setState(() {

          qrCode.text = _keyCode;
        });
      } catch (err) {
        Crashlytics().sendEmail("Clients getListClient", err.toString());
        AlertNotif().alert(context,
            "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      }
    }
  }

  Future<void> getClientBPrice() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    if (!ServerUrl.fid) {
      setState(() {
        error.text = "";
        qrCode.text = "";
      });
      payementCheckout();
      return null;
    }
    _focusNodeSelectClient.requestFocus();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Si vous avez une carte FID\n\nVeuillez scanner le QR Code\nou saisir le code manuellement',
            textAlign: TextAlign.center,
          ),
          content: RawKeyboardListener(
            onKey: (RawKeyEvent event) {
              handleKey(event, context);
            },
            focusNode: _focusNodeSelectClient,
            child: Container(
              width: width / 3,
              height: height / 6,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextField(
                      onChanged: (text) {
                        filterClientList(text);
                        setState(() {
                          error.text = "";
                        });
                      },
                      autofocus: true,
                      obscureText: true,
                      cursorColor: Color(0xff352C60),
                      controller: qrCode,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xfff5f5f5),
                        contentPadding: EdgeInsets.all(width / 50),
                        hintText: "QR Code",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        suffixIcon: Container(
                          margin: EdgeInsets.only(right: 16, bottom: 8, top: 8),
                          child: IconButton(
                            onPressed: () {
                              setState(() {
                                _keyCode = '';
                                qrCode.clear();
                                error.clear();
                              });
                            },
                            icon: Icon(Icons.clear),
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            SizedBox(
              child: FlatButton(
                color: Colors.red,
                child: Text('Non'),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    error.text = "";
                    qrCode.text = "";
                  });
                  payementCheckout();
                },
              ),
              width: width / 10,
              height: width / 20,
            ),
            SizedBox(
              width: width / 10,
              height: width / 20,
            ),
            SizedBox(
              child: FlatButton(
                color: Colors.purpleAccent,
                child: Text(
                  'Sélectionner un client',
                  textAlign: TextAlign.center,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    error.text = "";
                    qrCode.text = "";
                  });
                  dataDto.setCurrentPage(Clients(dataDto), "");
                },
              ),
              width: width / 10,
              height: width / 20,
            ),
            SizedBox(
                child: FlatButton(
                  color: Colors.green,
                  child: Text('Valider'),
                  onPressed: () {
                    debugPrint(qrCode.text + "hhhhhhhhhhhhhhh");
                    if (qrCode.text != "") {
                      var list = dataDto.listClientFiltred
                          .where((element) =>
                              element.qrCodePartn != null &&
                              element.qrCodePartn == qrCode.text)
                          .toList();
                      ClientBPrice c = list.length > 0 ? list[0] : null;
                      c != null
                          ? setState(() {
                              dataDto.clientBPrice = c;
                              Navigator.of(context).pop();
                              payementCheckout();
                            })
                          : Toast.show("QR Code non valide", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.CENTER,
                              backgroundColor: Colors.red,
                              textColor: Colors.white);
                    }
                  },
                ),
                width: width / 10,
                height: width / 20),
          ],
        );
      },
    ).whenComplete(() {
      setState(() {
        _keyCode = '';
        _focusNodeSelectClient.unfocus();
      });
    });
  }

  getListClient(ModeReglement modeReglement) async {
    debugPrint(qrCode.text + "hhhh");
    setModeReg(ModeReglement modeReglement) {
      Navigator.of(context).pop();
      setState(() {
        error.text = "";
        //qrCode.text = "";
      });
      createReg(modeReglement);
    }

    try {
      if(_keyCode.length>=10) {
        ResponseList responseList =
        await customerEndPointApi.findAllActiveClientByPartenaireUsingGET(
            dataDto.utilisateur.idPartenaire);
        List<ClientBPrice> list = responseList.result == 1
            ? responseList.objectResponse
            .map((i) => ClientBPrice.fromJson(i))
            .toList()
            : [];

        print(list.length);


        setState(() {
          dataDto.listClient = list;
          dataDto.listClientFiltred = list;
          if (qrCode.text != "") {
            debugPrint(qrCode.text + "1");
            String qrt = qrCode.text.toString();

            debugPrint(_keyCode + "2");
            var text1 = utf8.encode(_keyCode).toString();




            var filtred = list
                .where((element) =>
                    element.qrCodePartn.toString() == _keyCode.trim() )
                .toList();

            //  debugPrint(list.toString());
            ClientBPrice c = filtred.length > 0 ? filtred[0] : null;
            double d = dataDto.rendu != null && dataDto.rendu != 0
                ? dataDto.rendu
                : dataDto.total;
            if (number != null && number != "") {
              d = double.parse(number);
            }
            if (c != null && c.soldePartn < d) {
              setState(() {
                qrCode.clear();
                error.clear();
              });
              Toast.show(
                  "Le solde de la carte est insuffisant: " +
                      c.soldePartn.toStringAsFixed(int.parse(
                          dataDto.pointVente.chiffrevirgule != null
                              ? dataDto.pointVente.chiffrevirgule
                              : "3")) +
                      " DT",
                  context,
                  duration: Toast.LENGTH_LONG,
                  gravity: Toast.CENTER,
                  backgroundColor: Colors.red,
                  textColor: Colors.white);
              return;
            }
            c != null
                ? setState(() {
              dataDto.clientBPrice = c;
              setModeReg(modeReglement);
              payementCheckout();
            })
                : Toast.show("QR Code non valide", context,
                duration: Toast.LENGTH_LONG,
                gravity: Toast.CENTER,
                backgroundColor: Colors.red,
                textColor: Colors.white);
          }
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("Clients getListClient", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> _showMyDialogAfterPay() async {


    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(


          content: Container(
            width: width / 3,
            height: height * 0.40,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Votre commande a été traitée sous",
                  style: TextStyle(
                      fontWeight: FontWeight.w500, fontSize: width / 60),
                ),
                SizedBox(
                  height: 15,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.assignment_turned_in_outlined , size: 50, color: Colors.grey,),
                    SizedBox(width: 5,),
                    Text(
                      "N° "+ (dataDto.lastTicketNumber).toString(),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: width / 40 ,color: Colors.purple),
                    ),

                  ],
                ),



                SizedBox(
                  height: 40,
                ),

                Text(
                  "Votre solde restant",
                  style: TextStyle(
                      fontWeight: FontWeight.w500, fontSize: width / 60),
                ),
                SizedBox(
                  height: 15,
                ),

               Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Icon(Icons.monetization_on_outlined , size: 50, color: Colors.grey,),
                   SizedBox(width: 5,),
                   Text(
                     (dataDto.clientBPrice.soldePartn - double.parse(dataDto.total)).round().toString()+ ' DT',
                     style: TextStyle(
                         fontWeight: FontWeight.bold, fontSize: width / 40 ,color: Colors.purple),
                   ),

                 ],
               )




              ],
            ),
          ),


        );
      },
    );
  }

  void createReg(ModeReglement modeReglement) async {
    double d = 0;
    dataDto.payementList.forEach((element) {
      d += element.montant;
    });
    if (d >= double.parse(dataDto.total)) {
      return;
    }

    if (number == "" || number == null) {
      number = dataDto.rendu != 0 ? dataDto.rendu.toString() : dataDto.total;
    }

    ReglmDetailsDto m = ReglmDetailsDto();
    m.montant = dataDto.rendu != null
        ? dataDto.rendu - double.parse(number) < 0
            ? dataDto.rendu
            : double.parse(number)
        : double.parse(number);
    m.idModReg = modeReglement.idModeReglement;
    m.dateReg = finaldate;
    m.numReg = chqNumber.text!=null&&chqNumber.text!=""?chqNumber.text:null;
    m.idClientPartenaire = dataDto.clientBPrice != null
        ? dataDto.clientBPrice.idClientPartenaire
        : null;
    double recived =
        modeReglement.ffidelite == 1 && dataDto.rendu - double.parse(number) < 0
            ? dataDto.rendu
            : double.parse(number);
    setState(() {
      dataDto.payementDtos.add(PayementDto(
          dataDto.rendu.toStringAsFixed(3) + " DT",
          recived.toStringAsFixed(3) + " DT",
          dataDto.rendu - double.parse(number) < 0 &&
                  modeReglement.ffidelite != 1
              ? (dataDto.rendu - double.parse(number)).toStringAsFixed(3) +
                  " DT"
              : "",
          modeReglement.designation,
          dataDto.clientBPrice != null ? dataDto.clientBPrice.nom : ""));
      dataDto.payementList.add(m);
      dataDto.rendu -= double.parse(number);
      number = "";
      chqNumber.text = "";
    });
  }

  getListClientTest(ModeReglement modeReglement) async {
    debugPrint(qrCode.text + "hhhh");
    setModeReg(ModeReglement modeReglement) {
      Navigator.of(context).pop();
      setState(() {
        error.text = "";
        //qrCode.text = "";
      });
      createReg(modeReglement);
    }

    try {

        ResponseList responseList =
        await customerEndPointApi.findAllActiveClientByPartenaireUsingGET(
            dataDto.utilisateur.idPartenaire);
        List<ClientBPrice> list = responseList.result == 1
            ? responseList.objectResponse
            .map((i) => ClientBPrice.fromJson(i))
            .toList()
            : [];


        setState(() {
          dataDto.listClient = list;
          dataDto.listClientFiltred = list;
          if (test.text != "") {
            var filtred = list
                .where((element) =>
            element.qrCodePartn.toString() == test.text.trim())
                .toList();
            ClientBPrice c = filtred.length > 0 ? filtred[0] : null;

            double d = dataDto.rendu != null && dataDto.rendu != 0
                ? dataDto.rendu
                : dataDto.total;
            if (number != null && number != "") {
              d = double.parse(number);
            }
            if (c != null && c.soldePartn < d) {
              setState(() {
                qrCode.clear();
                error.clear();
              });
              Toast.show(
                  "Le solde de la carte est insuffisant: " +
                      c.soldePartn.toStringAsFixed(int.parse(
                          dataDto.pointVente.chiffrevirgule != null
                              ? dataDto.pointVente.chiffrevirgule
                              : "3")) +
                      " DT",
                  context,
                  duration: Toast.LENGTH_LONG,
                  gravity: Toast.CENTER,
                  backgroundColor: Colors.red,
                  textColor: Colors.white);
              return;
            }
            c != null
                ? setState(() {
              dataDto.clientBPrice = c;
              setModeReg(modeReglement);
              print(c.prenom);
            //  payementCheckout();
            })
                : Toast.show("QR Code non valide", context,
                duration: Toast.LENGTH_LONG,
                gravity: Toast.CENTER,
                backgroundColor: Colors.red,
                textColor: Colors.white);
          }
        });

    } catch (err) {
      Crashlytics().sendEmail("Clients getListClient", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void callbackNumbersQrCode(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        test.text = "";
      });
      return;
    }
    setState(() {
      this.test.text += number;
      this. test.selection =
          TextSelection.fromPosition(TextPosition(offset: test.text.length));
    });
  }

  Future<void> _showMyDialogTest(ModeReglement modeReglement) async {
    setState(() {
      qrCode.text = "";
      _keyCode = '';
      test.clear();
    });

    setModeReg(ModeReglement modeReglement) {
      Navigator.of(context).pop();
      setState(() {
        error.text = "";
        qrCode.text = "";
      });
      createReg(modeReglement);
    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    _focusNodeQrCode.requestFocus();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text(
              'Veuillez saisir votre QR Code manuellement',
              textAlign: TextAlign.center,

            ),
            content: Container(
              width: width / 3,
              height: height * 0.57,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextField(
                      onChanged: (text) {
                        //  filterClientList(text);
                        setState(() {
                          error.text = "";
                        });
                      },
                      autofocus: true,
                      cursorColor: Color(0xff352C60),
                      controller: test,
                      obscureText: false,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xfff5f5f5),
                        contentPadding: EdgeInsets.all(width / 50),
                        hintText: "QR Code",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        suffixIcon: Container(
                          margin: EdgeInsets.only(right: 16, bottom: 8, top: 8),
                          child: IconButton(
                            onPressed: () {
                              setState(() {
                                _keyCode = '';
                                qrCode.clear();
                                error.clear();
                                test.clear();
                              });
                            },
                            icon: Icon(Icons.clear),
                          ),
                        ),
                      )),

                  SizedBox(height: 20,),

                  Container(
                    alignment: Alignment.center,
                    color: Colors.transparent,
                    width: MediaQuery.of(context).size.width * 0.3,
                    height: MediaQuery.of(context).size.width * 0.23,
                    child: NumbersLogin(callbackNumbersQrCode),
                  ),


                ],
              ),
            ),

            actions: <Widget>[
              SizedBox(
                child: FlatButton(
                  color: Color(0xffCF0606),
                  child: Text('Annuler',style: TextStyle(color: Colors.white),),
                  onPressed: () {
                    Navigator.of(context).pop();
                    setState(() {
                      error.text = "";
                      qrCode.text = "";
                      test.clear();
                    });
                  },
                ),
                /* width: width / 10,
              height: width / 20,*/
              ),
              SizedBox(
                child: FlatButton(
                  color: Colors.green,
                  child: Text('Valider',style: TextStyle(color: Colors.white),),
                  onPressed: () {
                    getListClientTest(modeReglement);
                  },
                ),
/*                width: width / 10,
                height: width / 20*/),
            ],
          );
      },
    ).whenComplete(() {
      setState(() {
        _keyCode = '';
        test.clear();
        _focusNodeQrCode.unfocus();
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    final column = Stack(
      fit: StackFit.loose,
      children: [
        Column(
          children: <Widget>[
            SizedBox(
              height: 16,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Paiement",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: width / 50),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Veuillez ouvrir votre application mobile "+ dataDto.pointVente.designation+" pour récupérer votre code à barre",
                      style: TextStyle(
                          fontWeight: FontWeight.w500, fontSize: width / 90),
                    )
                  ],
                ),
                Expanded(
                  child: Container(),
                ),
                SizedBox(
                  height: width > height ? width / 20 : width / 10,
                  width: width > height ? width / 20 : width / 10,
                  child: FlatButton(
                      color: Color(0xff32C533),
                      onPressed: () => {
                            dataDto.payementDtos = [],
                            dataDto.payementList = [],
                            dataDto.currentPage = "",
                            dataDto.clientBPrice = null,
                            if (dataDto.commandeDetails
                                    .where(
                                        (element) => element.recharge == true)
                                    .toList()
                                    .length >
                                0)
                              {
                                dataDto.commandeDetails = [],
                                dataDto.callbackCommande(null, context),
                              },
                            dataDto.setCurrentPage(
                                CommandeScreen(dataDto), "CommandeScreen")
                          },
                      padding: EdgeInsets.all(0),
                      child: Image.asset(
                        'assets/back/back.png',
                        color: Colors.white,
                        fit: BoxFit.contain,
                        height: width > height ? width / 40 : width / 20,
                        width: width > height ? width / 40 : width / 20,
                      )),
                )
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      color: Color(0xffCFC5FF),
                      padding: EdgeInsets.only(
                          left: width / 50,
                          right: width / 50,
                          bottom: width / 80),
                      margin: EdgeInsets.only(right: 16),
                      child: NumbersPaye(dataDto, callbackNumbers, number),
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        /*SizedBox(
                          height: width / 20,
                          child: FlatButton(
                              color: Color(0xffCF5FFE),
                              padding: EdgeInsets.only(left: 8),
                              onPressed: () {
                                dataDto.setCurrentPage(Clients(dataDto), "");
                              },
                              child: Row(
                                children: [
                                  Image.asset(
                                    'assets/user/user.png',
                                    color: Colors.white,
                                    fit: BoxFit.contain,
                                    height: width / 40,
                                    width: width / 40,
                                  ),
                                  Expanded(
                                    child: Text(
                                      dataDto.clientBPrice != null
                                          ? dataDto.clientBPrice.nom +
                                              " " +
                                              dataDto.clientBPrice.prenom
                                          : "Client",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                ],
                              )),
                        ),*/
                        /*                        Text(
                          "Moyens de paiement",
                          style: TextStyle(
                              fontSize: width / 100,
                              fontWeight: FontWeight.bold),
                        ),*/
                        SizedBox(
                          height: 16,
                        ),


                        Expanded(
                            child: ListView.builder(
                          itemCount: ServerUrl.fid
                              ? modeReglement.length
                              : modeReglement
                                  .where((element) => element.ffidelite != 1)
                                  .toList()
                                  .length,
                          padding: EdgeInsets.all(0),
                          itemBuilder: (BuildContext context, int index) {
                            ModeReglement reg = ServerUrl.fid
                                ? modeReglement[index]
                                : modeReglement
                                    .where((element) => element.ffidelite != 1)
                                    .toList()[index];
                            return reg.ffidelite == 1 ?
                            Stack(
                              children: [
                                Lottie.asset("assets/money_animation.json"),
                            SizedBox(height: 30,),
                            dataDto.clientBPrice!=null ?
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,

                              children: [

                                Icon(Icons.account_circle_rounded , size: 40, color: Color(0xffCF5FFE),) ,
                               Text(
                                 '  '+dataDto.clientBPrice.nom + ' '+ dataDto.clientBPrice.prenom ,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold, fontSize: width / 90),
                                  ),

                              ],
                            ) : SizedBox(),

                                Container(
                                  padding: EdgeInsets.only(top: 20),
                                  height: width / 5,
                                  margin: EdgeInsets.only(bottom: 8),
                                  child: GestureDetector(
                                    onTap: (){
                                      if (dataDto.rendu == null ||
                                          dataDto.rendu > 0)
                                        if (reg.fdate == 1)
                                        {
                                          callDatePicker(reg);
                                      }
                                      else if (reg.ffidelite == 1)
                                      {_showMyDialogTest(reg);
                                      }
                                      else
                                      {createReg(reg);}


                                    },

                                    child: Image.asset("assets/temp_card.png" ),
                                  ),
                                ),



                              ],
                            ) : Container();
                          },
                        )),
                      //
                      ],
                    ),
                    width: width / 4,
                    margin: EdgeInsets.only(right: 16),
                  ),
                  Container(
                    alignment: Alignment.topCenter,
                    padding: EdgeInsets.all(0),
                    width: width / 3,
                    color: Color(0xffE7D7EE),
                    child: SingleChildScrollView(
                      padding: EdgeInsets.all(0),
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 16, right: 16, top: 8, bottom: 8),
                        child: Column(
                          children: [
                            Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 8,
                                      bottom: dataDto.rendu != null &&
                                              dataDto.rendu > 0
                                          ? 0
                                          : 8),
                                  child: Text(
                                    "Montant : " +
                                        double.parse(dataDto.total)
                                            .toStringAsFixed(3) +
                                        " DT",
                                    style: TextStyle(
                                        color: Color(0xff352C60),
                                        fontWeight: FontWeight.bold,
                                        fontSize: width > height
                                            ? width / 60
                                            : width / 20),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                dataDto.rendu != null && dataDto.rendu > 0
                                    ? Container(
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(bottom: 8),
                                        child: dataDto.rendu != null &&
                                                dataDto.rendu > 0
                                            ? Text(
                                                "Reste: " +
                                                    dataDto.rendu
                                                        .toStringAsFixed(3) +
                                                    " DT",
                                                style: TextStyle(
                                                    color: Color(0xffCF0606),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: width > height
                                                        ? width / 80
                                                        : width / 30),
                                                textAlign: TextAlign.center,
                                              )
                                            : Container(),
                                      )
                                    : Container(),
                              ],
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text('Dû',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: width / 110,
                                              color: Color(0xff424242)))),
                                  Expanded(
                                      child: Text(
                                    'Reçu',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 110,
                                        color: Color(0xff424242)),
                                  )),
                                  Expanded(
                                      child: Text(
                                    'Rendu',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 110,
                                        color: Color(0xff424242)),
                                  )),
                                  Expanded(
                                      child: Text(
                                    'Client',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 110,
                                        color: Color(0xff424242)),
                                  )),
                                  Expanded(
                                      child: Text(
                                    'Méthode',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 110,
                                        color: Color(0xff424242)),
                                  )),
                                  SizedBox(
                                    width: 32,
                                  )
                                ],
                              ),
                              padding: EdgeInsets.only(top: 16, bottom: 16),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          width: 1, color: Color(0xffbdbdbd)))),
                            ),
                            for (var items in dataDto.payementDtos)
                              Container(
                                  padding: EdgeInsets.only(top: 8, bottom: 8),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: Text(items.du,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: width / 110,
                                                  color: Color(0xff424242)))),
                                      Expanded(
                                          child: Text(
                                        items.recu,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: width / 110,
                                            color: Color(0xff424242)),
                                      )),
                                      Expanded(
                                          child: Text(
                                        items.rendu,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: width / 110,
                                            color: Color(0xff424242)),
                                      )),
                                      Expanded(
                                          child: Text(
                                        items.client,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: width / 110,
                                            color: Color(0xff424242)),
                                      )),
                                      Expanded(
                                          child: Text(
                                        items.mode,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: width / 110,
                                            color: Color(0xff424242)),
                                      )),
                                      Container(
                                        alignment: Alignment.center,
                                        width: 32,
                                        height: 32,
                                        child: FlatButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {
                                              setState(() {
                                                dataDto.payementDtos = [];
                                                dataDto.payementList = [];
                                                dataDto.rendu =
                                                    double.parse(dataDto.total);
                                                number = "";
                                              });
                                            },
                                            child: Icon(
                                              Icons.delete_outline,
                                              color: Color(0xff424242),
                                            )),
                                      ),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              width: 1,
                                              color: Color(0xffbdbdbd))))),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 8,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  SizedBox(
                    height: width / 20,
                    width: width / 3,
                    child: FlatButton(
                        color: Color(0xff32C533),
                        onPressed: () => {
                              if (dataDto.clientBPrice != null ||
                                  dataDto.rendu > 0)
                                {
                                  payementCheckout()

                                }
                              else
                                {getClientBPrice()}
                            },
                        padding: EdgeInsets.all(0),
                        child: loading
                            ? SizedBox(
                                height: width / 60,
                                width: width / 60,
                                child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white)),
                              )
                            : Text(
                                "Valider",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500),
                              )),
                  )
                ],
              ),
            )
          ],
        ),
        search != ""
            ? Positioned(
                child: Container(
                  color: Colors.white,
                  child: Container(
                      child: SingleChildScrollView(
                          scrollDirection:
                              width > height ? Axis.vertical : Axis.horizontal,
                          child: DataTable(
                            columns: [
                              DataColumn(
                                label: Text('Nom'),
                              ),
                              DataColumn(label: Text('Email')),
                              DataColumn(label: Text('Téléphone')),
                              DataColumn(label: Text('Solde')),
                            ],
                            rows: dataDto.listClientFiltred
                                .where((c) =>
                                    c.prenom != null &&
                                        c.prenom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nom != null &&
                                        c.nom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nTel != null &&
                                        c.nTel
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.email != null &&
                                        c.email
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.qrCodePartn != null &&
                                        c.qrCodePartn
                                            .toLowerCase()
                                            .contains(search.toLowerCase()))
                                .toList()
                                .map(
                                  (client) => DataRow(cells: [
                                    DataCell(
                                      Text(
                                        client.nom + " " + client.prenom,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.textController.text = "";
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.email != null
                                            ? client.email
                                            : "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.textController.text = "";
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.nTel,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.textController.text = "";
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.soldePartn.toStringAsFixed(3) +
                                            " DT",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.textController.text = "";
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                  ]),
                                )
                                .toList(),
                          ))),
                ),
                top: 0,
                left: 0,
                right: 0,
              )
            : SizedBox(),
      ],
    );
    final columnVertical = Column(
      children: <Widget>[
        SizedBox(
          height: 16,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Paiement",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: width > height ? width / 50 : width / 20),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  "Veuillez sélectionner une méthode de paiement",
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: width > height ? width / 100 : width / 30),
                )
              ],
            ),
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height ? width / 20 : width / 10,
              width: width > height ? width / 20 : width / 10,
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () => {
                        dataDto.payementDtos = [],
                        dataDto.payementList = [],
                        dataDto.currentPage = "",
                        dataDto.commandeDetails
                                    .where(
                                        (element) => element.recharge == true)
                                    .toList()
                                    .length >
                                0
                            ? dataDto.commandeDetails = []
                            : null,
                        dataDto.setCurrentPage(CommandeScreen(dataDto), "")
                      },
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 16,
        ),
        Expanded(
          child: Column(
            children: [
              Container(
                  height: width / 8,
                  margin: EdgeInsets.only(bottom: 8),
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: ServerUrl.fid
                        ? dataDto.modeReg.length
                        : dataDto.modeReg
                            .where((element) => element.ffidelite != 1)
                            .toList()
                            .length,
                    padding: EdgeInsets.all(0),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        padding: EdgeInsets.all(0),
                        height: width / 25,
                        margin: EdgeInsets.only(right: 8),
                        child: FlatButton(
                            color: Color(0xff8066FF),
                            onPressed: () => {
                                  if (dataDto.rendu > 0 &&
                                      double.parse(number) > 0)
                                    {createReg(dataDto.modeReg[index])}
                                },
                            child: Text(
                              dataDto.modeReg[index].designation,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize:
                                      width > height ? width / 100 : width / 30,
                                  color: Colors.white),
                            )),
                      );
                    },
                  )),
              Expanded(
                child: Container(
                  color: Color(0xffCFC5FF),
                  padding: EdgeInsets.only(left: width / 50, right: width / 50),
                  child: NumbersPaye(dataDto, callbackNumbers, number),
                ),
              ),
              Container(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(0),
                  child: DataTable(
                    columnSpacing: width / 10,
                    columns: [
                      DataColumn(
                        label: Text('Du'),
                      ),
                      DataColumn(label: Text('Reçu')),
                      DataColumn(label: Text('Rendu')),
                      DataColumn(label: Text('Méthode')),
                    ],
                    rows: dataDto.payementDtos
                        .map(
                          (mode) => DataRow(cells: [
                            DataCell(
                              Text(
                                mode.du,
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 8),
                              ),
                              onTap: () {
                                print('Selected ${mode.du}');
                                // dataDto.setCurrentPage(DetailClient(client, dataDto));
                              },
                            ),
                            DataCell(Text(
                              mode.recu,
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 8),
                            )),
                            DataCell(Text(
                              mode.rendu,
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 8),
                            )),
                            DataCell(
                                Row(
                                  children: [
                                    Text(
                                      mode.mode,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 8),
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Icon(
                                      Icons.delete_outline,
                                      color: Colors.black,
                                      size: 30.0,
                                    ),
                                  ],
                                ), onTap: () {
                              setState(() {
                                dataDto.payementDtos = [];
                                dataDto.payementList = [];
                                dataDto.rendu = double.parse(dataDto.total);
                                number = "";
                              });
                            }),
                          ]),
                        )
                        .toList(),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            top: 8,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: width / 8,
                width: width / 3,
                child: FlatButton(
                    color: Color(0xffCF5FFE),
                    padding: EdgeInsets.only(left: 8),
                    onPressed: () {},
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/user/user.png',
                          color: Colors.white,
                          fit: BoxFit.contain,
                          height: width / 20,
                          width: width / 20,
                        ),
                        Expanded(
                          child: Text(
                            dataDto.clientBPrice != null
                                ? dataDto.clientBPrice.nom +
                                    " " +
                                    dataDto.clientBPrice.prenom
                                : "Client",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    )),
              ),
              SizedBox(
                width: 16,
              ),
              Row(
                children: [
                  SizedBox(
                    height: width / 8,
                    width: width / 3,
                    child: FlatButton(
                        color: Color(0xff32C533),
                        onPressed: () => {
                          payementCheckout()
                        },
                        padding: EdgeInsets.all(0),
                        child: loading
                            ? SizedBox(
                                height: width / 60,
                                width: width / 60,
                                child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white)),
                              )
                            : Text(
                                "Valider",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500),
                              )),
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
    return width > height ? column : columnVertical;
  }
}
