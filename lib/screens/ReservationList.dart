import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/product/api/product_end_point_api.dart';
import 'package:ad_caisse/product/model/produit_dto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/reservation/api/reservation_end_point_api.dart';
import 'package:ad_caisse/reservation/model/ReservationResponse.dart';
import 'package:ad_caisse/reservation/model/reservation_dto.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/Reservation.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'NewReservation.dart';

class ReservationList extends StatefulWidget {
  final DataDto dataDto;

  ReservationList(this.dataDto);

  @override
  ReservationListState createState() => ReservationListState(this.dataDto);
}

class ReservationListState extends State<ReservationList> {
  DataDto dataDto;
  String search = "";
  var reservationEndPointApi = ReservationEndPointApi();

  ReservationListState(this.dataDto);

  @override
  void initState() {
    dataDto.searchClient("Rechercher Reservation");
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    getAllReservation();
    super.initState();
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  void treatOrCancelReservation(
      ReservationResponse reservationResponse, int fTraite) async {
    try {
      ReservationDto reservationDto = ReservationDto();
      reservationDto.calledfrom = "CAISSE";
      reservationDto.dateReseravation =
          reservationResponse.vReservationTable.dateReseravation;
      reservationDto.idPointVente = dataDto.pointVente.idPointVente;
      reservationDto.idSession = dataDto.session.idSession;
      reservationDto.idTable = reservationResponse.vReservationTable.idTable;
      reservationDto.fTraite = fTraite;
      reservationDto.nbrPers = reservationResponse.vReservationTable.nbrPers;
      reservationDto.idConsommateur =
          reservationResponse.vReservationTable.idConsommateur;
      reservationDto.src = reservationResponse.vReservationTable.src;
      reservationDto.dateFinReseravation =
          reservationResponse.vReservationTable.dateFinReseravation;
      reservationDto.idEmployeeService =
          reservationResponse.vReservationTable.idEmployeeService;
      reservationDto.idReservation =
          reservationResponse.vReservationTable.idReservation;
      ResponseSingle responseSingle = await reservationEndPointApi
          .treatOrCancelReservationUsingPOST(reservationDto);
      print("reservationEndPointApi: " + responseSingle.result.toString());
      if (responseSingle.result != 0) {
        AlertNotif().alert(context, responseSingle.errorDescription);
        return;
      }
      if (fTraite == 1) {
        setState(() {
          ClientBPrice clientBPrice = ClientBPrice();
          clientBPrice.nom = reservationResponse.nomClient;
          clientBPrice.prenom = reservationResponse.prenomClient;
          clientBPrice.nTel = reservationResponse.telClient;
          clientBPrice.idClientPartenaire =
              reservationResponse.vReservationTable.idConsommateur;
          dataDto.clientBPrice = clientBPrice;
          dataDto.tableCaisse =
              reservationResponse.vReservationTable.tableCaisse;
        });

        reservationResponse.vReservationTable.produitsQtes.forEach((element) {
          element.produit.employe =
              reservationResponse.vReservationTable.employee;
          dataDto.callbackNumbers(element.quantite.toString(), context);
          dataDto.callbackCommande(element.produit, context);
        });
        dataDto.setCurrentPage(CommandeScreen(dataDto), "CommandeScreen");
      } else {
        getAllReservation();
      }
    } catch (err) {
      Crashlytics().sendEmail(
          "ReservationList treatOrCancelReservation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getAllReservation() async {
    try {
      ProductEndPointApi productEndPointApi = ProductEndPointApi();
      ReservationDto reservationDto = ReservationDto();
      reservationDto.idPointVente = dataDto.caisse.idPointVente;
      reservationDto.fTraite = 0;
      reservationDto.calledfrom = "CAISSE";
      ResponseList responseList = await reservationEndPointApi
          .findReservationByStatusAndIdPointVenteUsingPOST(reservationDto);
      // print("getAllReservation: " + responseList.objectResponse.toString());
      List<ReservationResponse> res = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        res = responseList.objectResponse
            .map((e) => ReservationResponse.fromJson(e))
            .toList();
      }
      res.forEach((element) async {
        element.vReservationTable.produitsQtes.forEach((element) async {
          ResponseSingle single = await productEndPointApi
              .findproduitByIdProduitUsingGET(element.idProduit);
          if (single.result == 1 && single.objectResponse != null) {
            ProduitDto produit = ProduitDto.fromJson(single.objectResponse);
            produit.produit.produitpointvente = produit.produitPointVentes
                        .where((element) =>
                            element.idPointVente ==
                            dataDto.pointVente.idPointVente)
                        .toList()
                        .length >
                    0
                ? produit.produitPointVentes
                    .where((element) =>
                        element.idPointVente == dataDto.pointVente.idPointVente)
                    .toList()[0]
                : null;
            setState(() {
              element.produit = produit.produit;
            });
          }
        });
      });
      res.sort((a, b) => b.vReservationTable.dateReseravation
          .compareTo(a.vReservationTable.dateReseravation));
      setState(() {
        dataDto.reservations = res;
        dataDto.reservations.sort((a, b) => b.vReservationTable.dateReseravation
            .compareTo(a.vReservationTable.dateReseravation));
      });
    } catch (err) {
      Crashlytics()
          .sendEmail("ReservationList getAllReservation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Réservations",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: width > height ? width / 50 : width / 30),
            ),
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () => {
                        dataDto.setCurrentPage(
                            Reservation(dataDto), "Reservation")
                      },
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        dataDto.reservations.length > 0
            ? Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Text(
                            "Client",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            dataDto.pointVente.fGestionTable == 1
                                ? "Table"
                                : "Services",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                        ),
                        dataDto.pointVente.fGestionTable == 1
                            ? Expanded(
                                child: Text(
                                  "Pax",
                                  style: TextStyle(
                                    fontSize:
                                        SizeClacule().calcule(context, 60),
                                  ),
                                ),
                              )
                            : SizedBox(),
                        Expanded(
                          child: Text(
                            "Heure",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "Etat",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                          padding: EdgeInsets.only(left: 8),
                          width: 100,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 8),
                          width: 60,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            for (ReservationResponse item in dataDto
                                .reservations
                                .where((r) =>
                                    r.nomClient != null &&
                                        r.nomClient
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    r.prenomClient != null &&
                                        r.prenomClient
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    r.telClient != null &&
                                        r.telClient
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    r.vReservationTable.dateReseravation
                                        .toIso8601String()
                                        .contains(search.toLowerCase()) ||
                                    r.vReservationTable.tableCaisse != null &&
                                        r.vReservationTable.tableCaisse.numTable
                                            .toString()
                                            .contains(search.toLowerCase()))
                                .toList())
                              Column(
                                children: [
                                  FlatButton(
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 8,
                                        ),
                                        Expanded(
                                          child: item.nomClient != null &&
                                                  item.prenomClient != null
                                              ? Text(
                                                  item.nomClient +
                                                      " " +
                                                      item.prenomClient,
                                                  style: TextStyle(
                                                      fontSize: SizeClacule()
                                                          .calcule(context, 60),
                                                      fontWeight:
                                                          FontWeight.w500),
                                                )
                                              : Text(""),
                                        ),
                                        dataDto.pointVente.fGestionTable == 1
                                            ? Expanded(
                                                child: Text(
                                                  "Table N° " +
                                                      item.vReservationTable
                                                          .tableCaisse.numTable
                                                          .toString(),
                                                  style: TextStyle(
                                                      fontSize: SizeClacule()
                                                          .calcule(context, 60),
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              )
                                            : Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    for (var pr in item
                                                        .vReservationTable
                                                        .produitsQtes)
                                                      Column(
                                                        children: [
                                                          Text(
                                                            pr.designation !=
                                                                    null
                                                                ? pr.quantite
                                                                        .toString() +
                                                                    " X " +
                                                                    pr
                                                                        .designation
                                                                : pr.produit !=
                                                                        null
                                                                    ? pr.quantite
                                                                            .toString() +
                                                                        " X " +
                                                                        pr.produit
                                                                            .designation
                                                                    : "",
                                                            style: TextStyle(
                                                                fontSize:
                                                                    SizeClacule()
                                                                        .calcule(
                                                                            context,
                                                                            75),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                          SizedBox(
                                                            height: 8,
                                                          )
                                                        ],
                                                      )
                                                  ],
                                                ),
                                              ),
                                        dataDto.pointVente.fGestionTable == 1
                                            ? Expanded(
                                                child: Text(
                                                  item.vReservationTable
                                                              .nbrPers !=
                                                          null
                                                      ? item.vReservationTable
                                                          .nbrPers
                                                          .toString()
                                                      : "",
                                                  style: TextStyle(
                                                      fontSize: SizeClacule()
                                                          .calcule(context, 60),
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              )
                                            : SizedBox(),
                                        Expanded(
                                          child: Text(
                                            DateFormat("dd-MM-yyyy HH:mm")
                                                .format(item.vReservationTable
                                                    .dateReseravation),
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            item.vReservationTable.fTraite != 0
                                                ? "Traité"
                                                : "Non Traité",
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                          padding: EdgeInsets.only(left: 8),
                                          alignment: Alignment.centerLeft,
                                          width: 100,
                                        ),
                                        Container(
                                          child: Row(
                                            children: [
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Container(
                                                width: 40,
                                                height: 40,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(40),
                                                ),
                                                padding: EdgeInsets.all(0),
                                                child: FlatButton(
                                                    padding: EdgeInsets.all(0),
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              40),
                                                    ),
                                                    onPressed: () {
                                                      treatOrCancelReservation(
                                                          item, -1);
                                                    },
                                                    child: Icon(
                                                      Icons.cancel,
                                                      color: Colors.red,
                                                      size: 40.0,
                                                    )),
                                              ),
                                              SizedBox(
                                                width: 8,
                                              )
                                            ],
                                          ),
                                          width: 60,
                                        ),
                                      ],
                                    ),
                                    color: Color(0xffE7D7EE),
                                    padding: EdgeInsets.only(
                                      top: 16,
                                      bottom: 16,
                                    ),
                                    onPressed: () {
                                      dataDto.setCurrentPage(
                                          NewReservation(dataDto, item),
                                          "NewReservation");
                                    },
                                  ),
                                  SizedBox(
                                    height: 8,
                                  )
                                ],
                              )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : Expanded(
                child: Container(),
              )
      ],
    );
  }
}
