import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/customer/api/customer_end_point_api.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/pos/api/caisse_end_point_api.dart';
import 'package:ad_caisse/pos/model/caisse.dart';
import 'package:ad_caisse/pos/model/operation.dart';
import 'package:ad_caisse/pos/model/operation_type.dart';
import 'package:ad_caisse/product/api/fournisseur_end_point_api.dart';
import 'package:ad_caisse/product/model/fournisseur_dto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/transaction/api/operation_end_point_api.dart';
import 'package:ad_caisse/transaction/api/operation_type_end_point_api.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'NumbersLogin.dart';

class Depense extends StatefulWidget {
  final DataDto dataDto;

  Depense(this.dataDto);

  @override
  _MyHomePageState createState() => _MyHomePageState(this.dataDto);
}

class _MyHomePageState extends State<Depense> {
  DataDto dataDto;
  String dropdownValue = 'Type de dépense';
  FournisseurDto dropdownValueFournisseur ;
  Caisse dropdownValueCiasse ;
  Utilisateur dropdownValueEmploye ;
  List<String> operationTypes = [];
  List<Caisse> caisses = [];
  List<FournisseurDto> fournisseurs = [];
  List<Caisse> caisseList = [];
  List<OperationType> OperationTypeList = [];
  List<FournisseurDto> fournisseurList = [];
  List<Utilisateur> employe = [];
  TextEditingController montant = new TextEditingController();
  TextEditingController numCommande = new TextEditingController();
  TextEditingController commantaire = new TextEditingController();
  DateTime finaldate;
  bool loading = false;
  bool loadingService = true;
  int focused;

  _MyHomePageState(this.dataDto);

  var operationEndPointApi = OperationEndPointApi();
  var operationTypeEndPointApi = OperationTypeEndPointApi();
  var customerEndPointApi = CustomerEndPointApi();
  var caisseEndPointApi = CaisseEndPointApi();
  var fournisseurEndPointApi = FournisseurEndPointApi();
  final utilisateurEndPointApi = UtilisateurEndPointApi();

  @override
  void initState() {
    findAllByIdPatenaireBprice();
    dataDto.searchClient("Rechercher");
    dataDto.textController.addListener(txtListener);
    super.initState();
  }

  txtListener() {
    switch (dataDto.focused) {
      case 3:
        {
          setState(() {
            commantaire.text = dataDto.textController.text;
          });
        }
        break;
    }
  }

  void callbackNumbers(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        dropdownValue = 'Type de dépense';
        dropdownValueFournisseur ;
        dropdownValueCiasse ;
        dropdownValueEmploye;
        commantaire.text = "";
        numCommande.text = "";
        montant.text = "";
        commantaire.clear();
        dataDto.searchController.clear();
      });
      return;
    }
    if (focused == 1) {
      setState(() {
        montant.text += number;
      });
    } else if (focused == 2) {
      numCommande.text += number;
    }
  }

  Future<void> findAllOperationType() async {
    setState(() {
      operationTypes = [];
      operationTypes.add("Type de dépense");
    });
    try {
      ResponseList responseList =
          await operationTypeEndPointApi.findAllByIdPointVenteUsingGET2();
      if (responseList != null &&
          responseList.result == 1 &&
          responseList.objectResponse != null) {
        List<OperationType> list =
            OperationType.listFromJson(responseList.objectResponse);
        setState(() {
          OperationTypeList = list;
        });
        list.forEach((element) {
          print(element.designation);
          setState(() {
            operationTypes.add(element.designation);
          });
        });
        setState(() {
          loadingService = false;
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("Depence findAllOperationType", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> findAllCaisseByidPointVente() async {
    setState(() {
      caisses = [];
     // caisses.add("N° Caisse destinataire");
    });
    try {
      ResponseList responseList = await caisseEndPointApi
          .findAllCaisseByidPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList != null &&
          responseList.result == 1 &&
          responseList.objectResponse != null) {
        List<Caisse> list = Caisse.listFromJson(responseList.objectResponse);
        setState(() {
          caisses = list;
        });
        // caisseList.forEach((element) {
        //   setState(() {
        //     caisses.add(element.numCaisse.toString());
        //   });
        // });
      }
    } catch (err) {
      Crashlytics()
          .sendEmail("Depence findAllCaisseByidPointVente", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }

    findAllOperationType();
  }

  Future<void> findAllByIdPatenaireBprice() async {
    setState(() {
      fournisseurs = [];
      //fournisseurs.add('Fournisseur');
    });
    try {
      ResponseList responseList = await fournisseurEndPointApi
          .findAllByIdPatenaireBpriceUsingGET(dataDto.pointVente.idPartenaire);
      if (responseList != null &&
          responseList.result == 1 &&
          responseList.objectResponse != null) {
        List<FournisseurDto> list =
            FournisseurDto.listFromJson(responseList.objectResponse);
        setState(() {
          fournisseurs = list;
        });
        // list.forEach((element) {
        //   setState(() {
        //     fournisseurs.add(element.societe);
        //   });
        // });
      }


      ResponseList responseListEmpl = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseListEmpl.result == 1) {
        employe = responseListEmpl.objectResponse
            .map((e) => Utilisateur.fromJson(e))
            .toList();
      }
    } catch (err) {
      Crashlytics()
          .sendEmail("Depence findAllByIdPatenaireBprice", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }

    findAllCaisseByidPointVente();
  }

  Future<void> createOperation() async {
    if (montant.text == "" || dropdownValue == 'Type de dépense') {
      return;
    }
    setState(() {
      loading = true;
    });
    try {
      FournisseurDto fournisseurDto;
      Caisse caisse;
      OperationType operationType;
      // List<FournisseurDto> f = fournisseurList
      //     .where((element) => element.societe == dropdownValueFournisseur)
      //     .toList();
      // List<Caisse> c = caisseList
      //     .where(
      //         (element) => element.numCaisse.toString() == dropdownValueCiasse)
      //     .toList();
      List<OperationType> o = OperationTypeList.where(
          (element) => element.designation == dropdownValue).toList();
      // if (f.length > 0) {
      //   fournisseurDto = f[0];
      // }
      // if (c.length > 0) {
      //   caisse = c[0];
      // }
      if (o.length > 0) {
        operationType = o[0];
      }
      Operation operationInput = Operation();
      operationInput.montant = double.parse(montant.text);
      operationInput.idPointVente = dataDto.pointVente.idPointVente;
      operationInput.numCommande =
          numCommande.text != "" ? int.parse(numCommande.text) : null;
      operationInput.commenatire = commantaire.text;
      operationInput.idUtilisateur = dataDto.utilisateur.idUtilisateur;
      operationInput.idFournisseur =
      dropdownValueFournisseur != null ? dropdownValueFournisseur.idFournisseur : null;
      operationInput.caiIdCaisse = dropdownValueCiasse != null ? dropdownValueCiasse.idCaisse : null;
      operationInput.idEmploye = dropdownValueEmploye != null ? dropdownValueEmploye.idUtilisateur : null;
      operationInput.idCaisse = dataDto.caisse.idCaisse;

      operationInput.idTypeOperation =
          operationType != null ? operationType.idTypeOperation : null;
      operationInput.dateOperation = DateTime.now();
      ResponseSingle responseSingle =
          await operationEndPointApi.createOperationUsingPOST(operationInput);
      if (responseSingle.result == 1) {
        callbackNumbers("C", context);
        _showToast(context);
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
      setState(() {
        loading = false;
      });
    } catch (err) {
      setState(() {
        loading = false;
      });
      Crashlytics().sendEmail("Depense createOperation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void _showToast(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(  backgroundColor: Colors.green,
        content: const Text('Ajouté avec succés'),

      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ListView(
      padding: EdgeInsets.all(0),
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () => {
                        dataDto.setCurrentPage(
                            CommandeScreen(dataDto), "CommandeScreen")
                      },
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        loadingService
            ? Center(
                child: SizedBox(
                height: width > height ? width / 20 : width / 15,
                width: width > height ? width / 20 : width / 15,
                child: CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xffCF5FFE))),
              ))
            : Row(
                children: [
                  Expanded(
                      child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: TextField(
                                  cursorColor: Color(0xff352C60),
                                  controller: montant,
                                  onTap: () {
                                    setState(() {
                                      focused = 1;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    contentPadding: EdgeInsets.fromLTRB(
                                        20.0, 15.0, 20.0, 15.0),
                                    hintText: "Montant:",
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(0)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                  ))),
                          Expanded(
                              child: Container(
                            color: Colors.white,
                            margin: EdgeInsets.only(left: 8),
                            padding: EdgeInsets.only(left: 8, right: 8),
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownValue,
                              icon: Icon(Icons.arrow_drop_down_circle),
                              iconSize: 24,
                              iconEnabledColor: Colors.purple,
                              elevation: 0,
                              style: TextStyle(color: Colors.black),
                              underline: Container(
                                height: 0,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                });
                              },
                              items: operationTypes
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value[0].toUpperCase() +
                                      value.substring(1)),
                                );
                              }).toList(),
                            ),
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      dropdownValue != "Type de dépense" &&
    ( dropdownValue == "paiement facture" ||   dropdownValue == "avoir")
                          ? Row(
                              children: [
                                dropdownValue != "paiement facture"
                                    ? Expanded(
                                        child: TextField(
                                            cursorColor: Color(0xff352C60),
                                            controller: numCommande,
                                            onTap: () {
                                              setState(() {
                                                focused = 2;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              filled: true,
                                              fillColor: Colors.white,
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      20.0, 15.0, 20.0, 15.0),
                                              hintText: "N° Commande/Facture:",
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(0)),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white),
                                                borderRadius:
                                                    BorderRadius.circular(0),
                                              ),
                                              enabledBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white),
                                                borderRadius:
                                                    BorderRadius.circular(0),
                                              ),
                                            )))
                                    : SizedBox(),
                                Expanded(
                                    child: Container(
                                  color: Colors.white,
                                  margin: EdgeInsets.only(
                                      left: dropdownValue != "paiement facture"
                                          ? 8
                                          : 0),
                                  padding: EdgeInsets.only(left: 8, right: 8),
                                  child: DropdownButton<FournisseurDto>(
                                    isExpanded: true,
                                    value: dropdownValueFournisseur,
                                    icon: Icon(Icons.arrow_drop_down_circle),
                                    iconSize: 24,
                                    iconEnabledColor: Colors.purple,
                                    elevation: 0,
                                    hint: Text("Veuillez choisir un fournisseur"),
                                    style: TextStyle(color: Colors.black),
                                    underline: Container(
                                      height: 0,
                                    ),
                                    onChanged: (FournisseurDto newValue) {
                                      setState(() {
                                        dropdownValueFournisseur = newValue;
                                      });
                                    },
                                    items: fournisseurs
                                        .map<DropdownMenuItem<FournisseurDto>>(
                                            (FournisseurDto value) {
                                      return DropdownMenuItem<FournisseurDto>(
                                        value: value,
                                        child: Text(value.nom+ (value.societe!=null?" ("+value.societe+")":"")),
                                      );
                                    }).toList(),
                                  ),
                                )),
                              ],
                            )
                          : SizedBox(),
                      dropdownValue != "Type de dépense" &&
                              dropdownValue == "transfert"
                          ? Row(
                              children: [
                                Expanded(
                                    child: Container(
                                  color: Colors.white,
                                  margin: EdgeInsets.only(left: 0),
                                  padding: EdgeInsets.only(left: 8, right: 8),
                                  child: DropdownButton<Caisse>(
                                    isExpanded: true,
                                    value: dropdownValueCiasse,
                                    icon: Icon(Icons.arrow_drop_down_circle),
                                    iconSize: 24,
                                    iconEnabledColor: Colors.purple,
                                    elevation: 0,
                                    hint: Text("Veuillez choisir une caisse destinataire"),
                                    style: TextStyle(color: Colors.black),
                                    underline: Container(
                                      height: 0,
                                    ),
                                    onChanged: (Caisse newValue) {
                                      setState(() {
                                        dropdownValueCiasse = newValue;
                                      });
                                    },
                                    items: caisses
                                        .map<DropdownMenuItem<Caisse>>(
                                            (Caisse value) {
                                      return DropdownMenuItem<Caisse>(
                                        value: value,
                                        child: Text("Caisse N° "+value.numCaisse.toString() ),
                                      );
                                    }).toList(),
                                  ),
                                )),
                              ],
                            )
                          : SizedBox(),
                      dropdownValue != "Type de dépense" &&
                          (dropdownValue == "salaire" || dropdownValue == "prime" ||dropdownValue == "avance")
                          ? Row(
                        children: [
                          Expanded(
                              child: Container(
                                color: Colors.white,
                                margin: EdgeInsets.only(left: 0),
                                padding: EdgeInsets.only(left: 8, right: 8),
                                child: DropdownButton<Utilisateur>(
                                  isExpanded: true,
                                  value: dropdownValueEmploye,
                                  icon: Icon(Icons.arrow_drop_down_circle),
                                  iconSize: 24,
                                  iconEnabledColor: Colors.purple,
                                  elevation: 0,
                                  hint: Text("Veuillez choisir un employé"),
                                  style: TextStyle(color: Colors.black),
                                  underline: Container(
                                    height: 0,
                                  ),
                                  onChanged: (Utilisateur newValue) {
                                    setState(() {
                                      dropdownValueEmploye = newValue;
                                    });
                                  },
                                  items: employe
                                      .map<DropdownMenuItem<Utilisateur>>(
                                          (Utilisateur value) {
                                        return DropdownMenuItem<Utilisateur>(
                                          value: value,
                                          child: Text(value.nom + " "+value.prenom ),
                                        );
                                      }).toList(),
                                ),
                              )),
                        ],
                      )
                          : SizedBox(),
                      SizedBox(
                        height: 16,
                      ),
                      Container(
                        height: height / 3,
                        child: TextField(
                            cursorColor: Color(0xff352C60),
                            expands: true,
                            maxLines: null,
                            controller: commantaire,
                            textAlign: TextAlign.start,
                            textAlignVertical: TextAlignVertical.top,
                            onTap: () {
                              setState(() {
                                dataDto.focused = 3;
                              });
                              dataDto.callbackKeyboard(false);
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              hintText: "Commentaire:",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0)),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(0),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(0),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width > height ? width / 20 : width / 10,
                            child: FlatButton(
                                color: Color(0xff32C533),
                                onPressed: () => {createOperation()},
                                padding: EdgeInsets.only(
                                    left: width / 25, right: width / 25),
                                child: loading
                                    ? SizedBox(
                                        height: width > height
                                            ? width / 40
                                            : width / 30,
                                        width: width > height
                                            ? width / 40
                                            : width / 30,
                                        child: CircularProgressIndicator(
                                            valueColor:
                                                new AlwaysStoppedAnimation<
                                                    Color>(Colors.white)),
                                      )
                                    : Text(
                                        "Enregistrer",
                                        style: TextStyle(color: Colors.white),
                                      )),
                          ),
                          Container()
                        ],
                      ),
                    ],
                  )),
                  SizedBox(
                    width: width / 16,
                  ),
                  Container(
                    alignment: Alignment.topCenter,
                    color: Colors.transparent,
                    width: width * 0.3,
                    height: width * 0.215,
                    child: NumbersLogin(callbackNumbers),
                  ),
                  SizedBox(
                    width: width / 16,
                  ),
                ],
              )
      ],
    );
  }
}
