import 'dart:async';

import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/pos/api/journee_end_point_api.dart';
import 'package:ad_caisse/pos/model/AutreSessionRes.dart';
import 'package:ad_caisse/pos/model/ClotureJourneeDto.dart';
import 'package:ad_caisse/pos/model/ClotureJourneeRes.dart';
import 'package:ad_caisse/pos/model/PreColture.dart';
import 'package:ad_caisse/product/api/product_end_point_api.dart';
import 'package:ad_caisse/product/model/Famille.dart';
import 'package:ad_caisse/request/ClotureRequest.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/NumbersLogin.dart';
import 'package:ad_caisse/transaction/model/commande.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/PrintingTicket.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multiselect/flutter_multiselect.dart';
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:http/http.dart' as http;

import 'Login_Screen.dart';

class Setting extends StatefulWidget {
  final DataDto dataDto;

  Setting(this.dataDto);

  @override
  SettingState createState() => SettingState(this.dataDto);
}

class SettingState extends State<Setting> with SingleTickerProviderStateMixin  {
  final DataDto dataDto;
  bool loadingComm = false;
  bool loadingTicket = false;
  bool printerStatus = false;
  bool printerCuisineStatus = false;
  TextEditingController montantCloture = new TextEditingController();
  TextEditingController printer = new TextEditingController();
  TextEditingController printerCuisine = new TextEditingController();
  TextEditingController tiroir = new TextEditingController();
  JourneeEndPointApi journeeEndPointApi = JourneeEndPointApi();
  TextEditingController codeWifi = new TextEditingController();
  String idCommande;
  bool isSwitched = false;
  bool isImpIng = false;
  bool isActiveVente = false;
  bool isSwitchedRetour = false;
  bool isNumTicketFromLocal = false;
  bool isDesImpTicket = false;
  bool isSwitchedImpTicketAnnulation = false;
  String dropdownValue = '';
  String dropdownValueCuisine = '';
  String dropdownValuePrintAux1 = '';
  String dropdownValuePrintAux2 = '';
  String formatPapier = '80 mm';
  List<String> formatPapiers = ['80 mm', '57 mm'];
  List<String> printers = [];
  pw.Document _document = pw.Document();
  bool _loadingDoc = true;
  bool _loading = false;
  double fontSize = 13;
  int margin = 0;
  List<Famille> familles;
  String resFamille;
  String resFamilleCuisine;
  String resFamillePrinterAux1;
  String resFamillePrinterAux2;
  String familleImprimante1;
  List<Map<String, String>> data;
  SettingState(this.dataDto);
  List<String> initFamille1 =
      ServerUrl.familleone != null && ServerUrl.familleone.length > 0
          ? ServerUrl.familleone
          : [];
  List<String> initFamilleCuisine =
      ServerUrl.familletwo != null && ServerUrl.familletwo.length > 0
          ? ServerUrl.familletwo
          : [];
  List<String> initFamillePrintAux1 = ServerUrl.famillePrinterAux1 != null &&
          ServerUrl.famillePrinterAux1.length > 0
      ? ServerUrl.famillePrinterAux1
      : [];
  List<String> initFamillePrintAux2 = ServerUrl.famillePrinterAux2 != null &&
          ServerUrl.famillePrinterAux2.length > 0
      ? ServerUrl.famillePrinterAux2
      : [];

  String codWifi;

  TextEditingController codeActionAnnulee = new TextEditingController();
  TextEditingController codeActionRemize = new TextEditingController();

  bool isSwitchedActionAnnule = false;
  bool isSwitchedActionRemize = false;
  TabController tabController ;



  @override
  void initState() {
    dataDto.searchClient("Rechercher");
    tabController = TabController(length: 3, vsync: this);

    generateDoc();
    getCodeWifi();
    getPrinterList();
    getAllFamilles();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    if (codeActionAnnulee.text != null) {
      saveSharedPreferences("codeActionAnnulee", codeActionAnnulee.text);
    }

    if (codeActionRemize.text != null) {
      saveSharedPreferences("codeActionRemize", codeActionRemize.text);
    }

    if (codeActionRemize.text != null) {
      saveSharedPreferences("codeW", codeWifi.text);
    }
  }

  Future<void> getCodeWifi() async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    codWifi = prefs.getString('codeW');
    if (codWifi.isNotEmpty) codeWifi.text = codWifi;
  }

  Future<void> getAllFamilles() async {
    try {
      familles = [];
      ProductEndPointApi produitProintVenteEndPointApi = ProductEndPointApi();
      ResponseList list = await produitProintVenteEndPointApi
          .findAllFamilleByPartenaire(dataDto.pointVente.idPartenaire);
      getPrinterList();
      if (list.result == 1 && list.objectResponse != null) {
        familles = list.objectResponse.map((e) => Famille.fromJson(e)).toList();
      }
    } catch (err) {
      Crashlytics().sendEmail("Setting getFamilles", err.toString());
    }
  }

  onChanged(double value) {
    debugPrint('fontSize: $value');
    try {
      setState(() {
        fontSize = value;
      });
      saveSharedPreferences('fontSize', fontSize.toString());
      generateDoc();
    } catch (err) {
      debugPrint("First text field: $err");
    }
  }

  onChangedMargin(double value) {
    try {
      setState(() {
        margin = value.ceil();
      });
      saveSharedPreferences('margin_printer', margin.toString());
      generateDoc();
    } catch (err) {
      debugPrint("First text field: $err");
    }
  }

  getPrinterList() async {
    try {
      LocalStorageInterface prefs = await LocalStorage.getInstance();
      bool notif = prefs.getBool('notif');
      List<Printer> listPrinter =
          ServerUrl.list.where((element) => element.name != null).toList();
      List<String> list = [];
      for (int i = 0; i < listPrinter.length; i++) {
        list.add(listPrinter[i].name);
      }
      String principalPrinter = prefs.getString("principalPrinter");
      String cuisinePrinter = prefs.getString("principalCuisine");
      String printerAux1 = prefs.getString("printerAux1");
      String printerAux2 = prefs.getString("printerAux2");
      String formatPapierSaved = prefs.getString('formatPapier');
      String fontSizeSaved = prefs.getString('fontSize');
      bool retour = prefs.getBool('retour_automatique');
      String marginSaved = prefs.getString('margin_printer');
      bool isImpAnnulation = prefs.getBool("imp_Annulation");
      String codeA = prefs.getString('codeActionAnnulee');
      String codeR = prefs.getString('codeActionRemize');
      bool annulee = prefs.getBool('action_Annulee');
      bool remize = prefs.getBool('action_Remize');
      bool impIng = prefs.getBool('imp_Ing');
      bool activeVente = prefs.getBool('active_vente');
      bool numTicketFromLocal = prefs.getBool("num_ticket_from_local");
      bool desImpTicket = prefs.getBool("des_imp_ticket");

      setState(() {
        fontSize = fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
        isSwitchedRetour = retour != null ? retour : false;
        dataDto.isSwitchedRetour = retour != null ? retour : false;
        isSwitched = notif != null ? notif : false;
        formatPapier = formatPapierSaved != null ? formatPapierSaved : '80 mm';
        margin = marginSaved != null ? int.parse(marginSaved) : 0;
        isSwitchedActionAnnule = annulee != null ? annulee : false;
        isSwitchedActionRemize = remize != null ? remize : false;
      });

      if (codeA != null) {
        setState(() {
          codeActionAnnulee.text = codeA;
        });
      }

      if (activeVente != null) {
        setState(() {
          isActiveVente = activeVente;
        });
      }
     if (numTicketFromLocal != null) {
        setState(() {
          isNumTicketFromLocal = numTicketFromLocal;
        });
      }

      if (desImpTicket != null) {
        setState(() {
          isDesImpTicket = desImpTicket;
        });
      }

      if (codeR != null) {
        setState(() {
          codeActionRemize.text = codeR;
        });
      }

      if (isImpAnnulation != null) {
        setState(() {
          isSwitchedImpTicketAnnulation = isImpAnnulation;
        });
      }

      if (impIng != null) {
        setState(() {
          isImpIng = impIng;
        });
      }

      if (principalPrinter != null) {
        setState(() {
          dropdownValue = principalPrinter;
        });
      }

      if (cuisinePrinter != null) {
        setState(() {
          dropdownValueCuisine = cuisinePrinter;
        });
      }
      if (printerAux1 != null) {
        setState(() {
          dropdownValuePrintAux1 = printerAux1;
        });
      }
      if (printerAux2 != null) {
        setState(() {
          dropdownValuePrintAux2 = printerAux2;
        });
      }

      // list.insert(0, '');
      if (list != null) {
        list.insert(0, '');
        setState(() {
          printers = list;
        });
      } else {
        printers.insert(0, '');
      }
    } catch (err) {
      debugPrint("GetPrinter: " + err.toString());
    }
  }

  List<String> splitFamille(String src) {
    if (src != null) src.trim();
    String tr = src.replaceAll(' ', '');
    print('7777777777777777777777777$tr');
    List<String> result = tr.split(",");
    return result;
  }

  void preCloture() async {
    try {
      setState(() {
        _loading = true;
      });
      PreColture colture = await ClotureRequest().getPreColture(dataDto);
      if (colture.commandesNonPayeAndPartielPayee != null &&
          colture.commandesNonPayeAndPartielPayee.length > 0) {
        setState(() {
          _loading = false;
        });
        showCommandeDialog(colture);
        return;
      }
      if (colture.nombreCommandePartiellementPayes != null &&
          colture.nombreCommandePartiellementPayes > 0) {
        AlertNotif().alert(
            context,
            "Commande Partiellement Payes : " +
                colture.nombreCommandePartiellementPayes.toString());
        return;
      }

      if (colture.nombreCommandeNonPayes != null &&
          colture.nombreCommandeNonPayes > 0) {
        AlertNotif().alert(
            context,
            "Nombre Commande non payes : " +
                colture.nombreCommandeNonPayes.toString());
        return;
      }
      setState(() {
        _loading = false;
      });
      setInputMontant(colture);
    } catch (err) {
      Crashlytics().sendEmail("Setting preCloture", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void cloture(PreColture colture) async {
    try {
      if (montantCloture.text == "") {
        montantCloture.text = "0";
      }
      if (colture.montantTirroir == null) {
        colture.montantTirroir = 0;
      }

      ResponseSingle responseSingle = await ClotureRequest().cloture(
          dataDto, colture, double.parse(montantCloture.text), context);
      if (responseSingle == null) {
        AlertNotif().alert(context, 'Une erreur est survenue');
        return;
      }
      if (responseSingle.result == 1) {
        Utils().printSessionCloture(context, colture, dataDto);

        showClotureDialog(colture);
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics().sendEmail("Setting cloture", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

/*
  void montantInput(PreColture colture) {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                TextField(
                    cursorColor: Color(0xff352C60),
                    controller: montantCloture,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xfff5f5f5),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Montant Clôture:",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    )),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xffCF0606),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'OK',
                style: TextStyle(color: Colors.white),
              ),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xFF32C533),
              onPressed: () {
                montantCloture.text != "" ? cloture(colture) : null;
              },
            ),
          ],
        );
      },
    );
  }
*/


  void setInputMontant(PreColture colture) {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                TextField(
                    cursorColor: Color(0xff352C60),
                    controller: montantCloture,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xfff5f5f5),
                      contentPadding:
                      EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Montant Clôture:",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    )),
                SizedBox(height: 16.0),
                Container(
                  alignment: Alignment.center,
                  color: Colors.transparent,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.width * 0.215,
                  child: NumbersLogin(callbackNumbersMontant),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xffCF0606),
              onPressed: () {
                Navigator.of(context).pop();
                montantCloture.clear();
              },
            ),
            RaisedButton(
              child: Text(
                'OK',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.all(16),
              elevation: 0,
              color: Color(0xFF32C533),
              onPressed: () {
        montantCloture.text != "" ? cloture(colture) : null;

        },

            ),
          ],
        );
      },
    );
  }
  void callbackNumbersMontant(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        montantCloture.text = "";
      });
      return;
    }
    setState(() {
      montantCloture.text += number;
    });
  }

  saveSharedPreferences(String key, String value) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setString(key, value);
  }

  setNotif(bool notif) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("notif", notif);
  }

  setRetour(bool notif) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("retour_automatique", notif);
  }

  setNumTicketFromLocal(bool isNumTicket) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("num_ticket_from_local", isNumTicket);
  }

  setDesImpTicket(bool isDesImpTicket) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("des_imp_ticket", isDesImpTicket);
  }

  setImpAnnulation(bool notif) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("imp_Annulation", notif);
  }

  setImpIng(bool notif) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("imp_Ing", notif);
  }

  setIsActiveVente(bool notif) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("active_vente", notif);
  }

  getSharedPreferences() async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    String printerip = prefs.getString('printer');
    String printercuisineip = prefs.getString('printercuisine');
    String tiroirCaisse = prefs.getString('tiroir');
    String fontSizeSaved = prefs.getString('fontSize');
    bool notif = prefs.getBool('notif');
    setState(() {
      fontSize = fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
      printer.text = printerip;
      printerCuisine.text = printercuisineip;
      tiroir.text = tiroirCaisse;
      isSwitched = notif;
    });
  }

  setActionAnnule(bool notif) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("action_Annulee", notif);
  }

  setActionRemize(bool notif) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setBool("action_Remize", notif);
  }

  Future<void> showCommandeDialog(PreColture colture) async {
    List<CommandeTransaction> commandes =
        colture.commandesNonPayeAndPartielPayee;
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          backgroundColor: Color(0xffEEEEEE),
          scrollable: true,
          contentPadding:
              EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 16),
          content: Container(
            height: height * 0.80,
            width: width * 0.6,
            child: Column(
              children: [
                Container(
                  color: Colors.red,
                  child: Text(
                    'Vous avez ' +
                        commandes.length.toString() +
                        " commandes non encore payées",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  width: width * 0.6,
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Row(
                  children: [
                    Container(
                      child: Text(
                        "N°",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                      padding: EdgeInsets.only(left: 8),
                      width: 60,
                    ),
                    Expanded(
                      child: Text(
                        "Date",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "Montant",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "Clients",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    dataDto.pointVente.fGestionTable == 1
                        ? Container(
                            width: 80,
                            child: Text(
                              "N° Table",
                              style: TextStyle(
                                fontSize: SizeClacule().calcule(context, 60),
                              ),
                            ))
                        : SizedBox(
                            width: 0,
                          )
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        for (var item in commandes.reversed)
                          Column(
                            children: [
                              FlatButton(
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 8),
                                          width: 60,
                                          child: Text(
                                            item.numCommande.toString(),
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            DateFormat("dd-MM-yyyy HH:mm")
                                                .format(item.dateCreation),
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            item.montant.toStringAsFixed(3) +
                                                " DT",
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              for (var items in item.clients)
                                                Column(
                                                  children: [
                                                    Text(
                                                      items.nom != null
                                                          ? items.prenom +
                                                              " " +
                                                              items.nom
                                                          : "",
                                                      style: TextStyle(
                                                          fontSize:
                                                              SizeClacule()
                                                                  .calcule(
                                                                      context,
                                                                      60),
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                    SizedBox(
                                                      height: 8,
                                                    )
                                                  ],
                                                )
                                            ],
                                          ),
                                        ),
                                        dataDto.pointVente.fGestionTable == 1
                                            ? Container(
                                                alignment: Alignment.center,
                                                width: 80,
                                                child: Text(
                                                  item.numTable != null
                                                      ? item.numTable.toString()
                                                      : "",
                                                  style: TextStyle(
                                                    fontSize: SizeClacule()
                                                        .calcule(context, 60),
                                                  ),
                                                ))
                                            : SizedBox(
                                                width: 0,
                                              )
                                      ],
                                    ),
                                    item.idCommande == idCommande
                                        ? Container(
                                            margin: EdgeInsets.all(16),
                                            child: TextField(
                                                cursorColor: Colors.black,
                                                keyboardType:
                                                    TextInputType.multiline,
                                                maxLines: null,
                                                decoration: InputDecoration(
                                                  filled: true,
                                                  fillColor: Colors.transparent,
                                                  contentPadding:
                                                      EdgeInsets.only(left: 16),
                                                  hintText: "Commentaire",
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Colors.grey),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              0)),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .grey),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(0)),
                                                  enabledBorder:
                                                      UnderlineInputBorder(
                                                          borderSide:
                                                              BorderSide(
                                                                  color: Colors
                                                                      .grey),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(0)),
                                                )),
                                          )
                                        : SizedBox(),
                                    item.idCommande == idCommande
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: width / 10,
                                                child: OutlineButton(
                                                  color: Colors.red,
                                                  borderSide: BorderSide(
                                                      color: Colors.red),
                                                  child: Text(
                                                    'Annuler',
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  ),
                                                  onPressed: () {
                                                    setState(() {
                                                      idCommande = null;
                                                    });
                                                    Navigator.of(context).pop();
                                                    showCommandeDialog(colture);
                                                  },
                                                ),
                                              ),
                                              SizedBox(
                                                width: 24,
                                              ),
                                              SizedBox(
                                                width: width / 10,
                                                child: OutlineButton(
                                                  color: Colors.green,
                                                  borderSide: BorderSide(
                                                      color: Colors.green),
                                                  child: Text('Valider',
                                                      style: TextStyle(
                                                          color: Colors.black)),
                                                  onPressed: () {
                                                    setState(() {
                                                      idCommande = null;
                                                    });
                                                    Navigator.of(context).pop();
                                                    showCommandeDialog(colture);
                                                  },
                                                ),
                                              )
                                            ],
                                          )
                                        : SizedBox()
                                  ],
                                ),
                                color: Color(0xffE7D7EE),
                                padding: EdgeInsets.only(
                                  top: 16,
                                  bottom: 16,
                                ),
                                onPressed: () {
                                  setState(() {
                                    idCommande = item.idCommande;
                                    Navigator.of(context).pop();
                                    showCommandeDialog(colture);
                                  });
                                },
                              ),
                              SizedBox(
                                height: 8,
                              )
                            ],
                          )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: height / 20,
                ),
                Text(
                  "Etes vous sûr de vouloir continuer la cloture ?",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: width / 10,
                      child: FlatButton(
                        color: Colors.red,
                        child: Text(
                          'Annuler',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          setState(() {
                            idCommande = null;
                          });
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    SizedBox(
                      width: 24,
                    ),
                    SizedBox(
                      width: width / 10,
                      child: FlatButton(
                        color: Colors.green,
                        child: Text('Continuer',
                            style: TextStyle(color: Colors.white)),
                        onPressed: () {
                          setState(() {
                            idCommande = null;
                          });
                          Navigator.of(context).pop();
                          setInputMontant(colture);
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> showClotureDialog(PreColture colture) async {
    if (colture.montantParModeRegl == null) {
      colture.montantParModeRegl = [];
    }
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          backgroundColor: Color(0xffEEEEEE),
          contentPadding: EdgeInsets.all(0),
          scrollable: true,
          content: Container(
            height: height * 0.6,
            width: width * 0.6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: Colors.purpleAccent,
                  child: Text(
                    dataDto.utilisateur.prenom + " " + dataDto.utilisateur.nom,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  width: width * 0.6,
                  padding: EdgeInsets.all(24),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: Text(
                    "Votre session a été cloturée avec succès",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  width: width * 0.6,
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: Text(
                    "Montant par moyen de paiement",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  width: width * 0.6,
                  margin: EdgeInsets.only(bottom: 8, left: 16),
                ),
                Expanded(
                    child: GridView.count(
                  padding: EdgeInsets.only(top: 8, left: 16),
                  crossAxisCount: width > height ? 4 : 2,
                  mainAxisSpacing: 8.0,
                  crossAxisSpacing: 8.0,
                  childAspectRatio: 2,
                  children: colture.montantParModeRegl
                      .map(
                        (item) => SizedBox(
                          child: Container(
                              alignment: Alignment.center,
                              color: Color(0xffE7D7EE),
                              padding: EdgeInsets.all(0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(item.fnum == 1
                                      ? item.nombreReg.toString() +
                                          " " +
                                          item.designation
                                      : item.designation),
                                  SizedBox(
                                    height: 16,
                                  ),
                                  Text(item.totale.toStringAsFixed(int.parse(
                                          dataDto.pointVente.chiffrevirgule !=
                                                  null
                                              ? dataDto
                                                  .pointVente.chiffrevirgule
                                              : "3")) +
                                      " DT")
                                ],
                              )),
                        ),
                      )
                      .toList(),
                ))
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.purpleAccent,
              child: Text(
                'Quiter',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showClotureJourneeDialog(
      ClotureJourneeRes colture, AutreSessionRes autreSessionRes) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          backgroundColor: Color(0xffEEEEEE),
          contentPadding: EdgeInsets.all(0),
          scrollable: true,
          content: Container(
            height: height * 0.6,
            width: width * 0.6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: colture != null ? Colors.green : Colors.red,
                  child: Text(
                    dataDto.utilisateur.prenom + " " + dataDto.utilisateur.nom,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  width: width * 0.6,
                  padding: EdgeInsets.all(24),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: colture != null
                      ? Text(
                          "C.A globale: " +
                              colture.chiffreAffaireGlobaleJournee
                                  .toStringAsFixed(int.parse(
                                      dataDto.pointVente.chiffrevirgule)) +
                              " DT",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      : Text(
                          "Impossible de clôturer la journèe car les sessions suivantes sont encore actives: ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                  width: width * 0.6,
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                colture != null
                    ? Container(
                        child: Text(
                          "Montant par moyen de paiement",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        width: width * 0.6,
                        margin: EdgeInsets.only(bottom: 8, left: 16),
                      )
                    : SizedBox(),
                colture != null
                    ? Expanded(
                        child: GridView.count(
                        padding: EdgeInsets.only(top: 8, left: 16),
                        crossAxisCount: width > height ? 4 : 2,
                        mainAxisSpacing: 8.0,
                        crossAxisSpacing: 8.0,
                        childAspectRatio: 2,
                        children: colture.montantParModeReglParJournee
                            .map(
                              (item) => SizedBox(
                                child: Container(
                                    alignment: Alignment.center,
                                    color: Color(0xffE7D7EE),
                                    padding: EdgeInsets.all(0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(item.fnum == 1
                                            ? item.nombreReg.toString() +
                                                " " +
                                                item.designation
                                            : item.designation),
                                        SizedBox(
                                          height: 16,
                                        ),
                                        Text(item.totale.toStringAsFixed(
                                                int.parse(dataDto.pointVente
                                                    .chiffrevirgule)) +
                                            " DT")
                                      ],
                                    )),
                              ),
                            )
                            .toList(),
                      ))
                    : Expanded(
                        child: GridView.count(
                        padding: EdgeInsets.only(top: 8, left: 16),
                        crossAxisCount: width > height ? 4 : 2,
                        mainAxisSpacing: 8.0,
                        crossAxisSpacing: 8.0,
                        childAspectRatio: 2,
                        children: autreSessionRes.autreSession
                            .map(
                              (item) => SizedBox(
                                child: Container(
                                    alignment: Alignment.center,
                                    color: Color(0xffE7D7EE),
                                    padding: EdgeInsets.all(0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(item.utilisateur.prenom +
                                            " " +
                                            item.utilisateur.nom),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text("Ouverture"),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text(DateFormat("dd-MM-yyyy HH:mm")
                                            .format(item.dateDebut))
                                      ],
                                    )),
                              ),
                            )
                            .toList(),
                      ))
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.purpleAccent,
              child: Text(
                'Quiter',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Utils().printJourneeCloture(context, colture, dataDto);
                Navigator.of(context).pop();
                if (colture != null) {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }

  clotureJournee() async {
    try {
      setState(() {
        _loading = true;
      });
      ClotureJourneeDto clotureJourneeDto = ClotureJourneeDto(
          idUser: dataDto.utilisateur.idUtilisateur,
          idJournee: dataDto.journee.idJournee,
          dateCloture: DateTime.now());
      ResponseSingle responseSingle =
          await journeeEndPointApi.clotureJourneeUsingPOST(clotureJourneeDto);
      if (responseSingle.result == 1) {
        setState(() {
          _loading = false;
        });
        ClotureJourneeRes clotureJourneeRes =
            ClotureJourneeRes.fromJson(responseSingle.objectResponse);
        showClotureJourneeDialog(clotureJourneeRes, null);
      } else if (responseSingle.result == -1) {
        setState(() {
          _loading = false;
        });
        AutreSessionRes autreSessionRes =
            AutreSessionRes.fromJson(responseSingle.objectResponse);
        showClotureJourneeDialog(null, autreSessionRes);
      } else {
        setState(() {
          _loading = false;
        });
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics().sendEmail("Setting clotureJournee", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void showConfirmDialog() {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text('Etes-vous sûr de vouloir Clôturer la journée ?'),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Non', style: TextStyle(color: Colors.white)),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xffCF0606),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            _loadingDoc
                ? Container(
                    margin: EdgeInsets.all(16),
                    child: CircularProgressIndicator(),
                    alignment: Alignment.center,
                  )
                : RaisedButton(
                    child: Text(
                      'Oui',
                      style: TextStyle(color: Colors.white),
                    ),
                    elevation: 0,
                    padding: EdgeInsets.all(16),
                    color: Color(0xFF32C533),
                    onPressed: () {
                      Navigator.of(context).pop();
                      clotureJournee();
                    },
                  ),
          ],
        );
      },
    );
  }

  generateDoc() async {
    setState(() {
      _loadingDoc = true;
      _document = pw.Document();
    });
    final pdf = pw.Document();
    try {
      var dataBold = await rootBundle.load("assets/Montserrat-Bold.ttf");
      var bold = pw.Font.ttf(dataBold);
      var dataMedium = await rootBundle.load("assets/Montserrat-Medium.ttf");
      var medium = pw.Font.ttf(dataMedium);
      var dataRegular = await rootBundle.load("assets/Montserrat-Regular.ttf");
      var regular = pw.Font.ttf(dataRegular);
      pdf.addPage(pw.Page(
          margin: pw.EdgeInsets.all(margin != null ? margin.ceilToDouble() : 0),
          pageFormat: formatPapier.contains('80')
              ? PdfPageFormat.roll80
              : PdfPageFormat.roll57,
          build: (pw.Context context) {
            return pw.Column(children: [
              pw.Center(
                child: pw.Column(children: [
                  pw.Text(dataDto.pointVente.designation,
                      style: pw.TextStyle(font: bold, fontSize: fontSize)),
                  pw.Text(dataDto.pointVente.adresse,
                      style: pw.TextStyle(font: bold, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text('N° 100',
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text(DateFormat("dd-MM-yyyy HH:mm").format(DateTime.now()),
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                ]),
              ),
              pw.SizedBox(height: 12),
              pw.Row(children: [
                pw.Text("5 Produits",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.Expanded(
                    child: pw.Container(
                  alignment: pw.Alignment.centerRight,
                  child: pw.Text("5.000",
                      style: pw.TextStyle(font: medium, fontSize: fontSize)),
                ))
              ]),
              pw.SizedBox(height: 8),
              pw.Row(children: [
                pw.Text("",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.Expanded(
                    child: pw.Container(
                  padding: pw.EdgeInsets.only(top: 4, bottom: 4),
                  alignment: pw.Alignment.centerRight,
                  child: pw.Text("TOTAL 5.000 DT",
                      style: pw.TextStyle(font: medium, fontSize: fontSize)),
                ))
              ]),
              pw.SizedBox(height: 12),
              pw.Center(
                  child: pw.Column(children: [
                pw.Text("********************",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.Text("Merci de votre Visite",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.Text("À bientot",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.SizedBox(height: 4),
                pw.Text("********************",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
              ])),
            ]); // Center
          }));
    } catch (err) {
      debugPrint('$err');
    }

    setState(() {
      _document = pdf;
      _loadingDoc = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 8,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Paramètres",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width > height ? width / 50 : width / 30),
              ),
              Expanded(
                child: Container(),
              ),
              SizedBox(
                height: width > height
                    ? SizeClacule().calcule(context, 15)
                    : SizeClacule().calcule(context, 7.5),
                width: width > height
                    ? SizeClacule().calcule(context, 15)
                    : SizeClacule().calcule(context, 7.5),
                child: FlatButton(
                    color: Color(0xff32C533),
                    onPressed: () => {
                          dataDto.setCurrentPage(
                              CommandeScreen(dataDto), "CommandeScreen")
                        },
                    padding: EdgeInsets.all(0),
                    child: Image.asset(
                      'assets/back/back.png',
                      color: Colors.white,
                      fit: BoxFit.contain,
                      height: width > height ? width / 40 : width / 20,
                      width: width > height ? width / 40 : width / 20,
                    )),
              )
            ],
          ),
          SizedBox(
            height: 8,
          ),
          _loading
              ? Container(
                  margin: EdgeInsets.all(16),
                  child: CircularProgressIndicator(),
                  alignment: Alignment.center,
                )
              : Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: width > height
                          ? SizeClacule().calcule(context, 4)
                          : SizeClacule().calcule(context, 1),
                      height: width > height
                          ? SizeClacule().calcule(context, 15)
                          : SizeClacule().calcule(context, 7.5),
                      child: FlatButton(
                          padding: EdgeInsets.all(0),
                          color: Color(0xff8066FF),
                          onPressed: () {
                            preCloture();
                          },
                          child: Text(
                            "Clôture de votre session",
                            style: TextStyle(color: Colors.white),
                          )),
                    ),
                    dataDto.utilisateurType.fGerant == 1
                        ? SizedBox(
                            width: width > height
                                ? SizeClacule().calcule(context, 4)
                                : SizeClacule().calcule(context, 1),
                            height: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            child: FlatButton(
                                padding: EdgeInsets.all(0),
                                color: Colors.deepPurpleAccent,
                                onPressed: () {
                                  showConfirmDialog();
                                },
                                child: Text(
                                  "Clôture de la journée",
                                  style: TextStyle(color: Colors.white),
                                )),
                          )
                        : SizedBox(),
                    SizedBox(
                      width: width > height
                          ? SizeClacule().calcule(context, 4)
                          : SizeClacule().calcule(context, 1),
                      height: width > height
                          ? SizeClacule().calcule(context, 15)
                          : SizeClacule().calcule(context, 7.5),
                      child: FlatButton(
                          padding: EdgeInsets.all(0),
                          color: Colors.deepPurpleAccent,
                          onPressed: () {
                            //Utils().openDrawer(context);
                          },
                          child: Text(
                            "Ouverture tiroir caisse",
                            style: TextStyle(color: Colors.white),
                          )),
                    ),
                  ],
                ),
          SizedBox(
            height: !kIsWeb ? 30 : 0,
          ),
          Container(
            child: TabBar(
              controller: tabController,
              unselectedLabelColor: Colors.grey,
              labelColor: Color(0xff9d197f),
              //  labelStyle: TextStyle(fontSize: ),
              tabs: [
                Tab(text: 'Imprimante'),
                Tab(text: 'Administration'),
                Tab(text: 'Ticket')
              ],
            ),
          ),
          SizedBox(
            height: !kIsWeb ? 30 : 0,
          ),
          Container(
            height: height,
            child: TabBarView(
              controller: tabController,
              children: [
                Column(
                  children: [
                    !kIsWeb
                        ? Row(
                            children: [
                              Container(
                                child: Text("Imprimante Principale"),
                                width: width / 6,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              DropdownButton<String>(
                                value: dropdownValue,
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 0,
                                style: TextStyle(color: Colors.black),
                                underline: Container(
                                  height: 2,
                                  color: Colors.deepPurpleAccent,
                                ),
                                onChanged: (String newValue) {
                                  saveSharedPreferences(
                                      "principalPrinter", newValue);
                                  setState(() {
                                    dropdownValue = newValue;
                                  });
                                },
                                items: printers.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              SizedBox(
                                height: 80,
                                width: 250,
                                child: MultiSelect(
                                    //--------customization selection modal-----------
                                    responsiveDialogSize: Size.square(500),
                                    buttonBarColor: Colors.red,
                                    cancelButtonText: "Quitter",
                                    clearButtonText: "Effacer",
                                    saveButtonText: "Enregistrer",
                                    titleText: "Familles",
                                    hintText: "cliquez ici pour choisir ..",
                                    searchBoxHintText: "Recherche...",
                                    checkBoxColor: Colors.green,
                                    selectedOptionsInfoText:
                                        "Sélectionnez une ou plusieurs familles",
                                    selectedOptionsBoxColor: Colors.green,
                                    //autovalidate: true,
                                    maxLength: 10, // optional
                                    //--------end customization selection modal------------
                                    validator: (dynamic value) {
                                      if (value == null) {
                                        return 'Please select one or more option(s)';
                                      }
                                      return null;
                                    },
                                    errorText:
                                        'Please select one or more option(s)',
                                    dataSource: familles
                                        .map((e) => e.toJson())
                                        .toList(),
                                    textField: 'designation',
                                    valueField: 'designation',
                                    filterable: true,
                                    required: true,
                                    initialValue: initFamille1,
                                    onSaved: (value) {
                                      print('The saved values are $value');
                                      resFamille = value.toString();
                                      saveSharedPreferences(
                                          "familleprincipalPrinter",
                                          resFamille.substring(
                                              1, resFamille.length - 1));
                                      ServerUrl.refreshFamille();
                                    }),
                              ),
                            ],
                          )
                        : SizedBox(),
                    SizedBox(
                      height: 20,
                    ),
                    dataDto.pointVente.fImprimCuisine == 1 && !kIsWeb
                        ? Row(
                            children: [
                              Container(
                                child: Text("Imprimante Cuisine"),
                                width: width / 6,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              DropdownButton<String>(
                                value: dropdownValueCuisine,
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 0,
                                style: TextStyle(color: Colors.black),
                                underline: Container(
                                  height: 2,
                                  color: Colors.deepPurpleAccent,
                                ),
                                onChanged: (String newValue) {
                                  saveSharedPreferences(
                                      "principalCuisine", newValue);
                                  setState(() {
                                    dropdownValueCuisine = newValue;
                                  });
                                },
                                items: printers.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              SizedBox(
                                height: 80,
                                width: 250,
                                child: MultiSelect(
                                    //--------customization selection modal-----------
                                    responsiveDialogSize: Size.square(500),
                                    buttonBarColor: Colors.red,
                                    cancelButtonText: "Quitter",
                                    clearButtonText: "Effacer",
                                    saveButtonText: "Enregistrer",
                                    titleText: "Familles",
                                    hintText: "cliquez ici pour choisir ..",
                                    searchBoxHintText: "Recherche...",
                                    checkBoxColor: Colors.green,
                                    selectedOptionsInfoText:
                                        "Sélectionnez une ou plusieurs familles",
                                    selectedOptionsBoxColor: Colors.green,
                                    //autovalidate: true,
                                    maxLength: 10, // optional
                                    //--------end customization selection modal------------
                                    validator: (dynamic value) {
                                      if (value == null) {
                                        return 'Please select one or more option(s)';
                                      }
                                      return null;
                                    },
                                    errorText:
                                        'Please select one or more option(s)',
                                    dataSource: familles
                                        .map((e) => e.toJson())
                                        .toList(),
                                    textField: 'designation',
                                    valueField: 'designation',
                                    filterable: true,
                                    required: true,
                                    initialValue: initFamilleCuisine,
                                    onSaved: (value) {
                                      print('The saved values are $value');
                                      resFamilleCuisine = value.toString();
                                      saveSharedPreferences(
                                          "famillecuisinePrinter",
                                          resFamilleCuisine.substring(
                                              1, resFamilleCuisine.length - 1));
                                      ServerUrl.refreshFamille();
                                    }),
                              ),
                            ],
                          )
                        : Container(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text("Imprimante Aux 1"),
                          width: width / 6,
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        DropdownButton<String>(
                          value: dropdownValuePrintAux1,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 0,
                          style: TextStyle(color: Colors.black),
                          underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                          ),
                          onChanged: (String newValue) {
                            saveSharedPreferences("printerAux1", newValue);
                            setState(() {
                              dropdownValuePrintAux1 = newValue;
                            });
                          },
                          items: printers
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        SizedBox(
                          height: 80,
                          width: 250,
                          child: MultiSelect(
                              //--------customization selection modal-----------
                              responsiveDialogSize: Size.square(500),
                              buttonBarColor: Colors.red,
                              cancelButtonText: "Quitter",
                              clearButtonText: "Effacer",
                              saveButtonText: "Enregistrer",
                              titleText: "Familles",
                              hintText: "cliquez ici pour choisir ..",
                              searchBoxHintText: "Recherche...",
                              checkBoxColor: Colors.green,
                              selectedOptionsInfoText:
                                  "Sélectionnez une ou plusieurs familles",
                              selectedOptionsBoxColor: Colors.green,
                              //autovalidate: true,
                              maxLength: 10, // optional
                              //--------end customization selection modal------------
                              validator: (dynamic value) {
                                if (value == null) {
                                  return 'Please select one or more option(s)';
                                }
                                return null;
                              },
                              errorText: 'Please select one or more option(s)',
                              dataSource:
                                  familles.map((e) => e.toJson()).toList(),
                              textField: 'designation',
                              valueField: 'designation',
                              filterable: true,
                              required: true,
                              initialValue: initFamillePrintAux1,
                              onSaved: (value) {
                                print('The saved values are $value');
                                resFamillePrinterAux1 = value.toString();
                                saveSharedPreferences(
                                    "famillePrinterAux1",
                                    resFamillePrinterAux1.substring(
                                        1, resFamillePrinterAux1.length - 1));
                                ServerUrl.refreshFamille();
                              }),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text("Imprimante Aux 2"),
                          width: width / 6,
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        DropdownButton<String>(
                          value: dropdownValuePrintAux2,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 0,
                          style: TextStyle(color: Colors.black),
                          underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                          ),
                          onChanged: (String newValue) {
                            saveSharedPreferences("printerAux2", newValue);
                            setState(() {
                              dropdownValuePrintAux2 = newValue;
                            });
                          },
                          items: printers
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        SizedBox(
                          height: 80,
                          width: 250,
                          child: MultiSelect(
                              //--------customization selection modal-----------
                              responsiveDialogSize: Size.square(500),
                              buttonBarColor: Colors.red,
                              cancelButtonText: "Quitter",
                              clearButtonText: "Effacer",
                              saveButtonText: "Enregistrer",
                              titleText: "Familles",
                              hintText: "cliquez ici pour choisir ..",
                              searchBoxHintText: "Recherche...",
                              checkBoxColor: Colors.green,
                              selectedOptionsInfoText:
                                  "Sélectionnez une ou plusieurs familles",
                              selectedOptionsBoxColor: Colors.green,
                              //autovalidate: true,
                              maxLength: 10, // optional
                              //--------end customization selection modal------------
                              validator: (dynamic value) {
                                if (value == null) {
                                  return 'Please select one or more option(s)';
                                }
                                return null;
                              },
                              errorText: 'Please select one or more option(s)',
                              dataSource:
                                  familles.map((e) => e.toJson()).toList(),
                              textField: 'designation',
                              valueField: 'designation',
                              filterable: true,
                              required: true,
                              initialValue: initFamillePrintAux2,
                              onSaved: (value) {
                                print('The saved values are $value');
                                resFamillePrinterAux2 = value.toString();
                                saveSharedPreferences(
                                    "famillePrinterAux2",
                                    resFamillePrinterAux2.substring(
                                        1, resFamillePrinterAux2.length - 1));
                                ServerUrl.refreshFamille();
                              }),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: !kIsWeb ? 16 : 0,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          child: Text("Vente en ligne"),
                          width: width / 7,
                        ),
                        SizedBox(
                          width: 0,
                        ),
                        Switch(
                          inactiveThumbColor: Colors.green,
                          inactiveTrackColor: Colors.green,
                          value: isActiveVente,
                          onChanged: (value) async {
                            setIsActiveVente(value);
                            setState(() {
                              isActiveVente = value;
                            });
                            if (isActiveVente) {
                              var response = await http
                                  .get(Uri.parse(ServerUrl.url +
                                  "/bp-api-pos/v1/changeStatusPv/" +
                                  dataDto.pointVente.idPointVente +
                                  "/" +
                                  "1"))
                                  .timeout(
                                Duration(seconds: 3),
                                onTimeout: () {
                                  return null;
                                },
                              );
                              print(response.body);
                            } else {
                              var response1 = await http
                                  .get(Uri.parse(ServerUrl.url +
                                  "/bp-api-pos/v1/changeStatusPv/" +
                                  dataDto.pointVente.idPointVente +
                                  "/" +
                                  "0"))
                                  .timeout(
                                Duration(seconds: 3),
                                onTimeout: () {
                                  return null;
                                },
                              );
                              print(response1.body);
                            }
                          },
                          activeColor: Colors.red,
                        )
                      ],
                    ),
                    !kIsWeb
                        ? Row(
                            children: [
                              Container(
                                child: Text("Afficher les Alertes"),
                                width: width / 7,
                              ),
                              SizedBox(
                                width: 0,
                              ),
                              Switch(
                                value: isSwitched,
                                onChanged: (value) {
                                  setNotif(value);
                                  setState(() {
                                    isSwitched = value;
                                  });
                                },
                                activeColor: Colors.green,
                              )
                            ],
                          )
                        : SizedBox(),
                    dataDto.caisseType.code == 'CENTRALE' &&
                            (dataDto.utilisateurType.code == 'PROP' ||
                                dataDto.utilisateurType.code == 'GERANT')
                        ? Row(
                            children: [
                              Container(
                                child: Text("Validation d'Annulation"),
                                width: width / 7,
                              ),
                              SizedBox(
                                width: 0,
                              ),
                              Switch(
                                value: isSwitchedActionAnnule,
                                onChanged: (value) {
                                  setActionAnnule(value);
                                  setState(() {
                                    isSwitchedActionAnnule = value;
                                  });
                                },
                                activeColor: Colors.green,
                              ),
                              SizedBox(width: 10),
                              isSwitchedActionAnnule
                                  ? Container(
                                      width: 400,
                                      child: TextField(
                                          cursorColor: Color(0xff352C60),
                                          controller: codeActionAnnulee,
                                          onSubmitted: (value) {
                                            // or do whatever you want when you are done editing
                                            // call your method/print values etc
                                            //saveSharedPreferences("codeActionAnnulee",value);
                                          },
                                          decoration: InputDecoration(
                                            labelText: "Code Action :",
                                            filled: true,
                                            fillColor: Colors.white,
                                            contentPadding: EdgeInsets.fromLTRB(
                                                20.0, 15.0, 20.0, 15.0),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0)),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                              borderRadius:
                                                  BorderRadius.circular(0),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                              borderRadius:
                                                  BorderRadius.circular(0),
                                            ),
                                          )),
                                    )
                                  : Text(''),
                            ],
                          )
                        : SizedBox(),
                    SizedBox(
                      height: 10,
                    ),
                    dataDto.caisseType.code == 'CENTRALE' &&
                            (dataDto.utilisateurType.code == 'PROP' ||
                                dataDto.utilisateurType.code == 'GERANT')
                        ? Row(
                            children: [
                              Container(
                                child: Text("Validation du Remise"),
                                width: width / 7,
                              ),
                              SizedBox(
                                width: 0,
                              ),
                              Switch(
                                value: isSwitchedActionRemize,
                                onChanged: (value) {
                                  setActionRemize(value);
                                  setState(() {
                                    isSwitchedActionRemize = value;
                                  });
                                },
                                activeColor: Colors.green,
                              ),
                              SizedBox(width: 10),
                              isSwitchedActionRemize
                                  ? Container(
                                      width: 400,
                                      child: TextField(
                                          cursorColor: Color(0xff352C60),
                                          controller: codeActionRemize,
                                          onSubmitted: (value) {
                                            // or do whatever you want when you are done editing
                                            // call your method/print values etc
                                            //saveSharedPreferences("codeActionRemize",value);
                                          },
                                          decoration: InputDecoration(
                                            labelText: "Code Action :",
                                            filled: true,
                                            fillColor: Colors.white,
                                            contentPadding: EdgeInsets.fromLTRB(
                                                20.0, 15.0, 20.0, 15.0),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0)),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                              borderRadius:
                                                  BorderRadius.circular(0),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                              borderRadius:
                                                  BorderRadius.circular(0),
                                            ),
                                          )),
                                    )
                                  : Text(''),
                            ],
                          )
                        : SizedBox(),
                  ],
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Container(
                                  child: Text("Retour automatique"),
                                  width: width / 7,
                                ),
                                SizedBox(
                                  width: 0,
                                ),
                                Switch(
                                  value: isSwitchedRetour,
                                  onChanged: (value) {
                                    setRetour(value);
                                    setState(() {
                                      isSwitchedRetour = value;
                                      dataDto.isSwitchedRetour = value;
                                    });
                                  },
                                  activeColor: Colors.green,
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  child: Text("Désactiver Impression ticket"),
                                  width: width / 7,
                                ),
                                SizedBox(
                                  width: 0,
                                ),
                                Switch(
                                  value: isDesImpTicket,
                                  onChanged: (value) {
                                    setDesImpTicket(value);
                                    setState(() {
                                      isDesImpTicket = value;
                                    });
                                  },
                                  activeColor: Colors.green,
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  child: Text("Réinitialisation Numéro ticket"),
                                  width: width / 7,
                                ),
                                SizedBox(
                                  width: 0,
                                ),
                                Switch(
                                  value: isNumTicketFromLocal,
                                  onChanged: (value) {
                                    setNumTicketFromLocal(value);
                                    setState(() {
                                      isNumTicketFromLocal = value;
                                    });
                                  },
                                  activeColor: Colors.green,
                                )
                              ],
                            ),
                            dataDto.pointVente
                                .fAffectEmployetoservice ==
                                2? SizedBox():Row(
                              children: [
                                Container(
                                  child:
                                      Text("Impression tickets d'annulation "),
                                  width: width / 7,
                                ),
                                SizedBox(
                                  width: 0,
                                ),
                                Switch(
                                  value: isSwitchedImpTicketAnnulation,
                                  onChanged: (value) {
                                    setImpAnnulation(value);
                                    setState(() {
                                      isSwitchedImpTicketAnnulation = value;
                                    });
                                  },
                                  activeColor: Colors.green,
                                )
                              ],
                            ),
                            dataDto.pointVente
                                .fAffectEmployetoservice ==
                                2? SizedBox(): Row(
                              children: [
                                Container(
                                  child: Text("Impression ingredients"),
                                  width: width / 7,
                                ),
                                SizedBox(
                                  width: 0,
                                ),
                                Switch(
                                  value: isImpIng,
                                  onChanged: (value) {
                                    setImpIng(value);
                                    setState(() {
                                      isImpIng = value;
                                    });
                                  },
                                  activeColor: Colors.green,
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  child: Text("Format de papier"),
                                  width: width / 7,
                                ),
                                SizedBox(
                                  width: 16,
                                ),
                                DropdownButton<String>(
                                  value: formatPapier,
                                  icon: Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 0,
                                  style: TextStyle(color: Colors.black),
                                  underline: Container(
                                    height: 2,
                                    color: Colors.deepPurpleAccent,
                                  ),
                                  onChanged: (String newValue) {
                                    saveSharedPreferences(
                                        "formatPapier", newValue);
                                    generateDoc();
                                    setState(() {
                                      formatPapier = newValue;
                                    });
                                  },
                                  items: formatPapiers
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  child: Text("Taille de police: " +
                                      fontSize.ceil().toString()),
                                  width: width / 7.3,
                                ),
                                Slider(
                                    divisions: 35,
                                    min: 5.0,
                                    max: 40.0,
                                    value: fontSize,
                                    activeColor: Colors.deepPurpleAccent,
                                    onChanged: onChanged)
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  child: Text("Marge: " + margin.toString()),
                                  width: width / 7.3,
                                ),
                                Slider(
                                    divisions: 35,
                                    min: 0.0,
                                    max: 40.0,
                                    value: margin.ceilToDouble(),
                                    activeColor: Colors.deepPurpleAccent,
                                    onChanged: onChangedMargin)
                              ],
                            ),
                            Container(
                              width: 400,
                              child: TextField(
                                  cursorColor: Color(0xff352C60),
                                  controller: codeWifi,
                                  onSubmitted: (value) {
                                    saveSharedPreferences("codeW", value);
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Code Wifi Ticket:",
                                    filled: true,
                                    fillColor: Colors.white,
                                    contentPadding: EdgeInsets.fromLTRB(
                                        20.0, 15.0, 20.0, 15.0),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(0)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                  )),
                            ),
                          ],
                        ),
                        _loadingDoc
                            ? Container(
                                margin: EdgeInsets.all(16),
                                child: CircularProgressIndicator(),
                                alignment: Alignment.center,
                              )
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 16,
                                  ),
                                  Text(
                                    "Aperçu du ticket",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  IconButton(
                                    icon: const Icon(
                                      Icons.print,
                                    ),
                                    onPressed: () async {
                                      PrintingTicket().printTicket(_document,
                                          dropdownValue, context, true);
                                    },
                                  ),
                                  Container(
                                      alignment: Alignment.center,
                                      margin:
                                          EdgeInsets.only(bottom: 16, left: 16),
                                      width: _document.document
                                          .page(0)
                                          .pageFormat
                                          .width,
                                      height: _document.document
                                          .page(0)
                                          .pageFormat
                                          .width,
                                      child: PdfPreview(
                                        pdfPreviewPageDecoration:
                                            const BoxDecoration(
                                          color: Colors.white,
                                        ),
                                        scrollViewDecoration:
                                            const BoxDecoration(
                                          color: Colors.white,
                                        ),
                                        build: (format) => _document.save(),
                                        useActions: false,
                                      )),
                                ],
                              )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
