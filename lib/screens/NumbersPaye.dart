import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/dto/PayementNumbers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NumbersPaye extends StatelessWidget {
  DataDto d;
  Function callback;
  String number;

  NumbersPaye(this.d, this.callback, this.number);

  var numbers = [
    PayementNumbers(1, "1"),
    PayementNumbers(2, "2"),
    PayementNumbers(3, "3"),
    PayementNumbers(10, "+10"),
    PayementNumbers(4, "4"),
    PayementNumbers(5, "5"),
    PayementNumbers(6, "6"),
    PayementNumbers(20, "+20"),
    PayementNumbers(7, "7"),
    PayementNumbers(8, "8"),
    PayementNumbers(9, "9"),
    PayementNumbers(50, "+50"),
    PayementNumbers(-1, "+"),
    PayementNumbers(0, "0"),
    PayementNumbers(-2, "."),
    PayementNumbers(-3, "C"),
  ];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        SizedBox(
          height: 8,
        ),
        Text(
          number,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: width / 70),
        ),
        SizedBox(
          height: 8,
        ),
        Expanded(
          child: GridView.count(
            crossAxisCount: 4,
            mainAxisSpacing: 4.0,
            crossAxisSpacing: 4.0,
            padding: EdgeInsets.all(0),
            children: numbers
                .map(
                  (item) => Container(
                    child: SizedBox(
                      child: FlatButton(
                          color: item == "C"
                              ? Color(0xffCF0606)
                              : Color(0xffE7D7EE),
                          padding: EdgeInsets.all(0),
                          onPressed: () => {callback(item, context)},
                          child: Text(
                            item.designation,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize:
                                    width > height ? width / 80 : width / 20,
                                color: item == "C"
                                    ? Colors.white
                                    : Color(0xff352C60)),
                          )),
                    ),
                  ),
                )
                .toList(),
          ),
        )
      ],
    );
  }
}
