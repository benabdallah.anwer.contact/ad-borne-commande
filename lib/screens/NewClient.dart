import 'package:ad_caisse/customer/api/customer_end_point_api.dart';
import 'package:ad_caisse/customer/model/client_dto.dart';
import 'package:ad_caisse/customer/model/client_partner_dto.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/otp/api/otp_controller_api.dart';
import 'package:ad_caisse/otp/model/etat.dart';
import 'package:ad_caisse/otp/model/one_time_pwd.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/screens/Clients.dart';
import 'package:ad_caisse/screens/DetailClient.dart';
import 'package:ad_caisse/screens/NumbersLogin.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewClient extends StatefulWidget {
  final DataDto dataDto;

  NewClient(this.dataDto);

  @override
  _MyHomePageState createState() => _MyHomePageState(this.dataDto);
}

class _MyHomePageState extends State<NewClient> {
  DataDto dataDto;
  String dropdownValue = 'Sexe';
  TextEditingController nom = new TextEditingController();
  TextEditingController prenom = new TextEditingController();
  TextEditingController tel = new TextEditingController();
  TextEditingController mail = new TextEditingController();
  TextEditingController date = new TextEditingController();
  TextEditingController carte = new TextEditingController();
  TextEditingController code = new TextEditingController();
  TextEditingController adress = new TextEditingController();
  var otpControllerApi = OtpControllerApi();
  DateTime finaldate;
  bool loading = false;

  _MyHomePageState(this.dataDto);

  var customerEndPointApi = CustomerEndPointApi();

  @override
  void initState() {
    dataDto.searchClient("Rechercher");
    dataDto.textController.addListener(txtListener);
    super.initState();
  }

  txtListener() {
    switch (dataDto.focused) {
      case 1:
        {
          setState(() {
            nom.text = dataDto.textController.text;
            nom.selection = TextSelection.fromPosition(
                TextPosition(offset: nom.text.length));
          });
        }
        break;
      case 2:
        {
          setState(() {
            prenom.text = dataDto.textController.text;
            prenom.selection = TextSelection.fromPosition(
                TextPosition(offset: prenom.text.length));
          });
        }
        break;

      case 3:
        {
          setState(() {
            tel.text = dataDto.textController.text;
            tel.selection = TextSelection.fromPosition(
                TextPosition(offset: tel.text.length));
          });
        }
        break;
      case 4:
        {
          setState(() {
            mail.text = dataDto.textController.text;
            mail.selection = TextSelection.fromPosition(
                TextPosition(offset: mail.text.length));
          });
        }
        break;
      case 5:
        {
          setState(() {
            carte.text = dataDto.textController.text;
            carte.selection = TextSelection.fromPosition(
                TextPosition(offset: carte.text.length));
          });
        }
        break;
      case 6:
        {
          setState(() {
            adress.text = dataDto.textController.text;
            adress.selection = TextSelection.fromPosition(
                TextPosition(offset: adress.text.length));
          });
        }
        break;
    }

  }

  void callDatePicker() async {
    DateTime order = await getDate();
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    if (order != null) {
      date.text = dateFormat.format(order);
      setState(() {
        finaldate = order;
      });
    }
  }

  Future<DateTime> getDate() {
    // Imagine that this function is
    // more complex and slow.
    return showDatePicker(
      context: context,
      helpText: "Date de naissance",
      initialDate: finaldate != null ? finaldate : DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: Color(0xff352C60),
          ),
          child: child,
        );
      },
    );
  }

  void callbackNumbers(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        code.text = "";
      });
      return;
    }
    setState(() {
      code.text += number;
      code.selection =
          TextSelection.fromPosition(TextPosition(offset: code.text.length));
    });
  }

  void otpInput() {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: Container(
            child: Column(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text('Prière de saisir le code reçu par SMS'),
                SizedBox(height: 16.0),
                TextField(
                    cursorColor: Color(0xff352C60),
                    controller: code,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xfff5f5f5),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Code:",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    )),
                SizedBox(height: 16.0),
                Container(
                  alignment: Alignment.center,
                  color: Colors.transparent,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.width * 0.215,
                  child: NumbersLogin(callbackNumbers),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              padding: EdgeInsets.all(16),
              elevation: 0,
              color: Color(0xffCF0606),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'OK',
                style: TextStyle(color: Colors.white),
              ),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xFF32C533),
              onPressed: () {
                code.text.isNotEmpty ? checkOtp(context) : null;
              },
            ),
          ],
        );
      },
    );
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    int d;
    try {
      d = int.parse(s);
      return d is int;
    } on Exception catch (_) {
      print("throwing new error");
      return false;
    }
  }

  Future<void> sendOtp() async {
    if (nom.text.isEmpty ||
        prenom.text.isEmpty ||
        tel.text.isEmpty ||
        mail.text.isEmpty ||
        date.text.isEmpty ||
        dropdownValue == 'Sexe') {
      AlertNotif().alert(context, "Veuillez remplir tous les champs.");
      return;
    }
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(mail.text);
    if (!emailValid) {
      AlertNotif().alert(context, "Email non valide.");
      return;
    }

    if (tel.text.length < 8 || !isNumeric(tel.text)) {
      AlertNotif().alert(context, "Numéro de téléphone non valide.");
      return;
    }

    setState(() {
      loading = true;
      code.text = "";
    });
    try {
      OneTimePwd oneTimePwd =
          await otpControllerApi.getCodeUsingGET(dataDto.pointVente.idPartenaire , tel.text);
      print("OneTimePwd: " + oneTimePwd.etat.toString());
      if (oneTimePwd.etat == 1) {
        setState(() {
          loading = false;
        });
      }
      setState(() {
        loading = false;
      });
      otpInput();
    } catch (err) {
      Crashlytics().sendEmail("NewClient sendOtp", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
    setState(() {
      loading = false;
    });
  }

  Future<void> checkOtp(BuildContext context) async {
    setState(() {
      loading = true;
    });
    try {
      Etat etat = await otpControllerApi.getEtatUsingGET(
          tel.text, dataDto.pointVente.idPartenaire.toString(), code.text);
      print("OneTimePwdEtat: " + etat.code.toString());
      if (etat.code == 0) {
        setState(() {
          loading = false;
        });
      }
      Navigator.of(context).pop();
      createClient(etat.code);
    } catch (err) {
      Crashlytics().sendEmail("NewClient checkOtp", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> createClient(int codeResp) async {
    try {
      if (codeResp != 0) {
        AlertNotif()
            .alert(context, "Veuillez vérifier votre code et réessayer")
            .then((value) => otpInput());
        return;
      }
      ClientDto clientDto = ClientDto();
      clientDto.nom = nom.text;
      clientDto.prenom = prenom.text;
      clientDto.nTel = tel.text;
      clientDto.email = mail.text;
      clientDto.genre = dropdownValue;
      clientDto.dateNaissance = finaldate;
      clientDto.dateCreation = DateTime.now();
      clientDto.adress = adress.text;
      ResponseSingle responseSingle =
          await customerEndPointApi.createClientBpriceUsingPOST(clientDto);
      print("createClientBprice: " + responseSingle.errorDescription);
      if (responseSingle.result != 1) {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
      if (responseSingle.result == 1) {
        ClientPartnerDto clientPartnerDto = ClientPartnerDto();
        clientPartnerDto.idClient =
            ClientBPrice.fromJson(responseSingle.objectResponse).idClient;
        clientPartnerDto.nom = nom.text;
        clientPartnerDto.prenom = prenom.text;
        clientPartnerDto.idPartenaire = dataDto.utilisateur.idPartenaire;
        clientPartnerDto.qrCode = carte.text;
        clientPartnerDto.idJournee = dataDto.journee.idJournee;
        clientPartnerDto.otp = code.text;
        clientPartnerDto.sourceCreation = "caisse";
        clientPartnerDto.adress=adress.text;
        ResponseSingle response = await customerEndPointApi
            .createClientPartnerUsingPOST(clientPartnerDto);
        print("createClientPartner: " + responseSingle.errorDescription);
        if (response.result != 1) {
          AlertNotif().alert(context, response.errorDescription);
        }
        if (response.result == 1) {
          dataDto.listClient = [];
          ResponseList responseList =
              await customerEndPointApi.findAllActiveClientByPartenaireUsingGET(
                  dataDto.utilisateur.idPartenaire);
          List<ClientBPrice> list = responseList.objectResponse
              .map((i) => ClientBPrice.fromJson(i))
              .toList();
          List<ClientBPrice> client =
              list.where((element) => element.nTel == tel.text).toList();
          setState(() {
            dataDto.listClient = list;
            dataDto.listClientFiltred = list;
          });
          if (client.length > 0) {
            dataDto.setCurrentPage(DetailClient(client[0], dataDto), "");
          } else {
            dataDto.setCurrentPage(Clients(dataDto), "");
          }
        }
      }
    } catch (err) {
      Crashlytics().sendEmail("NewClient createClient", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ListView(
      padding: EdgeInsets.all(0),
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () =>
                      {dataDto.setCurrentPage(Clients(dataDto), "Clients")},
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        TextField(
            cursorColor: Color(0xff352C60),
            onTap: () {
              setState(() {
                dataDto.focused = 1;
              });
              dataDto.textController.text="";
              dataDto.callbackKeyboard(false);
            },
            controller: nom,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: "Nom:",
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
            )),
        SizedBox(
          height: 8,
        ),
        TextField(
            cursorColor: Color(0xff352C60),
            controller: prenom,
            onTap: () {
              setState(() {
                dataDto.focused = 2;
              });
              dataDto.textController.text="";
              dataDto.callbackKeyboard(false);
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: "Prénom:",
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
            )),
        SizedBox(
          height: 8,
        ),
        TextField(
            cursorColor: Color(0xff352C60),
            keyboardType: TextInputType.numberWithOptions(),
            controller: tel,
            onTap: () {
              setState(() {
                dataDto.focused = 3;
              });
              dataDto.textController.text="";
              dataDto.callbackKeyboard(true);
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: "Tél:",
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
            )),
        SizedBox(
          height: 8,
        ),
        TextField(
            keyboardType: TextInputType.emailAddress,
            cursorColor: Color(0xff352C60),
            controller: mail,
            onTap: () {
              setState(() {
                dataDto.focused = 4;
              });
              dataDto.textController.text="";
              dataDto.callbackKeyboard(false);
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: "Email:",
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
            )),
        SizedBox(
          height: 8,
        ),
        TextField(
            cursorColor: Color(0xff352C60),
            controller: adress,
            onTap: () {
              setState(() {
                dataDto.focused = 6;
              });
              dataDto.textController.text="";
              dataDto.callbackKeyboard(false);
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: "Adresse de livraison:",
              border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
            )),
        SizedBox(
          height: 8,
        ),
        FlatButton(
          color: Colors.white,
          onPressed: callDatePicker,
          padding: EdgeInsets.all(0),
          child: TextField(
              cursorColor: Color(0xff352C60),
              controller: date,
              enabled: false,
              decoration: InputDecoration(
                disabledBorder: InputBorder.none,
                filled: true,
                fillColor: Colors.white,
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                hintText: "Date de naissance:",
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(0),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(0),
                ),
              )),
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          children: [
            Expanded(
                child: Container(
              color: Colors.white,
              margin: EdgeInsets.only(right: 8),
              padding: EdgeInsets.only(left: 8, right: 8),
              child: DropdownButton<String>(
                isExpanded: true,
                value: dropdownValue,
                icon: Icon(Icons.arrow_drop_down_circle),
                iconSize: 24,
                iconEnabledColor: Colors.purple,
                elevation: 0,
                style: TextStyle(color: Colors.black),
                underline: Container(
                  height: 0,
                ),
                onChanged: (String newValue) {
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: <String>['Sexe', 'Homme', 'Femme']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            )),
            Expanded(
                child: TextField(
                    cursorColor: Color(0xff352C60),
                    controller: carte,
                    onTap: () {
                      setState(() {
                        dataDto.focused = 5;
                      });
                      dataDto.callbackKeyboard(false);
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Afecter carte:",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(0),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    )))
          ],
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: width > height ? width / 20 : width / 10,
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () {
                    ServerUrl.url.contains("bp-api-local")
                        ? createClient(0)
                        : sendOtp();
                  },
                  padding: EdgeInsets.only(left: width / 25, right: width / 25),
                  child: Text(
                    "Enregistrer",
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            Container()
          ],
        )
      ],
    );
  }
}
