import 'package:flutter/material.dart';

class Numbers extends StatelessWidget {
  var numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".", "C"];
  Function callback;
  Numbers(this.callback);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return GridView.count(
          padding: EdgeInsets.only(left: 8,right: 8),
          crossAxisCount: 4,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          children: numbers
              .map(
                (item) => Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                        width: 1,
                        color: item == "C" ? Color(0xffCF0606) : Color(0xff707070)),
                  ),
                  child: SizedBox(child: FlatButton(
                      color: item == "C" ? Color(0xffCF0606) : Color(0xffE7D7EE),
                      onPressed: () => {this.callback(item,context)},
                      child: Text(
                        item,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width / 60,
                            color:item == "C" ? Colors.white :  Color(0xff352C60)),
                      )),
                  ),),
              )
              .toList(),
    );
  }
}
