import 'dart:convert';

import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/pos/api/table_caisse_end_point_api.dart';
import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/product/api/product_end_point_api.dart';
import 'package:ad_caisse/product/api/produit_proint_vente_end_point_api.dart';
import 'package:ad_caisse/product/model/categorie_article.dart';
import 'package:ad_caisse/product/model/pack.dart';
import 'package:ad_caisse/product/model/produit.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/reservation/api/reservation_end_point_api.dart';
import 'package:ad_caisse/reservation/model/ReservationResponse.dart';
import 'package:ad_caisse/reservation/model/produit_and_qte.dart';
import 'package:ad_caisse/reservation/model/reservation_dto.dart';
import 'package:ad_caisse/screens/Clients.dart';
import 'package:ad_caisse/screens/Reservation.dart';
import 'package:ad_caisse/screens/ReservationList.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart' show DateFormat, Intl;

import 'Animator.dart';
import 'CommandeScreen.dart';

class NewReservation extends StatefulWidget {
  final DataDto dataDto;
  final ReservationResponse reservationResponse;

  NewReservation(this.dataDto, this.reservationResponse);

  @override
  ReservationState createState() =>
      ReservationState(this.dataDto, this.reservationResponse);
}

class ReservationState extends State<NewReservation> {
  DataDto dataDto;
  final api_instance = new TableCaisseEndPointApi();
  ReservationResponse reservationResponse;
  ReservationEndPointApi reservationEndPointApi = ReservationEndPointApi();
  ProductEndPointApi productEndPointApi = ProductEndPointApi();
  final utilisateurEndPointApi = UtilisateurEndPointApi();
  DateTime finaldate;
  String selected;
  Utilisateur selectedUser;
  String dropdownValue = "1";
  String search = "";
  List<CategorieArticle> listCat;
  var list = [];
  List<Produit> listProduits = [];
  List<Produit> listAllProduits = [];
  List<Pack> packs = [];
  List<Pack> allPacks = [];
  TextEditingController searchController = new TextEditingController();
  TextEditingController nom = new TextEditingController();
  TextEditingController prenom = new TextEditingController();
  TextEditingController tel = new TextEditingController();
  bool showClientPassager = false;
  int fTraiteStatus = -1;

  ReservationState(this.dataDto, this.reservationResponse);

  @override
  void initState() {
    getAllProductAndPack();
    if (dataDto.finalTime != null) {
      DateTime dateTime = new DateTime(
          dataDto.reservationDate.year,
          dataDto.reservationDate.month,
          dataDto.reservationDate.day,
          dataDto.finalTime.hour,
          dataDto.finalTime.minute);
      dataDto.reservationDate = dateTime;
    }
    listCat = dataDto.listCat;
    dataDto.isModified = dataDto.clientBPrice != null;
    print("isModified: " + (dataDto.clientBPrice != null).toString());
    dataDto.searchClient("Rechercher Client");
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    if (dataDto.pax.text != "" &&
        dataDto.dropDownList.contains(dataDto.pax.text)) {
      dropdownValue = dataDto.pax.text;
    }
    if (this.reservationResponse != null) {
      dataDto.produitsQtes =
          this.reservationResponse.vReservationTable.produitsQtes;
      dataDto.reservationDate =
          this.reservationResponse.vReservationTable.dateReseravation;
      dataDto.comment.text =
          this.reservationResponse.vReservationTable.commentaire;
      dataDto.tableCaisse =
          this.reservationResponse.vReservationTable.tableCaisse;
      if (this.reservationResponse.vReservationTable.tableCaisse != null) {
        dropdownValue =
            this.reservationResponse.vReservationTable.nbrPers != null
                ? this.reservationResponse.vReservationTable.nbrPers.toString()
                : "1";
        dataDto.dropDownList = [];
        for (int i = 1;
            i <=
                this
                    .reservationResponse
                    .vReservationTable
                    .tableCaisse
                    .capaciteTable;
            i++) {
          dataDto.dropDownList.add(i.toString());
        }
        ;
      }
      dataDto.reservationId =
          this.reservationResponse.vReservationTable.idReservation;
      dataDto.pax.text =
          this.reservationResponse.vReservationTable.nbrPers != null
              ? this.reservationResponse.vReservationTable.nbrPers.toString()
              : "";
      dataDto.finalTime = TimeOfDay.fromDateTime(
          this.reservationResponse.vReservationTable.dateReseravation);
      List<ClientBPrice> list = dataDto.listClient
          .where((client) =>
              client.idClientPartenaire ==
              this.reservationResponse.vReservationTable.idConsommateur)
          .toList();
      dataDto.clientBPrice = list.length > 0 ? list[0] : null;
      nom.text = reservationResponse.nomClient;
      prenom.text = reservationResponse.prenomClient;
      tel.text = reservationResponse.telClient;
    }

    super.initState();
    // WidgetsBinding.instance.addPostFrameCallback((_) => checkDate());
  }

  txtListener() {
    switch (dataDto.focused) {
      case -1:
        {
          setState(() {
            dataDto.searchController.text = dataDto.textController.text;
          });
        }
        break;
      case 1:
        {
          setState(() {
            nom.text = dataDto.textController.text;
          });
        }
        break;
      case 2:
        {
          setState(() {
            prenom.text = dataDto.textController.text;
          });
        }
        break;

      case 3:
        {
          setState(() {
            tel.text = dataDto.textController.text;
          });
        }
        break;
      case 4:
        {
          setState(() {
            dataDto.comment.text = dataDto.textController.text;
          });
        }
        break;
    }
  }

  void getAllProductAndPack() async {
    try {
      ProduitProintVenteEndPointApi produitProintVenteEndPointApi =
          ProduitProintVenteEndPointApi();
      ResponseList list = await produitProintVenteEndPointApi
          .findAllByIdPointVenteUsingGET2(dataDto.pointVente.idPointVente);
      if (list.result == 1 && list.objectResponse != null) {
        listAllProduits.addAll(Produit.listFromJson(list.objectResponse));
      }
      dataDto.listCat.forEach((element) {
        setState(() {
          allPacks.addAll(element.packs);
        });
        if (element.fils != null) {
          element.fils.forEach((element) {
            allPacks.addAll(element.packs);
          });
        }
      });

      dataDto.searchClient("Rechercher Produit");
    } catch (err) {
      Crashlytics()
          .sendEmail("NewReservation getAllProductAndPack", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  checkDate() {
    Duration duration = dataDto.reservationDate.difference(DateTime.now());
    print("duration: " + duration.inMilliseconds.toString());
    if (duration.inMilliseconds < 0 && reservationResponse == null) {
      callDatePicker();
    }
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    if (reservationResponse != null && fTraiteStatus != 1) {
      setState(() {
        dataDto.clientBPrice = null;
        dataDto.tableCaisse = null;
        dataDto.reservationId = null;
        dataDto.produitsQtes = [];
        dataDto.dropDownList = ["1"];
        dataDto.reservationDate = DateTime.now();
        dataDto.finalTime = null;
        dataDto.isModified = false;
      });
    }
    super.dispose();
  }

  void callTimePicker() async {
    TimeOfDay timeOfDay = await getTime();

    if (timeOfDay != null) {
      DateTime dateTime = new DateTime(
          dataDto.reservationDate.year,
          dataDto.reservationDate.month,
          dataDto.reservationDate.day,
          timeOfDay.hour,
          timeOfDay.minute);
      setState(() {
        dataDto.finalTime = timeOfDay;
        dataDto.reservationDate = dateTime;
        dataDto.isModified = true;
      });
    }
  }

  Future<TimeOfDay> getTime() {
    // Imagine that this function is
    // more complex and slow.
    return showTimePicker(
      context: context,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: Color(0xff352C60),
          ),
          child: child,
        );
      },
      initialTime: TimeOfDay.now(),
    );
  }

  Future<void> paxValide() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('BPrice Reservation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Capcité du table: " +
                    dataDto.tableCaisse.capaciteTable.toString() +
                    " , Pax: " +
                    dataDto.pax.text),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.red,
              child: Text('Annuler'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              color: Colors.green,
              child: Text('Valider'),
              onPressed: () {
                Navigator.of(context).pop();
                createReservation();
              },
            ),
          ],
        );
      },
    );
  }

  void treatOrCancelReservation(
      ReservationResponse reservationResponse, int fTraite) async {
    try {
      ReservationDto reservationDto = ReservationDto();
      reservationDto.calledfrom = "CAISSE";
      reservationDto.dateReseravation =
          reservationResponse.vReservationTable.dateReseravation;
      reservationDto.idPointVente = dataDto.pointVente.idPointVente;
      reservationDto.idSession = dataDto.session.idSession;
      reservationDto.idTable = reservationResponse.vReservationTable.idTable;
      reservationDto.fTraite = fTraite;
      reservationDto.nbrPers = reservationResponse.vReservationTable.nbrPers;
      reservationDto.idConsommateur =
          reservationResponse.vReservationTable.idConsommateur;
      reservationDto.src = reservationResponse.vReservationTable.src;
      reservationDto.dateFinReseravation =
          reservationResponse.vReservationTable.dateFinReseravation;
      reservationDto.idEmployeeService =
          reservationResponse.vReservationTable.idEmployeeService;
      reservationDto.idReservation =
          reservationResponse.vReservationTable.idReservation;
      ResponseSingle responseSingle = await reservationEndPointApi
          .treatOrCancelReservationUsingPOST(reservationDto);
      if (responseSingle.result != 0) {
        AlertNotif().alert(context, responseSingle.errorDescription);
        return;
      }
      if (fTraite == 1) {
        var clients = dataDto.listClient
            .where((element) =>
                element.idClientPartenaire ==
                reservationResponse.vReservationTable.idConsommateur)
            .toList();
        dataDto.clientBPrice = clients.length > 0 ? clients[0] : null;
        dataDto.tableCaisse = reservationResponse.vReservationTable.tableCaisse;

        dataDto.setCurrentPage(CommandeScreen(dataDto), "CommandeScreen");
        reservationResponse.vReservationTable.produitsQtes.forEach((element) {
          element.produit.employe =
              reservationResponse.vReservationTable.employee;
          setState(() {
            fTraiteStatus = 1;
          });
          dataDto.callbackNumbers(element.quantite.toString(), context);
          dataDto.callbackCommande(element.produit, context);
        });
      }
    } catch (err) {
      Crashlytics()
          .sendEmail("NewReservation treatOrCancelReservation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void createReservation() async {
    try {
      if (dataDto.clientBPrice == null && tel.text == "") {
        AlertNotif().alert(context, "Veuillez sélectionner un client");
        return;
      }
      if (dataDto.finalTime == null) {
        AlertNotif()
            .alert(context, "Veuillez sélectionner l'heure de réservation");
        return;
      }
      Duration duration = dataDto.reservationDate.difference(DateTime.now());
      print("duration: " + duration.inMilliseconds.toString());
      if (duration.inMilliseconds < 0 && reservationResponse == null) {
        AlertNotif().alert(context, "Veuillez sélectionner une date valide");
        return;
      }

      if (dataDto.pointVente.fGestionTable == 1 &&
          dataDto.tableCaisse == null) {
        AlertNotif().alert(context, "Veuillez sélectionner une table");
        return;
      }
      if (dataDto.pointVente.fGestionTable == 0 &&
          dataDto.produitsQtes.length == 0) {
        AlertNotif().alert(context, "Veuillez sélectionner un service");
        return;
      }
      if (dataDto.pax.text == "" && dataDto.pointVente.fGestionTable == 1) {
        //AlertNotif().alert(context, "Veuillez nous mentionner Pax");
        dataDto.pax.text = '1';
      }
      List<ReservationResponse> list = [];
      dataDto.reservations.forEach((element) {
        if (element.vReservationTable != null &&
            element.vReservationTable.dateReseravation.millisecondsSinceEpoch -
                    dataDto.reservationDate.millisecondsSinceEpoch <
                3600000 &&
            element.vReservationTable.dateReseravation.millisecondsSinceEpoch -
                    dataDto.reservationDate.millisecondsSinceEpoch >
                -3600000 &&
            dataDto.tableCaisse != null &&
            element.vReservationTable.tableCaisse.idTable ==
                dataDto.tableCaisse.idTable) {
          list.add(element);
        }
      });
      if (list.length > 0 && reservationResponse == null) {
        list.forEach((element) {
          print(element.toJson().toString());
        });
        AlertNotif().alert(
            context, "Vous avez déjà une réservation avec les mêmes données");
        return;
      }
      ReservationDto reservationDto = ReservationDto();
      reservationDto.idEmployeeService = dataDto.selectedUser != null
          ? dataDto.selectedUser.idUtilisateur
          : null;
      reservationDto.idReservation = dataDto.reservationId;
      reservationDto.commentaire = dataDto.comment.text;
      reservationDto.nbrPers = dataDto.pointVente.fGestionTable == 1
          ? int.parse(dataDto.pax.text)
          : null;
      reservationDto.idSession = dataDto.session.idSession;
      reservationDto.idPointVente = dataDto.caisse.idPointVente;
      reservationDto.dateReseravation = dataDto.reservationDate;
      reservationDto.src = "caisse";
      reservationDto.calledfrom = "CAISSE";
      reservationDto.idConsommateur = dataDto.clientBPrice != null
          ? dataDto.clientBPrice.idClientPartenaire
          : null;
      reservationDto.prenom = prenom.text;
      reservationDto.nom = nom.text;
      reservationDto.nTel = tel.text;
      reservationDto.idTable = dataDto.pointVente.fGestionTable == 1
          ? dataDto.tableCaisse.idTable
          : null;
      reservationDto.produitsQtes = dataDto.produitsQtes;
      ResponseSingle responseSingle = dataDto.reservationId == null
          ? await reservationEndPointApi
              .createReservationUsingPOST(reservationDto)
          : await reservationEndPointApi
              .updateReservationUsingPOST(reservationDto);
      print("createReservation: " +
          jsonEncode(responseSingle.objectResponse) +
          " result: " +
          responseSingle.result.toString());
      if (dataDto.reservationId != null) {
        print("ReservationId: " + " ReservationId: " + dataDto.reservationId !=
                null
            ? dataDto.reservationId
            : "null");
      }
      if (dataDto.reservationId != null && responseSingle.result == 0) {
        dataDto.clientBPrice = null;
        dataDto.tableCaisse = null;
        dataDto.pax.text = "";
        dataDto.comment.text = "";
        dataDto.finalTime = null;
        dataDto.produitsQtes = [];
        dataDto.pax.text = "";
        dataDto.comment.text = "";
        dataDto.reservationId = null;
        dataDto.setCurrentPage(ReservationList(dataDto), "ReservationList");
        return;
      } else if (responseSingle.result == 1) {
        dataDto.clientBPrice = null;
        dataDto.tableCaisse = null;
        dataDto.pax.text = "";
        dataDto.comment.text = "";
        dataDto.finalTime = null;
        dataDto.produitsQtes = [];
        dataDto.pax.text = "";
        dataDto.comment.text = "";
        dataDto.reservationId = null;
        dataDto.setCurrentPage(Reservation(dataDto), "Reservation");
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics()
          .sendEmail("NewReservation createReservation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void clientPassager() {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          content: Container(
            width: width / 3,
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Image.asset('assets/logo/logo.png',
                      width: 50, height: 50, fit: BoxFit.scaleDown),
                  SizedBox(height: 16.0),
                  Text(
                    "Nouveau Client Passager",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  TextField(
                      cursorColor: Color(0xff352C60),
                      controller: nom,
                      onTap: () {
                        setState(() {
                          dataDto.focused = 1;
                        });
                        dataDto.callbackKeyboard(false);
                        dataDto.textController.text="";
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xfff5f5f5),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "Nom:",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      )),
                  SizedBox(
                    height: 8,
                  ),
                  TextField(
                      cursorColor: Color(0xff352C60),
                      keyboardType: TextInputType.phone,
                      controller: prenom,
                      onTap: () {
                        setState(() {
                          dataDto.focused = 2;
                        });
                        dataDto.callbackKeyboard(false);
                        dataDto.textController.text="";

                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xfff5f5f5),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "Prénom:",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      )),
                  SizedBox(
                    height: 8,
                  ),
                  TextField(
                      cursorColor: Color(0xff352C60),
                      controller: tel,
                      onTap: () {
                        setState(() {
                          dataDto.focused = 3;
                        });
                        dataDto.callbackKeyboard(true);
                        dataDto.textController.text="";

                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xfff5f5f5),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        hintText: "Téléphone:",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      )),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              elevation: 0,
              color: Color(0xffCF0606),
              onPressed: () {
                setState(() {
                  nom.text = "";
                  prenom.text = "";
                  tel.text = "";
                });
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'OK',
                style: TextStyle(color: Colors.white),
              ),
              elevation: 0,
              color: Color(0xFF32C533),
              onPressed: () {
                if (nom.text == "" || prenom.text == "" || tel.text == "") {
                  debugPrint("no");
                } else {
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        );
      },
    );
  }

  Color fromHex(String hexString) {
    if (hexString.contains("rgba")) {
      String a = hexString.replaceAll("rgba", "");
      String b = a.replaceAll("(", "");
      String c = b.replaceAll(")", "");
      List<String> listString = c.split(",");
      print("RGBA: " + listString.toString());
      if (listString.length == 4) {
        return Color.fromARGB(
            int.parse(listString[0]),
            int.parse(listString[1]),
            int.parse(listString[2]),
            listString[3].contains(".")
                ? int.parse(listString[3].replaceAll("0.", ""))
                : int.parse(listString[3]));
      }
      return Color.fromARGB(100, 250, 100, 0);
    }
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  void callDatePicker() async {
    DateTime order = await getDate();
    if (order != null) {
      DateTime dateTime = order;
      if (dataDto.finalTime != null) {
        dateTime = new DateTime(order.year, order.month, order.day,
            dataDto.finalTime.hour, dataDto.finalTime.minute);
      }
      setState(() {
        finaldate = order;
        dataDto.reservationDate = dateTime;
        dataDto.isModified = true;
      });
    } else {
      checkDate();
    }
  }

  Future<DateTime> getDate() {
    // Imagine that this function is
    // more complex and slow.
    return showDatePicker(
      context: context,
      helpText: "Date de réservation",
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2050),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: Color(0xff352C60),
          ),
          child: child,
        );
      },
    );
  }

  Future<void> showTableDialog() async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      ResponseList responseList =
          await api_instance.findAllByIdPointVenteAndFDefautUsingGET1(
              dataDto.pointVente.idPointVente);
      List<TableCaisse> tables = responseList.objectResponse
          .map((e) => TableCaisse.fromJson(e))
          .toList();
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: Text(
              'Veuillez sélectionner une table',
              textAlign: TextAlign.center,
            ),
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            content: Container(
              height: height * 0.60,
              width: width * 0.5,
              padding: EdgeInsets.all(16),
              child: GridView.count(
                padding: EdgeInsets.only(top: 8),
                crossAxisCount: width > height ? 4 : 2,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                childAspectRatio: 1,
                children: tables
                    .map(
                      (item) => SizedBox(
                        child: WidgetANimator(FlatButton(
                            color: Color(0xffE7D7EE),
                            padding: EdgeInsets.all(0),
                            onPressed: () => {
                                  setState(() {
                                    dataDto.dropDownList = [];
                                  }),
                                  for (int i = 1; i <= item.capaciteTable; i++)
                                    {
                                      setState(() {
                                        dataDto.dropDownList.add(i.toString());
                                      })
                                    },
                                  setState(() {
                                    dropdownValue = "1";
                                    dataDto.pax.text = "1";
                                    dataDto.isModified = true;
                                    dataDto.tableCaisse = item;
                                  }),
                                  Navigator.pop(context)
                                },
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    color: Colors.green,
                                    width: 16,
                                    height: 16,
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Table N°",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w300,
                                          fontSize: width > height
                                              ? width / 80
                                              : width / 35,
                                          color: Color(0xff352C60)),
                                    ),
                                    Text(
                                      item.numTable.toString(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: width > height
                                              ? width / 80
                                              : width / 35,
                                          color: Color(0xff352C60)),
                                    ),
                                    Text(
                                      "Capacité: " +
                                          item.capaciteTable.toString(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w300,
                                          fontSize: width > height
                                              ? width / 80
                                              : width / 35,
                                          color: Color(0xff352C60)),
                                    ),
                                  ],
                                )
                              ],
                            ))),
                      ),
                    )
                    .toList(),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                child: Text('Annuler'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("NewReservation showTableDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> _showMyDialog(Produit produit) async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      List<Utilisateur> services = [];
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1) {
        services = responseList.objectResponse
            .map((e) => Utilisateur.fromJson(e))
            .toList();
      }
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: Text(
              'Veuillez sélectionner un employé',
              textAlign: TextAlign.center,
            ),
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            content: Container(
              height: height * 0.60,
              width: width * 0.5,
              padding: EdgeInsets.all(16),
              child: GridView.count(
                padding: EdgeInsets.only(top: 8),
                crossAxisCount: width > height ? 4 : 2,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                childAspectRatio: 2,
                children: services
                    .map(
                      (item) => SizedBox(
                        child: WidgetANimator(FlatButton(
                            color: Color(0xffE7D7EE),
                            padding: EdgeInsets.all(0),
                            onPressed: () => {
                                  produit.employe = item,
                                  // this.dataDto.callbackCommande(produit, context),
                                  setState(() {
                                    dataDto.isModified = true;
                                    selectedUser = item;
                                    dataDto.produitsQtes = [];
                                    dataDto.produitsQtes.add(ProduitAndQte(
                                        idProduit: produit.idProduit,
                                        quantite: dataDto.pax.text != ""
                                            ? int.parse(dataDto.pax.text)
                                            : 1,
                                        designation: produit.designation));
                                    dataDto.pax.text = "";
                                  }),
                                  Navigator.pop(context)
                                },
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      item.prenom + " " + item.nom,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: width > height
                                              ? width / 80
                                              : width / 35,
                                          color: Color(0xff352C60)),
                                    ),
                                  ],
                                )
                              ],
                            ))),
                      ),
                    )
                    .toList(),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                child: Text('Annuler'),
                onPressed: () {
                  setState(() {
                    dataDto.produitsQtes.add(ProduitAndQte(
                        idProduit: produit.idProduit,
                        quantite: dataDto.pax.text != ""
                            ? int.parse(dataDto.pax.text)
                            : 1,
                        designation: produit.designation));
                    dataDto.pax.text = "";
                  });
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("NewReservation _showMyDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> showServiceDialog() async {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    String search;
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Color(0xffEEEEEE),
          title: Text(
            'Veuillez sélectionner un service',
            textAlign: TextAlign.center,
          ),
          titleTextStyle: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          content: Container(
            width: width * 0.8,
            height: height * 0.8,
            child: Column(
              children: [
                Container(
                  color: Colors.white,
                  child: TextField(
                    autofocus: true,
                    cursorColor: Color(0xff352C60),
                    controller: searchController,
                    onChanged: (search) {
                      Navigator.of(context).pop();
                      if (search != "") {
                        setState(() {
                          listProduits = listAllProduits
                              .where((element) =>
                                  element.designation != null &&
                                  element.designation
                                      .toLowerCase()
                                      .contains(search.toLowerCase()))
                              .toList();
                        });
                      } else {
                        setState(() {
                          listProduits = [];
                          packs = [];
                        });
                      }
                      showServiceDialog();
                    },
                    textAlignVertical: TextAlignVertical.center,
                    obscureText: false,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      disabledBorder: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.search,
                        size: width > height
                            ? SizeClacule().calcule(context, 40)
                            : SizeClacule().calcule(context, 20),
                        color: Colors.grey,
                      ),
                      hintText: "Rechercher Produit",
                      hintStyle: TextStyle(
                          fontSize: width > height
                              ? SizeClacule().calcule(context, 70)
                              : SizeClacule().calcule(context, 35)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(0),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    ),
                  ),
                ),
                list.length > 0
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 16),
                            height: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            width: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            child: FlatButton(
                                color: Color(0xff32C533),
                                onPressed: () => {
                                      Navigator.of(context).pop(),
                                      if (listProduits.length > 0 &&
                                          list.length == 1)
                                        {
                                          setState(() {
                                            listProduits = [];
                                            packs = [];
                                            list = [];
                                          })
                                        }
                                      else
                                        {
                                          setState(() {
                                            listCat = list[list.length - 1];
                                            listProduits = [];
                                            packs = [];
                                            list.removeLast();
                                          }),
                                        },
                                      showServiceDialog()
                                    },
                                padding: EdgeInsets.all(0),
                                child: Image.asset(
                                  'assets/back/back.png',
                                  color: Colors.white,
                                  fit: BoxFit.contain,
                                  height:
                                      width > height ? width / 40 : width / 20,
                                  width:
                                      width > height ? width / 40 : width / 20,
                                )),
                          )
                        ],
                      )
                    : Container(),
                Expanded(
                  child: listProduits.length > 0 || packs.length > 0
                      ? Column(
                          children: [
                            listProduits.length > 0
                                ? Expanded(
                                    child: GridView.count(
                                    padding: EdgeInsets.only(top: 8),
                                    crossAxisCount: width > height ? 4 : 3,
                                    mainAxisSpacing: 8.0,
                                    crossAxisSpacing: 8.0,
                                    childAspectRatio: 2,
                                    children: listProduits
                                        .map(
                                          (item) => SizedBox(
                                            child: FlatButton(
                                                color: item.couleur == null
                                                    ? Color(0xffE7D7EE)
                                                    : fromHex(item.couleur),
                                                onPressed: () => {
                                                      setState(() {
                                                        dataDto.isModified =
                                                            true;
                                                        item.employe = dataDto
                                                            .selectedUser;
                                                        dataDto.produitsQtes =
                                                            [];
                                                        dataDto.produitsQtes
                                                            .add(ProduitAndQte(
                                                                idProduit: item
                                                                    .idProduit,
                                                                quantite: dataDto
                                                                            .pax
                                                                            .text !=
                                                                        ""
                                                                    ? int.parse(
                                                                        dataDto
                                                                            .pax
                                                                            .text)
                                                                    : 1,
                                                                designation: item
                                                                    .designation));
                                                        dataDto.pax.text = "";
                                                      }),
                                                      Navigator.of(context)
                                                          .pop()
                                                    },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      item.designation != null
                                                          ? item.designation
                                                          : "",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize:
                                                              width > height
                                                                  ? width / 80
                                                                  : width / 35,
                                                          color: Colors.black),
                                                    ),
                                                    SizedBox(
                                                      height: 8,
                                                    ),
                                                    Text(
                                                      item.produitpointvente !=
                                                                  null &&
                                                              item.produitpointvente
                                                                      .prix !=
                                                                  null
                                                          ? item.produitpointvente
                                                                  .prix
                                                                  .toStringAsFixed(
                                                                      int.parse(dataDto
                                                                          .pointVente
                                                                          .chiffrevirgule)) +
                                                              " DT"
                                                          : "",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize:
                                                              width > height
                                                                  ? width / 100
                                                                  : width / 35,
                                                          color: Color(
                                                              0xff352C60)),
                                                    )
                                                  ],
                                                )),
                                          ),
                                        )
                                        .toList(),
                                  ))
                                : SizedBox(
                                    width: 0,
                                    height: 0,
                                  ),
                          ],
                        )
                      : GridView.count(
                          padding: EdgeInsets.only(top: 8),
                          crossAxisCount: width > height ? 4 : 3,
                          mainAxisSpacing: 8.0,
                          crossAxisSpacing: 8.0,
                          childAspectRatio: 2,
                          children: listCat
                              .map(
                                (item) => SizedBox(
                                  child: FlatButton(
                                      color: item.couleur == null
                                          ? Color(0xffE7D7EE)
                                          : fromHex(item.couleur),
                                      onPressed: () => {
                                            Navigator.of(context).pop(),
                                            item.fils.length > 0 ||
                                                    item.produits.length > 0 ||
                                                    item.packs.length > 0
                                                ? list.add(listCat)
                                                : null,
                                            if (item.fils.length > 0)
                                              {
                                                setState(() {
                                                  listCat = item.fils;
                                                })
                                              }
                                            else
                                              {
                                                setState(() {
                                                  listProduits =
                                                      item.produits.length > 0
                                                          ? item.produits
                                                          : [];
                                                  packs = item.packs.length > 0
                                                      ? item.packs
                                                      : [];
                                                })
                                              },
                                            showServiceDialog()
                                          },
                                      child: Text(
                                        item.designation,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: width > height
                                                ? width / 80
                                                : width / 30,
                                            color: Color(0xff352C60)),
                                      )),
                                ),
                              )
                              .toList(),
                        ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.red,
              child: Text('Annuler'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Intl.defaultLocale = 'fr';

    return Stack(
      fit: StackFit.loose,
      children: [
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Réservation client",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width > height ? width / 50 : width / 30),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  SizedBox(
                    height: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    width: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    child: FlatButton(
                        color: Color(0xff32C533),
                        onPressed: () => {
                              dataDto.reservationId != null
                                  ? dataDto.setCurrentPage(
                                      ReservationList(dataDto),
                                      "ReservationList")
                                  : dataDto.setCurrentPage(
                                      Reservation(dataDto), "Reservation")
                            },
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          'assets/back/back.png',
                          color: Colors.white,
                          fit: BoxFit.contain,
                          height: width > height ? width / 40 : width / 20,
                          width: width > height ? width / 40 : width / 20,
                        )),
                  )
                ],
              ),
              Container(
                  width: width / 2,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 12,
                      ),
                      Row(
                        children: [
                          Text(
                            "Date de réservation",
                            style: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: width / 100),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                              child: Container(
                            color: Colors.white,
                            alignment: Alignment.center,
                            height: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            padding: EdgeInsets.only(left: 16, right: 16),
                            child: Text(
                              DateFormat.yMMMEd()
                                  .format(dataDto.reservationDate),
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: width / 100),
                            ),
                          )),
                          SizedBox(
                            width: 8,
                          ),
                          SizedBox(
                            width: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            height: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            child: FlatButton(
                              onPressed: () {
                                //dataDto.setCurrentPage(Reservation(dataDto), "");
                                callDatePicker();
                              },
                              child: Image.asset(
                                'assets/calendar/calendar.png',
                                fit: BoxFit.contain,
                                height: width > height
                                    ? SizeClacule().calcule(context, 30)
                                    : SizeClacule().calcule(context, 15),
                                width: width > height
                                    ? SizeClacule().calcule(context, 30)
                                    : SizeClacule().calcule(context, 15),
                              ),
                              color: Color(0xffCF5FFE),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Container(
                            alignment: Alignment.center,
                            color: Colors.white,
                            height: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            child: SizedBox(
                              height: width > height
                                  ? SizeClacule().calcule(context, 15)
                                  : SizeClacule().calcule(context, 7.5),
                              child: FlatButton(
                                  onPressed: () {
                                    callTimePicker();
                                  },
                                  child: Text(
                                    dataDto.finalTime != null
                                        ? dataDto.finalTime.format(context)
                                        : 'Heure',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: width / 100,
                                        color: Colors.black),
                                  )),
                            ),
                          )
                        ],
                      )
                    ],
                  )),
              SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Container(
                      width: width / 2,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: Container(
                                alignment: Alignment.centerLeft,
                                color: Colors.white,
                                height: width > height
                                    ? SizeClacule().calcule(context, 15)
                                    : SizeClacule().calcule(context, 7.5),
                                padding: EdgeInsets.only(left: 16, right: 16),
                                child: Text(
                                  dataDto.clientBPrice != null
                                      ? dataDto.clientBPrice.nom +
                                          " " +
                                          dataDto.clientBPrice.prenom
                                      : nom.text != "" && prenom.text != ""
                                          ? prenom.text + ' ' + nom.text
                                          : "Veuillez choisir votre client",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: width / 100),
                                ),
                              )),
                            ],
                          )
                        ],
                      )),
                  SizedBox(
                    width: 8,
                  ),
                  SizedBox(
                    width: width / 4,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: width > height
                              ? SizeClacule().calcule(context, 15)
                              : SizeClacule().calcule(context, 7.5),
                          height: width > height
                              ? SizeClacule().calcule(context, 15)
                              : SizeClacule().calcule(context, 7.5),
                          child: FlatButton(
                            padding: EdgeInsets.all(0),
                            onPressed: () {
                              //clientPassager();
                              setState(() {
                                showClientPassager = true;
                              });
                            },
                            child: Icon(
                              nom.text != ""
                                  ? Icons.person_outline
                                  : Icons.person_add,
                              color: Colors.white,
                              size: width > height
                                  ? SizeClacule().calcule(context, 30)
                                  : SizeClacule().calcule(context, 15),
                            ),
                            color: Color(0xffCF5FFE),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: SizedBox(
                          height: width > height
                              ? SizeClacule().calcule(context, 15)
                              : SizeClacule().calcule(context, 7.5),
                          child: FlatButton(
                            onPressed: () {
                              setState(() {
                                dataDto.currentPage = "NewReservation";
                              });
                              dataDto.setCurrentPage(Clients(dataDto), "");
                            },
                            child: Text(
                              "Selectionner un client",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                  fontSize: width / 100),
                            ),
                            color: Color(0xffCF5FFE),
                          ),
                        )),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Container(
                      width: width / 2,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: Container(
                                color: Colors.white,
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(left: 16),
                                height: width > height
                                    ? SizeClacule().calcule(context, 15)
                                    : SizeClacule().calcule(context, 7.5),
                                child: dataDto.pointVente.fGestionTable == 1
                                    ? Text(
                                        dataDto.tableCaisse != null
                                            ? "Table N° " +
                                                dataDto.tableCaisse.numTable
                                                    .toString()
                                            : "Table",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: width / 100),
                                      )
                                    : dataDto.produitsQtes.length > 0
                                        ? Container(
                                            padding: EdgeInsets.all(0),
                                            child: ListView(
                                              // This next line does the trick.
                                              scrollDirection: Axis.horizontal,
                                              padding: EdgeInsets.all(0),
                                              children: [
                                                for (var item
                                                    in dataDto.produitsQtes)
                                                  Row(
                                                    children: [
                                                      Text(
                                                        item.designation != null
                                                            ? item.quantite
                                                                    .toString() +
                                                                " X " +
                                                                item.designation
                                                            : item.produit !=
                                                                    null
                                                                ? item.quantite
                                                                        .toString() +
                                                                    " X " +
                                                                    item.produit
                                                                        .designation
                                                                : "",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w300,
                                                            fontSize:
                                                                width / 100),
                                                      ),
                                                    ],
                                                  )
                                              ],
                                            ))
                                        : Text(
                                            "Services",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w300,
                                                fontSize: width / 100),
                                          ),
                              )),
                              SizedBox(
                                width: 8,
                              ),
                              Container(
                                height: width > height
                                    ? SizeClacule().calcule(context, 15)
                                    : SizeClacule().calcule(context, 7.5),
                                width: width > height
                                    ? SizeClacule().calcule(
                                        context,
                                        dataDto.pointVente.fGestionTable == 1
                                            ? 10
                                            : 8)
                                    : SizeClacule().calcule(
                                        context,
                                        dataDto.pointVente.fGestionTable == 1
                                            ? 5
                                            : 4),
                                padding: EdgeInsets.only(left: 16, right: 8),
                                alignment: Alignment.centerLeft,
                                color: Colors.white,
                                child: dataDto.pointVente.fGestionTable == 1
                                    ? DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          isExpanded: true,
                                          value: dropdownValue,
                                          icon: Icon(
                                              Icons.arrow_drop_down_circle),
                                          iconSize: 32,
                                          iconEnabledColor: Color(0xffCF5FFE),
                                          items: dataDto.dropDownList
                                              .map<DropdownMenuItem<String>>(
                                                  (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (String newValue) {
                                            setState(() {
                                              dataDto.isModified =
                                                  reservationResponse
                                                              .vReservationTable
                                                              .nbrPers
                                                              .toString() !=
                                                          newValue
                                                      ? true
                                                      : dataDto.isModified;
                                              dropdownValue = newValue;
                                              dataDto.pax.text = newValue;
                                            });
                                          },
                                          style: TextStyle(color: Colors.grey),
                                        ),
                                      )
                                    : TextField(
                                        cursorColor: Colors.black,
                                        controller: dataDto.pax,
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.transparent,
                                          hintText: dataDto.pointVente
                                                      .fGestionTable ==
                                                  1
                                              ? "Pax"
                                              : "Qantité",
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.transparent),
                                              borderRadius:
                                                  BorderRadius.circular(0)),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.transparent),
                                              borderRadius:
                                                  BorderRadius.circular(0)),
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.transparent),
                                              borderRadius:
                                                  BorderRadius.circular(0)),
                                        )),
                              )
                            ],
                          )
                        ],
                      )),
                  SizedBox(
                    width: 8,
                  ),
                  SizedBox(
                    width: width / 4,
                    height: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    child: FlatButton(
                      onPressed: () {
                        dataDto.pointVente.fGestionTable == 1
                            ? showTableDialog()
                            : showServiceDialog();
                      },
                      child: Text(
                        dataDto.pointVente.fGestionTable == 1
                            ? "Choisir une table"
                            : "Choisir un service",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: width / 100),
                      ),
                      color: Color(0xffCF5FFE),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Container(
                    color: Colors.white,
                    width: width / 2,
                    height: width > height
                        ? SizeClacule().calcule(context, 4)
                        : SizeClacule().calcule(context, 2),
                    child: TextField(
                        cursorColor: Colors.black,
                        controller: dataDto.comment,
                        onTap: () {
                          setState(() {
                            dataDto.focused = 4;
                          });
                          dataDto.callbackKeyboard(false);
                        },
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.transparent,
                          contentPadding: EdgeInsets.only(left: 16),
                          hintText: "Commentaire",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                              borderRadius: BorderRadius.circular(0)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                              borderRadius: BorderRadius.circular(0)),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                              borderRadius: BorderRadius.circular(0)),
                        )),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Container(
                    height: width > height
                        ? SizeClacule().calcule(context, 4)
                        : SizeClacule().calcule(context, 2),
                    child: Column(
                      children: [
                        Expanded(
                          child: Container(),
                        ),
                        SizedBox(
                          width: width / 4,
                          height: width > height
                              ? SizeClacule().calcule(context, 15)
                              : SizeClacule().calcule(context, 7.5),
                          child: FlatButton(
                            onPressed: () {
                              setState(() {
                                reservationResponse = null;
                                dataDto.reservationDate = DateTime.now();
                                dataDto.reservationId = null;
                                dataDto.tableCaisse = null;
                                dataDto.clientBPrice = null;
                                dataDto.pax.text = "";
                                dataDto.comment.text = "";
                                dataDto.produitsQtes = [];
                                dataDto.finalTime = null;
                                dataDto.isModified = false;
                              });
                            },
                            child: Text(
                              "Annuler",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                  fontSize: width / 100),
                            ),
                            color: Color(0xffCF0606),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        SizedBox(
                          width: width / 4,
                          height: width > height
                              ? SizeClacule().calcule(context, 15)
                              : SizeClacule().calcule(context, 7.5),
                          child: FlatButton(
                            onPressed: () {
                              dataDto.tableCaisse != null &&
                                      dataDto.pax.text != "" &&
                                      dataDto.tableCaisse.capaciteTable <
                                          int.parse(dataDto.pax.text)
                                  ? paxValide()
                                  : reservationResponse != null
                                      ? treatOrCancelReservation(
                                          reservationResponse, 1)
                                      : createReservation();
                            },
                            child: Text(
                              reservationResponse != null
                                  ? "Passer Commande"
                                  : 'Valider',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                  fontSize: width / 100),
                            ),
                            color: Color(0xff32C533),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        reservationResponse != null && dataDto.isModified
                            ? SizedBox(
                                width: width / 4,
                                height: width > height
                                    ? SizeClacule().calcule(context, 15)
                                    : SizeClacule().calcule(context, 7.5),
                                child: FlatButton(
                                  onPressed: () {
                                    dataDto.tableCaisse != null &&
                                            dataDto.pax.text != "" &&
                                            dataDto.tableCaisse.capaciteTable <
                                                int.parse(dataDto.pax.text)
                                        ? paxValide()
                                        : createReservation();
                                  },
                                  child: Text(
                                    'Appliquer les modifications',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                        fontSize: width / 100),
                                  ),
                                  color: Colors.blueAccent,
                                ),
                              )
                            : SizedBox(),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        search != ""
            ? Positioned(
                child: Container(
                  color: Colors.white,
                  child: Container(
                      child: SingleChildScrollView(
                          scrollDirection:
                              width > height ? Axis.vertical : Axis.horizontal,
                          child: DataTable(
                            columns: [
                              DataColumn(
                                label: Text('Nom'),
                              ),
                              DataColumn(label: Text('Email')),
                              DataColumn(label: Text('Téléphone')),
                              DataColumn(label: Text('Solde')),
                            ],
                            rows: dataDto.listClientFiltred
                                .where((c) =>
                                    c.prenom != null &&
                                        c.prenom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nom != null &&
                                        c.nom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nTel != null &&
                                        c.nTel
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.email != null &&
                                        c.email
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.qrCodePartn != null &&
                                        c.qrCodePartn
                                            .toLowerCase()
                                            .contains(search.toLowerCase()))
                                .toList()
                                .map(
                                  (client) => DataRow(cells: [
                                    DataCell(
                                      Text(
                                        client.nom + " " + client.prenom,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.isModified = true;
                                          dataDto.clientBPrice = client;
                                          search = "";
                                          dataDto.searchController.text = "";
                                          dataDto.textController.clear();
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.email != null
                                            ? client.email
                                            : "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.isModified = true;
                                          dataDto.clientBPrice = client;
                                          search = "";
                                          dataDto.searchController.text = "";
                                          dataDto.textController.clear();
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.nTel,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.isModified = true;
                                          dataDto.clientBPrice = client;
                                          search = "";
                                          dataDto.searchController.text = "";
                                          dataDto.textController.clear();
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.soldePartn.toStringAsFixed(3) +
                                            " DT",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${client.nom}');
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.isModified = true;
                                          dataDto.clientBPrice = client;
                                          search = "";
                                          dataDto.searchController.text = "";
                                          dataDto.textController.clear();
                                        });
                                      },
                                    ),
                                  ]),
                                )
                                .toList(),
                          ))),
                ),
                top: 0,
                left: 0,
                right: 0,
              )
            : SizedBox(),
        showClientPassager
            ? Positioned(
                top: 0,
                child: Container(
                  padding: EdgeInsets.only(top: 16),
                  width: width,
                  height: height,
                  alignment: Alignment.topCenter,
                  color: Colors.black38,
                  child: Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(16),
                    width: width / 3,
                    child: SingleChildScrollView(
                      child: ListBody(
                        children: <Widget>[
                          Image.asset('assets/logo/logo.png',
                              width: 50, height: 50, fit: BoxFit.scaleDown),
                          SizedBox(height: 16.0),
                          Text(
                            "Nouveau Client Passager",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          TextField(
                              cursorColor: Color(0xff352C60),
                              controller: nom,
                              onTap: () {
                                setState(() {
                                  dataDto.focused = 1;
                                });
                                dataDto.callbackKeyboard(false);
                                dataDto.textController.text="";
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Color(0xfff5f5f5),
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                hintText: "Nom:",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                              )),
                          SizedBox(
                            height: 8,
                          ),
                          TextField(
                              cursorColor: Color(0xff352C60),
                              keyboardType: TextInputType.phone,
                              controller: prenom,
                              onTap: () {
                                setState(() {
                                  dataDto.focused = 2;
                                });
                                dataDto.callbackKeyboard(false);
                                dataDto.textController.text="";
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Color(0xfff5f5f5),
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                hintText: "Prénom:",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                              )),
                          SizedBox(
                            height: 8,
                          ),
                          TextField(
                              cursorColor: Color(0xff352C60),
                              controller: tel,
                              onTap: () {
                                setState(() {
                                  dataDto.focused = 3;
                                });
                                dataDto.callbackKeyboard(true);
                                dataDto.textController.text="";
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Color(0xfff5f5f5),
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                hintText: "Téléphone:",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              RaisedButton(
                                child: Text('Annuler',
                                    style: TextStyle(color: Colors.white)),
                                elevation: 0,
                                color: Color(0xffCF0606),
                                onPressed: () {
                                  dataDto.searchClient("Rechercher Client");
                                  setState(() {
                                    showClientPassager = false;
                                    dataDto.hideKeyboard = true;
                                    nom.text = "";
                                    prenom.text = "";
                                    tel.text = "";
                                  });
                                  // Navigator.of(context).pop();
                                },
                              ),
                              RaisedButton(
                                child: Text(
                                  'OK',
                                  style: TextStyle(color: Colors.white),
                                ),
                                elevation: 0,
                                color: Color(0xFF32C533),
                                onPressed: () {
                                  if (nom.text == "" ||
                                      prenom.text == "" ||
                                      tel.text == "") {
                                    debugPrint("no");
                                  } else {
                                    dataDto.searchClient("Rechercher Client");
                                    setState(() {
                                      dataDto.hideKeyboard = true;
                                      showClientPassager = false;
                                    });
                                  }
                                },
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ))
            : SizedBox()
      ],
    );
  }
}
