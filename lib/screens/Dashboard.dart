import 'dart:async';
import 'dart:convert';
import 'dart:io' show Platform;

import 'package:ad_caisse/customer/api/customer_end_point_api.dart';
import 'package:ad_caisse/loyality/api/fid_cred_end_point_api.dart';

import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/dto/StompMessage.dart';
import 'package:ad_caisse/loyality/model/mode_reglement.dart' as reg;
import 'package:ad_caisse/ordering/api/ordering_end_point_api.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/ordering/model/commande.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/ordering/model/commande_dto.dart';
import 'package:ad_caisse/payement/api/payment_end_point_api.dart';
import 'package:ad_caisse/payement/model/reglement_dto.dart';
import 'package:ad_caisse/payement/model/reglm_details_dto.dart';
import 'package:ad_caisse/pos/api/mode_reglement_end_point_api.dart';
import 'package:ad_caisse/pos/api/table_caisse_end_point_api.dart';
import 'package:ad_caisse/pos/model/PreColture.dart';
import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/product/api/pack_end_point_api.dart';
import 'package:ad_caisse/product/model/pack.dart';
import 'package:ad_caisse/product/model/produit.dart';
import 'package:ad_caisse/request/ClotureRequest.dart';
import 'package:ad_caisse/request/CommandeRequest.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ModeReglementList.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/Clients.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/Commandes.dart';
import 'package:ad_caisse/screens/Login_Screen.dart';
import 'package:ad_caisse/screens/Notifications.dart';
import 'package:ad_caisse/screens/Numbers.dart';
import 'package:ad_caisse/screens/NumbersLogin.dart';
import 'package:ad_caisse/screens/Payement.dart';
import 'package:ad_caisse/screens/Recharge.dart';
import 'package:ad_caisse/screens/Reservation.dart';
import 'package:ad_caisse/screens/Tables.dart';
import 'package:ad_caisse/screens/Transactions.dart';
import 'package:ad_caisse/screens/VirtualKeyboardScreen.dart';
import 'package:ad_caisse/transaction/api/commande_end_point_api.dart';
import 'package:ad_caisse/transaction/model/CommandeDetailsDto.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/transaction/model/commande.dart';
import 'package:ad_caisse/transaction/model/commande_details.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';
import 'package:toast/toast.dart';

import 'Animator.dart';
import 'Depense.dart';
import 'NumbersWithTablesFunctionality.dart';
import 'Setting.dart';

class Dashboard extends StatefulWidget {
  DataDto dataDto;

  Dashboard(this.dataDto);

  @override
  State<StatefulWidget> createState() => new _State(this.dataDto);
}

class _State extends State<Dashboard> {
  DataDto dataDto;

  _State(this.dataDto);
  final _controller = ScrollController();
  Timer _timer;
  bool isConnected = true;
  bool loading = false;
  bool loadingEsp = false;
  double drawer = 0.0;
  String number = "";
  String numTable = "";
  Widget currentPage;
  String current;
  bool impAnnulation;
  List<String> category = [];
  var orderingEndPointApi = OrderingEndPointApi();
  var modeReglementEndPointApi = ModeReglementEndPointApi();
  var fidCredEndPointApi = FidCredEndPointApi();

  var commandeEndPointApi = CommandeEndPointApi();
  var paymentEndPointApi = PaymentEndPointApi();
  var customerEndPointApi = CustomerEndPointApi();
  TextEditingController montant = new TextEditingController();
  TextEditingController remise = new TextEditingController();
  TextEditingController qrCode = new TextEditingController();
  TextEditingController error = new TextEditingController();
  TextEditingController searchController = new TextEditingController();
  TextEditingController codeRemizeController = new TextEditingController();
  final List<DropdownMenuItem> items = [];
  ClientBPrice selectedValue;
  FocusNode _focusNodeSelectClient = FocusNode();
  String _keyCode = '';
  List<StompMessage> messages = [];
  bool isLoading = false;
  bool isLoadingTable = false;
  List<TableCaisse> tables = [];
  final api_instance = new TableCaisseEndPointApi();

  bool actionRemize;
  String codeRemize;
  var subscription;

  @override
  void initState() {
    getTables();
    getListNotifications();
    getActions();
    getActionAnnuTicket();
    getIsSwitchedRetour();
  //  if (dataDto.caisseType.code == 'CENTRALE') registerSocket();
    getModeReglementList();
    findAllActiveClientByPartenaire();
    interntChecker();
    DataDto dto = this.dataDto;
    dto.callbackCommande = callbackCommande;
    dto.callbackNumbers = callbackNumbers;
    dto.setClientBPrice = setClientBPrice;
    dto.setCurrentPage = setCurrentPage;
    dataDto.callbackKeyboard = callbackKeyboard;
    dto.total = "0.000";
    dto.commande = null;
    dto.searchClient = searchClient;
    dataDto = dto;
    currentPage = CommandeScreen(dataDto);
    current = "CommandeScreen";
    dataDto.listClient.forEach((element) {
      items.add(DropdownMenuItem(
        child: Text(element.nom + " " + element.prenom),
        value: element,
      ));
    });
    super.initState();
  }

  getIsSwitchedRetour() async {
    try {
      LocalStorageInterface prefs = await LocalStorage.getInstance();
      bool retour = prefs.getBool('retour_automatique');

      int lastTicketNumber = prefs.getInt('lastTicketNumber');
      if (lastTicketNumber != null) {
        setState(() {
          dataDto.lastTicketNumber = lastTicketNumber;
        });
      }
      setState(() {
        dataDto.isSwitchedRetour = retour != null ? retour : false;
      });
    } catch (err) {
      debugPrint("getIsSwitchedRetour: " + err.toString());
    }
  }

  getModeReglementList() async {
    var list = await ModeReglementList().getModeReglementList(dataDto.caisse);
    setState(() {
      dataDto.modeReg = list;
    });
  }

  findAllActiveClientByPartenaire() async {
    ResponseList responseList =
        await customerEndPointApi.findAllActiveClientByPartenaireUsingGET(
            dataDto.utilisateur.idPartenaire);
    List<ClientBPrice> listClient = [];
    if (responseList.result == 1 && responseList.objectResponse != null) {
      listClient = responseList.objectResponse
          .map((i) => ClientBPrice.fromJson(i))
          .toList();
    }
    setState(() {
      dataDto.listClient = listClient;
      dataDto.listClientFiltred = listClient;
    });
  }

  interntChecker() {
    _timer = Timer.periodic(new Duration(seconds: 25), (timer) async {
      bool res = await Utils().checkInternet();
      setState(() {
        isConnected = res;
      });
    });
  }

  /// besion akoya
  void getCommandes() async {
    try {
      ResponseList responseList =
          await commandeEndPointApi.gettransactionbyjourneeandfpayeUsingGET(
              dataDto.journee.idJournee, 0);
      List<CommandesDtoTransaction> list = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        list = List.from(responseList.objectResponse)
            .map((e) => CommandesDtoTransaction.fromJson(e))
            .toList()
            .where((element) => element.commandeDetails?.isNotEmpty)
            .toList();
      }
      dataDto.listCommande = list;
      dataDto.setCurrentPage(Commandes(dataDto), "Commandes");
    } catch (err) {
      Crashlytics().sendEmail("Transactions getCommandes", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  /// besion akoya

  @override
  void dispose() {
    _focusNodeSelectClient.dispose();
    dataDto.focusNode.dispose();
    super.dispose();
    _timer?.cancel();
  }

/*  registerSocket() {
    if (dataDto.caisseType.code == 'CENTRALE') {
      String url = ServerUrl.url + '/bp-api-admin/jsa-stomp-endpoint';
      if (ServerUrl.url.contains('zull-serveur')) {
        url = 'http://62.171.144.28:8080/bp-api-admin/jsa-stomp-endpoint';
      }

      final stompClient = StompClient(
          config: StompConfig.SockJS(
        url: url,
        onConnect: (StompClient client, StompFrame frame) {
          client.subscribe(
              destination: '/topic/notification',
              callback: (StompFrame frame) {
                debugPrint(frame.body);
                Map<String, dynamic> result = json.decode(frame.body);
                StompMessage _stompMessage = StompMessage.fromJson(result);

*//*if (_stompMessage.idPointVente ==
                        dataDto.pointVente.idPointVente){
                  if (!_stompMessage.body.contains("paiement")){
                    if (_stompMessage.ftraite == 0) {
                      alert(context, _stompMessage);
                    }
                  }else{
                    payer(_stompMessage);
                  }
                }*//*
                if (_stompMessage.idPointVente ==
                        dataDto.pointVente.idPointVente &&
                    _stompMessage.ftraite == 0) {
                  findAllActiveClientByPartenaire();
                  alert(context, _stompMessage);
                }
              });
        },
        onStompError: (error) {
          debugPrint('StompClient: ' + error.toString());
        },
        onWebSocketError: (dynamic error) =>
            debugPrint('StompClient: ' + error.toString()),
      ));
      stompClient.activate();
    }
  }*/

  Future<void> getActions() async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();

    bool actionR = prefs.getBool('action_Remize');
    String codeR = prefs.getString("codeActionRemize");

    if (actionR != null) {
      setState(() {
        actionRemize = actionR;
      });
    }

    if (codeR.isNotEmpty) {
      setState(() {
        codeRemize = codeR;
      });
    }
  }

  Future<void> alert(BuildContext context, StompMessage _stompMessage) async {
    String details = "";
    _stompMessage.details.forEach((element) {
      details = details +
          element.quantite.toString() +
          " X " +
          element.designation +
          '\n';
    });
    String message =
        DateFormat("dd-MM-yyyy HH:mm").format(_stompMessage.dateEnvoie) +
            '\n' +
            _stompMessage.prenom +
            ' ' +
            _stompMessage.nom +
            '\n' +
            _stompMessage.ntel +
            '\n' +
            'Adresse: ' +
            _stompMessage.body +
            '\n' +
            details;

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  message,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('En Attente', style: TextStyle(color: Colors.white)),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: Colors.black26,
              onPressed: () {
                setState(() {
                  dataDto.stompMessage = null;
                  dataDto.numberNotifications++;
                });
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'Valider',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: new Color(0xFFCF5FFE),
              onPressed: () {
                setCurrentStompMessage(_stompMessage);
                setCurrentPage(CommandeScreen(dataDto), 'current');
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  setCurrentStompMessage(StompMessage _stompMessage) {
    callbackNumbers('C', context);
    List<ClientBPrice> clientBPrices = dataDto.listClient
        .where((element) =>
            element.idClientPartenaire == _stompMessage.idClientPartenaire)
        .toList();
    if (clientBPrices.isEmpty) {
      findAllActiveClientByPartenaire();
      clientBPrices = dataDto.listClient
          .where((element) =>
              element.idClientPartenaire == _stompMessage.idClientPartenaire)
          .toList();
    }
    if (clientBPrices.isNotEmpty) {
      setState(() {
        dataDto.clientBPrice = clientBPrices.first;
      });
    }
    setState(() {
      dataDto.stompMessage = _stompMessage;
    });
    for (int i = 0; i < _stompMessage.details.length; i++) {
      var element = _stompMessage.details[i];
      Produit produit = Produit();
      produit.designation = element.designation;
      produit.prixTtc = element.prix;
      produit.idProduit = element.idProduit;
      setState(() {
        number = element.quantite.ceil().toString();
        dataDto.ingredients = element.ingredient;
      });
      callbackCommande(produit, context);
    }
  }

  void callbackNumbersRemise(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        remise.text = "";
      });
      return;
    }
    setState(() {
      this.remise.text += number;
     this. remise.selection =
          TextSelection.fromPosition(TextPosition(offset: remise.text.length));
    });
  }

  void callbackNumbers(String number, BuildContext context) {
    setState(() {
      dataDto.hideKeyboard = true;
    });
    if (dataDto.saved) {
      setState(() {
        dataDto.saved = false;
        dataDto.total = "0.000";
        dataDto.commande = null;
        dataDto.commandeDelete = [];
        dataDto.commandeDetails = [];
        dataDto.clientBPrice = null;
        dataDto.tableCaisse = null;
        dataDto.listClientFiltred = dataDto.listClient;
      });
      return;
    }
    if (number == "C") {
      setState(() {
        dataDto.total = "000.000";
        dataDto.clientBPrice = null;
        dataDto.commandeDetails = [];
        dataDto.commande = null;
        dataDto.commandeDetails = [];
        dataDto.tableCaisse = null;
        dataDto.stompMessage = null;
        this.number = "";
        this.numTable = "";
      });
      setTotalPrice();
      return;
    }

    if (number == "T") {
      getTables();
      getTab();

      return;
    }

    if (number == "N") {
      if (dataDto.tableCaisse != null) {
        printNote(dataDto.tableCaisse);
      }
      return;
    }

    setState(() {
      this.number += number;
      this.numTable += number;
    });
  }

  void setCurrentCommnde(CommandeTransaction commande,
      List<CommandeDetails> commandeDetails, BuildContext context) {
    dataDto.commandeDetails = [];
    List<CommandeDetails> list = [];
    commandeDetails.forEach((element) {
      CommandeDetails commandeDetails = CommandeDetails();
      commandeDetails.prix = element.prix;
      commandeDetails.idProduit = element.idProduit;
      commandeDetails.quantite = element.quantite;
      commandeDetails.idCommande = element.idCommande;
      commandeDetails.idDetailComm = element.idDetailComm;
      commandeDetails.isAnnule = element.isAnnule;
      commandeDetails.designation = element.designation;
      list.add(commandeDetails);
    });

    if (commande.clients.length > 0) {
      print("Table: " + commande.clients.length.toString());
      ClientBPrice clientBPrice = ClientBPrice();
      clientBPrice.nom = commande.clients[0].nom;
      clientBPrice.prenom = commande.clients[0].prenom;
      clientBPrice.idClient = commande.clients[0].idClient;
      clientBPrice.idPartenaire = commande.clients[0].idPartenaire;
      clientBPrice.idClientPartenaire = commande.clients[0].idClientPartenaire;
      clientBPrice.nTel = commande.clients[0].nTel;
      clientBPrice.qrCodeBprice = commande.clients[0].qrCodeBprice;
      clientBPrice.dateNaissance = commande.clients[0].dateNaissance;
      clientBPrice.email = commande.clients[0].email;
      clientBPrice.soldePartn = commande.clients[0].soldeBprice;
      dataDto.clientBPrice = clientBPrice;
    }
    dataDto.commande = commande;
    dataDto.commandeDetails = list;
    dataDto.callbackCommande(null, context);
    dataDto.rendu = null;
    dataDto.payementList = [];
  }

  Future<void> printNote(TableCaisse item) async {
    CommandesDtoTransaction comm = new CommandesDtoTransaction();
    comm.commande = item.commande;
    item.commandeDetails.forEach((element) {
      CommandeDetailsDto j = new CommandeDetailsDto();
      j.commandeDetails = CommandeDetailsTransaction.fromJson(element.toJson());
      j.nomProduit = element.designation;
      comm.commandeDetails.add(j);
    });

    debugPrint('print note ');
    await Utils().printService(context, comm, dataDto);
  }

  void getTables() async {
    try {
      ResponseList responseList =
          await api_instance.findAllByIdPointVenteAndFDefautUsingGET1(
              dataDto.pointVente.idPointVente);
      if (responseList.objectResponse != null) {
        List<TableCaisse> tables = responseList.objectResponse
            .map((e) => TableCaisse.fromJson(e))
            .toList();
        setState(() {
          this.tables = tables;
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("Tables getTables", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  getTab() async {
    final scaffold = ScaffoldMessenger.of(context);
    setState(() {
      dataDto.loadingTable = true;
    });
    getTables();
    await Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        dataDto.loadingTable = false;
      });

      if (tables.isNotEmpty) {
        for (var table in this.tables) {
          if (table.numTable.toString() == numTable) {
            if (table.etat == "occupée") {
              setState(() {
                dataDto.tableCaisse = table;
              });
              print(table.commandeDetails);
              setCurrentCommnde(table.commande, table.commandeDetails, context);
              return;
            } else {
              setState(() {
                dataDto.tableCaisse = table;
              });
              return;
            }
          }
        }
      }
    });

    if (dataDto.tableCaisse == null && numTable != "") {
      scaffold.showSnackBar(
        SnackBar(
          backgroundColor: Color(0xffCF0606),
          content: Text('Table N°' + numTable.toString() + " n'existe pas "),
        ),
      );
    }
    setState(() {
      numTable = "";
      number = "";
    });
  }

  void setTotalPrice() {
    double totalPrice = 0;
    this.dataDto.commandeDetails.forEach((element) {
      totalPrice += element.prix * element.quantite;
    });
    setState(() {
      this.dataDto.total = totalPrice.toStringAsFixed(3);
    });
  }

  void callbackKeyboard(bool isNumericMode) {
    setState(() {
      dataDto.isNumericMode = isNumericMode;
      dataDto.hideKeyboard = false;
    });
  }

  void callbackCommande(var selected, BuildContext context) {
    if (number.isEmpty || number == '0') {
      setState(() {
        number = "1";
      });
    }

    if (selected == null) {
      setTotalPrice();
      return;
    }

    var commande = Commande();
    commande.idPointVente = dataDto.caisse.idPointVente;
    commande.fPaye = 0;
    commande.idSession = dataDto.session.idSession;
    commande.montant = double.parse(dataDto.total);

    if (dataDto.stompMessage != null &&
        dataDto.stompMessage.idMessageCommande != null) {
      commande.idMessageCommande = dataDto.stompMessage.idMessageCommande;
      commande.adresse = dataDto.stompMessage.body;
    }
    var commandeDto = CommandeDto();
    commandeDto.commande = commande;
    commandeDto.idsession = dataDto.session.idSession;
    var commandeDetail;

    if (selected.idProduit != null) {
      if ((selected.mesure == null ||
              selected.mesure.toString().contains('unité')) &&
          isNumeric(this.number)) {
        AlertNotif().alert(context, "Veuillez vérifier la quantité entrée.");
        setState(() {
          this.number = "";
        });
        return;
      }
      debugPrint('taaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaable ' +
          dataDto.ingredients.toString());
      if (dataDto.tableCaisse == null) {
        commandeDetail = this
            .dataDto
            .commandeDetails
            .where((i) => i.idProduit == selected.idProduit)
            .toList();
      } else {
        commandeDetail = this.dataDto.commandeDetails;
      }
    }

    if (selected.idPack != null) {
      commandeDetail = this
          .dataDto
          .commandeDetails
          .where((i) => i.idPack == selected.idPack)
          .toList();
    }

    var objIndex = commandeDetail.length > 0
        ? this.dataDto.commandeDetails.indexOf(commandeDetail[0])
        : -1;
    if (objIndex != -1 &&
        (selected.mesure != null && selected?.mesure?.toString().isEmpty)) {
      this.dataDto.commandeDetails[objIndex].mesure =
          selected.mesure != null ? selected.mesure : '';
      this.dataDto.commandeDetails[objIndex].quantite += double.parse(number);
      this.dataDto.commandeDetails[objIndex].prix = selected.prixTtc == null
          ? selected.prix
          : selected.produitpointvente.prix;
      if (selected.employe != null) {
        this.dataDto.commandeDetails[objIndex].employe = selected.employe;
      }
      var exist = dataDto.commandeDetails[objIndex];
      var idExist = exist.idDetailComm;
      var l = dataDto.commandeDelete
          .where((element) => element.idDetailComm == idExist)
          .toList();
      if (exist.idDetailComm != null && exist.idDetailComm != "") {
        CommandeDetails c = CommandeDetails();
        c.idDetailComm = idExist;
        dataDto.commandeDetails[objIndex].idDetailComm = null;
        if (l.length == 0) {
          dataDto.commandeDelete.add(c);
        }
      }
      setState(() {
        number = "";
      });
      commandeDto.details = this.dataDto.commandeDetails;
      detectPack(commandeDto);
      setTotalPrice();

      return;
    }

    var commandeDetailNew = CommandeDetails();

    if (selected.employe != null) {
      commandeDetailNew.employe = selected.employe;
    }
    commandeDetailNew.prix = selected.prixTtc == null
        ? selected.prix
        : selected.produitpointvente != null
            ? selected.produitpointvente.prix
            : selected.prixTtc;
    commandeDetailNew.quantite = double.parse(number);
    commandeDetailNew.mesure = selected.mesure != null ? selected.mesure : '';
    commandeDetailNew.idProduit = selected.idProduit;
    commandeDetailNew.idPack = selected.idPack;
    commandeDetailNew.designation = selected.designation;
    commandeDetailNew.ingredients =
        dataDto.ingredients != null && dataDto.ingredients.isNotEmpty
            ? dataDto.ingredients
            : '';

    commandeDetailNew.idsIngrdComp =
        dataDto.idsIngrdComp != null && dataDto.idsIngrdComp.isNotEmpty
            ? dataDto.idsIngrdComp
            : null;
    debugPrint('ingriiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiid ' +
        commandeDetailNew.ingredients.toString());
    setState(() {
      this.dataDto.commandeDetails.add(commandeDetailNew);
      number = "";
      numTable = "";
      dataDto.ingredients = null;
      dataDto.idsIngrdComp = [];
    });
    commandeDto.details = this.dataDto.commandeDetails;
    debugPrint('ingriiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiid ' +
        this.dataDto.commandeDetails.toString());
    detectPack(commandeDto);
    setTotalPrice();
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return s.isNotEmpty && s.contains('.');
  }

  void setCurrentPage(Widget widget, String current) {
    setState(() {
      dataDto.hideKeyboard = true;
      dataDto.textController.clear();
      dataDto.searchController.clear();
    });
    if (dataDto.saved) {
      setState(() {
        dataDto.saved = false;
        dataDto.total = "0.000";
        dataDto.commande = null;
        dataDto.commandeDelete = [];
        dataDto.commandeDetails = [];
        dataDto.clientBPrice = null;
        dataDto.tableCaisse = null;
        dataDto.listClientFiltred = dataDto.listClient;
      });
    }
    ;

    switch (current) {
      case "Commandes":
        {
          setState(() {
            dataDto.hint = "Rechercher Commande";
          });
        }
        break;

      case "Tickets":
        {
          setState(() {
            dataDto.hint = "Rechercher Ticket";
          });
        }
        break;

      case "Transactions":
        {
          setState(() {
            dataDto.hint = "Rechercher";
            dataDto.searchController.clear();
          });
        }
        break;

      case "Clients":
        {
          setState(() {
            dataDto.hint = "Rechercher Client";
            dataDto.searchController.clear();
          });
        }
        break;

      case "ReservationList":
        {
          setState(() {
            dataDto.hint = "Rechercher Reservation";
            dataDto.searchController.clear();
          });
        }
        break;

      case "CommandeScreen":
        {
          setState(() {
            dataDto.hint = "Rechercher Produit";
            dataDto.searchController.clear();
          });
        }
        break;

      case "CommandeScreen":
        {
          setState(() {
            dataDto.hint = "Rechercher Produit";
            dataDto.searchController.clear();
          });
        }
        break;

      case "Tables":
        {
          setState(() {
            dataDto.hint = "Rechercher Table";
            dataDto.searchController.clear();
          });
        }
        break;

      case "NewReservation":
        {
          setState(() {
            dataDto.hint = "Rechercher Client";
            dataDto.searchController.clear();
          });
        }
        break;

      case "Recharge":
        {
          setState(() {
            dataDto.hint = "Rechercher Client";
            dataDto.searchController.clear();
          });
        }
        break;

      case "Payement":
        {
          setState(() {
            dataDto.hint = "Rechercher Client";
            dataDto.searchController.clear();
          });
        }
        break;

      case "Reservation":
        {
          setState(() {
            dataDto.hint = dataDto.pointVente.fAffectEmployetoservice == 1
                ? "Rechercher employé"
                : "Rechercher";
          });
        }
        break;

      default:
        {
          setState(() {
            dataDto.hint = "Rechercher";
            dataDto.searchController.clear();
          });
        }
        break;
    }
    setState(() {
      currentPage = widget;
      this.current = current;
      dataDto.searchController.text = "";
    });
  }

  void setClientBPrice(ClientBPrice clientBPrice) {
    setState(() {
      dataDto.clientBPrice = clientBPrice;
    });
  }

  void searchClient(String hint) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        dataDto.hint = hint;
        searchController.clear();
        dataDto.searchController.clear();
      });
    });
  }

  void searchValue(String value) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        dataDto.searchController.text = value;
      });
    });
  }

  Future<void> showTableDialog(int paye) async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      final api_instance = new TableCaisseEndPointApi();

      ResponseList responseList =
          await api_instance.findAllByIdPointVenteAndFDefautUsingGET1(
              dataDto.pointVente.idPointVente);
      List<TableCaisse> tables = [];
      if (responseList.objectResponse != null) {
        tables = responseList.objectResponse
            .map((e) => TableCaisse.fromJson(e))
            .toList();
      }

      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                backgroundColor: Color(0xffEEEEEE),
                title: Text(
                  dataDto.pointVente.fGestionTable == 2
                      ? 'Veuillez sélectionner un Bipeur'
                      : 'Veuillez sélectionner une table',
                  textAlign: TextAlign.center,
                ),
                titleTextStyle: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
                content: Container(
                  height: height * 0.60,
                  width: width * 0.5,
                  padding: EdgeInsets.all(16),
                  child: tables.length > 0
                      ? GridView.count(
                          padding: EdgeInsets.only(top: 8),
                          crossAxisCount: width > height ? 6 : 3,
                          mainAxisSpacing: 8.0,
                          crossAxisSpacing: 8.0,
                          childAspectRatio: 1,
                          children: tables
                              .map(
                                (item) => SizedBox(
                                  child: WidgetANimator(FlatButton(
                                      color: Color(0xffE7D7EE),
                                      padding: EdgeInsets.all(0),
                                      onPressed: () => {
                                            setState(() {
                                              dataDto.tableCaisse = item;
                                            }),
                                            if (paye == 2)
                                              {
                                                if (item.etat == "occupéé" ||
                                                    item.commande != null)
                                                  setState(() {
                                                    item.etat = "libre";
                                                    item.commande = null;
                                                    item.commandeDetails = null;
                                                  }),
                                                api_instance
                                                    .updateTableCaisseUsingPUT(
                                                        item),
                                                createCommande(1)
                                              }
                                            else if (paye == 3)
                                              createCommande(0)
                                            else
                                              createCommande(paye),
                                            Navigator.pop(context)
                                          },
                                      child: Stack(
                                        fit: StackFit.expand,
                                        children: [
                                          Positioned(
                                            top: 0,
                                            right: 0,
                                            child: Container(
                                              color: item.etat == "occupée"
                                                  ? Color(0xffCF0606)
                                                  : Colors.green,
                                              width: 16,
                                              height: 16,
                                            ),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                dataDto.pointVente
                                                            .fGestionTable ==
                                                        1
                                                    ? "Table N°"
                                                    : "Bipeur N°",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: width > height
                                                        ? width / 80
                                                        : width / 35,
                                                    color: Color(0xff352C60)),
                                              ),
                                              Text(
                                                item.numTable.toString(),
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: width > height
                                                        ? width / 80
                                                        : width / 35,
                                                    color: Color(0xff352C60)),
                                              ),
                                              Text(
                                                item.commande != null
                                                    ? item.commande.montant
                                                            .toStringAsFixed(
                                                                2) +
                                                        " DT "
                                                    : "",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: width > height
                                                        ? width / 130
                                                        : width / 35,
                                                    color: Color(0xff352C60)),
                                              ),
                                            ],
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            right: -5,
                                            child: item.etat == "occupée"
                                                ? IconButton(
                                                    color: Colors.deepPurple,
                                                    iconSize: 15,
                                                    icon:
                                                        const Icon(Icons.login),
                                                    padding: EdgeInsets.all(0),
                                                    onPressed: () => {
                                                      setState(() {
                                                        item.etat = "libre";
                                                        item.commande = null;
                                                        item.commandeDetails =
                                                            null;
                                                      }),
                                                      api_instance
                                                          .updateTableCaisseUsingPUT(
                                                              item),
                                                    },
                                                  )
                                                : SizedBox(),
                                          ),
                                        ],
                                      ))),
                                ),
                              )
                              .toList(),
                        )
                      : Container(
                          alignment: Alignment.center,
                          child: Text(
                            "Aucune table n'est disponible pour le moment",
                            style: TextStyle(
                                color: Color(0xffCF0606),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                ),
                actions: <Widget>[
                  FlatButton(
                    color: Colors.green,
                    child: Text('Libérer tous les Bipeurs'),
                    onPressed: () {
                      tables.forEach((item) {
                        if (item.etat == "occupéé" || item.commande != null) {
                          setState(() {
                            item.etat = "libre";
                            item.commande = null;
                            item.commandeDetails = null;
                          });
                          api_instance.updateTableCaisseUsingPUT(item);
                        }
                      });
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  FlatButton(
                    color: Color(0xffCF0606),
                    child: Text(
                      'Annuler',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("Dashboard showTableDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> selectModeTable(int paye) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Sur place ou à emporter',
            textAlign: TextAlign.center,
          ),
          content: SingleChildScrollView(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: SizeClacule().calcule(context, 15),
                  width: SizeClacule().calcule(context, 7),
                  child: paye != 2
                      ? FlatButton(
                          color: Color(0xffCF5FFE),
                          onPressed: () {
                            Navigator.of(context).pop();
                            setState(() {
                              dataDto.commandeType = 'emporter';
                            });
                            createCommande(paye);
                          },
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Text(
                            "à emporter",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ))
                      : null,
                ),
                SizedBox(
                  width: 16,
                ),
                SizedBox(
                  height: SizeClacule().calcule(context, 15),
                  width: SizeClacule().calcule(context, 7),
                  child: dataDto.pointVente.fGestionTable != 0
                      ? FlatButton(
                          color: Color(0xffCF5FFE),
                          onPressed: () {
                            Navigator.of(context).pop();
                            setState(() {
                              dataDto.commandeType = 'table';
                            });
                            showTableDialog(paye);
                          },
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Text(
                            dataDto.pointVente.fGestionTable == 1
                                ? "Table"
                                : "Bipeur",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ))
                      : SizedBox(),
                ),
                SizedBox(
                  width: 16,
                ),
                SizedBox(
                  height: SizeClacule().calcule(context, 15),
                  width: SizeClacule().calcule(context, 7),
                  child: FlatButton(
                      color: Color(0xffCF5FFE),
                      onPressed: () {
                        Navigator.of(context).pop();
                        setState(() {
                          dataDto.commandeType = 'livraison';
                        });
                        if (dataDto.pointVente.fGestionTable == null ||
                            dataDto.pointVente.fGestionTable == 1)
                          createCommande(paye);
                        else
                          showTableDialog(3);
                      },
                      padding: EdgeInsets.only(left: 16, right: 16),
                      child: Text(
                        "Livraison",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      )),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
                color: Color(0xffCF0606),
                onPressed: () => {Navigator.of(context).pop()},
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Text(
                  "Annuler",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center,
                )),
          ],
        );
      },
    );
  }

  filterClientList(String qr) {
    if (qr != "" && qr.length == 10) {
      var list = dataDto.listClientFiltred
          .where((element) =>
              element.qrCodePartn != null && element.qrCodePartn == qr)
          .toList();
      ClientBPrice c = list.length > 0 ? list[0] : null;

      c != null
          ? setState(() {
              dataDto.clientBPrice = c;
              error.text = "";
              qrCode.text = "";
              Navigator.of(context).pop();
              createCommande(1);
            })
          : Toast.show("QR Code non valide", context,
              duration: Toast.LENGTH_LONG,
              gravity: Toast.CENTER,
              backgroundColor: Color(0xffCF0606),
              textColor: Colors.white);
    }
  }

  handleKey(RawKeyEvent event, BuildContext context) {
    if (event.character != null) {
      if (event.physicalKey == PhysicalKeyboardKey.escape) {
        setState(() {
          _keyCode = '';
          dataDto.searchController.clear();
          dataDto.textController.clear();
          error.text = "";
          qrCode.text = "";
        });
        return;
      }
      _keyCode += event.character;
      debugPrint("RawKeyEvent: $_keyCode");
      List<ClientBPrice> list = dataDto.listClientFiltred
          .where((element) =>
              element.qrCodePartn != null &&
              element.qrCodePartn.toLowerCase() == _keyCode.toLowerCase())
          .toList();
      if (list.isNotEmpty) {
        setState(() {
          _keyCode = '';
          dataDto.clientBPrice = list[0];
          error.clear();
          qrCode.clear();
          Navigator.of(context).pop();
          createCommande(1);
        });
      }
    }
  }

  Future<void> getClientBPrice() {
    if (dataDto.saved) {
      setState(() {
        dataDto.saved = false;
        loading = false;
        loadingEsp = false;
        dataDto.total = "0.000";
        dataDto.commande = null;
        dataDto.commandeDelete = [];
        dataDto.commandeDetails = [];
        dataDto.clientBPrice = null;
        dataDto.tableCaisse = null;
        dataDto.listClientFiltred = dataDto.listClient;
      });
      return null;
    }
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    if (dataDto.commandeDetails.length == 0 ||
        dataDto.clientBPrice != null ||
        !ServerUrl.fid) {
      createCommande(1);
      return null;
    }
    _focusNodeSelectClient.requestFocus();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  'Si vous avez une carte FID\n\nVeuillez scanner le QR Code\nou saisir le code manuellement',
                  textAlign: TextAlign.center,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Align(
                    alignment: Alignment.centerRight,
                    child: CircleAvatar(
                      child: Icon(
                        Icons.clear,
                        color: Colors.white,
                      ),
                      radius: 14,
                      backgroundColor: Color(0xffCF0606),
                    )),
              ),
            ],
          ),
          content: RawKeyboardListener(
            onKey: (RawKeyEvent event) {
              handleKey(event, context);
            },
            focusNode: _focusNodeSelectClient,
            child: Container(
              width: width / 3,
              height: height / 6,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextField(
                      onChanged: (text) {
                        filterClientList(text);
                        setState(() {
                          error.text = "";
                        });
                      },
                      autofocus: false,
                      cursorColor: Color(0xff352C60),
                      controller: qrCode,
                      obscureText: false,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xfff5f5f5),
                        contentPadding: EdgeInsets.all(width / 50),
                        hintText: "QR Code",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        suffixIcon: Container(
                          margin: EdgeInsets.only(right: 16, bottom: 8, top: 8),
                          child: IconButton(
                            onPressed: () {
                              setState(() {
                                _keyCode = '';
                                qrCode.clear();
                                error.clear();
                              });
                            },
                            icon: Icon(Icons.clear),
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            SizedBox(
              child: FlatButton(
                color: Color(0xffCF0606),
                child: Text(
                  'Non',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    error.text = "";
                    qrCode.text = "";
                  });
                  createCommande(1);
                },
              ),
              width: width / 10,
              height: width / 20,
            ),
            SizedBox(
              width: width / 10,
              height: width / 20,
            ),
            SizedBox(
              child: FlatButton(
                color: Colors.purpleAccent,
                child: Text(
                  'Sélectionner un client',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    error.text = "";
                    qrCode.text = "";
                  });
                  dataDto.setCurrentPage(Clients(dataDto), "");
                },
              ),
              width: width / 10,
              height: width / 20,
            ),
            SizedBox(
                child: FlatButton(
                  color: Colors.green,
                  child: Text(
                    'Valider',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (qrCode.text != "") {
                      var list = dataDto.listClientFiltred
                          .where((element) =>
                              element.qrCodePartn != null &&
                              element.qrCodePartn == qrCode.text)
                          .toList();
                      ClientBPrice c = list.length > 0 ? list[0] : null;
                      c != null
                          ? setState(() {
                              dataDto.clientBPrice = c;
                              error.text = "";
                              qrCode.text = "";
                              Navigator.of(context).pop();
                              createCommande(1);
                            })
                          : Toast.show("QR Code non valide", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.CENTER,
                              backgroundColor: Color(0xffCF0606),
                              textColor: Colors.white);
                    }
                  },
                ),
                width: width / 10,
                height: width / 20),
          ],
        );
      },
    ).whenComplete(() {
      setState(() {
        _keyCode = '';
        _focusNodeSelectClient.unfocus();
      });
    });
  }

  Future<void> showClientMyDialog() async {
    if (dataDto.commandeDetails.length == 0 || dataDto.clientBPrice != null) {
      createCommande(1);
      return;
    }
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('BPrice'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Avez-vous une carte FID ?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Color(0xffCF0606),
              child: Text('Non'),
              onPressed: () {
                Navigator.of(context).pop();
                createCommande(1);
              },
            ),
            FlatButton(
              color: Colors.green,
              child: Text('Oui'),
              onPressed: () {
                Navigator.of(context).pop();
                dataDto.setCurrentPage(Clients(dataDto), "");
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showRemiseMyDialog(
      CommandeDetails commandeDetails, int index) async {


    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          title: Text('Remise ' + commandeDetails.designation),
          content: Container(
            width: width / 3,
            height: height * 0.55,
            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Prix initial",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        TextField(
                            cursorColor: Colors.black,
                            textAlign: TextAlign.center,
                            expands: false,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              enabled: false,
                              hintStyle: TextStyle(color: Colors.black),
                              hintText: commandeDetails.prix.toStringAsFixed(
                                      int.parse(dataDto
                                                  .pointVente.chiffrevirgule !=
                                              null
                                          ? dataDto.pointVente.chiffrevirgule
                                          : '3')) +
                                  " DT",
                              filled: true,
                              contentPadding: EdgeInsets.only(left: 8),
                              fillColor: Colors.transparent,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                            )),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Prix final",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        TextField(
                            cursorColor: Colors.black,
                            textAlign: TextAlign.center,
                            expands: false,
                            controller: montant,
                            onChanged: (m) {
                              remise.text = m != ""
                                  ? ((100 -
                                          (double.parse(m) /
                                                  commandeDetails.prix) *
                                              100))
                                      .toStringAsFixed(3)
                                  : "";
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: commandeDetails.prix.toStringAsFixed(
                                  int.parse(
                                      dataDto.pointVente.chiffrevirgule != null
                                          ? dataDto.pointVente.chiffrevirgule
                                          : "3")),
                              filled: true,
                              contentPadding: EdgeInsets.only(left: 8),
                              fillColor: Colors.transparent,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                            )),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Remise (%)",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        TextField(
                            cursorColor: Colors.black,
                            textAlign: TextAlign.center,
                            expands: false,
                            controller: remise,
                            keyboardType: TextInputType.number,
                            onChanged: (r) {
                              montant.text = r != ""
                                  ? (commandeDetails.prix -
                                          (commandeDetails.prix / 100) *
                                              double.parse(r))
                                      .toStringAsFixed(3)
                                  : "";
                            },
                            decoration: InputDecoration(
                              hintText: "0",
                              filled: true,
                              contentPadding: EdgeInsets.only(left: 8),
                              fillColor: Colors.transparent,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                 alignment: Alignment.center,
                 color: Colors.transparent,
                 width: MediaQuery.of(context).size.width * 0.3,
                 height: MediaQuery.of(context).size.width * 0.21,
                 child: NumbersLogin(callbackNumbersRemise),
               ),
            ]),
          ),
          actions: <Widget>[
            FlatButton(
              color: Color(0xffCF0606),
              child: Text(
                'Annuler',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.all(16),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  montant.text = "";
                  remise.text = "";
                });
              },
            ),
            FlatButton(
              color: Colors.green,
              child: Text(
                'Valider',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.all(16),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  dataDto.commandeDetails.elementAt(index).prixSansRemise =
                      commandeDetails.prix;
                  dataDto.commandeDetails.elementAt(index).prix =
                      double.parse(montant.text);
                  dataDto.commandeDetails.elementAt(index).remise =
                      double.parse(remise.text);
                  montant.text = "";
                  remise.text = "";
                });
                setTotalPrice();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showMontantParMoyenPaiement(PreColture colture) async {
    if (colture.montantParModeRegl == null) {
      colture.montantParModeRegl = [];
    }
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          backgroundColor: Color(0xffEEEEEE),
          contentPadding: EdgeInsets.all(0),
          scrollable: true,
          content: Container(
            height: height * 0.6,
            width: width * 0.6,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        "Montant par moyen de paiement",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff352C60),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      width: width * 0.6,
                      padding: EdgeInsets.all(30),
                      margin: EdgeInsets.only(bottom: 16),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text(
                            "C.A.Session :  " +
                                colture.chiffreAffaire.toStringAsFixed(
                                    int.parse(
                                        dataDto.pointVente.chiffrevirgule !=
                                                null
                                            ? dataDto.pointVente.chiffrevirgule
                                            : "3")) +
                                "DT",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff352C60),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          width: width * 0.19,
                          padding: EdgeInsets.only(bottom: 10),
                          margin: EdgeInsets.only(bottom: 16),
                        ),
                        Container(
                          child: Text(
                            "C.A.Ouverture :  " +
                                colture.montantOuverture.toStringAsFixed(
                                    int.parse(
                                        dataDto.pointVente.chiffrevirgule !=
                                                null
                                            ? dataDto.pointVente.chiffrevirgule
                                            : "3")) +
                                ' DT',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff352C60),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          width: width * 0.19,
                          padding: EdgeInsets.only(bottom: 10),
                          margin: EdgeInsets.only(bottom: 16),
                        ),
                        Container(
                          child: Text(
                            "M.C.non payè :  " +
                                colture.montantNonEncorePayes.toStringAsFixed(
                                    int.parse(
                                        dataDto.pointVente.chiffrevirgule !=
                                                null
                                            ? dataDto.pointVente.chiffrevirgule
                                            : "3")) +
                                ' DT',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff352C60),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          width: width * 0.19,
                          padding: EdgeInsets.only(bottom: 10),
                          margin: EdgeInsets.only(bottom: 16),
                        ),
                      ],
                    ),
                    Expanded(
                        child: GridView.count(
                      padding: EdgeInsets.only(top: 8, left: 16),
                      crossAxisCount: width > height ? 4 : 2,
                      mainAxisSpacing: 8.0,
                      crossAxisSpacing: 8.0,
                      childAspectRatio: 2,
                      children: colture.montantParModeRegl
                          .map(
                            (item) => SizedBox(
                              child: Container(
                                  alignment: Alignment.center,
                                  color: Color(0xffE7D7EE),
                                  padding: EdgeInsets.all(0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        item.fnum == 1
                                            ? item.nombreReg.toString() +
                                                " " +
                                                item.designation
                                            : item.designation,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 16,
                                      ),
                                      Text(item.totale.toStringAsFixed(
                                              int.parse(dataDto.pointVente
                                                          .chiffrevirgule !=
                                                      null
                                                  ? dataDto
                                                      .pointVente.chiffrevirgule
                                                  : "3")) +
                                          " DT")
                                    ],
                                  )),
                            ),
                          )
                          .toList(),
                    ))
                  ],
                ),
                Center(
                  child: Lottie.asset(
                      'assets/qr-code-home/animation-montant.json',
                      height: height * 0.4),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Color(0xffCF0606),
              child: Text(
                'Annuler',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void getMontantParMoyenPaiement() async {
    try {
      setState(() {
        isLoading = true;
      });
      PreColture colture = await ClotureRequest().getPreColture(dataDto);
      showMontantParMoyenPaiement(colture);
      setState(() {
        isLoading = false;
      });
    } catch (err) {
      Crashlytics().sendEmail("Montant par moyen de paiement", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      setState(() {
        isLoading = false;
      });
    }
  }

  void showCodeActionRemize(int remizeGlobal,
      {CommandeDetails commandeDetails, int index}) {
    final scaffold = ScaffoldMessenger.of(context);
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Veuillez saisir le Code de validation ',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                SizedBox(height: 16.0),
                TextField(
                    obscureText: true,
                    cursorColor: Color(0xff352C60),
                    controller: codeRemizeController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xfff5f5f5),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Code Action",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    )),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xffCF0606),
              onPressed: () {
                codeRemizeController.clear();
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'Valider',
                style: TextStyle(color: Colors.white),
              ),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xFF32C533),
              onPressed: () {
                if (codeRemize.toLowerCase() ==
                    codeRemizeController.text.toLowerCase()) {
                  codeRemizeController.clear();
                  Navigator.of(context).pop();
                  remizeGlobal == 1
                      ? this.showRemiseGlobalMyDialog()
                      : this.showRemiseMyDialog(commandeDetails, index);
                } else {
                  scaffold.showSnackBar(
                    SnackBar(
                      backgroundColor: Color(0xffCF0606),
                      content: Text("Code Action invalide"),
                    ),
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showRemiseGlobalMyDialog() async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          title: Text('Remise Globale'),
          content: Container(
            width: width / 3,
            height: height * 0.55,
            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Prix initial",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        TextField(
                            cursorColor: Colors.black,
                            textAlign: TextAlign.center,
                            expands: false,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              enabled: false,
                              hintStyle: TextStyle(color: Colors.black),
                              hintText: dataDto.total.toString() + " DT",
                              filled: true,
                              contentPadding: EdgeInsets.only(left: 8),
                              fillColor: Colors.transparent,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                            )),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Prix final",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        TextField(
                            cursorColor: Colors.black,
                            textAlign: TextAlign.center,
                            expands: false,
                            controller: montant,
                            onChanged: (m) {
                              remise.text = m != ""
                                  ? ((100 -
                                          (double.parse(m) /
                                                  double.parse(dataDto.total)) *
                                              100))
                                      .toStringAsFixed(2)
                                  : "";
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: dataDto.total.toString(),
                              filled: true,
                              contentPadding: EdgeInsets.only(left: 8),
                              fillColor: Colors.transparent,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                            )),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Remise (%)",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        TextField(
                            cursorColor: Colors.black,
                            textAlign: TextAlign.center,
                            expands: false,
                            controller: remise,
                            keyboardType: TextInputType.number,
                            onChanged: (r) {
                              montant.text = r != ""
                                  ? (double.parse(dataDto.total) -
                                          (double.parse(dataDto.total) / 100) *
                                              double.parse(r))
                                      .toStringAsFixed(3)
                                  : "";
                            },
                            decoration: InputDecoration(
                              hintText: "0",
                              filled: true,
                              contentPadding: EdgeInsets.only(left: 8),
                              fillColor: Colors.transparent,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(0)),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FlatButton(
                    color: Colors.green,
                    child: Text(
                      'Gratuit',
                      style: TextStyle(
                          fontWeight: FontWeight.w500, color: Colors.white),
                    ),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 100 / 100))
                            .toString();
                        remise.text = "100";
                      });
                    },
                  ),
                  FlatButton(
                    color: Color(0xff352C60),
                    child: Text('80%',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 80 / 100))
                            .toString();
                        remise.text = "80";
                      });
                    },
                  ),
                  FlatButton(
                    color: Color(0xff352C60),
                    child: Text('70%',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 70 / 100))
                            .toString();
                        remise.text = "70";
                      });
                    },
                  ),
                  FlatButton(
                    color: Color(0xff352C60),
                    child: Text('60%',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 60 / 100))
                            .toString();
                        remise.text = "60";
                      });
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 12,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FlatButton(
                    color: Colors.green,
                    child: Text(
                      '50%',
                      style: TextStyle(
                          fontWeight: FontWeight.w500, color: Colors.white),
                    ),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 50 / 100))
                            .toString();
                        remise.text = "50";
                      });
                    },
                  ),
                  FlatButton(
                    color: Color(0xff352C60),
                    child: Text('40%',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 40 / 100))
                            .toString();
                        remise.text = "40";
                      });
                    },
                  ),
                  FlatButton(
                    color: Color(0xff352C60),
                    child: Text('30%',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 30 / 100))
                            .toString();
                        remise.text = "30";
                      });
                    },
                  ),
                  FlatButton(
                    color: Color(0xff352C60),
                    child: Text('20%',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 20 / 100))
                            .toString();
                        remise.text = "20";
                      });
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 12,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FlatButton(
                    color: Colors.green,
                    child: Text(
                      '10%',
                      style: TextStyle(
                          fontWeight: FontWeight.w500, color: Colors.white),
                    ),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 10 / 100))
                            .toString();
                        remise.text = "10";
                      });
                    },
                  ),
                  FlatButton(
                    color: Color(0xff352C60),
                    child: Text('5%',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      setState(() {
                        montant.text = (double.parse(dataDto.total) -
                                (double.parse(dataDto.total) * 5 / 100))
                            .toString();
                        remise.text = "5";
                      });
                    },
                  ),
                ],
              ),
              // Container(
              //   alignment: Alignment.center,
              //   color: Colors.transparent,
              //   width: MediaQuery.of(context).size.width * 0.3,
              //   height: MediaQuery.of(context).size.width * 0.2,
              //   child: NumbersLogin(callbackNumbersRemise),
              // ),
            ]),
          ),
          actions: <Widget>[
            FlatButton(
              color: Color(0xffCF0606),
              child: Text(
                'Annuler',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.all(16),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  montant.text = "";
                  remise.text = "";
                });
              },
            ),
            FlatButton(
              color: Colors.green,
              child: Text(
                'Valider',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.all(16),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  dataDto.total = montant.text;
                  dataDto.commandeDetails.forEach((element) {
                    element.remise = double.parse(remise.text);
                    element.prixSansRemise = element.prix;
                    element.prix = element.prix -
                        (element.prix * double.parse(remise.text) / 100);
                    element.isModifier = 1;
                  });
                  montant.text = "";
                  remise.text = "";
                });
                setTotalPrice();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> getActionAnnuTicket() async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    bool impAnnu = prefs.getBool('imp_Annulation');

    if (impAnnu != null) {
      setState(() {
        impAnnulation = impAnnu;
      });
    }
  }

  Future<void> createCommande(int paye) async {
    try {
      getActionAnnuTicket();
      if (this.dataDto.commandeDelete.length == 0 &&
          this.dataDto.commandeDetails.length == 0) {
        return;
      }
      if (this.dataDto.commandeDelete.length > 0) {
        for (CommandeDetails element in this.dataDto.commandeDelete) {
          debugPrint(
              "************takrizzzzzz***************************$element.idDetailComm");
          if (element.idDetailComm != null && element.idDetailComm.isNotEmpty) {
            await CommandeRequest().cancelOrderDetails(element.idDetailComm);
          }
        }
      }

      List<CommandeDetails> listCommande = [];

      if (dataDto.commandeDetails.length > 0) {
        for (CommandeDetails element in dataDto.commandeDetails) {
          debugPrint(
              "************************************id*****************************************$element.idDetailComm");

          if (element.idDetailComm == null ||
              element.idDetailComm.isEmpty ||
              element.isModifier == 1) {
            listCommande.add(element);
          }
        }
      }

      if (listCommande.length == 0 && paye == 0) {
        if (dataDto.pointVente.fImprimCuisine == 1) {
          if (dataDto.commandeDelete.length > 0 && impAnnulation == true) {
            print(dataDto.commandeDelete.length.toString() +
                'commande annuleeeeeeeeeeeeeeeeeeeee');
            await Utils().cuisineCommandeAnnulee(
                context, dataDto.commandeDelete, dataDto);
          }
        }
        setState(() {
          loading = false;
          loadingEsp = false;
          dataDto.stompMessage = null;
          dataDto.ingredients = null;
          dataDto.idsIngrdComp = [];
          dataDto.commandeType = null;
          dataDto.commandeDelete = [];
        });
        callbackNumbers("C", context);
        return;
      }
      if (this.dataDto.commandeDetails.length == 0) {
        return;
      }

      setState(() {
        paye == 0 ? loading = true : loadingEsp = true;
      });

      ResponseSingle responseSingle = await CommandeRequest()
          .createCommandeEsp(paye, this.dataDto, context);
      if (responseSingle.result != 1) {
        AlertNotif().alert(context, responseSingle.errorDescription);
        setState(() {
          loading = false;
          loadingEsp = false;
        });
        return;
      }
      if (dataDto.pointVente.fImprimCuisine == 1) {
        if (dataDto.commandeDelete.length > 0 &&
            impAnnulation == true &&
            impAnnulation != null) {
          print(dataDto.commandeDelete.length.toString() +
              'commande annuleeeeeeeeeeeeeeeeeeeee');
          await Utils()
              .cuisineCommandeAnnulee(context, dataDto.commandeDelete, dataDto);
        }
        await Utils().cuisine(context, listCommande, dataDto,
            Commande.fromJson(responseSingle.objectResponse));
      }
      if (dataDto.stompMessage != null) {
        String url = ServerUrl.url + '/bp-api-admin/v1/addNotification';
        setState(() {
          List<Details> listNew = [];
          dataDto.stompMessage.ftraite = 1;
          dataDto.stompMessage.dateTraite = DateTime.now();

          setState(() {
            dataDto.stompMessage.details = [];
          });
          for (var item in dataDto.commandeDetails) {
            Details detail = Details();
            detail.prix = item.prix;
            detail.ingredient = item.ingredients;
            detail.designation = item.designation;
            detail.idProduit = item.idProduit;
            detail.quantite = item.quantite;
            listNew.add(detail);
          }
          setState(() {
            dataDto.stompMessage.details = listNew;
          });
        });
        final res = await http.post(Uri.parse(url),
            body: jsonEncode(dataDto.stompMessage),
            headers: {"Content-Type": "application/json"});
        if (res.statusCode >= 400) {
          debugPrint(res.statusCode.toString() + ' ' + res.body);
        } else if (res.body != null) {
          debugPrint(res.body);
        }
      }

      setState(() {
        loading = false;
        loadingEsp = false;
        dataDto.saved = true;
        dataDto.stompMessage = null;
        dataDto.ingredients = null;
        dataDto.idsIngrdComp = [];
        dataDto.commandeType = null;
        dataDto.commandeDelete = [];
      });
    } catch (err) {
      debugPrint('createCommande ' + err.toString());
      setState(() {
        loading = false;
        loadingEsp = false;
      });
      Crashlytics().sendEmail("Dashboard createCommande", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  ///Paiement par fidilitè ( App Customer )
/*  payer(StompMessage message) async {

    setState(() {
      dataDto.payementList=[];
    });
    ResponseList responseList =
    await commandeEndPointApi.gettransactionbyjourneeandfpayeUsingGET(
        dataDto.journee.idJournee, 0);
    CommandesDtoTransaction test ;
    if (responseList.result == 1 && responseList.objectResponse != null) {
       test = List.from(responseList.objectResponse)
          .map((e) => CommandesDtoTransaction.fromJson(e))
          .toList()
          .where((element) => element.commande.idMessageCommande==message.idMessageCommande)
          .toList()[0];
    }

    ModeReglement reglement =
    dataDto.modeReg.where((i) => i.ffidelite == 1).toList()[0];
    ReglmDetailsDto m = ReglmDetailsDto();
    m.montant = test.commande.montant;
    m.idModReg = reglement.idModeReglement;
    m.idClientPartenaire = message.idClientPartenaire;
    dataDto.payementList.add(m);
    dataDto.payementList.forEach((element) {
      element.idCommande = test.commande.idCommande;
    });
    ReglementDto reglementDto = ReglementDto();
    reglementDto.idPointVente = dataDto.caisse.idPointVente;
    reglementDto.dateTransaction = new DateTime.now();
    reglementDto.idCommande = test.commande.idCommande;
    reglementDto.reglements = dataDto.payementList;

    ResponseSingle response =
    await paymentEndPointApi.checkAndSavePaymentUsingPOST(reglementDto);
    CommandesDtoTransaction commandesDtoTransaction =
    CommandesDtoTransaction();
    commandesDtoTransaction.commande.idCommande = test.commande.idCommande;
    commandesDtoTransaction.commande.montant = test.commande.montant;
    commandesDtoTransaction.commande.numCommande = test.commande.numCommande;
    commandesDtoTransaction.commande.dateCreation =
        test.commande.dateCreation;
    test.commande.clients.forEach((element) {
      commandesDtoTransaction.commande.clients
          .add(ClientTransaction.fromJson(element.toJson()));
    });
    if(dataDto.commande!=null&& dataDto.commande.clients!=null)
      commandesDtoTransaction.commande.clients=dataDto.commande.clients;
    commandesDtoTransaction.commandeDetails = [];
    dataDto.commandeDetails.forEach((element) {
      CommandeDetailsDto commandeDetailsDto = CommandeDetailsDto();
      commandeDetailsDto.nomProduit = element.designation;
      commandeDetailsDto.commandeDetails =
          CommandeDetailsTransaction.fromJson(element.toJson());
      commandesDtoTransaction.commandeDetails.add(commandeDetailsDto);
    });
    if (response.result == 1) {
      Utils().printService(context, commandesDtoTransaction, dataDto);
      CommandePartnerDto commandePartnerDto = CommandePartnerDto();
      commandePartnerDto.idPartenaire = dataDto.utilisateur.idPartenaire;
      commandePartnerDto.idCommande = test.commande.idCommande;
      fidCredEndPointApi.checkAndSavePaymentUsingPOST(commandePartnerDto);
    }

    setState(() {
      dataDto.payementList=[];
    });
  }*/

  Future<void> detectPack(CommandeDto commandeDto) async {
    try {
      PackEndPointApi packEndPointApi = PackEndPointApi();
      commandeDto.details.forEach((elements) async {
        if (elements.idPack != null) {
          ResponseSingle response =
              await packEndPointApi.findByIdPacktUsingGET(elements.idPack);

          if (response.result == 1) {
            Pack pack = Pack.fromJson(response.objectResponse);
            commandeDto.details.remove(elements);
            pack.produits.forEach((element) {
              CommandeDetails details = CommandeDetails();
              details.idProduit = element.idProduit;
              details.quantite = element.quantite;
              commandeDto.details.add(details);
            });
          }
        }
      });
      ResponseSingle responseSingle = await orderingEndPointApi
          .detectPackAndRemiseAndUpdateDetailsCommandUsingPOST(commandeDto);
      if (responseSingle.result == 1) {
        CommandeDto commandeDtoRe =
            CommandeDto.fromJson(responseSingle.objectResponse);
        commandeDtoRe.details.forEach((element) {
          var list;
          if (element.employe != null) {
            list = commandeDto.details
                .where((elementDto) =>
                    elementDto.idProduit == element.idProduit &&
                    elementDto.employe.idUtilisateur ==
                        element.employe.idUtilisateur)
                .toList();
          } else {
            if (dataDto.tableCaisse == null)
              list = commandeDto.details
                  .where(
                      (elementDto) => elementDto.idProduit == element.idProduit)
                  .toList();
            else {
              list = commandeDto.details;
            }
          }
          if (dataDto.tableCaisse == null)
            element.idDetailComm =
                list.length > 0 ? list[0].idDetailComm : null;
          element.employe = list.length > 0 ? list[0].employe : null;
          element.mesure = list.length > 0 ? list[0].mesure : '';
          //  element.ingredients = list.length > 0 ? list[0].ingredients : '';
        });
        dataDto.commandeDetails = commandeDtoRe.details;
        setTotalPrice();
      }
    } catch (err) {
      Crashlytics().sendEmail("Dashboard detectPack", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  bool android() {
    try {
      return Platform.isAndroid;
    } catch (err) {
      return false;
    }
  }

  getListNotifications() async {
    List<StompMessage> _messages = [];
    debugPrint(dataDto.pointVente.idPointVente);
    String url = ServerUrl.url +
        '/bp-api-admin/v1/findAllMessageByIdPV' +
        dataDto.pointVente.idPointVente;
    debugPrint(url);
    final response = await http.get(Uri.parse(url)).timeout(
      Duration(seconds: 10),
      onTimeout: () {
        // time has run out, do what you wanted to do
        return null;
      },
    );

    if (response != null && response.statusCode == 200) {
      debugPrint(response.body);
      ResponseList responseList =
          ResponseList.fromJson(json.decode(response.body));
      if (responseList.result == 1 && responseList.objectResponse != null) {
        _messages = responseList.objectResponse
            .map((e) => StompMessage.fromJson(e))
            .toList();
      }
      setState(() {
        messages = _messages.where((element) => element.ftraite == 0).toList();
      });
      setState(() {
        dataDto.numberNotifications = messages.length;
      });
    } else {
      debugPrint(
          '---------------FAILED TO LOAD NOTIFICATIONS------------------');
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    // After 1 second, it takes you to the bottom of the ListView
    if (_controller.hasClients)
      Timer(
        Duration(seconds: 1),
        () => _controller.jumpTo(_controller.position.maxScrollExtent),
      );
    final emailField = Container(
      alignment: Alignment.centerLeft,
      height: width > height
          ? SizeClacule().calcule(context, 14)
          : SizeClacule().calcule(context, 7),
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
              child: TextField(
            focusNode: dataDto.focusNode,
            onTap: () {
              setState(() {
                dataDto.isNumericMode = false;
                dataDto.focused = -1;
                dataDto.hideKeyboard = false;
              });
            },
            cursorColor: Color(0xff352C60),
            controller: dataDto.searchController,
            textAlignVertical: TextAlignVertical.center,
            obscureText: false,
            decoration: InputDecoration(
              filled: false,
              fillColor: Colors.white,
              disabledBorder: InputBorder.none,
              prefixIcon: Icon(
                Icons.search,
                size: width > height
                    ? SizeClacule().calcule(context, 40)
                    : SizeClacule().calcule(context, 20),
                color: Colors.grey,
              ),
              hintText: dataDto.hint,
              hintStyle: TextStyle(
                  fontSize: width > height
                      ? SizeClacule().calcule(context, 70)
                      : SizeClacule().calcule(context, 35)),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(0),
              ),
            ),
          )),
          Container(
            margin: EdgeInsets.only(right: 16),
            child: IconButton(
              padding: EdgeInsets.all(0),
              hoverColor: Colors.transparent,
              focusColor: Colors.transparent,
              onPressed: () {
                setState(() {
                  dataDto.searchController.clear();
                  dataDto.focused = null;
                  dataDto.focusNode.unfocus();
                  dataDto.hideKeyboard = true;
                });
              },
              icon: Icon(Icons.clear),
            ),
          )
        ],
      ),
    );

    void callback(Widget nextPage, String name) {
      setState(() {
        dataDto.hideKeyboard = true;
      });
      List<CommandeDetails> list = this
          .dataDto
          .commandeDetails
          .where((element) => element.recharge == true)
          .toList();
      if (list.length > 0) {
        setState(() {
          this.dataDto.commandeDetails = [];
          this.dataDto.commande = null;
          this.dataDto.total = "0.000";
          this.dataDto.rendu = null;
          this.dataDto.clientBPrice = null;
          this.dataDto.payementDtos = [];
          dataDto.payementList = [];
        });
      }
      setState(() {
        currentPage = nextPage;
        drawer = 0;
        current = name;
      });
      dataDto.currentPage = name;
    }

    void _settingModalBottomSheet(context) {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      showModalBottomSheet(
          context: context,
          enableDrag: true,
          builder: (BuildContext bc) {
            return Scaffold(
              backgroundColor: Color(0xffffffff),
              body: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 12, right: 12),
                        margin: EdgeInsets.only(
                            bottom: dataDto.clientBPrice == null ? 0 : 8),
                        decoration: BoxDecoration(
                          color: Color(0xff352C60),
                        ),
                        height: width / 5,
                        alignment: Alignment.center,
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                              "TOTAL :",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: width / 35),
                              textAlign: TextAlign.start,
                            )),
                            FloatingActionButton(
                              backgroundColor: Colors.white,
                              child: Icon(
                                Icons.delete_outline,
                                color: Color(0xff352C60),
                              ),
                              onPressed: () => {
                                Navigator.pop(context),
                                this.dataDto.clientBPrice = null,
                                this.dataDto.commandeDetails = [],
                                this.dataDto.total = "000.000"
                              },
                            ),
                            Expanded(
                                child: Text(
                              dataDto.total + " DT",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: width / 35),
                              textAlign: TextAlign.end,
                            ))
                          ],
                        ),
                      ),
                      dataDto.clientBPrice != null
                          ? Container(
                              padding: EdgeInsets.only(left: 12, right: 12),
                              margin: EdgeInsets.only(bottom: 8),
                              decoration: BoxDecoration(
                                color: Color(0xff352C60),
                                border: Border(
                                    top: BorderSide(
                                        width: 1, color: Color(0xffE7D7EE))),
                              ),
                              height: width / 10,
                              alignment: Alignment.center,
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    dataDto.clientBPrice.nom +
                                        " " +
                                        dataDto.clientBPrice.prenom,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 35),
                                    textAlign: TextAlign.center,
                                  )),
                                ],
                              ),
                            )
                          : Container(),
                      Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: Color(0xffcecece),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: ListView.builder(
                                padding: EdgeInsets.only(bottom: 8),
                                itemCount: this.dataDto.commandeDetails.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Dismissible(
                                    key: Key(this
                                        .dataDto
                                        .commandeDetails[index]
                                        .idProduit),
                                    direction: DismissDirection.endToStart,
                                    onDismissed: (direction) {
                                      setState(() {
                                        this
                                            .dataDto
                                            .commandeDetails
                                            .removeAt(index);
                                      });
                                      Navigator.pop(context);
                                      setTotalPrice();
                                    },
                                    child: WidgetANimator(Card(
                                      margin: EdgeInsets.only(
                                          left: width / 100,
                                          right: width / 100,
                                          top: width / 90),
                                      elevation: 0,
                                      child: Padding(
                                          padding: EdgeInsets.only(
                                              top: width / 30,
                                              bottom: width / 30,
                                              left: width / 50,
                                              right: width / 50),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                  child: Text(
                                                this
                                                        .dataDto
                                                        .commandeDetails[index]
                                                        .quantite
                                                        .toString() +
                                                    " X " +
                                                    this
                                                        .dataDto
                                                        .commandeDetails[index]
                                                        .designation,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: width / 30),
                                              )),
                                              Text(
                                                (this
                                                                .dataDto
                                                                .commandeDetails[
                                                                    index]
                                                                .prix *
                                                            this
                                                                .dataDto
                                                                .commandeDetails[
                                                                    index]
                                                                .quantite)
                                                        .toStringAsFixed(3) +
                                                    " DT",
                                                textAlign: TextAlign.end,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: width / 30),
                                              )
                                            ],
                                          )),
                                    )),
                                  );
                                }),
                          )),
                      Container(
                        margin: EdgeInsets.only(left: 8, bottom: 8),
                        height: width / 10,
                        child: Row(
                          children: [
                            Expanded(
                                child: Container(
                              margin: EdgeInsets.only(right: 8),
                              child: FlatButton(
                                  color: Colors.green,
                                  padding: EdgeInsets.all(0),
                                  onPressed: () => {
                                        if (this
                                                .dataDto
                                                .commandeDetails
                                                .length !=
                                            0)
                                          {
                                            Navigator.pop(context),
                                            setState(() {
                                              dataDto.payementDtos = [];
                                            }),
                                            callback(
                                                Payement(dataDto), "Payement"),
                                            dataDto.hint = "Chercher Client"
                                          }
                                      },
                                  child: Text(
                                    "Payer",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: width / 30,
                                        color: Colors.white),
                                  )),
                            )),
                            dataDto.pointVente.fGestionTable == null ||
                                    dataDto.pointVente.fGestionTable == 1
                                ? Expanded(
                                    child: Container(
                                    margin: EdgeInsets.only(right: 8),
                                    child: FlatButton(
                                        color: Color(0xff352C60),
                                        padding: EdgeInsets.all(0),
                                        onPressed: () => {
                                              createCommande(1),
                                              Navigator.pop(context),
                                            },
                                        child: loadingEsp
                                            ? LinearProgressIndicator(
                                                backgroundColor: Colors.white,
                                              )
                                            : Text(
                                                "Espèce",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: width > height
                                                        ? width / 150
                                                        : width / 30,
                                                    color: Colors.white),
                                              )),
                                  ))
                                : SizedBox(),
                            Expanded(
                                child: Container(
                              margin: EdgeInsets.only(right: 8),
                              child: FlatButton(
                                  color: Colors.deepPurpleAccent,
                                  padding: EdgeInsets.all(0),
                                  onPressed: () => {
                                        createCommande(0),
                                        Navigator.pop(context),
                                      },
                                  child: loading
                                      ? LinearProgressIndicator(
                                          backgroundColor: Colors.white,
                                        )
                                      : Text(
                                          "Commander",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: width > height
                                                  ? width / 150
                                                  : width / 30,
                                              color: Colors.white),
                                        )),
                            )),
                            Expanded(
                                child: Container(
                              margin: EdgeInsets.only(right: 8),
                              child: FlatButton(
                                  color: Color(0xff352C60),
                                  padding: EdgeInsets.all(0),
                                  onPressed: () => {
                                        createCommande(1),
                                        Navigator.pop(context),
                                      },
                                  child: loadingEsp
                                      ? LinearProgressIndicator(
                                          backgroundColor: Colors.white,
                                        )
                                      : Text(
                                          "Espèce",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: width > height
                                                  ? width / 150
                                                  : width / 30,
                                              color: Colors.white),
                                        )),
                            )),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          });
    }

    return Scaffold(
        backgroundColor: Colors.black,
        floatingActionButton: width < height
            ? FloatingActionButton(
                child: Icon(Icons.shopping_basket),
                onPressed: () => {_settingModalBottomSheet(context)},
              )
            : null,
        body: Stack(
          children: [
            Padding(
                padding: EdgeInsets.only(top: android() ? 24 : 0),
                child: Row(
                  children: [
                    /// Menu
                    /*Container(
                      width: width > height
                          ? SizeClacule().calcule(context, 10)
                          : drawer,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Color(0xff352C60),
                      ),
                      child: Scaffold(
                        backgroundColor: Color(0xff352C60),
                        body: Container(
                          alignment: Alignment.center,
                          color: Colors.transparent,
                          child: Padding(
                            padding: const EdgeInsets.all(0),


                            child: (!(dataDto.caisseType.code == 'CENTRALE' &&
                                    (dataDto.utilisateurType.code ==
                                            'CAISSIER' ||
                                        dataDto.utilisateurType.code ==
                                            'PROP' ||
                                        dataDto.utilisateurType.code ==
                                            'GERANT')))
                                ? Stack(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 16.0),
                                          SizedBox(
                                            height: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            width: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            child: FlatButton(
                                                color: Color(0xff8066FF),
                                                onPressed: () => {
                                                      if (dataDto.saved)
                                                        {
                                                          setState(() {
                                                            dataDto.saved =
                                                                false;
                                                            dataDto.total =
                                                                "0.000";
                                                            dataDto.commande =
                                                                null;
                                                            dataDto.commandeDelete =
                                                                [];
                                                            dataDto.commandeDetails =
                                                                [];
                                                            dataDto.clientBPrice =
                                                                null;
                                                            dataDto.tableCaisse =
                                                                null;
                                                            dataDto.listClientFiltred =
                                                                dataDto
                                                                    .listClient;
                                                          }),
                                                        },
                                                      setState(() => {
                                                            currentPage =
                                                                Clients(
                                                                    dataDto),
                                                            current = "Clients",
                                                            dataDto.hint =
                                                                "Chercher Client"
                                                          }),
                                                    },
                                                padding: EdgeInsets.all(0),
                                                child: Image.asset(
                                                  'assets/user2/user2.png',
                                                  fit: BoxFit.contain,
                                                  height: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                  width: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                )),
                                          ),
                                          SizedBox(
                                            height: 16.0,
                                          ),
                                          SizedBox(
                                            height: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            width: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            child: FlatButton(
                                                color: Color(0xff8066FF),
                                                onPressed: () => {
                                                      getCommandes(),
                                                      setState(() {
                                                        dataDto.commandeDelete =
                                                            [];
                                                      }),
                                                    },
                                                padding: EdgeInsets.all(0),
                                                child: Image.asset(
                                                  'assets/transactionn/transactionn.png',
                                                  color: Colors.white,
                                                  fit: BoxFit.contain,
                                                  height: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                  width: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                )),
                                          ),

                                          SizedBox(height: 16),
                                          Icon(
                                            isConnected
                                                ? Icons
                                                    .signal_wifi_4_bar_outlined
                                                : Icons
                                                    .signal_wifi_off_outlined,
                                            size: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 25)
                                                : SizeClacule()
                                                    .calcule(context, 12),
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ],
                                  )
                                : Stack(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 16.0),
                                          SizedBox(
                                            height: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            width: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            child: FlatButton(
                                                color: Color(0xff8066FF),
                                                onPressed: () => {
                                                      if (dataDto.saved)
                                                        {
                                                          setState(() {
                                                            dataDto.saved =
                                                                false;
                                                            dataDto.total =
                                                                "0.000";
                                                            dataDto.commande =
                                                                null;
                                                            dataDto.commandeDelete =
                                                                [];
                                                            dataDto.commandeDetails =
                                                                [];
                                                            dataDto.clientBPrice =
                                                                null;
                                                            dataDto.tableCaisse =
                                                                null;
                                                            dataDto.listClientFiltred =
                                                                dataDto
                                                                    .listClient;
                                                          }),
                                                        },
                                                      setState(() => {
                                                            currentPage =
                                                                Clients(
                                                                    dataDto),
                                                            current = "Clients",
                                                            dataDto.hint =
                                                                "Chercher Client"
                                                          }),
                                                    },
                                                padding: EdgeInsets.all(0),
                                                child: Image.asset(
                                                  'assets/user2/user2.png',
                                                  fit: BoxFit.contain,
                                                  height: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                  width: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                )),
                                          ),
                                          SizedBox(
                                            height: 16.0,
                                          ),
                                          SizedBox(
                                            height: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            width: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            child: FlatButton(
                                                color: Color(0xff8066FF),
                                                onPressed: () => {
                                                      setCurrentPage(
                                                          Transactions(dataDto),
                                                          "Transactions"),
                                                      setState(() {
                                                        dataDto.commandeDelete =
                                                            [];
                                                      }),
                                                    },
                                                padding: EdgeInsets.all(0),
                                                child: Image.asset(
                                                  'assets/transactionn/transactionn.png',
                                                  color: Colors.white,
                                                  fit: BoxFit.contain,
                                                  height: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                  width: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 30)
                                                      : SizeClacule()
                                                          .calcule(context, 15),
                                                )),
                                          ),
                                          SizedBox(
                                            height: 16.0,
                                          ),
                                          dataDto.pointVente?.fGestionTable ==
                                                      1 ||
                                                  dataDto.pointVente
                                                          ?.fGestionTable ==
                                                      2
                                              ? SizedBox(
                                                  height: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 15)
                                                      : SizeClacule().calcule(
                                                          context, 7.5),
                                                  width: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 15)
                                                      : SizeClacule().calcule(
                                                          context, 7.5),
                                                  child: FlatButton(
                                                      color: Color(0xff8066FF),
                                                      onPressed: () => {
                                                            setState(() {
                                                              dataDto.commandeDelete =
                                                                  [];
                                                              drawer = 0;
                                                              setCurrentPage(
                                                                  Tables(
                                                                      dataDto),
                                                                  "Tables");
                                                            })
                                                          },
                                                      padding:
                                                          EdgeInsets.all(0),
                                                      child: Image.asset(
                                                        dataDto.pointVente
                                                                    .fGestionTable ==
                                                                1
                                                            ? 'assets/restaurant/restaurant.png'
                                                            : 'assets/restaurant/bipeur.png',
                                                        color: Colors.white,
                                                        fit: BoxFit.contain,
                                                        height: width > height
                                                            ? SizeClacule()
                                                                .calcule(
                                                                    context, 30)
                                                            : SizeClacule()
                                                                .calcule(
                                                                    context,
                                                                    15),
                                                        width: width > height
                                                            ? SizeClacule()
                                                                .calcule(
                                                                    context, 30)
                                                            : SizeClacule()
                                                                .calcule(
                                                                    context,
                                                                    15),
                                                      )),
                                                )
                                              : SizedBox(),
                                          dataDto.pointVente?.fGestionTable ==
                                                      1 ||
                                                  dataDto.pointVente
                                                          ?.fGestionTable ==
                                                      2
                                              ? SizedBox(
                                                  height: 16.0,
                                                )
                                              : SizedBox(),
                                          dataDto.pointVente?.fReservation ==
                                                  "1"
                                              ? SizedBox(
                                                  height: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 15)
                                                      : SizeClacule().calcule(
                                                          context, 7.5),
                                                  width: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 15)
                                                      : SizeClacule().calcule(
                                                          context, 7.5),
                                                  child: dataDto.caisseType.code ==
                                                              'CENTRALE' &&
                                                          (dataDto.utilisateurType
                                                                      .code ==
                                                                  'CAISSIER' ||
                                                              dataDto.utilisateurType
                                                                      .code ==
                                                                  'PROP' ||
                                                              dataDto.utilisateurType
                                                                      .code ==
                                                                  'GERANT')
                                                      ? FlatButton(
                                                          color:
                                                              Color(0xff8066FF),
                                                          onPressed: () => {
                                                                setState(() {
                                                                  drawer = 0;
                                                                  setCurrentPage(
                                                                      Reservation(
                                                                          dataDto),
                                                                      "Reservation");
                                                                })
                                                              },
                                                          padding:
                                                              EdgeInsets.all(0),
                                                          child: Image.asset(
                                                            'assets/work/work.png',
                                                            color: Colors.white,
                                                            fit: BoxFit.contain,
                                                            height: width >
                                                                    height
                                                                ? SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        30)
                                                                : SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        15),
                                                            width: width >
                                                                    height
                                                                ? SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        30)
                                                                : SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        15),
                                                          ))
                                                      : null,
                                                )
                                              : SizedBox(),
                                          SizedBox(
                                            height: dataDto.pointVente
                                                            .fReservation ==
                                                        "1" &&
                                                    !ServerUrl.url.contains(
                                                        "bp-api-local")
                                                ? 16
                                                : 0,
                                          ),
                                          dataDto.pointVente
                                                          .fAutoriserRecharge ==
                                                      1 &&
                                                  !ServerUrl.url
                                                      .contains("bp-api-local")
                                              ? SizedBox(
                                                  height: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 15)
                                                      : SizeClacule().calcule(
                                                          context, 7.5),
                                                  width: width > height
                                                      ? SizeClacule()
                                                          .calcule(context, 15)
                                                      : SizeClacule().calcule(
                                                          context, 7.5),
                                                  child: dataDto.caisseType.code ==
                                                              'CENTRALE' &&
                                                          (dataDto.utilisateurType
                                                                      .code ==
                                                                  'CAISSIER' ||
                                                              dataDto.utilisateurType
                                                                      .code ==
                                                                  'PROP' ||
                                                              dataDto.utilisateurType
                                                                      .code ==
                                                                  'GERANT')
                                                      ? FlatButton(
                                                          color:
                                                              Color(0xff8066FF),
                                                          onPressed: () => {
                                                                setState(() {
                                                                  drawer = 0;
                                                                  setCurrentPage(
                                                                      Recharge(
                                                                          dataDto),
                                                                      "Recharge");
                                                                })
                                                              },
                                                          padding:
                                                              EdgeInsets.all(0),
                                                          child: Image.asset(
                                                            'assets/money/money.png',
                                                            color: Colors.white,
                                                            fit: BoxFit.contain,
                                                            height: width >
                                                                    height
                                                                ? SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        30)
                                                                : SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        15),
                                                            width: width >
                                                                    height
                                                                ? SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        30)
                                                                : SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        15),
                                                          ))
                                                      : null,
                                                )
                                              : SizedBox(),
                                          SizedBox(height: 16),
                                          SizedBox(
                                            height: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            width: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            child: dataDto.caisseType.code ==
                                                        'CENTRALE' &&
                                                    (dataDto.utilisateurType
                                                                .code ==
                                                            'CAISSIER' ||
                                                        dataDto.utilisateurType
                                                                .code ==
                                                            'PROP' ||
                                                        dataDto.utilisateurType
                                                                .code ==
                                                            'GERANT')
                                                ? FlatButton(
                                                    color: Color(0xff8066FF),
                                                    onPressed: () => {
                                                          setState(() {
                                                            drawer = 0;
                                                            setCurrentPage(
                                                                Depense(
                                                                    dataDto),
                                                                "Depense");
                                                          })
                                                        },
                                                    padding: EdgeInsets.all(0),
                                                    child: Image.asset(
                                                      'assets/dep/dep.png',
                                                      color: Colors.white,
                                                      fit: BoxFit.contain,
                                                      height: width > height
                                                          ? SizeClacule()
                                                              .calcule(
                                                                  context, 30)
                                                          : SizeClacule()
                                                              .calcule(
                                                                  context, 15),
                                                      width: width > height
                                                          ? SizeClacule()
                                                              .calcule(
                                                                  context, 30)
                                                          : SizeClacule()
                                                              .calcule(
                                                                  context, 15),
                                                    ))
                                                : null,
                                          ),
                                          SizedBox(height: 16),
                                          SizedBox(
                                            height: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            width: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 15)
                                                : SizeClacule()
                                                    .calcule(context, 7.5),
                                            child: dataDto.caisseType.code ==
                                                        'CENTRALE' &&
                                                    (dataDto.utilisateurType
                                                                .code ==
                                                            'CAISSIER' ||
                                                        dataDto.utilisateurType
                                                                .code ==
                                                            'PROP' ||
                                                        dataDto.utilisateurType
                                                                .code ==
                                                            'GERANT')
                                                ? Container(
                                                    child: Stack(
                                                    fit: StackFit.passthrough,
                                                    clipBehavior: Clip
                                                        .antiAliasWithSaveLayer,
                                                    children: [
                                                      FlatButton(
                                                          color:
                                                              Color(0xff8066FF),
                                                          onPressed: () => {
                                                                setState(() {
                                                                  drawer = 0;
                                                                  setCurrentPage(
                                                                      Notifications(
                                                                          dataDto),
                                                                      "Notifications");
                                                                })
                                                              },
                                                          padding:
                                                              EdgeInsets.all(0),
                                                          child: Icon(
                                                            Icons.notifications,
                                                            size: width > height
                                                                ? SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        25)
                                                                : SizeClacule()
                                                                    .calcule(
                                                                        context,
                                                                        12),
                                                            color: Colors.white,
                                                          )),
                                                      dataDto.numberNotifications >
                                                              0
                                                          ? Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      left: 32,
                                                                      bottom:
                                                                          22,
                                                                      right: 4),
                                                              child:
                                                                  CircleAvatar(
                                                                child: Text(
                                                                  dataDto
                                                                      .numberNotifications
                                                                      .toString(),
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Colors
                                                                          .white,
                                                                      fontSize:
                                                                          9),
                                                                ),
                                                                radius: 6,
                                                                backgroundColor:
                                                                    Color(
                                                                        0xffCF0606),
                                                              ),
                                                            )
                                                          : SizedBox()
                                                    ],
                                                  ))
                                                : null,
                                          ),
                                          SizedBox(height: 16),
                                          Icon(
                                            isConnected
                                                ? Icons
                                                    .signal_wifi_4_bar_outlined
                                                : Icons
                                                    .signal_wifi_off_outlined,
                                            size: width > height
                                                ? SizeClacule()
                                                    .calcule(context, 25)
                                                : SizeClacule()
                                                    .calcule(context, 12),
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                          ),
                        ),
                      ),
                    ),*/
                    /// Menu
                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(color: Color(0xffEEEEEE)),
                        child: Scaffold(
                          backgroundColor: Colors.transparent,
                          body: Container(
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.all(8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  /// rechercher
                                         /*
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      width > height
                                          ? Container()
                                          : Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 1,
                                                    color: Color(0xff352C60)),
                                              ),
                                              width: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 15)
                                                  : SizeClacule()
                                                      .calcule(context, 7.5),
                                              height: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 15)
                                                  : SizeClacule()
                                                      .calcule(context, 7.5),
                                              alignment: Alignment.center,
                                              margin: EdgeInsets.only(right: 8),
                                              child: FlatButton(
                                                  onPressed: () => {
                                                        setState(() {
                                                          drawer == width / 5
                                                              ? drawer = 0
                                                              : drawer =
                                                                  width / 5;
                                                        })
                                                      },
                                                  padding: EdgeInsets.all(0),
                                                  child: Image.asset(
                                                    'assets/menu/menu.png',
                                                    color: Color(0xff352C60),
                                                    fit: BoxFit.contain,
                                                    height: width > height
                                                        ? SizeClacule().calcule(
                                                            context, 30)
                                                        : SizeClacule().calcule(
                                                            context, 15),
                                                    width: width > height
                                                        ? SizeClacule().calcule(
                                                            context, 30)
                                                        : SizeClacule().calcule(
                                                            context, 15),
                                                  )),
                                            ),
                                      Expanded(child: emailField),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      SizedBox(
                                        width: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        height: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        child: FlatButton(
                                            color: Color(0xff352C60),
                                            onPressed: () => {
                                                  callback(
                                                      CommandeScreen(dataDto),
                                                      "CommandeScreen"),
                                                  setState(() => {
                                                        dataDto.hint =
                                                            "Chercher produit"
                                                      })
                                                },
                                            padding: EdgeInsets.all(0),
                                            child: Image.asset(
                                              'assets/home-run/home-run.png',
                                              fit: BoxFit.contain,
                                              height: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 30)
                                                  : SizeClacule()
                                                      .calcule(context, 15),
                                              width: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 30)
                                                  : SizeClacule()
                                                      .calcule(context, 15),
                                            )),
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      SizedBox(
                                        width: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        height: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        child: FlatButton(
                                            padding: EdgeInsets.all(0),
                                            color: Color(0xff352C60),
                                            onPressed: () {
                                              setCurrentPage(
                                                  Setting(dataDto), "Setting");
                                            },
                                            child: Image.asset(
                                              'assets/gear/gear.png',
                                              fit: BoxFit.contain,
                                              height: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 30)
                                                  : SizeClacule()
                                                      .calcule(context, 15),
                                              width: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 30)
                                                  : SizeClacule()
                                                      .calcule(context, 15),
                                            )),
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      dataDto.pointVente.fDispCA == 1 &&
                                              dataDto.caisseType.code ==
                                                  'CENTRALE' &&
                                              (dataDto.utilisateurType.code ==
                                                      'CAISSIER' ||
                                                  dataDto.utilisateurType
                                                          .code ==
                                                      'PROP' ||
                                                  dataDto.utilisateurType
                                                          .code ==
                                                      'GERANT')
                                          ? SizedBox(
                                              width: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 15)
                                                  : SizeClacule()
                                                      .calcule(context, 7.5),
                                              height: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 15)
                                                  : SizeClacule()
                                                      .calcule(context, 7.5),
                                              child: FlatButton(
                                                  color: Color(0xff352C60),
                                                  onPressed: () => {
                                                        getMontantParMoyenPaiement()
                                                      },
                                                  padding: EdgeInsets.all(0),
                                                  child: isLoading
                                                      ? SizedBox(
                                                          height: width > height
                                                              ? width / 150
                                                              : width / 60,
                                                          width: width > height
                                                              ? width / 150
                                                              : width / 60,
                                                          child: CircularProgressIndicator(
                                                              valueColor:
                                                                  new AlwaysStoppedAnimation<
                                                                          Color>(
                                                                      Colors
                                                                          .white)),
                                                        )
                                                      : Image.asset(
                                                          'assets/money/ca-icon.png',
                                                          fit: BoxFit.contain,
                                                          height: width > height
                                                              ? SizeClacule()
                                                                  .calcule(
                                                                      context,
                                                                      30)
                                                              : SizeClacule()
                                                                  .calcule(
                                                                      context,
                                                                      15),
                                                          width: width > height
                                                              ? SizeClacule()
                                                                  .calcule(
                                                                      context,
                                                                      30)
                                                              : SizeClacule()
                                                                  .calcule(
                                                                      context,
                                                                      15),
                                                        )),
                                            )
                                          : SizedBox(),
                                      SizedBox(
                                        width: dataDto.pointVente.fDispCA == 1
                                            ? 8
                                            : 0,
                                      ),
                                      SizedBox(
                                        width: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        height: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        child: FlatButton(
                                            color: Color(0xffCF0606),
                                            onPressed: () => {
                                                  Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            LoginPage()),
                                                  )
                                                },
                                            padding: EdgeInsets.all(0),
                                            child: Image.asset(
                                              'assets/logout/logout.png',
                                              fit: BoxFit.contain,
                                              height: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 30)
                                                  : SizeClacule()
                                                      .calcule(context, 15),
                                              width: width > height
                                                  ? SizeClacule()
                                                      .calcule(context, 30)
                                                  : SizeClacule()
                                                      .calcule(context, 15),
                                            )),
                                      ),
                                    ],
                                  ),
*/
                                  /// rechercher

                                  SizedBox(
                                    height: 20,
                                  ),
                                  current == "Payement" ? SizedBox()
                                      :  Text(
                                    'La première borne Digitale en Mode SELF SERVICE',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold, fontSize: width / 50),
                                  ),
                                  SizedBox(
                                    height: current == "Payement" ? 0 : 20,
                                  ),
                                  Expanded(child: currentPage)
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    width > height
                        ? Container(
                            decoration: BoxDecoration(color: Color(0xffEEEEEE)),
                            padding:
                                EdgeInsets.only(top: 8, bottom: 8, right: 8),
                            width: current == "Payement" ||
                                    current == "NewReservation" ||
                                    current == "Reservation" ||
                                    current == "Recharge" ||
                                    current == "ReservationCoiff" ||
                                    current == "ReservationT" ||
                                    current == "Depense"
                                ? 0
                                : width / 4,
                            child: Container(
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.all(0),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 12, right: 12),
                                      margin: EdgeInsets.only(
                                          bottom: dataDto.clientBPrice !=
                                                      null ||
                                                  dataDto.tableCaisse != null ||
                                                  dataDto.commande != null
                                              ? 0
                                              : 8),
                                      decoration: BoxDecoration(
                                        color: Color(0xff352C60),
                                      ),
                                      height: width / 25,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                              child: Text(
                                            "TOTAL :",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: width / 80),
                                            textAlign: TextAlign.start,
                                          )),
                                          number.isNotEmpty
                                              ? SizedBox(
                                                  width: width / 25,
                                                  height: width / 35,
                                                  child: FlatButton(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        width /
                                                                            30),
                                                            side: BorderSide(
                                                                color: Colors
                                                                    .red)),
                                                    onPressed: () {
                                                      setState(() {
                                                        number = '';
                                                        numTable = "";
                                                      });
                                                    },
                                                    child: Text(
                                                      number.isEmpty
                                                          ? ""
                                                          : number,
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                )
                                              : SizedBox(),
                                          Expanded(
                                              child: Text(
                                            dataDto.total + " DT",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: width / 80),
                                            textAlign: TextAlign.end,
                                          ))
                                        ],
                                      ),
                                    ),
                                    dataDto.clientBPrice != null ||
                                            dataDto.tableCaisse != null ||
                                            dataDto.commande != null &&
                                                dataDto.commande.numCommande !=
                                                    null
                                        ? Container(
                                            padding: EdgeInsets.only(
                                                left: 12, right: 12),
                                            margin: EdgeInsets.only(bottom: 8),
                                            decoration: BoxDecoration(
                                              color: Color(0xff352C60),
                                              border: Border(
                                                  top: BorderSide(
                                                      width: 1,
                                                      color: Colors.white)),
                                            ),
                                            height: width / 25,
                                            alignment: Alignment.center,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                dataDto.commande != null &&
                                                        dataDto.commande
                                                                .numCommande !=
                                                            null
                                                    ? Expanded(
                                                        child: Text(
                                                        dataDto.commande !=
                                                                    null &&
                                                                dataDto.commande
                                                                        .numCommande !=
                                                                    null
                                                            ? "N° " +
                                                                dataDto.commande
                                                                    .numCommande
                                                                    .toString()
                                                            : "",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize:
                                                                width / 80),
                                                        textAlign:
                                                            dataDto.tableCaisse !=
                                                                        null ||
                                                                    dataDto.clientBPrice !=
                                                                        null
                                                                ? TextAlign.left
                                                                : TextAlign
                                                                    .center,
                                                      ))
                                                    : SizedBox(),
                                                dataDto.clientBPrice != null
                                                    ? Expanded(
                                                        child: Text(
                                                        dataDto.clientBPrice
                                                                    .nom !=
                                                                null
                                                            ? dataDto
                                                                    .clientBPrice
                                                                    .nom +
                                                                " " +
                                                                dataDto
                                                                    .clientBPrice
                                                                    .prenom
                                                            : "",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize:
                                                                width / 80),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ))
                                                    : SizedBox(),
                                                dataDto.tableCaisse != null
                                                    ? Expanded(
                                                        child: Text(
                                                        dataDto.tableCaisse !=
                                                                null
                                                            ? dataDto.pointVente
                                                                        .fGestionTable ==
                                                                    1
                                                                ? "Table N° " +
                                                                    dataDto
                                                                        .tableCaisse
                                                                        .numTable
                                                                        .toString()
                                                                : "Bipeur N°" +
                                                                    dataDto
                                                                        .tableCaisse
                                                                        .numTable
                                                                        .toString()
                                                            : "",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize:
                                                                width / 80),
                                                        textAlign: dataDto
                                                                        .commande !=
                                                                    null ||
                                                                dataDto.clientBPrice !=
                                                                    null
                                                            ? TextAlign.right
                                                            : TextAlign.center,
                                                      ))
                                                    : SizedBox(),
                                              ],
                                            ),
                                          )
                                        : Container(),
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              bottom: 8, left: 8, right: 8),
                                          padding: EdgeInsets.only(bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Color(0xffE7D7EE),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child:
                                              dataDto.commandeDetails.length > 0
                                                  ? ListView.builder(
                                                      controller: _controller,
                                                      padding: EdgeInsets.only(
                                                        top: width / 400,
                                                      ),
                                                      itemCount: this
                                                          .dataDto
                                                          .commandeDetails
                                                          .length,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return FlatButton(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    0),
                                                            onLongPress: () {
                                                              (dataDto
                                                                              .caisseType
                                                                              .code ==
                                                                          'CENTRALE' &&
                                                                      dataDto.utilisateurType
                                                                              .code ==
                                                                          'CAISSIER' &&
                                                                      actionRemize ==
                                                                          true)
                                                                  ? showCodeActionRemize(
                                                                      0,
                                                                      commandeDetails: this
                                                                              .dataDto
                                                                              .commandeDetails[
                                                                          index],
                                                                      index:
                                                                          index)
                                                                  : showRemiseMyDialog(
                                                                      this
                                                                          .dataDto
                                                                          .commandeDetails[index],
                                                                      index);
                                                            },
                                                            onPressed: () {
                                                              (dataDto
                                                                              .caisseType
                                                                              .code ==
                                                                          'CENTRALE' &&
                                                                      dataDto.utilisateurType
                                                                              .code ==
                                                                          'CAISSIER' &&
                                                                      actionRemize ==
                                                                          true)
                                                                  ? showCodeActionRemize(
                                                                      0,
                                                                      commandeDetails: this
                                                                              .dataDto
                                                                              .commandeDetails[
                                                                          index],
                                                                      index:
                                                                          index)
                                                                  : showRemiseMyDialog(
                                                                      this
                                                                          .dataDto
                                                                          .commandeDetails[index],
                                                                      index);
                                                            },
                                                            child: Dismissible(
                                                              key: Key(this
                                                                          .dataDto
                                                                          .commandeDetails
                                                                          .length >
                                                                      0
                                                                  ? this
                                                                      .dataDto
                                                                      .commandeDetails[
                                                                          index]
                                                                      .idProduit
                                                                  : "0"),
                                                              direction:
                                                                  DismissDirection
                                                                      .endToStart,
                                                              onDismissed:
                                                                  (DismissDirection
                                                                      direction) {
                                                                if (dataDto
                                                                        .commande !=
                                                                    null) {
                                                                  dataDto
                                                                      .commandeDelete
                                                                      .add(this
                                                                          .dataDto
                                                                          .commandeDetails[index]);
                                                                  setState(() {
                                                                    this
                                                                        .dataDto
                                                                        .commandeDetails
                                                                        .removeAt(
                                                                            index);
                                                                  });
                                                                  setTotalPrice();
                                                                } else {
                                                                  setState(() {
                                                                    this
                                                                        .dataDto
                                                                        .commandeDetails
                                                                        .removeAt(
                                                                            index);
                                                                  });
                                                                  setTotalPrice();
                                                                }
                                                              },
                                                              child:
                                                                  WidgetANimator(
                                                                      Card(
                                                                margin: EdgeInsets.only(
                                                                    left:
                                                                        width /
                                                                            180,
                                                                    right:
                                                                        width /
                                                                            180,
                                                                    top: width /
                                                                        400,
                                                                    bottom:
                                                                        width /
                                                                            400),
                                                                elevation: 0,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: width /
                                                                            100,
                                                                        bottom: width /
                                                                            100,
                                                                        left: width /
                                                                            150,
                                                                        right: width /
                                                                            150),
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .end,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .end,
                                                                      children: [
                                                                        this.dataDto.commandeDetails[index].employe !=
                                                                                null
                                                                            ? Text(
                                                                                this.dataDto.commandeDetails[index].employe.nom,
                                                                                textAlign: TextAlign.center,
                                                                                style: TextStyle(fontWeight: FontWeight.w400, fontSize: width / 100),
                                                                              )
                                                                            : SizedBox(
                                                                                height: 0,
                                                                              ),
                                                                        Row(
                                                                          children: [
                                                                            Text(
                                                                              this.dataDto.commandeDetails[index].quantite.toString().endsWith('.0') ? this.dataDto.commandeDetails[index].quantite.ceil().toString() + ' ' + this.dataDto.commandeDetails[index].mesure.replaceAll('unité', '') + " X " : this.dataDto.commandeDetails[index].quantite.toStringAsFixed(3) + ' ' + this.dataDto.commandeDetails[index].mesure.replaceAll('unité', '') + " X ",
                                                                              style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
                                                                            ),
                                                                            SizedBox(
                                                                              width: 8,
                                                                            ),
                                                                            Expanded(
                                                                                child: Text(
                                                                              this.dataDto.commandeDetails[index].designation,
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
                                                                            )),
                                                                            SizedBox(
                                                                              width: 8,
                                                                            ),
                                                                            Text(
                                                                              (this.dataDto.commandeDetails[index].prix * this.dataDto.commandeDetails[index].quantite).toStringAsFixed(3) + " DT",
                                                                              textAlign: TextAlign.end,
                                                                              style: TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        this.dataDto.commandeDetails[index].remise != null &&
                                                                                this.dataDto.commandeDetails[index].remise > 0
                                                                            ? Text(
                                                                                "Remise: " + this.dataDto.commandeDetails[index].remise.floor().toString() + "%",
                                                                                textAlign: TextAlign.center,
                                                                                style: TextStyle(fontWeight: FontWeight.w400, fontSize: width / 100),
                                                                              )
                                                                            : SizedBox(
                                                                                height: 0,
                                                                              ),
                                                                        this.dataDto.commandeDetails[index].ingredients !=
                                                                                null
                                                                            ? Text(
                                                                                this.dataDto.commandeDetails[index].ingredients.toString(),
                                                                                textAlign: TextAlign.start,
                                                                                style: TextStyle(fontWeight: FontWeight.w400, fontSize: width / 100),
                                                                              )
                                                                            : SizedBox(
                                                                                height: 0,
                                                                              )
                                                                      ],
                                                                    )),
                                                              )),
                                                            ));
                                                      })
                                                  : Container(),
                                        )),
                                    Container(
                                        height: width-height>450?height / 2.9:height / 3.5,
                                        child: current == "Payement" ||
                                            current == "NewReservation" ||
                                            current == "Reservation" ||
                                            current == "Depense"
                                            ? Container()
                                            : Numbers(this.callbackNumbers)),

                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 8, bottom: 4, top: 4, right: 8),
                                      height: width / 20,
                                      child:
                                      Row(
                                                  children: [
                                                    dataDto.caisseType.code ==
                                                                'CENTRALE' &&
                                                            (dataDto.utilisateurType
                                                                        .code ==
                                                                    'CAISSIER' ||
                                                                dataDto.utilisateurType
                                                                        .code ==
                                                                    'PROP' ||
                                                                dataDto.utilisateurType
                                                                        .code ==
                                                                    'GERANT')
                                                        ? Expanded(
                                                            child: SizedBox(
                                                            height: width / 20,
                                                            child: FlatButton(
                                                                color: Colors
                                                                    .green,
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(0),
                                                                onPressed:
                                                                    () => {
                                                                          if (dataDto
                                                                              .saved)
                                                                            {
                                                                              setState(() {
                                                                                dataDto.saved = false;
                                                                                loading = false;
                                                                                loadingEsp = false;
                                                                                dataDto.total = "0.000";
                                                                                dataDto.commande = null;
                                                                                dataDto.commandeDelete = [];
                                                                                dataDto.commandeDetails = [];
                                                                                dataDto.clientBPrice = null;
                                                                                dataDto.tableCaisse = null;
                                                                                dataDto.listClientFiltred = dataDto.listClient;
                                                                              })
                                                                            },
                                                                          if (this.dataDto.commandeDetails.length >
                                                                              0)
                                                                            {
                                                                              setState(() {
                                                                                dataDto.payementDtos = [];
                                                                                dataDto.rendu = double.parse(dataDto.total);
                                                                              }),
                                                                              callback(Payement(dataDto), "Payement"),
                                                                              dataDto.hint = "Chercher Client"
                                                                            }
                                                                        },
                                                                child: Text(
                                                                  "Payer",
                                                                  textAlign:
                                                                      TextAlign
                                                                          .center,
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      fontSize: width > height
                                                                          ? width /
                                                                              100
                                                                          : width /
                                                                              30,
                                                                      color: Colors
                                                                          .white),
                                                                )),
                                                          ))
                                                        : SizedBox(),

                                                  ],
                                                ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Container()
                  ],
                )),
            Positioned(
              child: dataDto.hideKeyboard
                  ? SizedBox()
                  : VirtualKeyboardScreen(dataDto),
              bottom: 0,
            )
          ],
        ));
  }
}
