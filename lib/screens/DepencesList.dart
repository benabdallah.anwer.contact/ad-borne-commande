import 'dart:convert';

import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/pos/api/caisse_end_point_api.dart';
import 'package:ad_caisse/pos/model/caisse.dart';
import 'package:ad_caisse/pos/model/operation.dart';
import 'package:ad_caisse/pos/model/operation_type.dart';
import 'package:ad_caisse/product/api/fournisseur_end_point_api.dart';
import 'package:ad_caisse/product/model/fournisseur_dto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/screens/Transactions.dart';
import 'package:ad_caisse/transaction/api/operation_end_point_api.dart';
import 'package:ad_caisse/transaction/api/operation_type_end_point_api.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class DepencesList extends StatefulWidget {
  final DataDto dataDto;

  DepencesList(this.dataDto);

  @override
  DepencesListState createState() => DepencesListState(this.dataDto);
}

class DepencesListState extends State<DepencesList> {
  DataDto dataDto;
  String search = "";

  DepencesListState(this.dataDto);

  var operationEndPointApi = OperationEndPointApi();
  var operationTypeEndPointApi = OperationTypeEndPointApi();
  var caisseEndPointApi = CaisseEndPointApi();
  var fournisseurEndPointApi = FournisseurEndPointApi();
  List<Caisse> caisseList = [];
  List<OperationType> operationTypeList = [];
  List<FournisseurDto> fournisseurList = [];
  bool loading = true;
  DateTime finaldate;

  @override
  void initState() {
    super.initState();
    this.finaldate = DateTime.now();
    findAllByIdPatenaireBprice();
    dataDto.searchClient("Rechercher Operation");
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  Future<void> findAllOperationType() async {
    try {
      ResponseList responseList =
          await operationTypeEndPointApi.findAllByIdPointVenteUsingGET2();
      if (responseList.result == 1 && responseList.objectResponse != null) {
        List<OperationType> list =
            OperationType.listFromJson(responseList.objectResponse);
        setState(() {
          operationTypeList = list;
        });
      } else {
        AlertNotif().alert(context, responseList.errorDescription);
      }
      getDepenses();
    } catch (err) {
      Crashlytics()
          .sendEmail("DepenceList findAllOperationType", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> findAllCaisseByidPointVente() async {
    try {
      ResponseList responseList = await caisseEndPointApi
          .findAllCaisseByidPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1 && responseList.objectResponse != null) {
        List<Caisse> list = Caisse.listFromJson(responseList.objectResponse);
        setState(() {
          caisseList = list;
        });
      } else {
        AlertNotif().alert(context, responseList.errorDescription);
      }
      findAllOperationType();
    } catch (err) {
      Crashlytics()
          .sendEmail("DepenceList findAllCaisseByidPointVente", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> findAllByIdPatenaireBprice() async {
    setState(() {
      loading = true;
    });
    try {
      ResponseList responseList = await fournisseurEndPointApi
          .findAllByIdPatenaireBpriceUsingGET(dataDto.pointVente.idPartenaire);
      if (responseList.result == 1 && responseList.objectResponse != null) {
        List<FournisseurDto> list =
            FournisseurDto.listFromJson(responseList.objectResponse);
        setState(() {
          fournisseurList = list;
        });
      } else {
       // AlertNotif().alert(context, responseList.errorDescription);
      }
      findAllCaisseByidPointVente();
    } catch (err) {
      Crashlytics()
          .sendEmail("DepenceList findAllCaisseByidPointVente", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getDepenses() async {
    try {
      var serveur = ServerUrl.url;
      String idCaisse = dataDto.caisse.idCaisse;
      var url = serveur +
          '/bp-api-transaction/v1/findAllByIdCaisseAndDateOperation/' +
          idCaisse +
          '?date=' +
          finaldate.toIso8601String();
      final response = await http.get(Uri.parse(url));
      List<Operation> list = [];
      if (response != null && response.statusCode == 200) {
        var res = ResponseList.fromJson(json.decode(response.body));
        if (res.result == 1 && res.objectResponse != null) {
          list = Operation.listFromJson(res.objectResponse);
        }
      }
      setState(() {
        loading = false;
        dataDto.operationList = list;
      });
    } catch (err) {
      Crashlytics().sendEmail("DepenceList getDepenses", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  String getFournisseurName(String id) {
    if (id == null) {
      return "---";
    }
    var list = fournisseurList
        .where((element) => element.idFournisseur == id)
        .toList();
    if (list.length > 0) {
      return list[0].societe;
    }
    return "---";
  }

  String getOperationTypeName(String id) {
    if (id == null) {
      return "---";
    }
    var list = operationTypeList
        .where((element) => element.idTypeOperation == id)
        .toList();
    if (list.length > 0) {
      return list[0].designation;
    }
    return "---";
  }

  String getNumCaisse(String id) {
    if (id == null) {
      return "---";
    }
    var list = caisseList.where((element) => element.idCaisse == id).toList();
    if (list.length > 0) {
      return list[0].numCaisse.toString();
    }
    return "---";
  }

  void callDatePicker() async {
    DateTime order = await getDate();
    if (order != null) {
      setState(() {
        finaldate = order;
      });
      findAllByIdPatenaireBprice();
    }
  }

  Future<DateTime> getDate() {
    // Imagine that this function is
    // more complex and slow.
    return showDatePicker(
      context: context,
      helpText: "Date",
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: Color(0xff352C60),
          ),
          child: child,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return loading
        ? Container(
            child: Center(
              child: SizedBox(
                width: width / 40,
                height: width / 40,
                child: CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Colors.purpleAccent)),
              ),
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Dépenses :",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: width > height ? width / 50 : width / 30),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Colors.purple,
                                width: 1,
                                style: BorderStyle.solid)),
                        color: Colors.transparent,
                        onPressed: callDatePicker,
                        padding: EdgeInsets.all(12),
                        child: Text(DateFormat("dd-MM-yyyy").format(finaldate),
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    width: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    child: FlatButton(
                        color: Color(0xff32C533),
                        onPressed: () => {
                              dataDto.setCurrentPage(
                                  Transactions(dataDto), "Transactions")
                            },
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          'assets/back/back.png',
                          color: Colors.white,
                          fit: BoxFit.contain,
                          height: width > height ? width / 40 : width / 20,
                          width: width > height ? width / 40 : width / 20,
                        )),
                  )
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 4,
                        ),
                        Expanded(
                          child: Text(
                            "Date",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Motif",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Montant",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Fournisseur",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ),
                        ),
                        Container(
                            width: 100,
                            child: Text(
                              "N° Caisse",
                              style: TextStyle(
                                fontSize: SizeClacule().calcule(context, 60),
                              ),
                            )),
                        Container(
                            width: 150,
                            child: Text(
                              "Commentaire ",
                              style: TextStyle(
                                fontSize: SizeClacule().calcule(context, 60),
                              ),
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            for (var item in dataDto.operationList.reversed)
                              Column(
                                children: [
                                  FlatButton(
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Expanded(
                                          child: Text(
                                            DateFormat("dd-MM-yyyy HH:mm")
                                                .format(item.dateOperation),
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            getOperationTypeName(
                                                item.idTypeOperation),
                                            style: TextStyle(
                                              fontSize: SizeClacule()
                                                  .calcule(context, 60),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            item.montant != null
                                                ? item.montant
                                                        .toStringAsFixed(3) +
                                                    " DT"
                                                : "",
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            getFournisseurName(
                                                item.idFournisseur),
                                            style: TextStyle(
                                              fontSize: SizeClacule()
                                                  .calcule(context, 60),
                                            ),
                                          ),
                                        ),
                                        Container(
                                            width: 100,
                                            child: Text(
                                              getNumCaisse(item.caiIdCaisse),
                                              style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                              ),
                                              textAlign: TextAlign.center,
                                            )),
                                        Container(
                                            width: 150,
                                            child: Text(
                                              item.commenatire,
                                              style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                              ),
                                            ))
                                      ],
                                    ),
                                    color: Color(0xffE7D7EE),
                                    padding: EdgeInsets.only(
                                      top: 16,
                                      bottom: 16,
                                    ),
                                    onPressed: () {
                                      print("Operation: ");
                                    },
                                  ),
                                  SizedBox(
                                    height: 8,
                                  )
                                ],
                              )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
  }
}
