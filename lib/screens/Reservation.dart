import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/product/api/product_end_point_api.dart';
import 'package:ad_caisse/product/model/produit_dto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/reservation/api/reservation_end_point_api.dart';
import 'package:ad_caisse/reservation/model/ReservationResponse.dart';
import 'package:ad_caisse/reservation/model/reservation_dto.dart';
import 'package:ad_caisse/screens/Animator.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/NewReservation.dart';
import 'package:ad_caisse/screens/ReservationCoiff.dart';
import 'package:ad_caisse/screens/ReservationList.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel, EventList;
import 'package:intl/intl.dart' show DateFormat, Intl;

import 'ReservationT.dart';

class Reservation extends StatefulWidget {
  final DataDto dataDto;

  Reservation(this.dataDto);

  @override
  ReservationState createState() => ReservationState(this.dataDto);
}

class ReservationState extends State<Reservation> {
  final DataDto dataDto;
  DateTime _currentDate = DateTime.now();
  DateTime _currentDate2 = DateTime.now();
  String _currentMonth = DateFormat.yMMM().format(DateTime.now());
  DateTime _targetDateTime = DateTime.now();
  EventList<Event> _markedDateMap = new EventList<Event>();
  List<Event> events = [];
  CalendarCarousel _calendarCarousel, _calendarCarouselNoHeader;
  String search = "";
  ReservationEndPointApi reservationEndPointApi = ReservationEndPointApi();
  Utilisateur selectedUser;
  List<Utilisateur> services = [];

  ReservationState(this.dataDto);

  final utilisateurEndPointApi = UtilisateurEndPointApi();

  @override
  void initState() {
    dataDto.searchClient("Rechercher");
    _currentDate2 = dataDto.reservationDate;
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    getAllEmpl();
    getAllReservation(null);
    super.initState();
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }
  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  void getAllEmpl() async {
    try {
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente?.idPointVente);
      if (responseList?.result == 1 && responseList?.objectResponse != null) {
        setState(() {
          services = responseList.objectResponse
              .map((e) => Utilisateur.fromJson(e))
              .toList();
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("Reservation getAllEmpl", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getAllReservation(Utilisateur utilisateur) async {
    try {
      setState(() {
        _markedDateMap = new EventList<Event>();
        events = [];
      });
      ProductEndPointApi productEndPointApi = ProductEndPointApi();
      ReservationDto reservationDto = ReservationDto();
      reservationDto.idPointVente = dataDto.caisse.idPointVente;
      reservationDto.fTraite = 0;
      reservationDto.calledfrom = "CAISSE";
      ResponseList responseList = await reservationEndPointApi
          .findReservationByStatusAndIdPointVenteUsingPOST(reservationDto);
      List<ReservationResponse> res = [];
      if (responseList?.result == 1 && responseList?.objectResponse != null) {
        res = responseList.objectResponse
            .map((e) => ReservationResponse.fromJson(e))
            .toList();
        dataDto.reservations = res;
      }
      List<ReservationResponse> filtredByEmpl = [];
      if (utilisateur != null) {
        filtredByEmpl = res
            .where((element) =>
                element.vReservationTable.employee != null &&
                element.vReservationTable.employee.idUtilisateur ==
                    utilisateur.idUtilisateur)
            .toList();
      } else {
        filtredByEmpl = dataDto.reservations;
      }
      String desg = "";
      filtredByEmpl?.forEach((reservation) {
        if (reservation.vReservationTable.produitsQtes != null &&
            reservation.vReservationTable.produitsQtes.length > 0) {
          reservation.vReservationTable.produitsQtes.forEach((element) async {
            ResponseSingle single = await productEndPointApi
                .findproduitByIdProduitUsingGET(element.idProduit);
            if (single.result == 1) {
              ProduitDto produit = ProduitDto.fromJson(single.objectResponse);
              setState(() {
                _markedDateMap.add(
                    new DateTime(
                      reservation.vReservationTable.dateReseravation.year,
                      reservation.vReservationTable.dateReseravation.month,
                      reservation.vReservationTable.dateReseravation.day,
                    ),
                    new Event(
                      date: new DateTime(
                          reservation.vReservationTable.dateReseravation.year,
                          reservation.vReservationTable.dateReseravation.month,
                          reservation.vReservationTable.dateReseravation.day,
                          reservation.vReservationTable.dateReseravation.hour,
                          reservation
                              .vReservationTable.dateReseravation.minute),
                      title: reservation.nomClient != null
                          ? reservation.nomClient +
                              " " +
                              reservation.prenomClient +
                              "," +
                              produit.produit.designation
                          : reservation.vReservationTable.nom != null
                              ? reservation.vReservationTable.nom +
                                  " " +
                                  reservation.vReservationTable.prenom +
                                  "," +
                                  produit.produit.designation
                              : "",
                      dot: Container(
                        margin: EdgeInsets.symmetric(horizontal: 1.0),
                        color: Colors.transparent,
                        height: 1.0,
                        width: 1.0,
                      ),
                    ));
              });
              produit.produit.produitpointvente = produit.produitPointVentes
                          .where((element) =>
                              element.idPointVente ==
                              dataDto.pointVente.idPointVente)
                          .toList()
                          .length >
                      0
                  ? produit.produitPointVentes
                      .where((element) =>
                          element.idPointVente ==
                          dataDto.pointVente.idPointVente)
                      .toList()[0]
                  : null;
              element.produit = produit.produit;
            }

            setState(() {
              this.events = _markedDateMap.getEvents(DateTime(
                  _currentDate2.year, _currentDate2.month, _currentDate2.day));
              dataDto.reservations = res;
            });
          });
        } else if (dataDto.pointVente.fGestionTable == 1) {
          setState(() {
            _markedDateMap.add(
                new DateTime(
                  reservation.vReservationTable.dateReseravation.year,
                  reservation.vReservationTable.dateReseravation.month,
                  reservation.vReservationTable.dateReseravation.day,
                ),
                new Event(
                  date: new DateTime(
                      reservation.vReservationTable.dateReseravation.year,
                      reservation.vReservationTable.dateReseravation.month,
                      reservation.vReservationTable.dateReseravation.day,
                      reservation.vReservationTable.dateReseravation.hour,
                      reservation.vReservationTable.dateReseravation.minute),
                  title: reservation.nomClient != null
                      ? reservation.nomClient +
                          " " +
                          reservation.prenomClient +
                          "," +
                          reservation.vReservationTable.tableCaisse.numTable
                              .toString()
                      : reservation.vReservationTable.nom != null
                          ? reservation.vReservationTable.nom +
                              " " +
                              reservation.vReservationTable.prenom +
                              "," +
                              reservation.vReservationTable.tableCaisse.numTable
                                  .toString()
                          : "",
                  dot: Container(
                    margin: EdgeInsets.symmetric(horizontal: 1.0),
                    color: Colors.transparent,
                    height: 1.0,
                    width: 1.0,
                  ),
                ));
          });
          setState(() {
            this.events = _markedDateMap.getEvents(DateTime(
                _currentDate2.year, _currentDate2.month, _currentDate2.day));
            dataDto.reservations = res;
          });
        }
        //  print("desg: " + desg);
        String table = dataDto.pointVente.fGestionTable == 1
            ? "," +
                reservation.vReservationTable.tableCaisse.numTable.toString() +
                "," +
                ""
            : "," + desg;
      });
    } catch (err) {
      Crashlytics().sendEmail("Reservation getAllReservation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> showEmpDialog() async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      List<Utilisateur> services = [];
      final utilisateurEndPointApi = UtilisateurEndPointApi();
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1) {
        services = responseList.objectResponse
            .map((e) => Utilisateur.fromJson(e))
            .toList();
      }
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: Text(
              'Veuillez sélectionner un employé',
              textAlign: TextAlign.center,
            ),
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            content: Container(
              height: height * 0.60,
              width: width * 0.5,
              padding: EdgeInsets.all(16),
              child: GridView.count(
                padding: EdgeInsets.only(top: 8),
                crossAxisCount: width > height ? 4 : 2,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                childAspectRatio: 2,
                children: services
                    .map(
                      (item) => SizedBox(
                        child: WidgetANimator(FlatButton(
                            color: Color(0xffE7D7EE),
                            padding: EdgeInsets.all(0),
                            onPressed: () => {
                                  getAllReservation(item),
                                  Navigator.pop(context)
                                },
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      item.prenom + " " + item.nom,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: width > height
                                              ? width / 80
                                              : width / 35,
                                          color: Color(0xff352C60)),
                                    ),
                                  ],
                                )
                              ],
                            ))),
                      ),
                    )
                    .toList(),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                child: Text('Annuler'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("Reservation showEmpDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Intl.defaultLocale = 'fr';

    _calendarCarouselNoHeader = CalendarCarousel<Event>(
      weekdayTextStyle: TextStyle(color: Colors.white, fontSize: width / 100),
      weekDayBackgroundColor: Color(0xffCFC5FF),
      weekDayPadding: EdgeInsets.only(top: width / 100, bottom: width / 100),
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => {
              _currentDate2 = date,
              dataDto.reservationDate = date,
              this.events = events
            });
        print('date $date');
      },
      weekDayMargin: EdgeInsets.all(0),
      markedDatesMap: _markedDateMap,
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: width / 100),
      thisMonthDayBorderColor: Colors.grey,
      todayButtonColor: Colors.green,
      todayTextStyle: TextStyle(
        color: Colors.white,
        fontSize: width / 100,
      ),
      weekFormat: false,
      height: width / 3.7,
      selectedDateTime: _currentDate2,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateCustomShapeBorder:
          CircleBorder(side: BorderSide(color: Colors.purple)),
      showHeader: false,
      headerMargin: EdgeInsets.all(0),
      selectedDayTextStyle: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: width / 100),
      locale: "fr",
      minSelectedDate: _currentDate.subtract(Duration(days: 360)),
      maxSelectedDate: _currentDate.add(Duration(days: 360)),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.grey,
        fontSize: width / 100,
      ),
      nextDaysTextStyle: TextStyle(
        color: Colors.grey,
        fontSize: width / 100,
      ),
      prevDaysTextStyle: TextStyle(
        color: Colors.grey,
        fontSize: width / 100,
      ),
      daysTextStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
          fontSize: width / 100),
      dayPadding: width / 250,
      selectedDayButtonColor: Color(0xff8066FF),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat.yMMM().format(_targetDateTime);
        });
      },
    );

    return Stack(
      fit: StackFit.loose,
      children: [
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Réservation client",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width > height ? width / 50 : width / 30),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Pour visualiser les réservations d'une journée, veuillez cliquez sur la date souhaitée et cliquez sur voir les réservations",
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize:
                                width > height ? width / 120 : width / 60),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Pour créer une réservation veuillez choisir la date dans la calnedrier, et cliquez sur Ajouter une réservation.",
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize:
                                width > height ? width / 120 : width / 60),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  SizedBox(
                    height: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    width: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    child: FlatButton(
                        color: Color(0xff32C533),
                        onPressed: () => {
                              dataDto.setCurrentPage(
                                  CommandeScreen(dataDto), "CommandeScreen")
                            },
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          'assets/back/back.png',
                          color: Colors.white,
                          fit: BoxFit.contain,
                          height: width > height ? width / 40 : width / 20,
                          width: width > height ? width / 40 : width / 20,
                        )),
                  )
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                      child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          color: Color(0xff8066FF),
                          height: width / 25,
                          margin: EdgeInsets.only(
                            top: width / 100,
                          ),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              SizedBox(
                                width: width / 50,
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: width / 60,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _targetDateTime = DateTime(
                                          _targetDateTime.year,
                                          _targetDateTime.month - 1);
                                      _currentMonth = DateFormat.yMMM()
                                          .format(_targetDateTime);
                                    });
                                  },
                                ),
                              ),
                              Text(
                                _currentMonth,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: width / 100,
                                    color: Colors.white),
                              ),
                              SizedBox(
                                width: width / 50,
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                    size: width / 60,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _targetDateTime = DateTime(
                                          _targetDateTime.year,
                                          _targetDateTime.month + 1);
                                      _currentMonth = DateFormat.yMMM()
                                          .format(_targetDateTime);
                                    });
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          color: Color(0xffE7D7EE),
                          height: width / 3.7,
                          child: _calendarCarouselNoHeader,
                        ),
                        //
                      ],
                    ),
                  )),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: width / 100,
                        ),
                        SizedBox(
                          height: width / 20,
                          width: width / 3,
                          child: FlatButton(
                            color: Color(0xffCFC5FF),
                            child: Text(
                              DateFormat.yMMMEd().format(_currentDate2),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: width / 100),
                            ),
                            onPressed: () {},
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        selectedUser != null
                            ? SizedBox(
                                height: width / 30,
                                width: width / 3,
                                child: FlatButton(
                                  color: Color(0xffCFC5FF),
                                  child: Text(
                                    "Collaborateur : " +
                                        selectedUser.prenom +
                                        " " +
                                        selectedUser.nom,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 100),
                                  ),
                                  onLongPress: () {
                                    getAllReservation(null);
                                    setState(() {
                                      selectedUser = null;
                                    });
                                  },
                                  onPressed: () {},
                                ),
                              )
                            : SizedBox(),
                        SizedBox(
                          height: selectedUser != null ? 8 : 0,
                        ),
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              this.events.length > 0
                                  ? Container(
                                      padding: EdgeInsets.only(
                                          right: 8, top: 8, bottom: 8),
                                      margin: EdgeInsets.only(bottom: 8),
                                      color: Color(0xffCFC5FF),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 8,
                                          ),
                                          Expanded(
                                            child: Text(
                                              "Client",
                                              style: TextStyle(
                                                  fontSize: SizeClacule()
                                                      .calcule(context, 60),
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            width: width / 18,
                                            child: Text(
                                              "Heure",
                                              style: TextStyle(
                                                  fontSize: SizeClacule()
                                                      .calcule(context, 60),
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          dataDto.pointVente.fGestionTable == 1
                                              ? Expanded(
                                                  child: Text(
                                                    "N° Table",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: SizeClacule()
                                                            .calcule(
                                                                context, 60),
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                )
                                              : Expanded(
                                                  child: Text(
                                                    "Service",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: SizeClacule()
                                                            .calcule(
                                                                context, 60),
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                        ],
                                      ),
                                    )
                                  : Container(),
                              for (Event item in this.events)
                                Column(
                                  children: [
                                    FlatButton(
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 8,
                                          ),
                                          Expanded(
                                            child: Text(
                                              item.title.split(",")[0],
                                              style: TextStyle(
                                                  fontSize: SizeClacule()
                                                      .calcule(context, 60),
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            width: width / 18,
                                            child: Text(
                                              item.date.minute > 0
                                                  ? item.date.hour.toString() +
                                                      ":" +
                                                      item.date.minute
                                                          .toString()
                                                  : item.date.hour.toString() +
                                                      ":" +
                                                      item.date.minute
                                                          .toString() +
                                                      "0",
                                              style: TextStyle(
                                                  fontSize: SizeClacule()
                                                      .calcule(context, 60),
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          dataDto.pointVente.fGestionTable == 1
                                              ? Expanded(
                                                  child: Text(
                                                    item.title
                                                                .split(",")
                                                                .length >
                                                            1
                                                        ? item.title
                                                            .split(",")[1]
                                                        : item.title.replaceAll(
                                                            ",", ""),
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: SizeClacule()
                                                            .calcule(
                                                                context, 60),
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                )
                                              : Expanded(
                                                  child: Text(
                                                    item.title
                                                                .split(",")
                                                                .length >
                                                            1
                                                        ? item.title
                                                            .split(",")[1]
                                                        : item.title.replaceAll(
                                                            ",", ""),
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: SizeClacule()
                                                            .calcule(
                                                                context, 60),
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                        ],
                                      ),
                                      color: Color(0xffE7D7EE),
                                      padding: EdgeInsets.only(
                                          top: 16, bottom: 16, right: 8),
                                      onPressed: () {
                                        if (dataDto.pointVente.fGestionTable ==
                                            1) {
                                          dataDto.setCurrentPage(
                                              NewReservation(
                                                  dataDto,
                                                  dataDto.reservations[
                                                      int.parse(item.title
                                                          .split(",")[2])]),
                                              "NewReservation");
                                        } else {
                                          dataDto.setCurrentPage(
                                              NewReservation(
                                                  dataDto,
                                                  dataDto.reservations[
                                                      int.parse(item.title
                                                          .split(",")[1])]),
                                              "NewReservation");
                                        }
                                      },
                                    ),
                                    SizedBox(
                                      height: 8,
                                    )
                                  ],
                                )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      width: width / 4,
                      margin: EdgeInsets.only(left: 8, top: 14),
                      child: Column(
                        children: [
                          SizedBox(
                            height: width / 20,
                            width: width / 3,
                            child: FlatButton(
                              color: Color(0xff32C533),
                              child: Text(
                                "Calendrier des réservation",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                    fontSize: width / 100),
                              ),
                              onPressed: () {
                                setState(() {
                                  dataDto.reservationId = null;
                                });

                                dataDto.pointVente.fGestionTable == 1
                                    ? dataDto.setCurrentPage(
                                        ReservationT(dataDto), 'ReservationT')
                                    : dataDto.setCurrentPage(
                                        ReservationCoiff(dataDto),
                                        'ReservationCoiff');
                              },
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          SizedBox(
                            height: width / 20,
                            width: width / 3,
                            child: FlatButton(
                              color: Color(0xffCF5FFE),
                              child: Text(
                                "Ajouter une réservation",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                    fontSize: width / 100),
                              ),
                              onPressed: () {
                                setState(() {
                                  dataDto.reservationId = null;
                                });

                                dataDto.pointVente.fGestionTable == 1
                                    ? dataDto.setCurrentPage(
                                        ReservationT(dataDto), 'ReservationT')
                                    : dataDto.setCurrentPage(
                                        ReservationCoiff(dataDto),
                                        'ReservationCoiff');
                              },
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          SizedBox(
                            height: width / 20,
                            width: width / 3,
                            child: FlatButton(
                              color: Color(0xffCF5FFE),
                              child: Text(
                                "Voir les réservations",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                    fontSize: width / 100),
                              ),
                              onPressed: () {
                                dataDto.setCurrentPage(ReservationList(dataDto),
                                    "ReservationList");
                              },
                            ),
                          ),
                        ],
                      ))
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: width > height
                            ? SizeClacule().calcule(context, 30)
                            : SizeClacule().calcule(context, 15),
                        width: width > height
                            ? SizeClacule().calcule(context, 30)
                            : SizeClacule().calcule(context, 15),
                        child: FlatButton(
                            color: Colors.green,
                            padding: EdgeInsets.all(0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(width),
                            ),
                            onPressed: () {},
                            child: Text(
                              '',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: width / 100,
                                  color: Colors.white),
                            )),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        'Aujourd\'hui',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width / 100,
                            color: Colors.black),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: width > height
                            ? SizeClacule().calcule(context, 30)
                            : SizeClacule().calcule(context, 15),
                        width: width > height
                            ? SizeClacule().calcule(context, 30)
                            : SizeClacule().calcule(context, 15),
                        child: FlatButton(
                            color: Colors.deepPurpleAccent,
                            padding: EdgeInsets.all(0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(width),
                            ),
                            onPressed: () {},
                            child: Text(
                              '',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: width / 100,
                                  color: Colors.white),
                            )),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        'Selectionnée',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width / 100,
                            color: Colors.black),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: width > height
                            ? SizeClacule().calcule(context, 30)
                            : SizeClacule().calcule(context, 15),
                        width: width > height
                            ? SizeClacule().calcule(context, 30)
                            : SizeClacule().calcule(context, 15),
                        child: FlatButton(
                            padding: EdgeInsets.all(0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(width),
                                side: BorderSide(color: Colors.purpleAccent)),
                            onPressed: () {},
                            child: Text(
                              '',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: width / 100,
                                  color: Colors.white),
                            )),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        'Réservation',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width / 100,
                            color: Colors.black),
                      )
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
        dataDto.pointVente.fAffectEmployetoservice == 1 && search != ""
            ? Positioned(
                child: Container(
                  color: Colors.white,
                  child: Container(
                      child: SingleChildScrollView(
                          scrollDirection:
                              width > height ? Axis.vertical : Axis.horizontal,
                          child: DataTable(
                            columns: [
                              DataColumn(
                                label: Text('Nom'),
                              ),
                              DataColumn(label: Text('Email')),
                            ],
                            rows: services
                                .where((c) =>
                                    c.prenom != null &&
                                        c.prenom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nom != null &&
                                        c.nom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.email != null &&
                                        c.email
                                            .toLowerCase()
                                            .contains(search.toLowerCase()))
                                .toList()
                                .map(
                                  (empl) => DataRow(cells: [
                                    DataCell(
                                      Text(
                                        empl.nom + " " + empl.prenom,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${empl.nom}');
                                        getAllReservation(empl);
                                        setState(() {
                                          selectedUser = empl;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        empl.email != null ? empl.email : "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${empl.nom}');
                                        getAllReservation(empl);
                                        setState(() {
                                          selectedUser = empl;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                  ]),
                                )
                                .toList(),
                          ))),
                ),
                top: 0,
                left: 0,
                right: 0,
              )
            : SizedBox()
      ],
    );
  }
}
