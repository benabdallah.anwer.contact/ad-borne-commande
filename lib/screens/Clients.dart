import 'package:ad_caisse/customer/api/customer_end_point_api.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/screens/DetailClient.dart';
import 'package:ad_caisse/screens/NewClient.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Clients extends StatefulWidget {
  static const routeName = '/clients';
  final DataDto dataDto;

  Clients(this.dataDto);

  @override
  _MyHomePageState createState() => _MyHomePageState(this.dataDto);
}

class _MyHomePageState extends State<Clients> {
  bool sort;
  List<ClientBPrice> clients;
  DataDto dataDto;
  String search = "";
  FocusNode _myFocusNode = FocusNode();
  String _keyCode = '';

  _MyHomePageState(this.dataDto);

  var customerEndPointApi = CustomerEndPointApi();

  @override
  void initState() {
    super.initState();
    _myFocusNode.requestFocus();
    dataDto.focusNode.addListener(focusListener);
    clients = dataDto.listClient;
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    dataDto.searchClient("Rechercher Client");
    getListClient();
  }

  focusListener() {
    debugPrint('Keayboard: ' + _myFocusNode.hasFocus.toString());
    if (!dataDto.focusNode.hasFocus) {
      setState(() {
        _keyCode = '';
        _myFocusNode.requestFocus();
      });
    }
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  @override
  void dispose() {
    dataDto.focusNode.removeListener(focusListener);
    _myFocusNode.dispose();
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  getListClient() async {
    try {
      ResponseList responseList =
          await customerEndPointApi.findAllActiveClientByPartenaireUsingGET(
              dataDto.utilisateur.idPartenaire);
      List<ClientBPrice> list = responseList != null &&
              responseList?.result == 1 &&
              responseList?.objectResponse != null
          ? responseList.objectResponse
              .map((i) => ClientBPrice.fromJson(i))
              .toList()
          : [];
      setState(() {
        clients = list;
        dataDto.listClient = list;
        dataDto.listClientFiltred = list;
      });
    } catch (err) {
      Crashlytics().sendEmail("Clients getListClient", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  handleKey(RawKeyEvent event) {
    if (event.character != null) {
      if (event.physicalKey == PhysicalKeyboardKey.escape) {
        setState(() {
          _keyCode = '';
        });
        return;
      }
      _keyCode += event.character;
      debugPrint("RawKeyEvent: $_keyCode");
      List<ClientBPrice> list = dataDto.listClientFiltred
          .where((element) =>
              element.qrCodePartn != null &&
              element.qrCodePartn.toLowerCase() == _keyCode.toLowerCase())
          .toList();
      if (list.isNotEmpty) {
        setState(() {
          _keyCode = '';
        });
        dataDto.setCurrentPage(DetailClient(list[0], dataDto), "");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return RawKeyboardListener(
        onKey: (RawKeyEvent event) {
          handleKey(event);
        },
        focusNode: _myFocusNode,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 16,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Clients",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: SizeClacule().calcule(context, 25),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              children: [
                SizedBox(
                  height: width > height
                      ? SizeClacule().calcule(context, 15)
                      : SizeClacule().calcule(context, 7.5),
                  child: FlatButton(
                      color: Color(0xffCF5FFE),
                      onPressed: () =>
                          {dataDto.setCurrentPage(NewClient(dataDto), "")},
                      padding: EdgeInsets.only(left: 16, right: 8),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/adduser/adduser.png',
                            color: Colors.white,
                            fit: BoxFit.contain,
                            height: width > height ? width / 40 : width / 20,
                            width: width > height ? width / 40 : width / 20,
                          ),
                          Container(
                              padding: EdgeInsets.only(left: 16, right: 16),
                              child: Text(
                                "Ajouter Client",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400),
                                textAlign: TextAlign.center,
                              ))
                        ],
                      )),
                )
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Expanded(
                child: Column(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        "Nom",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        "@ Adresse",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "Téléphone",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Solde",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Expanded(
                    child: SingleChildScrollView(
                  child: Column(
                    children: [
                      for (var client in dataDto.listClientFiltred
                          .where((c) =>
                              c.prenom != null &&
                                  c.prenom
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                              c.nom != null &&
                                  c.nom
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                              c.nTel != null &&
                                  c.nTel
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                              c.email != null &&
                                  c.email
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                              c.qrCodePartn != null &&
                                  c.qrCodePartn
                                      .toLowerCase()
                                      .contains(search.toLowerCase()))
                          .toList())
                        Column(
                          children: [
                            FlatButton(
                              onPressed: () {
                                print('Selected ${client.qrCodePartn}');
                                //dataDto.setSearchController();
                                dataDto.setCurrentPage(
                                    DetailClient(client, dataDto), "");
                              },
                              color: Color(0xffE7D7EE),
                              padding: EdgeInsets.only(
                                top: height / 30,
                                bottom: height / 30,
                                left: 16,
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      client.nom + " " + client.prenom,
                                      style: TextStyle(
                                        fontSize:
                                            SizeClacule().calcule(context, 60),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      client.adress != null ? client.adress: "",
                                      style: TextStyle(
                                        fontSize:
                                            SizeClacule().calcule(context, 60),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      client.nTel,
                                      style: TextStyle(
                                        fontSize:
                                            SizeClacule().calcule(context, 60),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      client.soldePartn.toStringAsFixed(3) +
                                          " DT",
                                      style: TextStyle(
                                        fontSize:
                                            SizeClacule().calcule(context, 60),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            )
                          ],
                        )
                    ],
                  ),
                ))
              ],
            ))
          ],
        ));
  }
}
