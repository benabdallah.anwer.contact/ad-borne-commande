import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/pos/api/table_caisse_end_point_api.dart';
import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/product/api/product_end_point_api.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/reservation/api/reservation_end_point_api.dart';
import 'package:ad_caisse/reservation/model/ReservationResponse.dart';
import 'package:ad_caisse/reservation/model/reservation_dto.dart';
import 'package:ad_caisse/screens/NewReservation.dart';
import 'package:ad_caisse/screens/Reservation.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:intl/intl.dart' show DateFormat, Intl;

class ReservationT extends StatefulWidget {
  final DataDto dataDto;

  ReservationT(this.dataDto);

  @override
  ReservationState createState() => ReservationState(this.dataDto);
}

class ReservationState extends State<ReservationT> {
  final DataDto dataDto;
  List<Event> events = [];
  String search = "";
  ReservationEndPointApi reservationEndPointApi = ReservationEndPointApi();
  List<Utilisateur> services = [];
  final api_instance = new TableCaisseEndPointApi();

  List<TableCaisse> tables = [];
  bool light = true;

  ReservationState(this.dataDto);

  final utilisateurEndPointApi = UtilisateurEndPointApi();

  @override
  void initState() {
    dataDto.searchClient("Rechercher");
    dataDto.searchController.addListener(_printLatestValue);
    //getAllEmpl();
    getTables();
    getAllReservation(dataDto.reservationDate);
    super.initState();
  }

  @override
  void dispose() {
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  void getTables() async {
    try {
      ResponseList responseList =
          await api_instance.findAllByIdPointVenteAndFDefautUsingGET1(
              dataDto.pointVente.idPointVente);

      if (responseList.objectResponse != null) {
        List<TableCaisse> tables = responseList.objectResponse
            .map((e) => TableCaisse.fromJson(e))
            .toList();
        setState(() {
          this.tables = tables;
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("ReservationT getTables", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  void getAllEmpl() async {
    try {
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1 && responseList.objectResponse != null) {
        setState(() {
          services = responseList.objectResponse
              .map((e) => Utilisateur.fromJson(e))
              .toList();
        });
      }
    } catch (err) {
      Crashlytics().sendEmail("ReservationT getAllEmpl", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getAllReservation(DateTime dateTime) async {
    try {
      ProductEndPointApi productEndPointApi = ProductEndPointApi();
      ReservationDto reservationDto = ReservationDto();
      reservationDto.idPointVente = dataDto.caisse.idPointVente;
      reservationDto.dateReseravation = dateTime;
      reservationDto.calledfrom = "CAISSE";
      ResponseList responseList = await reservationEndPointApi
          .findReservationByDateAndIdPointVenteUsingPOST(reservationDto);
      List<ReservationResponse> res = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        res = responseList.objectResponse
            .map((e) => ReservationResponse.fromJson(e))
            .toList();
      }

      setState(() {
        dataDto.reservations = res;
      });
    } catch (err) {
      Crashlytics().sendEmail("ReservationT getAllReservation", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void callDatePicker() async {
    DateTime order = await getDate();
    if (order != null) {
      DateTime dateTime = order;
      setState(() {
        dataDto.reservationDate = dateTime;
      });
      getAllReservation(dateTime);
    }
  }

  Future<DateTime> getDate() {
    // Imagine that this function is
    // more complex and slow.
    return showDatePicker(
      context: context,
      helpText: "Date de réservation",
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2050),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: Color(0xff352C60),
          ),
          child: child,
        );
      },
    );
  }

  ReservationResponse getReservation(int time, Utilisateur u) {
    var list = dataDto.reservations
        .where((element) =>
            element.vReservationTable.dateReseravation.hour == time &&
            element.vReservationTable.employee.idUtilisateur == u.idUtilisateur)
        .toList();
    if (list.length > 0) {
      return list[0];
    } else {
      return null;
    }
  }

  ReservationResponse getReservationT(int time, TableCaisse u) {
    var list = dataDto.reservations
        .where((element) =>
            element.vReservationTable.dateReseravation.hour == time &&
            element.vReservationTable.tableCaisse.idTable == u.idTable)
        .toList();
    if (list.length > 0) {
      if (list[0].nomClient == null || list[0].prenomClient == null) {
        list[0].nomClient = list[0].vReservationTable.nom != null
            ? list[0].vReservationTable.nom
            : "";
        list[0].prenomClient = list[0].vReservationTable.prenom != null
            ? list[0].vReservationTable.prenom
            : "";
      }
      return list[0];
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Intl.defaultLocale = 'fr';

    var dayLight = TableRow(
        decoration: BoxDecoration(color: Color(0xffCF5FFE)),
        children: [
          TableCell(
            child: Container(
              child: Text(
                '00H-01H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
            child: Container(
              child: Text(
                '01H-02H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
              child: Container(
            child: Text(
              '02H-03H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '03H-04H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '04H-05H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '05H-06H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '06H-07H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '07H-08H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '08H-09H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '09H-10H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '10H-11H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '11H-12H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
        ]);

    var dayNight = TableRow(
        decoration: BoxDecoration(color: Color(0xffCF5FFE)),
        children: [
          TableCell(
            child: Container(
              child: Text(
                '12H-13H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
            child: Container(
              child: Text(
                '13H-14H',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: width / 100),
              ),
              height: 32,
              alignment: Alignment.center,
            ),
          ),
          TableCell(
              child: Container(
            child: Text(
              '14H-15H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '15H-16H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '16H-17H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '17H-18H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '18H-19H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '19H-20H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '20H-21H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '21H-22H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '22H-23H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
          TableCell(
              child: Container(
            child: Text(
              '23H-00H',
              style:
                  TextStyle(fontWeight: FontWeight.w600, fontSize: width / 100),
            ),
            height: 32,
            alignment: Alignment.center,
          )),
        ]);

    var tableLight = Table(
      border: TableBorder.all(
          color: Colors.white, width: 1, style: BorderStyle.solid),
      children: [
        dayLight,
        for (TableCaisse u in tables)
          TableRow(children: [
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(0, u) != null &&
                        getReservationT(0, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(0, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(0, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 0, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                    getReservationT(0, u) != null
                        ? getReservationT(0, u).prenomClient +
                            " " +
                            getReservationT(0, u).nomClient
                        : "",
                    style: TextStyle(fontSize: width / 120),
                    textAlign: TextAlign.center,
                  ),
                  color: getReservationT(0, u) != null &&
                          getReservationT(0, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(0, u) != null &&
                              getReservationT(0, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(0, u) != null &&
                                  getReservationT(0, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(1, u) != null &&
                        getReservationT(1, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(1, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(1, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 1, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(1, u) != null
                          ? getReservationT(1, u).prenomClient +
                              " " +
                              getReservationT(1, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(1, u) != null &&
                          getReservationT(1, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(1, u) != null &&
                              getReservationT(1, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(1, u) != null &&
                                  getReservationT(1, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(2, u) != null &&
                        getReservationT(2, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(2, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(2, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 2, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(2, u) != null
                          ? getReservationT(2, u).prenomClient +
                              " " +
                              getReservationT(2, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(2, u) != null &&
                          getReservationT(2, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(2, u) != null &&
                              getReservationT(2, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(2, u) != null &&
                                  getReservationT(2, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(3, u) != null &&
                        getReservationT(3, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(3, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(3, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 3, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(3, u) != null
                          ? getReservationT(3, u).prenomClient +
                              " " +
                              getReservationT(3, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(3, u) != null &&
                          getReservationT(3, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(3, u) != null &&
                              getReservationT(3, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(3, u) != null &&
                                  getReservationT(3, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(4, u) != null &&
                        getReservationT(4, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(4, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(4, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 4, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(4, u) != null
                          ? getReservationT(4, u).prenomClient +
                              " " +
                              getReservationT(4, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(4, u) != null &&
                          getReservationT(4, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(4, u) != null &&
                              getReservationT(4, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(4, u) != null &&
                                  getReservationT(4, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(5, u) != null &&
                        getReservationT(5, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(5, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(5, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 5, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(5, u) != null
                          ? getReservationT(5, u).prenomClient +
                              " " +
                              getReservationT(5, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(5, u) != null &&
                          getReservationT(5, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(5, u) != null &&
                              getReservationT(5, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(5, u) != null &&
                                  getReservationT(5, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(6, u) != null &&
                        getReservationT(6, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(6, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(6, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 6, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(6, u) != null
                          ? getReservationT(6, u).prenomClient +
                              " " +
                              getReservationT(6, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(6, u) != null &&
                          getReservationT(6, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(6, u) != null &&
                              getReservationT(6, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(6, u) != null &&
                                  getReservationT(6, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(7, u) != null &&
                        getReservationT(7, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(7, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(7, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 7, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(7, u) != null
                          ? getReservationT(7, u).prenomClient +
                              " " +
                              getReservationT(7, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(7, u) != null &&
                          getReservationT(7, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(7, u) != null &&
                              getReservationT(7, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(7, u) != null &&
                                  getReservationT(7, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(8, u) != null &&
                        getReservationT(8, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(8, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(8, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 8, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(8, u) != null
                          ? getReservationT(8, u).prenomClient +
                              " " +
                              getReservationT(8, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(8, u) != null &&
                          getReservationT(8, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(8, u) != null &&
                              getReservationT(8, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(8, u) != null &&
                                  getReservationT(8, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(9, u) != null &&
                        getReservationT(9, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(9, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(9, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 9, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(9, u) != null
                          ? getReservationT(9, u).prenomClient +
                              " " +
                              getReservationT(9, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(9, u) != null &&
                          getReservationT(9, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(9, u) != null &&
                              getReservationT(9, u).vReservationTable.fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(9, u) != null &&
                                  getReservationT(9, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(10, u) != null &&
                        getReservationT(10, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(10, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(10, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 10, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(10, u) != null
                          ? getReservationT(10, u).prenomClient +
                              " " +
                              getReservationT(10, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(10, u) != null &&
                          getReservationT(10, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(10, u) != null &&
                              getReservationT(10, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(10, u) != null &&
                                  getReservationT(10, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(11, u) != null &&
                        getReservationT(11, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(11, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(11, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 11, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(11, u) != null
                          ? getReservationT(11, u).prenomClient +
                              " " +
                              getReservationT(11, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(11, u) != null &&
                          getReservationT(11, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(11, u) != null &&
                              getReservationT(11, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(11, u) != null &&
                                  getReservationT(11, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
          ]),
      ],
    );

    var tableNight = Table(
      border: TableBorder.all(
          color: Colors.white, width: 1, style: BorderStyle.solid),
      children: [
        dayNight,
        for (TableCaisse u in tables)
          TableRow(children: [
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(12, u) != null &&
                        getReservationT(12, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(12, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(12, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 12, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                    getReservationT(12, u) != null
                        ? getReservationT(12, u).prenomClient +
                            " " +
                            getReservationT(12, u).nomClient
                        : "",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: width / 120),
                  ),
                  color: getReservationT(12, u) != null &&
                          getReservationT(12, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(12, u) != null &&
                              getReservationT(12, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(12, u) != null &&
                                  getReservationT(12, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(13, u) != null &&
                        getReservationT(13, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(13, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(13, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 13, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(13, u) != null
                          ? getReservationT(13, u).prenomClient +
                              " " +
                              getReservationT(13, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(13, u) != null &&
                          getReservationT(13, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(13, u) != null &&
                              getReservationT(13, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(13, u) != null &&
                                  getReservationT(13, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(14, u) != null &&
                        getReservationT(14, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(14, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(14, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 14, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(14, u) != null
                          ? getReservationT(14, u).prenomClient +
                              " " +
                              getReservationT(14, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(14, u) != null &&
                          getReservationT(14, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(14, u) != null &&
                              getReservationT(14, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(14, u) != null &&
                                  getReservationT(14, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(15, u) != null &&
                        getReservationT(15, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(15, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(15, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 15, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(15, u) != null
                          ? getReservationT(15, u).prenomClient +
                              " " +
                              getReservationT(15, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(15, u) != null &&
                          getReservationT(15, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(15, u) != null &&
                              getReservationT(15, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(15, u) != null &&
                                  getReservationT(15, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(16, u) != null) {
                      if (getReservationT(16, u) != null &&
                          getReservationT(16, u).vReservationTable.fTraite !=
                              0) {
                        return;
                      }
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(16, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 16, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(16, u) != null
                          ? getReservationT(16, u).prenomClient +
                              " " +
                              getReservationT(16, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(16, u) != null &&
                          getReservationT(16, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(16, u) != null &&
                              getReservationT(16, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(16, u) != null &&
                                  getReservationT(16, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(17, u) != null &&
                        getReservationT(17, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(17, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(17, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 17, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(17, u) != null
                          ? getReservationT(17, u).prenomClient +
                              " " +
                              getReservationT(17, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(17, u) != null &&
                          getReservationT(17, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(17, u) != null &&
                              getReservationT(17, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(17, u) != null &&
                                  getReservationT(17, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(18, u) != null &&
                        getReservationT(18, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(18, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(18, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 18, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(18, u) != null
                          ? getReservationT(18, u).prenomClient +
                              " " +
                              getReservationT(18, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(18, u) != null &&
                          getReservationT(18, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(18, u) != null &&
                              getReservationT(18, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(18, u) != null &&
                                  getReservationT(18, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(19, u) != null &&
                        getReservationT(19, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(19, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(19, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 19, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(19, u) != null
                          ? getReservationT(19, u).prenomClient +
                              " " +
                              getReservationT(19, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(19, u) != null &&
                          getReservationT(19, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(19, u) != null &&
                              getReservationT(19, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(19, u) != null &&
                                  getReservationT(19, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(20, u) != null &&
                        getReservationT(20, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(20, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(20, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 20, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(20, u) != null
                          ? getReservationT(20, u).prenomClient +
                              " " +
                              getReservationT(20, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(20, u) != null &&
                          getReservationT(20, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(20, u) != null &&
                              getReservationT(20, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(20, u) != null &&
                                  getReservationT(20, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(21, u) != null &&
                        getReservationT(21, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(21, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(21, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 21, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(21, u) != null
                          ? getReservationT(21, u).prenomClient +
                              " " +
                              getReservationT(21, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(21, u) != null &&
                          getReservationT(21, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(21, u) != null &&
                              getReservationT(21, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(21, u) != null &&
                                  getReservationT(21, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(22, u) != null &&
                        getReservationT(22, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(22, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(22, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 22, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(22, u) != null
                          ? getReservationT(22, u).prenomClient +
                              " " +
                              getReservationT(22, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(22, u) != null &&
                          getReservationT(22, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(22, u) != null &&
                              getReservationT(22, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(22, u) != null &&
                                  getReservationT(22, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
            TableCell(
              child: SizedBox(
                height: height / 5,
                child: FlatButton(
                  onPressed: () {
                    if (getReservationT(23, u) != null &&
                        getReservationT(23, u).vReservationTable.fTraite != 0) {
                      return;
                    }
                    if (getReservationT(23, u) != null) {
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, getReservationT(23, u)),
                          "NewReservation");
                    } else {
                      dataDto.finalTime = TimeOfDay(hour: 23, minute: 0);
                      dataDto.tableCaisse = u;
                      dataDto.setCurrentPage(
                          NewReservation(dataDto, null), "NewReservation");
                    }
                  },
                  child: Text(
                      getReservationT(23, u) != null
                          ? getReservationT(23, u).prenomClient +
                              " " +
                              getReservationT(23, u).nomClient
                          : "",
                      style: TextStyle(fontSize: width / 120),
                      textAlign: TextAlign.center),
                  color: getReservationT(23, u) != null &&
                          getReservationT(23, u).vReservationTable.fTraite == 0
                      ? Colors.redAccent
                      : getReservationT(23, u) != null &&
                              getReservationT(23, u)
                                      .vReservationTable
                                      .fTraite ==
                                  1
                          ? Colors.lightGreen
                          : getReservationT(23, u) != null &&
                                  getReservationT(23, u)
                                          .vReservationTable
                                          .fTraite ==
                                      -1
                              ? Colors.amber
                              : Colors.green,
                ),
              ),
            ),
          ]),
      ],
    );

    return Stack(
      fit: StackFit.loose,
      children: [
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Table Réservations",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: width > height ? width / 50 : width / 30),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            FlatButton(
                                onPressed: () {
                                  callDatePicker();
                                },
                                child: Text(
                                  DateFormat("dd-MM-yyyy")
                                      .format(dataDto.reservationDate),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: width > height
                                          ? width / 60
                                          : width / 40),
                                )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "Après midi",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: width / 100),
                                ),
                                Switch(
                                  value: light,
                                  onChanged: (value) {
                                    setState(() {
                                      light = value;
                                      print(light);
                                    });
                                  },
                                  inactiveThumbColor: Colors.purpleAccent,
                                  activeColor: Colors.purpleAccent,
                                  activeTrackColor: Colors.grey,
                                ),
                                Text(
                                  "Matin",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: width / 100),
                                )
                              ],
                            ),
                          ],
                        )),
                  ),
                  SizedBox(
                    height: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    width: width > height
                        ? SizeClacule().calcule(context, 15)
                        : SizeClacule().calcule(context, 7.5),
                    child: FlatButton(
                        color: Color(0xff32C533),
                        onPressed: () => {
                              dataDto.setCurrentPage(
                                  Reservation(dataDto), "Reservation")
                            },
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          'assets/back/back.png',
                          color: Colors.white,
                          fit: BoxFit.contain,
                          height: width > height ? width / 40 : width / 20,
                          width: width > height ? width / 40 : width / 20,
                        )),
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 32,
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.blueGrey,
                                border: Border(
                                  left: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  top: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  right: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                )),
                            child: FlatButton(
                                padding: EdgeInsets.all(0),
                                onPressed: () {
                                  setState(() {
                                    light = !light;
                                  });
                                },
                                child: Text(
                                  light ? "Matin" : "Après midi",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: width / 100),
                                )),
                            alignment: Alignment.center,
                          ),
                        ),
                        for (TableCaisse table in tables)
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xffCFC5FF),
                                border: Border(
                                  left: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  top: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  right: BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                )),
                            child: Text(
                              "Table N°\n" + table.numTable.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            height: height / 5,
                            alignment: Alignment.center,
                          ),
                      ],
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    child: light ? tableLight : tableNight,
                    flex: 10,
                  ),
                ],
              )
            ],
          ),
        ),
        dataDto.pointVente.fAffectEmployetoservice == 1 && search != ""
            ? Positioned(
                child: Container(
                  color: Colors.white,
                  child: Container(
                      child: SingleChildScrollView(
                          scrollDirection:
                              width > height ? Axis.vertical : Axis.horizontal,
                          child: DataTable(
                            columns: [
                              DataColumn(
                                label: Text('Nom'),
                              ),
                              DataColumn(label: Text('Email')),
                            ],
                            rows: services
                                .where((c) =>
                                    c.prenom != null &&
                                        c.prenom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nom != null &&
                                        c.nom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.email != null &&
                                        c.email
                                            .toLowerCase()
                                            .contains(search.toLowerCase()))
                                .toList()
                                .map(
                                  (empl) => DataRow(cells: [
                                    DataCell(
                                      Text(
                                        empl.nom + " " + empl.prenom,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${empl.nom}');
                                        getAllReservation(
                                            dataDto.reservationDate);
                                        setState(() {
                                          // selectedUser = empl;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        empl.email != null ? empl.email : "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        print('Selected ${empl.nom}');
                                        getAllReservation(
                                            dataDto.reservationDate);
                                        setState(() {
                                          // selectedUser = empl;
                                          dataDto.searchController.text = "";
                                        });
                                      },
                                    ),
                                  ]),
                                )
                                .toList(),
                          ))),
                ),
                top: 0,
                left: 0,
                right: 0,
              )
            : SizedBox()
      ],
    );
  }
}
