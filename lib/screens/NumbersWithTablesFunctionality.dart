import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';

class NumbersWithTablesFunctionality extends StatelessWidget {
  var numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".","0.", "C","T","N"];
  Function callback;
  DataDto dataDto;

  NumbersWithTablesFunctionality(this.callback,this.dataDto);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return GridView.count(
      padding: EdgeInsets.only(left: 8,right: 8),
      crossAxisCount: 5,
      mainAxisSpacing: 8.0,
      crossAxisSpacing: 8.0,
      children: numbers
          .map(
            (item) => Container(

          decoration: BoxDecoration(
            border: Border.all(
                width: 1,
                color: item == "C" ? Color(0xffCF0606):item=="T"?Color(0xff9d197f) :item=="N"?Color(0xff70B3FF): Color(0xff707070)),
          ),
          child: SizedBox(child: FlatButton(
              color: item == "C" ? Color(0xffCF0606) :item=="T"?Color(0xff9d197f) :item=="N"?Color(0xff70B3FF):Color(0xffE7D7EE),
              onPressed: () => {this.callback(item,context)},
              child:item=="T"? dataDto.loadingTable==false?Image.asset(
                dataDto.pointVente.fGestionTable==1?'assets/restaurant/restaurant.png':'assets/restaurant/bipeur.png',
                color: Colors.white,
                fit: BoxFit.contain,
                height: width > height
                    ? SizeClacule()
                    .calcule(context, 30)
                    : SizeClacule()
                    .calcule(context, 15),
                width: width > height
                    ? SizeClacule()
                    .calcule(context, 30)
                    : SizeClacule()
                    .calcule(context, 15),
              ):SizedBox(
                height: width > height
                    ? width / 150
                    : width / 60,
                width: width > height
                    ? width / 150
                    : width / 60,
                child: CircularProgressIndicator(
                    valueColor:
                    new AlwaysStoppedAnimation<
                        Color>(
                        Colors
                            .white)),
              )
                  :item=="N"?Image.asset(
                'assets/bill/bill.png',
                color: Colors.white,
                fit: BoxFit.contain,
                height: width > height
                    ? SizeClacule()
                    .calcule(context, 30)
                    : SizeClacule()
                    .calcule(context, 15),
                width: width > height
                    ? SizeClacule()
                    .calcule(context, 30)
                    : SizeClacule()
                    .calcule(context, 15),
              )




                  :Text(
                item,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 60,
                    color:item == "C" ? Colors.white :  Color(0xff352C60)),
              )

          ),
          ),),
      )
          .toList(),
    );
  }
}
