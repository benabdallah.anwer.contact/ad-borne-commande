import 'dart:convert';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/Login_Screen.dart';
import 'package:ad_caisse/transaction/api/commande_end_point_api.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

class CommandesCuisine extends StatefulWidget {
  final DataDto dataDto;

  CommandesCuisine(this.dataDto);

  @override
  CommandesState createState() => CommandesState(this.dataDto);
}

class CommandesState extends State<CommandesCuisine> {
  DataDto dataDto;
  String search = "";
  var commandeEndPointApi = CommandeEndPointApi();

  CommandesState(this.dataDto);

  @override
  void initState() {
    super.initState();
    //dataDto.searchClient("Rechercher Commande");
    registerSocket();
    getCommandes();
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
  }

  registerSocket() {
    String url = ServerUrl.url + '/bp-api-admin/jsa-stomp-endpoint';
    if (ServerUrl.url.contains('zull-serveur')) {
      url = 'http://62.171.144.28:8080/bp-api-admin/jsa-stomp-endpoint';
    }

    final stompClient = StompClient(
        config: StompConfig.SockJS(
      url: url,
      onConnect: (StompClient client, StompFrame frame) {
        client.subscribe(
            destination: '/topic/cuisine',
            callback: (StompFrame frame) {
              debugPrint(frame.body);
              getCommandes();
            });
      },
      onStompError: (error) {
        debugPrint('StompClient: ' + error.toString());
      },
      onWebSocketError: (dynamic error) =>
          debugPrint('StompClient: ' + error.toString()),
    ));
    stompClient.activate();
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  void deleteCommande(commandesDtoTransaction) async {
    try {
      bool connected = await Utils().checkInternet();
      String url = ServerUrl.url;
      if (!connected) {
        url = ServerUrl.urlSynchro;
      }

      var response = await http.get(Uri.parse(url +
          '/bp-api-ordering/v1/commandeTriatementCuisine/' +
          commandesDtoTransaction.commande.idCommande));

      if (response != null && response.statusCode == 200) {
        debugPrint('deleteCommande: ' + response.body);
        ResponseSingle responseSingle =
            ResponseSingle.fromJson(json.decode(response.body));
        if (responseSingle.result != 1) {
          AlertNotif().alert(context, responseSingle.errorDescription);
        } else {
          await getCommandes();
        }
      }
    } catch (err) {
      Crashlytics().sendEmail("Commandes deleteCommande", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getCommandes() async {
    try {
      bool connected = await Utils().checkInternet();
      String url = ServerUrl.url;
      if (!connected) {
        url = ServerUrl.urlSynchro;
      }
      debugPrint(url +
          '/bp-api-transaction/v1/findAllByIdPointVenteAndStatus/' +
          dataDto.pointVente.idPointVente +
          '/' +
          'en cours de préparation');
      var response = await http.get(Uri.parse(url +
          '/bp-api-transaction/v1/findAllByIdPointVenteAndStatus/' +
          dataDto.pointVente.idPointVente +
          '/' +
          'en%20cours%20de%20pr%C3%A9paration'));

      if (response != null && response.statusCode == 200) {
        debugPrint(response.body);
        ResponseList responseList =
            ResponseList.fromJson(json.decode(response.body));
        List<CommandesDtoTransaction> list = [];
        if (responseList.result == 1 && responseList.objectResponse != null) {
          list = List.from(responseList.objectResponse)
              .map((e) => CommandesDtoTransaction.fromJson(e))
              .toList()
              .where((element) => element.commandeDetails?.isNotEmpty)
              .toList();
        }
        setState(() {
          dataDto.listCommande = list;
        });
       print(dataDto.listCommande[0].commandeDetails[0].commandeDetails.toString()) ;
      }
    } catch (err) {
      Crashlytics().sendEmail("Commandes getCommandes", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }


  Future<void> alert(CommandesDtoTransaction commandesDtoTransaction) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Êtes-vous sûr de vouloir traiter cette commande ?',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('ANNULER'),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: Colors.black26,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text('OK'),
              elevation: 0,
              color: Colors.green,
              onPressed: () {
                deleteCommande(commandesDtoTransaction);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 8,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Commandes :",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: width > height ? width / 50 : width / 30),
                ),
                Expanded(child: Container()),
                TextButton(
                    onPressed: () => {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()),
                          )
                        },
                    child: Image.asset(
                      'assets/logout/logout.png',
                      fit: BoxFit.contain,
                      color: Colors.red,
                      height: 24,
                      width: 24,
                    )),
                TextButton(
                    onPressed: () {
                      getCommandes();
                    },
                    child: Icon(Icons.refresh))
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Expanded(
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        child: Text(
                          "N°",
                          style: TextStyle(
                            fontSize: SizeClacule().calcule(context, 60),
                          ),
                        ),
                        padding: EdgeInsets.only(left: 8),
                        width: 60,
                      ),
                      Container(
                        child: Text(
                          "Date",
                          style: TextStyle(
                            fontSize: SizeClacule().calcule(context, 60),
                          ),
                        ),
                        width: 200,
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "Détail commande",
                          style: TextStyle(
                            fontSize: SizeClacule().calcule(context, 60),
                          ),
                        ),
                      ),
                      Container(

                        child: Text(
                          "Client",
                          style: TextStyle(
                            fontSize: SizeClacule().calcule(context, 60),
                          ),
                        ),
                        padding: EdgeInsets.only(left: 8),
                        width: 150,
                      ),
                      dataDto.pointVente?.fGestionTable == 1
                          ? Container(
                              width: 80,
                              child: Text(
                                "N° Table",
                                style: TextStyle(
                                  fontSize: SizeClacule().calcule(context, 60),
                                ),
                              ))
                          : SizedBox(
                              width: 0,
                            ),
                      Container(
                          width: 120,
                          margin: EdgeInsets.only(left: 16),
                          child: Text(
                            "Type",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                            ),
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
             Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          for (var item in dataDto.listCommande.reversed
                              .where((c) =>
                                  c.commande.numCommande
                                      .toString()
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                                  c.commande.numTable
                                      .toString()
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                                  c.commandeDetails
                                          .where((element) => element
                                              .commandeDetails.designation
                                              .toLowerCase()
                                              .contains(search.toLowerCase()))
                                          .toList()
                                          .length >
                                      0)
                              .toList())
                            Column(
                              children: [
                                FlatButton(
                                  child: Row(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(left: 8),
                                        width: 60,
                                        child: Text(
                                          item.commande.numCommande.toString(),
                                          style: TextStyle(
                                              fontSize: SizeClacule()
                                                  .calcule(context, 60),
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          DateFormat("dd-MM-yyyy HH:mm").format(
                                              item.commande.dateCreation),
                                          style: TextStyle(
                                              fontSize: SizeClacule()
                                                  .calcule(context, 60),
                                              fontWeight: FontWeight.w500),
                                        ),
                                        width: 200,
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            for (var items in item.commandeDetails)

                                              Column(
                                                children: [
                                                  Text(
                                                    items.commandeDetails.quantite.toString()+
                                                        " X " +
                                                        items.commandeDetails.designation.toString()+
                                                        " " ,
                                                    style: TextStyle(
                                                        fontSize: SizeClacule()
                                                            .calcule(
                                                                context, 80),
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                  items.commandeDetails
                                                              .ingredient !=
                                                          null
                                                      ? Text(items
                                                          .commandeDetails
                                                          .ingredient)
                                                      : SizedBox(),
                                                  SizedBox(
                                                    height: 8,
                                                  )
                                                ],
                                              )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 2),
                                        width: 150,
                                        child: Text(
                                          item.commande.clients!=null&&item.commande.clients.length>0?item.commande.clients[0].prenom+" "+item.commande.clients[0].nom :" "  ,
                                          style: TextStyle(
                                              fontSize: SizeClacule()
                                                  .calcule(context, 60),
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      dataDto.pointVente.fGestionTable == 1
                                          ? Container(
                                              alignment: Alignment.center,
                                              width: 80,
                                              child: Text(
                                                item.commande.numTable != null
                                                    ? item.commande.numTable
                                                        .toString()
                                                    : "",
                                                style: TextStyle(
                                                  fontSize: SizeClacule()
                                                      .calcule(context, 60),
                                                ),
                                              ))
                                          : SizedBox(
                                              width: 0,
                                            ),
                                      Container(
                                          width: 120,
                                          margin: EdgeInsets.only(left: 16),
                                          child: Text(
                                            item.commande.typeCommande != null
                                                ? item.commande.typeCommande
                                                : "",
                                            style: TextStyle(
                                              fontSize: SizeClacule()
                                                  .calcule(context, 60),
                                            ),
                                          ))
                                    ],
                                  ),
                                  color: Color(0xffE7D7EE),
                                  padding: EdgeInsets.only(
                                    top: 16,
                                    bottom: 16,
                                  ),
                                  onPressed: () {
                                    alert(item);
                                  },
                                ),
                                SizedBox(
                                  height: 8,
                                )
                              ],
                            )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
