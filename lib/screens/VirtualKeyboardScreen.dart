import 'package:ad_caisse/dto/DataDto.dart';
import 'package:flutter/material.dart';
import 'package:virtual_keyboard/virtual_keyboard.dart';

class VirtualKeyboardScreen extends StatefulWidget {
  DataDto dataDto;

  VirtualKeyboardScreen(this.dataDto);

  @override
  State<StatefulWidget> createState() => new _State(this.dataDto);
}

class _State extends State<VirtualKeyboardScreen> {
  DataDto dataDto;

  _State(this.dataDto);

  bool loading = false;
  int focused;

  //bool hideKeyboard = false;

  // Holds the text that user typed.
  String text = '';

  // True if shift enabled.
  bool shiftEnabled = false;

  _onKeyPress(VirtualKeyboardKey key) {
    debugPrint('VirtualKeyboardKey: ' + key.action.toString());
    if (key.keyType == VirtualKeyboardKeyType.String) {
      //  text = text + (shiftEnabled ? key.capsText : key.text);
      dataDto.textController.text = dataDto.textController.text +
          (shiftEnabled ? key.capsText : key.text);
    } else if (key.keyType == VirtualKeyboardKeyType.Action) {
      switch (key.action) {
        case VirtualKeyboardKeyAction.Backspace:
          if (dataDto.textController.text.isEmpty) {
            return;
          }
          dataDto.textController.text = dataDto.textController.text
              .substring(0, dataDto.textController.text.length - 1);
          break;
        case VirtualKeyboardKeyAction.Return:
          dataDto.textController.text = dataDto.textController.text + '\n';
          break;
        case VirtualKeyboardKeyAction.Space:
          dataDto.textController.text = dataDto.textController.text + key.text;
          break;
        case VirtualKeyboardKeyAction.Shift:
          shiftEnabled = !shiftEnabled;
          break;
        case VirtualKeyboardKeyAction.Num:
          dataDto.hideKeyboard = true;
          break;
        default:
      }
    }
    // Update the screen
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return SizedBox(
      width: dataDto.hideKeyboard ? 0 : width,
      child: Container(
        color: Color(0xffeeeeee),
        padding: EdgeInsets.all(2),
        child: VirtualKeyboard(
            fontSize: width / 55,
            textColor: Colors.black,
            type: dataDto.isNumericMode
                ? VirtualKeyboardType.Numeric
                : VirtualKeyboardType.Alphanumeric,
            onKeyPress: _onKeyPress),
      ),
    );
  }
}
