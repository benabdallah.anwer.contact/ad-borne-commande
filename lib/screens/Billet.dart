import 'dart:convert';

import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/pos/api/journee_end_point_api.dart';
import 'package:ad_caisse/pos/api/session_end_point_api.dart';
import 'package:ad_caisse/pos/model/billet_monnaie.dart';
import 'package:ad_caisse/pos/model/journee.dart';
import 'package:ad_caisse/pos/model/montant_ouverture.dart';
import 'package:ad_caisse/pos/model/session.dart';
import 'package:ad_caisse/pos/model/session_montanouverture.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/screens/Dashboard.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:flutter/material.dart';

import 'NumbersLogin.dart';

class Billet extends StatefulWidget {
  final DataDto dataDto;
  final List<BilletMonnaie> list;

  Billet(this.dataDto, this.list);

  @override
  State<StatefulWidget> createState() =>
      new BilletState(this.dataDto, this.list);
}

class BilletState extends State<Billet> {
  DataDto dataDto;
  List<BilletMonnaie> list;

  var createJournee = JourneeEndPointApi();
  var sessionEndPointApi = SessionEndPointApi();
  double montantOuverture = 0;
  TextEditingController montant = new TextEditingController();
  bool isAvaible ;



  BilletState(this.dataDto, this.list);
  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      isAvaible=false;
    });
    super.initState();
  }

  void createNewJournee() async {
    try {
      if (dataDto.journee != null) {
        creatNewSession(dataDto.journee);
        return;
      }
      Journee journee = Journee();
      journee.idPointVente = dataDto.caisse.idPointVente;
      journee.dateOuverture = DateTime.now();
      journee.fCloture = 0;
      ResponseSingle responseSingle =
          await createJournee.createjourneeUsingPOST(journee);
      Journee journeeRet = Journee.fromJson(responseSingle.objectResponse);
      dataDto.journee = journeeRet;
      if (responseSingle.objectResponse != null) {
        creatNewSession(journeeRet);
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics().sendEmail("Billet createNewJournee", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void creatNewSession(Journee journee) async {
    try {
      print("Montant: " + montant.text);
      print("fDetailMontant: " + dataDto.pointVente.fDetailMontant.toString());
      if (montant.text == "") {
        setState(() {
          montant.text = "0";
        });
      }
      double d = double.parse(montant.text);
      print("Montant: " + d.toStringAsFixed(3));
      Session session = Session();
      session.idJournee = journee.idJournee;
      session.idCaisse = dataDto.caisse.idCaisse;
      session.idUtilisateur = dataDto.utilisateur.idUtilisateur;
      session.dateDebut = DateTime.now();
      session.fFerme = 0;
      session.montantOuverture =
          dataDto.pointVente.fDetailMontant != 1 ? montantOuverture : d;
      print("SessionDto: " + jsonEncode(session.toJson()));
      ResponseSingle responseSingle =
          await sessionEndPointApi.createsessionUsingPOST(session);
      Session sessionRet = Session.fromJson(responseSingle.objectResponse);
      dataDto.session = sessionRet;
      if (responseSingle.objectResponse != null) {
        dataDto.pointVente.fDetailMontant != 1
            ? createListMontantOuverture(sessionRet)
            :  Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => Dashboard(dataDto)),
              );
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics().sendEmail("Billet creatNewSession", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void createListMontantOuverture(Session session) {
    try {
      List<MontantOuverture> listMontantOuverture = [];
      list.forEach((element) {
        MontantOuverture montantOuverture = MontantOuverture();
        montantOuverture.valeur = element.valeur;
        montantOuverture.qteBillet = element.qte;
        montantOuverture.idSession = session.idSession;
        listMontantOuverture.add(montantOuverture);
      });
      createMontantForCaisse(session, listMontantOuverture);
    } catch (err) {
      Crashlytics()
          .sendEmail("Billet createListMontantOuverture", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void createMontantForCaisse(
      Session session, List<MontantOuverture> listMontantOuverture) async {
    try {
      SessionMontanouverture sessionMontanouverture = SessionMontanouverture();

      sessionMontanouverture.idsession = session.idSession;
      sessionMontanouverture.montantOuvertureList = listMontantOuverture;
      ResponseSingle responseSingle = await sessionEndPointApi
          .createMontantForCaisseUsingPOST(sessionMontanouverture);
      if (responseSingle.objectResponse != null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => Dashboard(dataDto)),
        );
      } else {
        //AlertNotif().alert(context, responseSingle.errorDescription);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => Dashboard(dataDto)),
        );
      }
    } catch (err) {
      Crashlytics().sendEmail("Billet createMontantForCaisse", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void callbackNumbers(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        montant.text = "";
      });
      return;
    }
    setState(() {
      montant.text += number;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    List<BilletMonnaie> b = list.where((m) => m.fbillet == 1).toList();
    List<BilletMonnaie> m = list.where((m) => m.fbillet == 0).toList();
    void calculeMontantOuverture() {
      double d = 0;
      list.forEach((element) {
        if (element.valeur != null) {
          double t = element.qte * element.valeur;
          d += t;
        }
      });
      setState(() {
        montantOuverture = d;
      });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
              flex: 1,
              child: Center(
                child: Container(
                  color: Color(0xff352C60),
                  padding: EdgeInsets.only(top: 16),
                  child: Center(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.person_pin,
                        color: Colors.white,
                        size: width > height ? width / 24 : width / 8,
                      ),
                      FlatButton(
                          onPressed: () {},
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(),
                              ),
                              Text(
                                "Bienvenue",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: width > height
                                        ? width / 60
                                        : width / 30),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text(
                                dataDto.utilisateur.nom +
                                    " " +
                                    dataDto.utilisateur.prenom,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                    fontSize: width > height
                                        ? width / 60
                                        : width / 30),
                                textAlign: TextAlign.center,
                              ),
                              Expanded(
                                child: Container(),
                              ),
                            ],
                          ))
                    ],
                  )),
                ),
              )),
          dataDto.pointVente.fDetailMontant == 1
              ? Expanded(
                  flex: 2,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        color: Colors.transparent,
                        width: width * 0.3,
                        height: width * 0.215,
                        child: NumbersLogin(callbackNumbers),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Veuillez saisir le montant d'ouverture",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: width / 100),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Container(
                            alignment: Alignment.center,
                            width: width > height ? width / 3 : width,
                            child: TextField(
                                cursorColor: Colors.black,
                                textAlign: TextAlign.start,
                                controller: montant,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  enabled: true,
                                  hintStyle: TextStyle(color: Colors.black),
                                  hintText: "Montant",
                                  filled: true,
                                  contentPadding: EdgeInsets.only(left: 8),
                                  fillColor: Colors.transparent,
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1),
                                      borderRadius: BorderRadius.circular(0)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1),
                                      borderRadius: BorderRadius.circular(0)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1),
                                      borderRadius: BorderRadius.circular(0)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1),
                                      borderRadius: BorderRadius.circular(0)),
                                )),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 16),
                            width: width / 6,
                            height: width / 30,
                            child: RaisedButton(
                                elevation: 0,
                                color: Color(0xff32C533),
                                shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(width / 10),
                                ),
                                onPressed: () => {createNewJournee()},
                                padding: EdgeInsets.all(0),
                                child: Text(
                                  "Valider",
                                  style: TextStyle(
                                      fontSize: width > height
                                          ? width / 100
                                          : width / 40,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ],
                      )
                    ],
                  ))
              : Expanded(
                  flex: 3,
                  child: Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(top: 16),
                    child: Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Piéce",
                              style: TextStyle(
                                  color: Color(0xff352C60),
                                  fontWeight: FontWeight.w700),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 16),
                                child: ListView.builder(
                                    padding: const EdgeInsets.all(8),
                                    itemCount: m.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Container(
                                        margin: EdgeInsets.only(bottom: 8),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              height: width > height
                                                  ? width / 20
                                                  : width / 10,
                                              width: width > height
                                                  ? width / 20
                                                  : width / 10,
                                              child: FlatButton(
                                                  color: Color(0xff1352C60),
                                                  onPressed: () => {
                                                        setState(() {
                                                          m[index].qte == 0
                                                              ? 0
                                                              : m[index].qte -=
                                                                  1;
                                                        }),
                                                        calculeMontantOuverture()
                                                      },
                                                  padding: EdgeInsets.all(0),
                                                  child: Text(
                                                    "-",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  )),
                                            ),
                                            Expanded(
                                              child: SizedBox(
                                                height: width > height
                                                    ? width / 20
                                                    : width / 10,
                                                child: FlatButton(
                                                    color: Color(0xff64589E),
                                                    onPressed: () => {},
                                                    padding: EdgeInsets.all(0),
                                                    child: Text(
                                                      m[index].designation,
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    )),
                                              ),
                                            ),
                                            SizedBox(
                                              height: width > height
                                                  ? width / 20
                                                  : width / 10,
                                              width: width > height
                                                  ? width / 20
                                                  : width / 10,
                                              child: FlatButton(
                                                  color: Color(0xff1352C60),
                                                  onPressed: () => {
                                                        setState(() {
                                                          m[index].qte += 1;
                                                        }),
                                                        calculeMontantOuverture()
                                                      },
                                                  padding: EdgeInsets.all(0),
                                                  child: Text(
                                                    "+",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  )),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 8),
                                              height: width > height
                                                  ? width / 20
                                                  : width / 10,
                                              width: width > height
                                                  ? width / 20
                                                  : width / 10,
                                              child: FlatButton(
                                                  color: Color(0xffE4E4E4),
                                                  onPressed: () => {},
                                                  padding: EdgeInsets.all(0),
                                                  child: Text(
                                                    m[index].qte.toString(),
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  )),
                                            ),
                                          ],
                                        ),
                                      );
                                    }),
                              ),
                            )
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Billet",
                              style: TextStyle(
                                  color: Color(0xff352C60),
                                  fontWeight: FontWeight.w700),
                            ),
                            Expanded(
                              child: ListView.builder(
                                  padding: const EdgeInsets.all(8),
                                  itemCount: b.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                      margin: EdgeInsets.only(bottom: 8),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            height: width > height
                                                ? width / 20
                                                : width / 10,
                                            width: width > height
                                                ? width / 20
                                                : width / 10,
                                            child: FlatButton(
                                                color: Color(0xff1352C60),
                                                onPressed: () => {
                                                      setState(() {
                                                        b[index].qte == 0
                                                            ? 0
                                                            : b[index].qte -= 1;
                                                      }),
                                                      calculeMontantOuverture()
                                                    },
                                                padding: EdgeInsets.all(0),
                                                child: Text(
                                                  "-",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                )),
                                          ),
                                          Expanded(
                                            child: SizedBox(
                                              height: width > height
                                                  ? width / 20
                                                  : width / 10,
                                              child: FlatButton(
                                                  color: Color(0xff64589E),
                                                  onPressed: () => {},
                                                  padding: EdgeInsets.all(0),
                                                  child: Text(
                                                    b[index].designation,
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  )),
                                            ),
                                          ),
                                          SizedBox(
                                            height: width > height
                                                ? width / 20
                                                : width / 10,
                                            width: width > height
                                                ? width / 20
                                                : width / 10,
                                            child: FlatButton(
                                                color: Color(0xff1352C60),
                                                onPressed: () => {
                                                      setState(() {
                                                        b[index].qte += 1;
                                                      }),
                                                      calculeMontantOuverture()
                                                    },
                                                padding: EdgeInsets.all(0),
                                                child: Text(
                                                  "+",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                )),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 8),
                                            height: width > height
                                                ? width / 20
                                                : width / 10,
                                            width: width > height
                                                ? width / 20
                                                : width / 10,
                                            child: FlatButton(
                                                color: Color(0xffE4E4E4),
                                                onPressed: () => {},
                                                padding: EdgeInsets.all(0),
                                                child: Text(
                                                  b[index].qte.toString(),
                                                  style: TextStyle(
                                                      color: Colors.black),
                                                )),
                                          ),
                                        ],
                                      ),
                                    );
                                  }),
                            )
                          ],
                        )),
                        Column(
                          children: [
                            Container(
                              height: height * 0.60,
                              child: SingleChildScrollView(
                                child: DataTable(
                                  columns: [
                                    DataColumn(
                                      label: Text(
                                        'Qte',
                                        style: TextStyle(
                                            color: Color(0xff352C60),
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ),
                                    DataColumn(
                                        label: Text('Argent',
                                            style: TextStyle(
                                                color: Color(0xff352C60),
                                                fontWeight: FontWeight.w700))),
                                    DataColumn(
                                        label: Text('Montant',
                                            style: TextStyle(
                                                color: Color(0xff352C60),
                                                fontWeight: FontWeight.w700))),
                                  ],
                                  rows: list
                                      .map(
                                        (montant) => DataRow(cells: [
                                          DataCell(
                                            Text(
                                              montant.qte.toString(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w200),
                                            ),
                                            onTap: () {},
                                          ),
                                          DataCell(Text(
                                            montant.designation,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w200),
                                          )),
                                          DataCell(Text(
                                            montant.valeur != null
                                                ? (montant.valeur * montant.qte)
                                                        .toStringAsFixed(0) +
                                                    " TND"
                                                : montant.designation,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w200),
                                          )),
                                        ]),
                                      )
                                      .toList(),
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 8, right: 16, bottom: 8),
                                  child: Text(
                                    "Total",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 100),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 8, bottom: 8),
                                  child: Text(
                                      montantOuverture.toStringAsFixed(0) +
                                          " TND",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: width / 100)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: 8),
                                  child: RaisedButton(
                                      elevation: 0,
                                      color: Color(0xff32C533),
                                      shape: new RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(
                                            width / 10),
                                      ),
                                      onPressed: () => {

                                       if (! isAvaible) {
                                         createNewJournee(),
                                         setState(() {
                                           isAvaible=true ;
                                         }),
                                       }
                                      },
                                      padding: EdgeInsets.all(0),
                                      child: Text(
                                        "Valider",
                                        style: TextStyle(
                                            fontSize: width > height
                                                ? width / 100
                                                : width / 40,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.white),
                                        textAlign: TextAlign.center,
                                      )),
                                ),
                                RaisedButton(
                                    elevation: 0,
                                    color: Color(0xffCF0606),
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(width),
                                    ),
                                    onPressed: () => {
                                          setState(() {
                                            list.forEach((element) {
                                              element.qte = 0;
                                            });
                                            montantOuverture = 0;
                                          })
                                        },
                                    padding: EdgeInsets.all(0),
                                    child: Image.asset(
                                      'assets/repeat/repeat.png',
                                      fit: BoxFit.contain,
                                      height: width > height
                                          ? width / 60
                                          : width / 20,
                                      width: width > height
                                          ? width / 60
                                          : width / 20,
                                    )),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
