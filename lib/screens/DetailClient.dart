import 'package:ad_caisse/customer/api/customer_end_point_api.dart';
import 'package:ad_caisse/dto/CommandeClient.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/screens/Clients.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/NewReservation.dart';
import 'package:ad_caisse/screens/Payement.dart';
import 'package:ad_caisse/screens/Recharge.dart';
import 'package:ad_caisse/transaction/api/commande_details_end_point_api.dart';
import 'package:ad_caisse/transaction/api/commande_end_point_api.dart';
import 'package:ad_caisse/transaction/model/commande_details.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DetailClient extends StatefulWidget {
  final ClientBPrice clientBPrice;
  final DataDto dataDto;

  DetailClient(this.clientBPrice, this.dataDto);

  @override
  _MyHomePageState createState() =>
      _MyHomePageState(this.clientBPrice, this.dataDto);
}

class _MyHomePageState extends State<DetailClient> {
  ClientBPrice clientBPrice;
  DataDto dataDto;
  String search = "";
  List<CommandeClient> commandeClient = [];
  bool _loading = false;

  _MyHomePageState(this.clientBPrice, this.dataDto);

  var customerEndPointApi = CustomerEndPointApi();
  var commandeEndPointApi = CommandeEndPointApi();
  var commandeDetailsEndPointApi = CommandeDetailsEndPointApi();

  @override
  void initState() {
    dataDto.searchClient("Rechercher");
    findNTopbydidpointventeandIdclientpartenaire();
    super.initState();
  }

  Future<List<CommandeDetailsTransaction>> findAllByIdTicketUsingGET(
      String idTicket) async {
    List<CommandeDetailsTransaction> list = [];
    ResponseList responseList =
        await commandeDetailsEndPointApi.findAllByIdTicketUsingGET(idTicket);
    if (responseList.result == 1 && responseList.objectResponse != null) {
      list =
          CommandeDetailsTransaction.listFromJson(responseList.objectResponse);
    }
    return list;
  }

  void findNTopbydidpointventeandIdclientpartenaire() async {
    setState(() {
      _loading = true;
    });
    try {
      ResponseList responseList = await commandeEndPointApi
          .findNTopbydidpointventeandIdclientpartenaireUsingGET(
              dataDto.pointVente.idPointVente, clientBPrice.idClient, 5);

      if (responseList.result == 1 && responseList.objectResponse != null) {
        commandeClient = responseList.objectResponse
            .map((e) => CommandeClient.fromJson(e))
            .toList();
        for (var i = 0; i < commandeClient.length; i++) {
          commandeClient[i].commandeDetailsTransactions =
              await findAllByIdTicketUsingGET(commandeClient[i].idCommande);
        }
        setState(() {});
      }
    } catch (err) {
      Crashlytics().sendEmail(
          "DetailClient findNTopbydidpointventeandIdclientpartenaire",
          err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final row = Row(
      children: [
        Expanded(
            child: Column(
          children: [
            SizedBox(
              height: 8,
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 8),
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Color(0xff707070)),
                      ),
                      child: Text(
                          clientBPrice.isconnected == 1
                              ? "Connecté: Oui"
                              : "Connecté: Non",
                          style: TextStyle(
                              fontSize:
                                  width > height ? width / 80 : width / 30)),
                    ),
                  ),
                  Expanded(
                      child: Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Color(0xff707070)),
                          ),
                          child: Text(
                              "Solde: " +
                                  clientBPrice.soldePartn.toStringAsFixed(3) +
                                  " DT",
                              style: TextStyle(
                                  fontSize: width > height
                                      ? width / 80
                                      : width / 30))))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 8),
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Color(0xff707070)),
                      ),
                      child: Text("Email: " + (clientBPrice.email==null||clientBPrice.email==""?"":clientBPrice.email),
                          style: TextStyle(
                              fontSize:
                                  width > height ? width / 80 : width / 30)),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Color(0xff707070)),
                    ),
                    child: Text(
                        clientBPrice.qrCodePartn != null &&
                                clientBPrice.qrCodePartn != ""
                            ? "Code barre: ********" +  clientBPrice.qrCodePartn.substring( clientBPrice.qrCodePartn.length - 3)
                            : "",
                        style: TextStyle(
                            fontSize:
                                width > height ? width / 80 : width / 30)),
                  ))
                ],
              ),
            ),
            Container(
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                    margin: EdgeInsets.only(right: 8),
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Color(0xff707070)),
                    ),
                    child: Text("Téléphone: " + clientBPrice.nTel,
                        style: TextStyle(
                            fontSize:
                                width > height ? width / 80 : width / 30)),
                  )),
                  Expanded(
                      child: Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Color(0xff707070)),
                          ),
                          child: Text(
                            clientBPrice.dateNaissance != null
                                ? "Date de naissance: " +
                                    DateFormat("dd-MM-yyyy")
                                        .format(clientBPrice.dateNaissance)
                                : "",
                            style: TextStyle(
                                fontSize:
                                    width > height ? width / 80 : width / 30),
                          )))
                ],
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          "N°",
                          style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                              color: Colors.white),
                        ),
                        padding: EdgeInsets.only(left: 8),
                        width: 60,
                      ),
                      Expanded(
                        child: Text(
                          "Date",
                          style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                              color: Colors.white),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Montant",
                          style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                              color: Colors.white),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Clients",
                          style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                              color: Colors.white),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Détails",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 60),
                              color: Colors.white),
                        ),
                      ),
                      dataDto.pointVente.fGestionTable == 1
                          ? Container(
                              width: 80,
                              child: Text(
                                "N° Table",
                                style: TextStyle(
                                    fontSize:
                                        SizeClacule().calcule(context, 60),
                                    color: Colors.white),
                              ))
                          : SizedBox(
                              width: 0,
                            )
                    ],
                  ),
                  color: Color(0xffCF5FFE),
                  padding: EdgeInsets.only(top: 16, bottom: 16),
                ),
                SizedBox(
                  height: 8,
                ),
                SingleChildScrollView(
                  child: _loading
                      ? Container(
                          margin: EdgeInsets.all(16),
                          child: CircularProgressIndicator(),
                        )
                      : Column(
                          children: [
                            for (var item in commandeClient
                                .where((c) =>
                                    c.numCommande
                                        .toString()
                                        .toLowerCase()
                                        .contains(search.toLowerCase()) ||
                                    c.numTable
                                        .toString()
                                        .toLowerCase()
                                        .contains(search.toLowerCase()))
                                .toList())
                              Column(
                                children: [
                                  FlatButton(
                                    child: Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 8),
                                          width: 60,
                                          child: Text(
                                            item.numCommande.toString(),
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            DateFormat("dd-MM-yyyy HH:mm")
                                                .format(item.dateCreation),
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            item.montant.toStringAsFixed(3) +
                                                " DT",
                                            style: TextStyle(
                                                fontSize: SizeClacule()
                                                    .calcule(context, 60),
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              for (var items in item.clients)
                                                Column(
                                                  children: [
                                                    Text(
                                                      items.nom != null
                                                          ? items.prenom +
                                                              " " +
                                                              items.nom
                                                          : "",
                                                      style: TextStyle(
                                                          fontSize:
                                                              SizeClacule()
                                                                  .calcule(
                                                                      context,
                                                                      60),
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                    SizedBox(
                                                      height: 8,
                                                    )
                                                  ],
                                                )
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: [
                                              for (var element in item
                                                  .commandeDetailsTransactions)
                                                Container(
                                                  padding: EdgeInsets.all(4),
                                                  child: Text(
                                                    element != null &&
                                                            element.produit !=
                                                                null &&
                                                            element.produit
                                                                    .designation !=
                                                                null
                                                        ? element
                                                            .produit.designation
                                                        : "",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: SizeClacule()
                                                          .calcule(context, 60),
                                                    ),
                                                  ),
                                                )
                                            ],
                                          ),
                                        ),
                                        dataDto.pointVente.fGestionTable == 1
                                            ? Container(
                                                alignment: Alignment.center,
                                                width: 80,
                                                child: Text(
                                                  item.numTable != null
                                                      ? item.numTable.toString()
                                                      : "",
                                                  style: TextStyle(
                                                    fontSize: SizeClacule()
                                                        .calcule(context, 60),
                                                  ),
                                                ))
                                            : SizedBox(
                                                width: 0,
                                              )
                                      ],
                                    ),
                                    color: Color(0xffE7D7EE),
                                    padding: EdgeInsets.only(
                                      top: 16,
                                      bottom: 16,
                                    ),
                                    onPressed: () {},
                                  ),
                                  SizedBox(
                                    height: 8,
                                  )
                                ],
                              )
                          ],
                        ),
                ),
              ],
            )
          ],
        )),
      ],
    );
    final column = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 8),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 8),
                padding: EdgeInsets.all(16),
                width: width > height ? width / 5 : width / 2.2,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Color(0xff707070)),
                ),
                child: Text("Adresse: ",
                    style: TextStyle(
                        fontSize: width > height ? width / 100 : width / 30)),
              ),
              Container(
                  padding: EdgeInsets.all(16),
                  width: width > height ? width / 5 : width / 2.2,
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff707070)),
                  ),
                  child: Text(
                      "Solde: " +
                          clientBPrice.soldePartn.toStringAsFixed(3) +
                          " DT",
                      style: TextStyle(
                          fontSize: width > height ? width / 100 : width / 30)))
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 8),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 8),
                padding: EdgeInsets.all(16),
                width: width > height ? width / 5 : width / 2.2,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Color(0xff707070)),
                ),
                child: Text("Email: ",
                    style: TextStyle(
                        fontSize: width > height ? width / 100 : width / 30)),
              ),
              Container(
                padding: EdgeInsets.all(16),
                width: width > height ? width / 5 : width / 2.2,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Color(0xff707070)),
                ),
                child: Text("Code barre: ",
                    style: TextStyle(
                        fontSize: width > height ? width / 100 : width / 30)),
              )
            ],
          ),
        ),
        Container(
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 8),
                width: width > height ? width / 5 : width / 2.2,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Color(0xff707070)),
                ),
                child: Text("Téléphone: " + clientBPrice.nTel,
                    style: TextStyle(
                        fontSize: width > height ? width / 100 : width / 30)),
              ),
              Container(
                  padding: EdgeInsets.all(16),
                  width: width > height ? width / 5 : width / 2.2,
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff707070)),
                  ),
                  child: Text(
                    "N°: ",
                    style: TextStyle(
                        fontSize: width > height ? width / 100 : width / 30),
                  ))
            ],
          ),
        ),
        SizedBox(
          height: 16,
        ),
        SizedBox(
          height: width / 6,
          width: width / 2.2,
          child: FlatButton(
              color: Color(0xffCF5FFE),
              onPressed: () => {
                    // print("CurrentPage: "+dataDto.currentPage),
                    dataDto.setClientBPrice(clientBPrice),
                    dataDto.setCurrentPage(
                        dataDto.currentPage == "Payement"
                            ? Payement(dataDto)
                            : CommandeScreen(dataDto),
                        dataDto.currentPage)
                  },
              padding: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(
                          left: width > height ? width / 40 : width / 100,
                          right: width > height ? width / 40 : width / 100),
                      child: Text(
                        "Définir client",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ))
                ],
              )),
        )
      ],
    );
    return ListView(
      padding: EdgeInsets.all(0),
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            width > height
                ? Container(
                    child: SizedBox(
                      height: width > height
                          ? SizeClacule().calcule(context, 15)
                          : SizeClacule().calcule(context, 7.5),
                      width: width > height
                          ? SizeClacule().calcule(context, 6)
                          : SizeClacule().calcule(context, 3),
                      child: FlatButton(
                          color: Color(0xffCF5FFE),
                          onPressed: () => {
                                dataDto.setClientBPrice(clientBPrice),
                                if (dataDto.currentPage == "NewReservation")
                                  {
                                    dataDto.setCurrentPage(
                                        NewReservation(dataDto, null),
                                        dataDto.currentPage),
                                    dataDto.currentPage = ""
                                  }
                                else if (dataDto.currentPage == "Recharge")
                                  {
                                    dataDto.setCurrentPage(
                                        Recharge(dataDto), dataDto.currentPage),
                                    dataDto.currentPage = ""
                                  }
                                else
                                  {
                                    dataDto.setCurrentPage(
                                        dataDto.currentPage == "Payement"
                                            ? Payement(dataDto)
                                            : CommandeScreen(dataDto),
                                        dataDto.currentPage)
                                  }
                              },
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Text(
                            "Définir client",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          )),
                    ),
                    alignment: Alignment.center,
                  )
                : Container(),
            width > height
                ? SizedBox(
                    width: 16,
                  )
                : SizedBox(),
            Text(
              clientBPrice.nom + " " + clientBPrice.prenom,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: width > height ? width / 50 : width / 30),
            ),
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () =>
                      {dataDto.setCurrentPage(Clients(dataDto), "Clients")},
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        width > height ? row : column,
      ],
    );
  }
}
