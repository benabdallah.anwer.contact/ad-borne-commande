import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/dto/Ingredient.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/product/api/categorie_end_point_api.dart';
import 'package:ad_caisse/product/api/product_end_point_api.dart';
import 'package:ad_caisse/product/api/produit_proint_vente_end_point_api.dart';
import 'package:ad_caisse/product/model/categorie_article.dart';
import 'package:ad_caisse/product/model/pack.dart';
import 'package:ad_caisse/product/model/produit.dart';
import 'package:ad_caisse/request/CategoyList.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/screens/Animator.dart';
import 'package:ad_caisse/screens/NumbersLogin.dart';
import 'package:ad_caisse/screens/VirtualKeyboardScreen.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';

class CommandeScreen extends StatefulWidget {
  final DataDto dataDto;

  CommandeScreen(this.dataDto);

  @override
  State<StatefulWidget> createState() => new CommandeScreenState(this.dataDto);
}

class CommandeScreenState extends State<CommandeScreen> {
  DataDto dataDto;
  final api_instance = new ProductEndPointApi();
  final api_category = new CategorieEndPointApi();
  final utilisateurEndPointApi = UtilisateurEndPointApi();
  static const routeName = '/commande';
  FocusNode _myFocusNode = FocusNode();
  bool msgErr = false ;

  TextEditingController codeUtilisateurController = new TextEditingController();


  CommandeScreenState(this.dataDto);

  List<CategorieArticle> listCat;
  var list = [];
  List<Produit> listProduits = [];
  List<Produit> listAllProduits = [];
  List<Pack> packs = [];
  List<Pack> allPacks = [];
  Utilisateur selectedUser;
  String currentId = "";
  String _keyCode = '';
  bool _getCategoryList = true;
  String res="";
  FocusNode _myFocusNodeSelectEmploye = FocusNode();
  String _keyCodeEmploye = '';

  @override
  void initState() {
    listCat = dataDto.listCat;
    _myFocusNodeSelectEmploye.requestFocus();
    _myFocusNode.requestFocus();
    dataDto.focusNode.addListener(focusListener);
    dataDto.buildContext = context;
    dataDto.isRecharge = false;
    getAllProductAndPack();
    dataDto.searchController.addListener(search);
    dataDto.textController.addListener(txtListener);
    getCategoryList();
    super.initState();
  }

  focusListener() {
    debugPrint('Keayboard: ' + _myFocusNode.hasFocus.toString());
    if (!dataDto.focusNode.hasFocus) {
      setState(() {
        _keyCode = '';
        _myFocusNode.requestFocus();
      });
    }
  }

  getCategoryList() async {
    List<CategorieArticle> _listCat =
        await CategoryList().getCategoryList(dataDto.caisse);
    List<CategorieArticle> listOrderNull =
        _listCat.where((element) => element.order == null).toList();
    List<CategorieArticle> listOrder =
        _listCat.where((element) => element.order != null).toList();
    listOrder.sort((a, b) => a.order.compareTo(b.order));
    listOrder.addAll(listOrderNull);
    debugPrint('getCategoryList: ' + listOrder.length.toString());
    setState(() {
      dataDto.listCat = listOrder;
      listCat = listOrder;
    });
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  @override
  void dispose() {
    debugPrint('CommandeScreen Dispose');
    dataDto.focusNode.removeListener(focusListener);
    _myFocusNode.dispose();
    dataDto.searchController.removeListener(search);
    dataDto.textController.removeListener(txtListener);
    _myFocusNodeSelectEmploye.dispose();

    super.dispose();
  }

  Future<void> _showMyDialogWithScanQrCode(Produit produit) async {
    try {
      List<Utilisateur> services = [];
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1) {
        services = responseList.objectResponse
            .map((e) => Utilisateur.fromJson(e))
            .toList();
      }

      print(services.length);

      _myFocusNodeSelectEmploye.requestFocus();
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: RawKeyboardListener(
                onKey: (RawKeyEvent event) {
                  getEmployeFromQrCode(event,context,services,produit);
                },
                autofocus: true,
                focusNode: _myFocusNodeSelectEmploye,
                child:Column(
                  children: [
                    Image.asset('assets/logo/logo.png',
                        width: 80, height: 80, fit: BoxFit.scaleDown),
                    Row(
                      children: [
                        Text('Veuillez scanner le QR Code\n ou le code à barre de votre Employé'),
                        Lottie.asset('assets/qr-code-home/animation-qr.json',width: 150,height: 150)
                      ],
                    )
                  ],
                )),


            actions: <Widget>[
              FlatButton(
                color:  Color(0xffCF0606),
                child: Text('Annuler',style: TextStyle(color: Colors.white),),
                onPressed: () {
                  // this.dataDto.callbackCommande(produit, context);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      ).whenComplete(() {
        setState(() {
          _keyCodeEmploye = '';
          _myFocusNodeSelectEmploye.unfocus();
        });
      });
    } catch (err) {
      Crashlytics().sendEmail("CommandeScreen _showMyDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  getEmployeFromQrCode(RawKeyEvent event, BuildContext context , List<Utilisateur> employes , Produit produit ) {
    if (event.character != null) {
      if (event.physicalKey == PhysicalKeyboardKey.escape) {
        setState(() {
          _keyCodeEmploye = '';
        });
        return;
      }
     // _keyCodeEmploye += event.character;

      switch (event.character.toString()) {
        case "à":
          _keyCodeEmploye +='0';
          break;
        case "é":
          _keyCodeEmploye +='2';
          break;
        case "(":
          _keyCodeEmploye +='5';
          break;
        case "è":
          _keyCodeEmploye +='7';
          break;
        case "'":
          _keyCodeEmploye +='4';
          break;
        case "&":
          {
            _keyCodeEmploye +='1';
          }
          break;
        case "-":
          {
            _keyCodeEmploye +='6';
          }
          break;

        case "_":
          {
            _keyCodeEmploye +='8';
          }
          break;

        case "ç":
          {
            _keyCodeEmploye +='9';
          }
          break;

        case '"':
          {
            _keyCodeEmploye +='3';
          }
          break;


        default:
          {
            _keyCodeEmploye += event.character;
          }
          break;
      }
      debugPrint("RawKeyEvent: $_keyCodeEmploye");
      List<Utilisateur> list = employes
          .where((element) =>
      element.identifiant != null &&
          element.identifiant.toLowerCase() == _keyCodeEmploye.toLowerCase())
          .toList();
      if (list.isNotEmpty) {
        print(list[0].identifiant+"gggggggggggggggggggggggggggg");
        setState(() {
          _keyCodeEmploye = '';
          produit.employe=list[0];
          this
              .dataDto
              .callbackCommande(produit, context);
          selectedUser = list[0];

        });
        Navigator.of(context).pop();

      }
    }
  }

  Future<void> _showMyDialog(Produit produit) async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      List<Utilisateur> services = [];
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1) {
        services = responseList.objectResponse
            .map((e) => Utilisateur.fromJson(e))
            .toList();
      }
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: Text(
              'Veuillez sélectionner un employé',
              textAlign: TextAlign.center,
            ),
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            content: Container(
              height: height * 0.60,
              width: width * 0.5,
              padding: EdgeInsets.all(16),
              child: GridView.count(
                padding: EdgeInsets.only(top: 8),
                crossAxisCount: width > height ? 4 : 2,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                childAspectRatio: 2,
                children: services
                    .map(
                      (item) => SizedBox(
                        child: WidgetANimator(FlatButton(
                            color: Color(0xffE7D7EE),
                            padding: EdgeInsets.all(0),
                            onPressed: () => {
                                  produit.employe = item,
                                  this
                                      .dataDto
                                      .callbackCommande(produit, context),
                                  setState(() {
                                    selectedUser = item;
                                  }),
                                  Navigator.pop(context)
                                },
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      item.prenom + " " + item.nom,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: width > height
                                              ? width / 80
                                              : width / 35,
                                          color: Color(0xff352C60)),
                                    ),
                                  ],
                                )
                              ],
                            ))),
                      ),
                    )
                    .toList(),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                child: Text('Annuler',style: TextStyle(color :Colors.white),),
                onPressed: () {
                  this.dataDto.callbackCommande(produit, context);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("CommandeScreen _showMyDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> _showIngr(Produit produit) async {

    List<Ingredient> listIngrediant1=[];
    List<Ingredient> listIngrediant2=[];

    if (produit.ingredients.length>6){
      for (var i = 0; i < (produit.ingredients.length/2).round(); i++){
        listIngrediant1.add(produit.ingredients[i]);
      }

      for (var j = (produit.ingredients.length/2).round(); j < produit.ingredients.length; j++){
        listIngrediant2.add(produit.ingredients[j]);
      }

    }

    try {
      double width = MediaQuery.of(context).size.width;
      setState(() {
        dataDto.ingredients = '';
      });
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: Text(
              'Veuillez sélectionner les ingrédients',
              textAlign: TextAlign.center,
            ),
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            content:StatefulBuilder(
                builder: (BuildContext context, StateSetter setMyState) {
                  return produit.ingredients.length>6?
                  Container(
                    width: width*0.45,
                    child: Row(
                      children: [
                        SingleChildScrollView(
                          child: Container(

                            padding: EdgeInsets.only(left: 16,right: 16,bottom:16),
                            child: Column(
                              children: [
                                for (Ingredient item in listIngrediant1)
                                  SizedBox(
                                    child: WidgetANimator(
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              child: Text(
                                                item.designation,
                                                style: TextStyle(fontSize:width / 60),
                                              ),
                                              width: width / 7,
                                            ),
                                            SizedBox(
                                              width: 0,
                                            ),
                                            Switch(
                                              value: dataDto.ingredients
                                                  .contains(item.designation),
                                              onChanged: (value) {
                                                if (dataDto.ingredients
                                                    .contains(item.designation)) {
                                                  setMyState(() {
                                                    dataDto.ingredients = dataDto.ingredients
                                                        .replaceAll(
                                                        '${item.designation} / ', '');
                                                    if(item.isComps==1)
                                                    dataDto.idsIngrdComp.remove(item.idIngredient);
                                                  });
                                                  setState(() {
                                                    dataDto.ingredients = dataDto.ingredients
                                                        .replaceAll(
                                                        '${item.designation} / ', '');
                                                    if(item.isComps==1)
                                                      dataDto.idsIngrdComp.remove(item.idIngredient);
                                                  });
                                                } else {
                                                  setMyState(() {
                                                    dataDto.ingredients +=
                                                    '${item.designation} / ';
                                                    if(item.isComps==1)
                                                    dataDto.idsIngrdComp.add(item.idIngredient);
                                                  });
                                                }
                                                setState(() {});
                                                setMyState(() {});
                                                debugPrint(dataDto.ingredients);
                                              },
                                              activeColor: Colors.green,
                                            )
                                          ],
                                        )),
                                  ),

                              ],
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Container(

                            padding: EdgeInsets.only(left: 16,right: 16,bottom:16),
                            child: Column(
                              children: [
                                for (Ingredient item in listIngrediant2)
                                  SizedBox(
                                    child: WidgetANimator(
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              child: Text(
                                                item.designation,
                                                style: TextStyle(fontSize:width / 60),
                                              ),
                                              width: width / 7,
                                            ),
                                            SizedBox(
                                              width: 0,
                                            ),
                                            Switch(
                                              value: dataDto.ingredients
                                                  .contains(item.designation),
                                              onChanged: (value) {
                                                if (dataDto.ingredients
                                                    .contains(item.designation)) {
                                                  setMyState(() {
                                                    dataDto.ingredients = dataDto.ingredients
                                                        .replaceAll(
                                                        '${item.designation} / ', '');
                                                    if(item.isComps==1)
                                                      dataDto.idsIngrdComp.remove(item.idIngredient);
                                                  });
                                                  setState(() {
                                                    dataDto.ingredients = dataDto.ingredients
                                                        .replaceAll(
                                                        '${item.designation} / ', '');
                                                    if(item.isComps==1)
                                                      dataDto.idsIngrdComp.remove(item.idIngredient);
                                                  });
                                                } else {
                                                  setMyState(() {
                                                    dataDto.ingredients +=
                                                    '${item.designation} / ';
                                                    if(item.isComps==1)
                                                      dataDto.idsIngrdComp.add(item.idIngredient);
                                                  });
                                                }
                                                setState(() {});
                                                setMyState(() {});
                                                debugPrint(dataDto.ingredients);
                                              },
                                              activeColor: Colors.green,
                                            )
                                          ],
                                        )),
                                  ),

                              ],
                            ),
                          ),
                        )
                      ],
                    ),

                  ):
                  SingleChildScrollView(
                    child: Container(

                      padding: EdgeInsets.only(left: 16,right: 16,bottom:16),
                      child: Column(
                        children: [
                          for (Ingredient item in produit.ingredients)
                            SizedBox(
                              child: WidgetANimator(
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        child: Text(
                                          item.designation,
                                          style: TextStyle(fontSize:width / 60),
                                        ),
                                        width: width / 7,
                                      ),
                                      SizedBox(
                                        width: 0,
                                      ),
                                      Switch(
                                        value: dataDto.ingredients
                                            .contains(item.designation),
                                        onChanged: (value) {
                                          if (dataDto.ingredients
                                              .contains(item.designation)) {
                                            setMyState(() {
                                              dataDto.ingredients = dataDto.ingredients
                                                  .replaceAll(
                                                  '${item.designation} / ', '');
                                              if(item.isComps==1)
                                                dataDto.idsIngrdComp.remove(item.idIngredient);
                                            });
                                            setState(() {
                                              dataDto.ingredients = dataDto.ingredients
                                                  .replaceAll(
                                                  '${item.designation} / ', '');
                                              if(item.isComps==1)
                                                dataDto.idsIngrdComp.remove(item.idIngredient);
                                            });
                                          } else {
                                            setMyState(() {
                                              dataDto.ingredients +=
                                              '${item.designation} / ';
                                              if(item.isComps==1)
                                                dataDto.idsIngrdComp.add(item.idIngredient);
                                            });
                                          }
                                          setState(() {});
                                          setMyState(() {});
                                          debugPrint(dataDto.ingredients);
                                          debugPrint(dataDto.idsIngrdComp.toString());
                                        },
                                        activeColor: Colors.green,
                                      )
                                    ],
                                  )),
                            ),

                        ],
                      ),
                    ),
                  );
                }),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                padding: EdgeInsets.all(16),
                child: Text('Annuler',style: TextStyle(color: Colors.white),),
                onPressed: () {
                  setState(() {
                    dataDto.ingredients = null;
                    dataDto.idsIngrdComp = [];
                  });
                  Navigator.of(context).pop();
                  if (produit.typeProduit.toLowerCase() == "service" &&
                      dataDto.pointVente.fAffectEmployetoservice == 1) {
                    (!(dataDto.caisseType.code == 'CENTRALE' &&
                        (dataDto.utilisateurType.code ==
                            'CAISSIER' ||
                            dataDto.utilisateurType.code ==
                                'PROP' ||
                            dataDto.utilisateurType.code ==
                                'GERANT')))?
                    showCodeUtilisateur(produit) :
                    _showMyDialog(produit);
                    return;
                  }
                  this.dataDto.callbackCommande(produit, context);
                },
              ),
              FlatButton(
                color: Colors.green,
                padding: EdgeInsets.all(16),
                child: Text('Valider',style: TextStyle(color: Colors.white),),
                onPressed: () {
                  Navigator.of(context).pop();
                  if (produit.typeProduit.toLowerCase() == "service" &&
                      dataDto.pointVente.fAffectEmployetoservice == 1) {
                    (!(dataDto.caisseType.code == 'CENTRALE' &&
                        (dataDto.utilisateurType.code ==
                            'CAISSIER' ||
                            dataDto.utilisateurType.code ==
                                'PROP' ||
                            dataDto.utilisateurType.code ==
                                'GERANT')))?
                    showCodeUtilisateur(produit) :
                    _showMyDialog(produit);
                    return;
                  }
                  this.dataDto.callbackCommande(produit, context);
                },
              ),
            ],
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("CommandeScreen _showMyDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getAllProductAndPack() async {
    try {
      ProduitProintVenteEndPointApi produitProintVenteEndPointApi =
          ProduitProintVenteEndPointApi();
      ResponseList list = await produitProintVenteEndPointApi
          .findAllByIdPointVenteUsingGET2(dataDto.pointVente.idPointVente);
      if (list.result == 1 && list.objectResponse != null) {
        listAllProduits.addAll(Produit.listFromJson(list.objectResponse));
      }
      dataDto.listCat.forEach((element) {
        setState(() {
          allPacks.addAll(element.packs);
        });
        if (element.fils != null) {
          element.fils.forEach((element) {
            allPacks.addAll(element.packs);
          });
        }
      });

      dataDto.searchClient("Rechercher Produit");
    } catch (err) {
      Crashlytics()
          .sendEmail("CommandeScreen getAllProductAndPack", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  search() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
    });
    if (dataDto.searchController.text.isNotEmpty) {
      setState(() {
        listProduits = listAllProduits
            .where((element) =>
                element.designation != null &&
                element.designation
                    .toLowerCase()
                    .contains(dataDto.searchController.text.toLowerCase())||element.codeBarre!=null&&element.codeBarre.contains(dataDto.searchController.text.toLowerCase()))
            .toList();

        packs = allPacks
            .where((element) =>
                element.designation != null &&
                element.designation
                    .toLowerCase()
                    .contains(dataDto.searchController.text.toLowerCase()))
            .toList();
      });
    } else {
      setState(() {
        listProduits = [];
        packs = [];
      });
    }
  }

  handleKey(RawKeyEvent event) {
    if (event.character != null) {
      if (event.physicalKey == PhysicalKeyboardKey.escape) {
        setState(() {
          _keyCode = '';
        });
        return;
      }

      switch (event.character.toString()) {
        case "à":
          _keyCode +='0';
          break;
        case "é":
          _keyCode +='2';
          break;
        case "(":
          _keyCode +='5';
          break;
        case "è":
          _keyCode +='7';
          break;
        case "'":
          _keyCode +='4';
          break;
        case "&":
          {
            _keyCode +='1';
          }
          break;
        case "-":
          {
            _keyCode +='6';
          }
          break;

        case "_":
          {
            _keyCode +='8';
          }
          break;

        case "ç":
          {
            _keyCode +='9';
          }
          break;

        case '"':
          {
            _keyCode +='3';
          }
          break;


        default:
          {
            _keyCode += event.character.toString();
          }
          break;
      }


      if(_keyCode.contains("\r")){
        debugPrint("espace detecté************************:");
        var num=  "\r".allMatches(_keyCode).length;
        debugPrint("nombre ___________:$num");
        if(num>=2) {
          setState(() {
            _keyCode = '';
          });
          _myFocusNode.nextFocus();
        }

      }
      res=res+event.character.toString();


      debugPrint("RawKeyEvent: $_keyCode");
      if(_keyCode.length>4) {
        var list = listAllProduits
            .where((element) =>
        element.codeBarre != null && element.codeBarre != "" &&
            element.codeBarre.toLowerCase() == _keyCode.toLowerCase().trim())
            .toList();

        if (list.isNotEmpty) {
          _keyCode = '';
          setState(() {
            _keyCode = '';
          });
          var item = list[0];
          if (dataDto.saved) {
            setState(() {
              dataDto.saved = false;
              dataDto.total = "0.000";
              dataDto.commande = null;
              dataDto.commandeDelete = [];
              dataDto.commandeDetails = [];
              dataDto.clientBPrice = null;
              dataDto.tableCaisse = null;
              dataDto.listClientFiltred = dataDto.listClient;
            });
          }

          if (item.ingredients != null && item.ingredients.length > 0) {
            _showIngr(item);
            return;
          }

          if (item.typeProduit.toLowerCase() == "service" &&
              dataDto.pointVente.fAffectEmployetoservice == 1) {
            _showMyDialog(item);
            return;
          }
          this.dataDto.callbackCommande(item, context);
          return;
        }
        if (list.isEmpty) {
          List<ClientBPrice> listClient = dataDto.listClientFiltred
              .where((element) =>
          element.qrCodePartn != null && _keyCode.toLowerCase() != '' &&
              element.qrCodePartn.toLowerCase() == _keyCode.toLowerCase())
              .toList();
          if (listClient.isNotEmpty) {
            setState(() {
              _keyCode = '';
              dataDto.setClientBPrice(listClient[0]);
            });
          }
        }
      }
    }
  }

/// besion akoya
  void callbackNumbersMontant(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        codeUtilisateurController.text = "";
      });
      return;
    }
    setState(() {
      codeUtilisateurController.text += number;
    });
  }
  void showCodeUtilisateur(Produit produit) async {
    try{

      List<Utilisateur> services = [];
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByIdPointVenteUsingGET(dataDto.pointVente.idPointVente);
      if (responseList.result == 1) {
        services = responseList.objectResponse
            .map((e) => Utilisateur.fromJson(e))
            .toList();
      }



      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Image.asset('assets/logo/logo.png',
                      width: 50, height: 50, fit: BoxFit.scaleDown),
                  SizedBox(height: 16.0),
              Center(
                child:     Text(
                  'Veuillez saisir le Code de validation ',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
                  SizedBox(height: 16.0),
                  Column(
                    children: [
                      TextField(
                        autofocus: true,
                          obscureText: true,
                          cursorColor: Color(0xff352C60),
                          controller: codeUtilisateurController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Color(0xfff5f5f5),
                            contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Code ...",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0)),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(8),
                            ),
                          )),
                      SizedBox(height: 10.0),
                    msgErr? Text('Veuillez vérifier votre Code' ,style: TextStyle(color:Color(0xffCF0606),fontSize: 13 ),):SizedBox(),

                      SizedBox(height: 16.0),
                      Container(
                        alignment: Alignment.center,
                        color: Colors.transparent,
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: MediaQuery.of(context).size.width * 0.215,
                        child: NumbersLogin(callbackNumbersMontant),
                      ),
                    ],
                  )
                ],
              ),
            ),
            actions: <Widget>[
              RaisedButton(
                child: Text('Annuler', style: TextStyle(color: Colors.white)),
                elevation: 0,
                padding: EdgeInsets.all(16),
                color: Color(0xffCF0606),
                onPressed: () {
                  codeUtilisateurController.clear();
                  setState(() {
                    msgErr=false;
                  });
                  Navigator.of(context).pop();
                },
              ),
              RaisedButton(
                child: Text(
                  'Valider',
                  style: TextStyle(color: Colors.white),
                ),
                elevation: 0,
                padding: EdgeInsets.all(16),
                color: Color(0xFF32C533),
                onPressed: () {
                  List<Utilisateur> list = services
                      .where((element) =>
                  element.identifiant != null &&
                      element.identifiant == codeUtilisateurController.text)
                      .toList();
                  if (list.isEmpty){
                    setState(() {
                      msgErr=true ;
                    });
                    Navigator.of(context).pop();
                    showCodeUtilisateur(produit);


                  }

                  if (list.isNotEmpty) {
                    setState(() {
                      _keyCodeEmploye = '';
                      produit.employe=list[0];
                      this
                          .dataDto
                          .callbackCommande(produit, context);
                      selectedUser = list[0];

                    });

                    setState(() {
                      msgErr=false;
                    });

                    Navigator.of(context).pop();
                    codeUtilisateurController.clear();


                  }
                },
              ),
            ],
          );
        },
      ).whenComplete(() => (){
        codeUtilisateurController.clear();
        setState(() {
          msgErr=false;
        });
      });


    }catch (err) {
      Crashlytics().sendEmail("CommandeScreen showCodeUtilisateur", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }

  }

  /// besion akoya

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    Color fromHex(String hexString) {
      //print(hexString);
      if (hexString.contains("rgba")) {
        String a = hexString.replaceAll("rgba", "");
        String b = a.replaceAll("(", "");
        String c = b.replaceAll(")", "");
        List<String> listString = c.split(",");
        //print("RGBA: " + listString.toString());
        if (listString.length == 4) {
          return Color.fromARGB(
              int.parse(listString[0]),
              int.parse(listString[1]),
              int.parse(listString[2]),
              listString[3].contains(".")
                  ? int.parse(listString[3].replaceAll("0.", ""))
                  : int.parse(listString[3]));
        }
        return Color.fromARGB(100, 250, 100, 0);
      }
      final buffer = StringBuffer();
      if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
      buffer.write(hexString.replaceFirst('#', ''));
      return Color(int.parse(buffer.toString(), radix: 16));
    }

    List<Produit> sortListProduct(List<Produit> produits) {
      var filtredByOrder =
          produits.where((element) => element.orderproduct != null).toList();
      filtredByOrder.sort((a, b) => a.orderproduct.compareTo(b.orderproduct));
      var r =
          produits.where((element) => element.orderproduct == null).toList();
      filtredByOrder.addAll(r);
      return filtredByOrder;
    }

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: RawKeyboardListener(
        autofocus: true,
        onKey: (RawKeyEvent event) {
          handleKey(event);
        },
        focusNode: _myFocusNode,
        child: Column(
          children: [
            list.length > 0
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 16),
                        height: width > height
                            ? SizeClacule().calcule(context, 15)
                            : SizeClacule().calcule(context, 7.5),
                        width: width > height
                            ? SizeClacule().calcule(context, 15)
                            : SizeClacule().calcule(context, 7.5),
                        child: FlatButton(
                            color: Color(0xff32C533),
                            onPressed: () => {
                                  if (listProduits.length > 0 &&
                                      list.length == 1)
                                    {
                                      setState(() {
                                        listProduits = [];
                                        packs = [];
                                        list = [];
                                      })
                                    }
                                  else
                                    {
                                      setState(() {
                                        listCat = list[list.length - 1];
                                        listProduits = [];
                                        packs = [];
                                        list.removeLast();
                                      }),
                                    }
                                },
                            padding: EdgeInsets.all(0),
                            child: Image.asset(
                              'assets/back/back.png',
                              color: Colors.white,
                              fit: BoxFit.contain,
                              height: width > height ? width / 40 : width / 20,
                              width: width > height ? width / 40 : width / 20,
                            )),
                      )
                    ],
                  )
                : Container(),
            Expanded(
              child: listProduits.length > 0 || packs.length > 0
                  ? Column(
                      children: [
                        listProduits.length > 0
                            ? Expanded(
                                child: GridView.count(
                                padding: EdgeInsets.only(top: 8),
                                crossAxisCount: width > height ? 4 : 3,
                                mainAxisSpacing: 8.0,
                                crossAxisSpacing: 8.0,
                                childAspectRatio: 2,
                                children: listProduits
                                    .map(
                                      (item) => SizedBox(
                                        child: FlatButton(
                                            shape: RoundedRectangleBorder(
                                                side: BorderSide(
                                                    color: currentId ==
                                                            item.idProduit
                                                        ? Colors.red
                                                        : Colors.transparent,
                                                    width: 2),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        currentId ==
                                                                item.idProduit
                                                            ? 16
                                                            : 0)),
                                            color: item.couleur == null
                                                ? Color(0xffE7D7EE)
                                                : fromHex(item.couleur),
                                            onPressed: () {
                                              setState(() {
                                                currentId = item.idProduit;
                                              });
                                              if (dataDto.saved) {
                                                setState(() {
                                                  dataDto.saved = false;
                                                  dataDto.total = "0.000";
                                                  dataDto.commande = null;
                                                  dataDto.commandeDelete = [];
                                                  dataDto.commandeDetails = [];
                                                  dataDto.clientBPrice = null;
                                                  dataDto.tableCaisse = null;
                                                  dataDto.listClientFiltred =
                                                      dataDto.listClient;
                                                });
                                              }

                                              if (item.ingredients != null &&
                                                  item.ingredients.length > 0) {
                                                _showIngr(item);
                                                return;
                                              }

                                              if (item.typeProduit
                                                          .toLowerCase() ==
                                                      "service" &&
                                                  dataDto.pointVente
                                                          .fAffectEmployetoservice ==
                                                      1) {
                                                (!(dataDto.caisseType.code == 'CENTRALE' &&
                                                    (dataDto.utilisateurType.code ==
                                                        'CAISSIER' ||
                                                        dataDto.utilisateurType.code ==
                                                            'PROP' ||
                                                        dataDto.utilisateurType.code ==
                                                            'GERANT')))?
                                                showCodeUtilisateur(item) :
                                                _showMyDialog(item);

                                                return;
                                              }
                                              if (item.typeProduit
                                                  .toLowerCase() ==
                                                  "service" &&
                                                  dataDto.pointVente
                                                      .fAffectEmployetoservice ==
                                                      2) {
                                                _showMyDialogWithScanQrCode(item);
                                                return;
                                              }
                                              this.dataDto.callbackCommande(
                                                  item, context);
                                              if (dataDto.isSwitchedRetour) {
                                                if (listProduits.length > 0 &&
                                                    list.length == 1) {
                                                  setState(() {
                                                    listProduits = [];
                                                    packs = [];
                                                    list = [];
                                                  });
                                                } else {
                                                  setState(() {
                                                    listCat =
                                                        list[list.length - 1];
                                                    listProduits = [];
                                                    packs = [];
                                                    list.removeLast();
                                                  });
                                                }
                                              }
                                            },
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                item.urlImg != null &&
                                                        item.urlImg.isNotEmpty
                                                    ? Image.network(
                                                        item.urlImg,
                                                        height: 50,
                                                        errorBuilder:
                                                            (BuildContext
                                                                    context,
                                                                Object
                                                                    exception,
                                                                StackTrace
                                                                    stackTrace) {
                                                          return SizedBox();
                                                        },
                                                      )
                                                    : SizedBox(),
                                                Text(
                                                  item.designation != null
                                                      ? item.designation
                                                      : "",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: width > height
                                                          ? width / 80
                                                          : width / 35,
                                                      color: Colors.black),
                                                ),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Text(
                                                  item.produitpointvente !=
                                                              null &&
                                                          item.produitpointvente
                                                                  .prix !=
                                                              null
                                                      ? item.produitpointvente
                                                              .prix
                                                              .toStringAsFixed(int.parse(dataDto
                                                                          .pointVente
                                                                          .chiffrevirgule !=
                                                                      null
                                                                  ? dataDto
                                                                      .pointVente
                                                                      .chiffrevirgule
                                                                  : "3")) +
                                                          " DT"
                                                      : "",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: width > height
                                                          ? width / 100
                                                          : width / 35,
                                                      color: Color(0xff352C60)),
                                                )
                                              ],
                                            )),
                                      ),
                                    )
                                    .toList(),
                              ))
                            : SizedBox(
                                width: 0,
                                height: 0,
                              ),
                        packs.length > 0
                            ? Expanded(
                                child: GridView.count(
                                padding: EdgeInsets.only(top: 8),
                                crossAxisCount: width > height ? 4 : 3,
                                mainAxisSpacing: 8.0,
                                crossAxisSpacing: 8.0,
                                childAspectRatio: 2,
                                children: packs
                                    .map(
                                      (item) => SizedBox(
                                        child: FlatButton(
                                            color: Color(0xffE7D7EE),
                                            shape: RoundedRectangleBorder(
                                                side: BorderSide(
                                                    color: currentId ==
                                                            item.idPack
                                                        ? Colors.red
                                                        : Colors.transparent,
                                                    width: 0)),
                                            onPressed: () => {
                                                  setState(() {
                                                    currentId = item.idPack;
                                                  }),
                                                  if (dataDto.saved)
                                                    {
                                                      setState(() {
                                                        dataDto.saved = false;
                                                        dataDto.total = "0.000";
                                                        dataDto.commande = null;
                                                        dataDto.commandeDelete =
                                                            [];
                                                        dataDto.commandeDetails =
                                                            [];
                                                        dataDto.clientBPrice =
                                                            null;
                                                        dataDto.tableCaisse =
                                                            null;
                                                        dataDto.listClientFiltred =
                                                            dataDto.listClient;
                                                      }),
                                                    },
                                                  this.dataDto.callbackCommande(
                                                      item, context)
                                                },
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  item.designation,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: width > height
                                                          ? width / 80
                                                          : width / 35,
                                                      color: Colors.black),
                                                ),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Text(
                                                  item.prix.toStringAsFixed(3) +
                                                      " DT",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: width > height
                                                          ? width / 100
                                                          : width / 35,
                                                      color: Color(0xff352C60)),
                                                )
                                              ],
                                            )),
                                      ),
                                    )
                                    .toList(),
                              ))
                            : SizedBox(
                                width: 0,
                                height: 0,
                              )
                      ],
                    )
                  : listCat.isNotEmpty
                      ? GridView.count(
                          padding: EdgeInsets.only(top: 8),
                          crossAxisCount: width > height ? 4 : 3,
                          mainAxisSpacing: 8.0,
                          crossAxisSpacing: 8.0,
                          childAspectRatio: 2,
                          children: listCat
                              .map(
                                (item) => SizedBox(
                                  child: FlatButton(
                                      color: item.couleur == null
                                          ? Color(0xffE7D7EE)
                                          : fromHex(item.couleur),
                                      onPressed: () => {
                                            setState(() {
                                              dataDto.hideKeyboard = true;
                                              dataDto.textController.clear();
                                              dataDto.searchController.clear();
                                            }),
                                            item.fils.length > 0 ||
                                                    item.produits.length > 0 ||
                                                    item.packs.length > 0
                                                ? list.add(listCat)
                                                : null,
                                            if (item.fils.length > 0)
                                              {
                                                setState(() {
                                                  listCat = item.fils;
                                                })
                                              }
                                            else
                                              {
                                                setState(() {
                                                  listProduits =
                                                      item.produits.length > 0
                                                          ? sortListProduct(
                                                              item.produits)
                                                          : [];
                                                  packs = item.packs.length > 0
                                                      ? item.packs
                                                      : [];
                                                })
                                              }
                                          },
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          item.photo != null &&
                                                  item.photo.isNotEmpty
                                              ? Image.network(
                                                  item.photo,
                                                  height: width / 20,
                                                  errorBuilder: (BuildContext
                                                          context,
                                                      Object exception,
                                                      StackTrace stackTrace) {
                                                    return SizedBox();
                                                  },
                                                )
                                              : SizedBox(),
                                          SizedBox(
                                            height: 4,
                                          ),
                                          Text(
                                            item.designation,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: width > height
                                                    ? width / 80
                                                    : width / 30,
                                                color: Color(0xff352C60)),
                                          )
                                        ],
                                      )),
                                ),
                              )
                              .toList(),
                        )
                      : Center(
                          child: CircularProgressIndicator(),
                        ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 8),
              height: width > height ? height / 8 : height / 12,
              child: FutureBuilder<ResponseList>(
                future:
                    api_instance.findAllProductShortCutByIdPointVenteUsingGET(
                        dataDto.caisse.idPointVente),
                builder: (context, snapshot) {
                  List<Produit> list = [];
                  if (snapshot.hasData &&
                      snapshot.data.objectResponse != null) {
                    list = snapshot.data.objectResponse
                        .map((i) => Produit.fromJson(i))
                        .toList();
                  }
                  return snapshot.hasData
                      ? ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: list.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin: EdgeInsets.only(right: 8),
                              child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color:
                                              currentId == list[index].idProduit
                                                  ? Colors.red
                                                  : Colors.transparent,
                                          width: 2),
                                      borderRadius: BorderRadius.circular(
                                          currentId == list[index].idProduit
                                              ? 16
                                              : 0)),
                                  color: list[index].couleur == null
                                      ? Color(0xffE7D7EE)
                                      : fromHex(list[index].couleur),
                                  onPressed: () => {
                                        setState(() {
                                          currentId = list[index].idProduit;
                                        }),
                                        if (dataDto.saved)
                                          {
                                            setState(() {
                                              dataDto.saved = false;
                                              dataDto.total = "0.000";
                                              dataDto.commande = null;
                                              dataDto.commandeDelete = [];
                                              dataDto.commandeDetails = [];
                                              dataDto.clientBPrice = null;
                                              dataDto.tableCaisse = null;
                                              dataDto.listClientFiltred =
                                                  dataDto.listClient;
                                            }),
                                          },
                                        this.dataDto.callbackCommande(
                                            list[index], context)
                                      },
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        list[index].designation,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: width > height
                                                ? width / 80
                                                : width / 30,
                                            color: Colors.black),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        list[index]
                                                .produitpointvente
                                                .prix
                                                .toStringAsFixed(3) +
                                            " DT",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: width > height
                                                ? width / 100
                                                : width / 35,
                                            color: Color(0xff352C60)),
                                      )
                                    ],
                                  )),
                            );
                          },
                        )
                      : Center(child: CircularProgressIndicator());
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
