import 'dart:async';
import 'dart:io';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/screens/Login_Screen.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/material.dart';
import 'package:printing/printing.dart';
import 'package:path_provider_windows/path_provider_windows.dart';


class Loading extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _AnimatedFlutterLogoState();
}

class _AnimatedFlutterLogoState extends State<Loading> {
  Timer _timer;
  _AnimatedFlutterLogoState() {
    _timer = new Timer(const Duration(seconds: 4), () {
      updateApp();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    checkFile();
    ServerUrl.refreshFamille();
  }


  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  void _resetFile() async {
    PathProviderWindows pathProviderWindows = PathProviderWindows();
    String path = await pathProviderWindows.getTemporaryPath();
    print(path);
    final file = File(path + '\\' + 'preferences.json');
    final fileCopy = File(path + '\\' + 'preferencesCopy.json');
    final jsonString = await fileCopy.readAsString();
    if (await file.exists()) {
      await file.writeAsString(jsonString);
      await getSharedPreferences();
    }
  }

  checkFile()async{
    PathProviderWindows pathProviderWindows = PathProviderWindows();
    String path = await pathProviderWindows.getTemporaryPath();
    print(path);
    final file = File(path + '\\' + 'preferences.json');
    if (! await file.exists()){
      return false ;
    }
    final jsonString = await file.readAsString();
    if (jsonString[0]!='{' || jsonString.isEmpty){
      return true ;
    }else {
      return false ;
    }
  }

  Future<void> updateApp() async {
    print(checkFile().toString());
    if (checkFile()==false){
      await getSharedPreferences();
      await getPrinterList();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    }else {
      _resetFile();
      await getPrinterList();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );

    }
  }

  Future<void> getSharedPreferences() async {
    try {
      LocalStorageInterface prefs = await LocalStorage.getInstance();

      double versionCurrent = prefs.getDouble('current_version');
      if (versionCurrent==null){
        prefs.setDouble('current_version',0.0);
      }


      String savedIp = prefs.getString('my_ip');
      if (savedIp != null) {
        ServerUrl.url = savedIp;
      }
      bool fid = prefs.getBool("my_fid");
      if (fid != null) {
        ServerUrl.fid = fid;
      }
      String s = prefs.getString("my_ip_synchro");
      if (s != null) {
        ServerUrl.urlSynchro = s;
      }
   String familleOne = prefs.getString('familleprincipalPrinter');
      if(familleOne != null) {
        familleOne.trim();
        String tr=  familleOne.replaceAll(' ', '');
        ServerUrl.familleone=  tr.split(",");
      }
      String familleCuisine =   prefs.getString('famillecuisinePrinter');
      if(familleCuisine != null) {
        familleCuisine.trim();
        String tr=  familleCuisine.replaceAll(' ', '');
        ServerUrl.familletwo=  tr.split(",");
      }
    } catch (err) {
      debugPrint(err.toString());
      Crashlytics().sendEmail("Loading getSharedPreferences", err.toString());
    }
  }

  getPrinterList() async {
    try {
      List<Printer> list = await Printing.listPrinters();
      ServerUrl.list = list;
      debugPrint('getPrinterList ' + list.length?.toString());
    } catch (err) {
      debugPrint('getPrinterList ' + err.toString());
      Crashlytics().sendEmail("Loading getPrinterList", err.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/resto/resto.png"),
                  fit: BoxFit.cover)),
          child: Scaffold(
            backgroundColor: Colors.white12,
            body: Center(
              child: Container(
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/logo/logo.png',
                      height: 200,
                      width: 200,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
