import 'dart:convert';

import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/dto/StompMessage.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/product/model/produit.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class Notifications extends StatefulWidget {
  final DataDto dataDto;

  Notifications(this.dataDto);

  @override
  _MyHomePageState createState() => _MyHomePageState(this.dataDto);
}

class _MyHomePageState extends State<Notifications> {
  DataDto dataDto;
  String search = "";
  List<StompMessage> messages = [];
  bool _loading = false;
  var numberNotifications ;
  String url = ServerUrl.url + '/bp-api-admin/v1/addNotification';


  _MyHomePageState(this.dataDto);

  @override
  void initState() {
    getListNotifications();
    dataDto.searchClient("Rechercher");
    dataDto.textController.addListener(txtListener);
    super.initState();
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  Future<void> alert(StompMessage client) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Êtes-vous sûr de vouloir supprimer cette commande ?',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('ANNULER',style: TextStyle(color: Colors.white),),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: Color(0xffCF0606),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text('OK',style: TextStyle(color: Colors.white),),
              elevation: 0,
              color: Colors.green,
              onPressed: () async{
                Navigator.of(context).pop();

                setState(() {
                  dataDto.numberNotifications--;
                });
                client.ftraite=-1;
                client.dateTraite = DateTime.now();

                final res = await http.post(Uri.parse(url),
                    body: jsonEncode(client),
                    headers: {"Content-Type": "application/json"});
                if (res.statusCode >= 400) {
                  debugPrint(res.statusCode.toString() + ' ' + res.body);
                } else if (res.body != null) {
                  debugPrint(res.body);
                }
              },
            ),
          ],
        );
      },
    );
  }

  getListNotifications() async {
    setState(() {
      _loading = true;
    });
    List<StompMessage> _messages = [];
    debugPrint(dataDto.pointVente.idPointVente);
    String url = ServerUrl.url +
        '/bp-api-admin/v1/findAllMessageByIdPV' +
        dataDto.pointVente.idPointVente;
    debugPrint(url);
    final response = await http.get(Uri.parse(url)).timeout(
      Duration(seconds: 10),
      onTimeout: () {
        // time has run out, do what you wanted to do
        return null;
      },
    );

    if (response != null && response.statusCode == 200) {
      debugPrint(response.body);
      ResponseList responseList =
      ResponseList.fromJson(json.decode(response.body));
      if (responseList.result == 1 && responseList.objectResponse != null) {
        _messages = responseList.objectResponse
            .map((e) => StompMessage.fromJson(e))
            .toList();
      }
      setState(() {
        messages = _messages.where((element) => element.ftraite == 0).toList();
      });
    }else {
      debugPrint('-------------------------FAILED------------------------------');
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        SizedBox(
          height: 16,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Notifications",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: SizeClacule().calcule(context, 25),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 16,
        ),
        Expanded(
            child: Column(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        "Client",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        "Message",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "Téléphone",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Date",
                        style: TextStyle(
                          fontSize: SizeClacule().calcule(context, 60),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                _loading
                    ? Container(
                  margin: EdgeInsets.all(16),
                  height: 64,
                  width: 64,
                  child: CircularProgressIndicator(),
                )
                    : Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          for (var client in messages.reversed
                              .where((c) =>
                          c.prenom != null &&
                              c.prenom
                                  .toLowerCase()
                                  .contains(search.toLowerCase()) ||
                              c.nom != null &&
                                  c.nom
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                              c.ntel != null &&
                                  c.ntel
                                      .toLowerCase()
                                      .contains(search.toLowerCase()) ||
                              c.body != null &&
                                  c.body
                                      .toLowerCase()
                                      .contains(search.toLowerCase()))
                              .toList())

                            client.ftraite!=-1?   Column(
                              children: [
                                FlatButton(
                                  onPressed: () {
                                    setState(() {
                                      dataDto.numberNotifications--;
                                    });
                                    this.dataDto.callbackNumbers('C', context);
                                    setState(() {
                                      dataDto.stompMessage = client;
                                    });
                                    List<ClientBPrice> clientBPrices = dataDto
                                        .listClient
                                        .where((element) =>
                                    element.idClientPartenaire ==
                                        client.idClientPartenaire)
                                        .toList();
                                    if (clientBPrices.isNotEmpty) {
                                      setState(() {
                                        dataDto.clientBPrice =
                                            clientBPrices.first;
                                      });
                                    }
                                    for (int i = 0;
                                    i < client.details.length;
                                    i++) {
                                      var element = client.details[i];
                                      Produit produit = Produit();
                                      produit.designation = element.designation;
                                      produit.prixTtc = element.prix;
                                      produit.idProduit = element.idProduit;
                                      this.dataDto.ingredients =
                                          element.ingredient;
                                      this.dataDto.callbackNumbers(
                                          element.quantite.ceil().toString(),
                                          context);
                                      this
                                          .dataDto
                                          .callbackCommande(produit, context);
                                    }

                                    dataDto.setCurrentPage(
                                        CommandeScreen(dataDto),
                                        'CommandeScreen');


                                  },
                                  color: Color(0xffE7D7EE),
                                  padding: EdgeInsets.only(
                                    top: height / 30,
                                    bottom: height / 30,
                                    left: 16,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          client.body!=null? client.nom + " " + client.prenom +  "\n - "+client.body:client.nom + " " + client.prenom,
                                          style: TextStyle(
                                            fontSize: SizeClacule()
                                                .calcule(context, 70),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: client.details != null
                                              ? Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [
                                              for (var msg
                                              in client.details)
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 4, bottom: 4),
                                                  child: Text(
                                                    msg.quantite != null &&
                                                        msg.designation !=
                                                            null &&
                                                        msg.ingredient !=
                                                            null
                                                        ? msg.quantite
                                                        .toString() +
                                                        " X " +
                                                        msg.designation +
                                                        " " +
                                                        msg.ingredient
                                                        : "",
                                                    style: TextStyle(
                                                      fontSize:
                                                      SizeClacule()
                                                          .calcule(
                                                          context,
                                                          70),
                                                    ),
                                                  ),
                                                ),
                                            ],
                                          )
                                              : SizedBox()),
                                      Expanded(
                                        child: Text(
                                          client.ntel,
                                          style: TextStyle(
                                            fontSize: SizeClacule()
                                                .calcule(context, 60),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Text(
                                          DateFormat("dd-MM-yyyy HH:mm")
                                              .format(client.dateEnvoie),
                                          style: TextStyle(
                                            fontSize: SizeClacule()
                                                .calcule(context, 60),
                                          ),
                                        ),
                                      ),

                                      Container(
                                        margin: EdgeInsets.only(right: 4),
                                        child: IconButton(
                                          onPressed: () {
                                            alert(client);
                                            setState(() {
                                            });
                                          },
                                          color: Colors.red,
                                          icon: Icon(Icons.delete_outline),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                )
                              ],
                            ):SizedBox(height: 0,)
                        ],
                      ),
                    ))
              ],
            ))
      ],
    );
  }
}
