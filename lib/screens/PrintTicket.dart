import 'package:ad_caisse/customer/api/customer_end_point_api.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/Tickets.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class PrintTicket extends StatefulWidget {
  final DataDto dataDto;
  final CommandesDtoTransaction commandeDto;

  PrintTicket(this.dataDto, this.commandeDto);

  @override
  _MyHomePageState createState() =>
      _MyHomePageState(this.dataDto, this.commandeDto);
}

class _MyHomePageState extends State<PrintTicket> {
  DataDto dataDto;
  CommandesDtoTransaction commandeDto;
  TextEditingController codeAnnuleeController = new TextEditingController();
  bool actionAnnule;
  String codeAnnule ;

  _MyHomePageState(this.dataDto, this.commandeDto);

  var customerEndPointApi = CustomerEndPointApi();

  @override
  void initState() {
    print("PrintTicket");
    getActionAnnulee();
    super.initState();
  }
  Future<void> getActionAnnulee() async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();

    bool  actionA=prefs.getBool('action_Annulee');
    String codeA=prefs.getString("codeActionAnnulee");

    if (actionA!=null){
      setState(() {
        actionAnnule=actionA;
      });
    }

    if (codeA.isNotEmpty){
      setState(() {
        codeAnnule=codeA;
      });
    }




  }
  void showCodeActionAnnulee() {
    final scaffold = ScaffoldMessenger.of(context);
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text('Veuillez saisir le Code de validation ',style: TextStyle(fontWeight: FontWeight.w500),),
                SizedBox(height: 16.0),

                TextField(
                    obscureText: true,
                    cursorColor: Color(0xff352C60),
                    controller: codeAnnuleeController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xfff5f5f5),
                      contentPadding:
                      EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Code Action",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    )),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xffCF0606),
              onPressed: () {
                codeAnnuleeController.clear();
                Navigator.of(context).pop();

              },
            ),
            RaisedButton(
              child: Text(
                'Valider',
                style: TextStyle(color: Colors.white),
              ),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xFF32C533),
              onPressed: () {
                if (codeAnnule.toLowerCase()==codeAnnuleeController.text.toLowerCase()){
                  codeAnnuleeController.clear();
                  Navigator.of(context).pop();
                  alert();
                }else{

                  scaffold.showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.red,
                      content:  Text('Code Action invalide'),
                    ),
                  );

                }
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> alert() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Êtes-vous sûr de vouloir supprimer cette commande ?',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('ANNULER'),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: Colors.black26,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text('OK'),
              elevation: 0,
              color: Colors.green,
              onPressed: () {
                http.get(Uri.parse(ServerUrl.url +
                    '/bp-api-transaction/v1/AnnulerCommande/' +
                    commandeDto.commande.idCommande));
                Navigator.of(context).pop();
                setState(() {
                  dataDto.tableCaisse = null;
                  dataDto.listTickets = dataDto.listTickets
                      .where((element) =>
                  element.commande.idCommande !=
                      commandeDto.commande.idCommande)
                      .toList();
                });
                dataDto.setCurrentPage(Tickets(dataDto), dataDto.currentPage);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () => {
                    setState(() {
                      dataDto.tableCaisse = null;
                    }),
                    dataDto.setCurrentPage(
                        Tickets(dataDto), dataDto.currentPage)
                  },
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              width: width > height
                  ? SizeClacule().calcule(context, 3)
                  : SizeClacule().calcule(context, 0.7),
              padding: EdgeInsets.only(bottom: 16),
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    height: 16,
                  ),
                  Text(
                    dataDto.pointVente.designation,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: width > height ? width / 100 : width / 30,
                    ),
                  ),
                  Text(
                    dataDto.pointVente.adresse,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: width > height ? width / 100 : width / 30,
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    dataDto.tableCaisse != null
                        ? "N° " +
                        commandeDto.commande.numCommande.toString() +
                        " / Table N° " +
                        dataDto.tableCaisse.numTable.toString()
                        : "N° " + commandeDto.commande.numCommande.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: width > height ? width / 100 : width / 30,
                    ),
                  ),
                  Container(
                    height: 1,
                    width: width,
                    color: Colors.black12,
                    margin: EdgeInsets.only(top: 16, bottom: 16),
                  ),
                  Text(
                    DateFormat("dd-MM-yyyy HH:mm")
                        .format(commandeDto.commande.dateCreation),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: width > height ? width / 100 : width / 30,
                    ),
                  ),
                  for (var c in commandeDto.commande.clients)
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Text(
                        c.nom != null ? c.nom + " " + c.prenom : "",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: width > height ? width / 100 : width / 30,
                        ),
                      ),
                    ),
                  Container(
                    height: 1,
                    width: width,
                    color: Colors.black12,
                    margin: EdgeInsets.only(top: 16, bottom: 16),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (var c in commandeDto.commandeDetails)
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 8, right: 8),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          c.commandeDetails.quantite
                                              .toString() +
                                              " " +
                                              c.commandeDetails.designation +
                                              " ",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: width > height
                                                ? width / 100
                                                : width / 30,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        (c.commandeDetails.quantite *
                                            c.commandeDetails.prix)
                                            .toStringAsFixed(3) +
                                            " DT ",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: width > height
                                              ? width / 100
                                              : width / 30,
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                          ],
                        )
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Total à payer :",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: width > height ? width / 100 : width / 30,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Text(
                        commandeDto.commande.montant.toStringAsFixed(3) + " DT",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: width > height ? width / 100 : width / 30,
                        ),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    "Merci de votre Visite",
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
            padding: EdgeInsets.all(0),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          children: [
            width > height
                ? Expanded(
              child: Container(
                height: 60,
                margin: EdgeInsets.only(right: 16),
                child: FlatButton(
                  padding: EdgeInsets.all(0),
                  color: Colors.red,
                  onPressed: () {
                    (dataDto.caisseType.code=='CENTRALE'&&dataDto.utilisateurType.code=='CAISSIER' && actionAnnule==true) ?
                    showCodeActionAnnulee():
                    alert();
                  },
                  child: Text(
                    "Supprimer",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            )
                : Container(
              width: 0,
            ),
            Container(
              height: 60,
              width: width > height
                  ? SizeClacule().calcule(context, 3)
                  : SizeClacule().calcule(context, 2),
              child: FlatButton(
                padding: EdgeInsets.all(0),
                color: Color(0xff32C533),
                onPressed: () {
                   Utils().printService1(context, commandeDto, dataDto);
                  //Utils().cuisine(context, dataDto.commandeDetails, dataDto, null);
                },
                child: Text(
                  "Imprimer",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w400),
                ),
              ),
            ),
            Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  height: 60,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    color: Color(0xffCF5FFE),
                    onPressed: () {
                      setState(() {
                        dataDto.tableCaisse = null;
                      });
                      dataDto.setCurrentPage(
                          CommandeScreen(dataDto), 'CommandeScreen');
                    },
                    child: Text(
                      "Commande suivante",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w400),
                    ),
                  ),
                ))
          ],
        )
      ],
    );
  }
}
