import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/DepencesList.dart';
import 'package:ad_caisse/transaction/api/commande_end_point_api.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/material.dart';

import 'Commandes.dart';
import 'Tickets.dart';

class Transactions extends StatefulWidget {
  final DataDto dataDto;

  Transactions(this.dataDto);

  @override
  TransactionsState createState() => TransactionsState(this.dataDto);
}

class TransactionsState extends State<Transactions> {
  final DataDto dataDto;
  bool loadingComm = false;
  bool loadingTicket = false;
  bool loadingDepense = false;

  TransactionsState(this.dataDto);

  var commandeEndPointApi = CommandeEndPointApi();

  void getCommandes() async {
    try {
      setState(() {
        loadingComm = true;
      });
      ResponseList responseList =
          await commandeEndPointApi.gettransactionbyjourneeandfpayeUsingGET(
              dataDto.journee.idJournee, 0);
      List<CommandesDtoTransaction> list = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        list = List.from(responseList.objectResponse)
            .map((e) => CommandesDtoTransaction.fromJson(e))
            .toList()
            .where((element) => element.commandeDetails?.isNotEmpty)
            .toList();
      }
      dataDto.listCommande = list;
      dataDto.setCurrentPage(Commandes(dataDto), "Commandes");
      setState(() {
        loadingComm = false;
      });
    } catch (err) {
      setState(() {
        loadingComm = false;
      });
      Crashlytics().sendEmail("Transactions getCommandes", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getTickets() async {
    try {
      setState(() {
        loadingTicket = true;
      });
      ResponseList responseList =
          await commandeEndPointApi.gettransactionbyjourneeandfpayeUsingGET(
              dataDto.journee.idJournee, 1);
      List<CommandesDtoTransaction> list = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        list = List.from(responseList.objectResponse)
            .map((e) => CommandesDtoTransaction.fromJson(e))
            .toList()
            .where((element) => element.commandeDetails?.isNotEmpty)
            .toList();
      }
      dataDto.listTickets = list;
      dataDto.setCurrentPage(Tickets(dataDto), "Tickets");
      setState(() {
        loadingTicket = false;
      });
    } catch (err) {
      setState(() {
        loadingTicket = false;
      });
      Crashlytics().sendEmail("Transactions getTickets", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Transactions",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: width > height ? width / 50 : width / 30),
            ),
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () => {
                        dataDto.setCurrentPage(
                            CommandeScreen(dataDto), "CommandeScreen")
                      },
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        width > height
            ? Expanded(
                child: Row(
                children: [
                  Expanded(
                      child: Container(
                    width: width / 2,
                    color: Color(0xffCF5FFE),
                    child: FlatButton(
                        onPressed: () => {getCommandes()},
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            loadingComm
                                ? CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white),
                                  )
                                : Image.asset(
                                    'assets/list/list.png',
                                    color: Colors.white,
                                    fit: BoxFit.contain,
                                    height: width > height
                                        ? width / 20
                                        : width / 20,
                                    width: width > height
                                        ? width / 20
                                        : width / 20,
                                  ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Commandes",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize:
                                      width > height ? width / 50 : width / 15,
                                  color: Colors.white),
                            ),
                          ],
                        )),
                  )),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child:
                    dataDto.caisseType.code=='CENTRALE'&&(dataDto.utilisateurType.code=='CAISSIER'||dataDto.utilisateurType.code=='PROP'||dataDto.utilisateurType.code=='GERANT')?
                    Column(
                      children: [
                        Expanded(
                            child: Container(
                          width: width / 2,
                          color: Color(0xff70B3FF),
                          child: FlatButton(
                              onPressed: () => {getTickets()},
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  loadingTicket
                                      ? CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  Colors.white))
                                      : Image.asset(
                                          'assets/bill/bill.png',
                                          color: Colors.white,
                                          fit: BoxFit.contain,
                                          height: width > height
                                              ? width / 20
                                              : width / 20,
                                          width: width > height
                                              ? width / 20
                                              : width / 20,
                                        ),
                                  SizedBox(
                                    height: 16,
                                  ),
                                  Text(
                                    "Tickets",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: width > height
                                            ? width / 50
                                            : width / 15,
                                        color: Colors.white),
                                  ),
                                ],
                              )),
                        )),
                        SizedBox(
                          height: 8,
                        ),
                        Expanded(
                            child: Container(
                          width: width / 2,
                          color: Colors.amber,
                          child: FlatButton(
                              onPressed: () => {
                                    dataDto.setCurrentPage(
                                        DepencesList(dataDto), "DepencesList")
                                  },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  loadingDepense
                                      ? CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  Colors.white))
                                      : Image.asset(
                                          'assets/dep/dep.png',
                                          color: Colors.white,
                                          fit: BoxFit.contain,
                                          height: width > height
                                              ? width / 20
                                              : width / 20,
                                          width: width > height
                                              ? width / 20
                                              : width / 20,
                                        ),
                                  SizedBox(
                                    height: 16,
                                  ),
                                  Text(
                                    "Dépenses",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: width > height
                                            ? width / 50
                                            : width / 15,
                                        color: Colors.white),
                                  ),
                                ],
                              )),
                        ))
                      ],
                    ):SizedBox(),
                  )
                ],
              ))
            : Expanded(
                child: Column(
                children: [
                  Expanded(
                      child: Container(
                    width: width * 0.8,
                    color: Color(0xffCF5FFE),
                    child: FlatButton(
                        onPressed: () => {
                          getCommandes()},
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/list/list.png',
                              color: Colors.white,
                              fit: BoxFit.contain,
                              height: width / 10,
                              width: width / 10,
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Commandes",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize:
                                      width > height ? width / 50 : width / 15,
                                  color: Colors.white),
                            ),
                          ],
                        )),
                  )),
                  SizedBox(
                    height: 8,
                  ),
                  Expanded(
                    child: Container(
                      width: width * 0.8,
                      color: Color(0xff70B3FF),
                      child: FlatButton(
                          onPressed: () => {getTickets()},
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/bill/bill.png',
                                color: Colors.white,
                                fit: BoxFit.contain,
                                height: width / 10,
                                width: width / 10,
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Text(
                                "Tickets",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: width > height
                                        ? width / 50
                                        : width / 15,
                                    color: Colors.white),
                              ),
                            ],
                          )),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Expanded(
                      child: Container(
                    width: width * 0.8,
                    color: Colors.amber,
                    child: FlatButton(
                        onPressed: () => {
                              dataDto.setCurrentPage(
                                  DepencesList(dataDto), "DepencesList")
                            },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            loadingDepense
                                ? CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white))
                                : Image.asset(
                                    'assets/dep/dep.png',
                                    color: Colors.white,
                                    fit: BoxFit.contain,
                                    height: width > height
                                        ? width / 20
                                        : width / 20,
                                    width: width > height
                                        ? width / 20
                                        : width / 20,
                                  ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Dépenses",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize:
                                      width > height ? width / 50 : width / 15,
                                  color: Colors.white),
                            ),
                          ],
                        )),
                  ))
                ],
              ))
      ],
    );
  }
}
