import 'dart:math' as math;

import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/commande.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/ordering/model/commande_dto.dart';
import 'package:ad_caisse/product/api/produit_proint_vente_end_point_api.dart';
import 'package:ad_caisse/product/api/remise_rechage_partenaire_end_point_api.dart';
import 'package:ad_caisse/product/model/ProduitPV.dart';
import 'package:ad_caisse/product/model/remise_rechage_partenaire.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/screens/Clients.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'NumbersLogin.dart';
import 'Payement.dart';

class Recharge extends StatefulWidget {
  final DataDto dataDto;

  Recharge(this.dataDto);

  @override
  RechargeState createState() => RechargeState(this.dataDto);
}

class RechargeState extends State<Recharge> {
  DataDto dataDto;

  final produitProintVenteEndPointApi = ProduitProintVenteEndPointApi();
  int current = 0;
  String search = "";
  List<RemiseRechagePartenaire> listRemise = [];

  RechargeState(this.dataDto);

  @override
  void initState() {
    findAllAtifRemiseByIdpartner();
    dataDto.searchClient("Rechercher Client");
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
    super.initState();
    // current = dataDto.montant.text != "" ? int.parse(dataDto.montant.text) : 0;
  }

  txtListener() {
    switch (dataDto.focused) {
      case -1:
        {
          dataDto.searchController.text = dataDto.textController.text;
        }
        break;
    }
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  findAllAtifRemiseByIdpartner() async {
    try {
      RemiseRechagePartenaireEndPointApi rechagePartenaireEndPointApi =
          RemiseRechagePartenaireEndPointApi();
      ResponseList list = await rechagePartenaireEndPointApi
          .findAllAtifRemiseByIdpartnerUsingGET(
              dataDto.utilisateur.idPartenaire);
      if (list.result == 1 && list.objectResponse != null) {
        List<RemiseRechagePartenaire> listRemise =
            RemiseRechagePartenaire.listFromJson(list.objectResponse);
        setState(() {
          this.listRemise = listRemise;
        });
      }
    } catch (err) {
      Crashlytics()
          .sendEmail("Recharge findAllAtifRemiseByIdpartner", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    super.dispose();
  }

  void findProduitRechargeForPV() async {
    try {
      if (dataDto.montant.text == "" && current == 0) {
        AlertNotif().alert(context, "Veuillez mentionner le montant");
        return;
      }
      if (dataDto.clientBPrice == null) {
        AlertNotif().alert(context, "Veuillez choisir votre client SVP");
        return;
      }
      setState(() {
        this.dataDto.commandeDetails = [];
        this.dataDto.total = null;
        this.dataDto.isRecharge = false;
      });
      ResponseSingle responseSingle = await produitProintVenteEndPointApi
          .findProduitRechargeForPVUsingGET(dataDto.pointVente.idPointVente);
      if (responseSingle.result == 1) {
        ProduitPV produitPV = ProduitPV.fromJson(responseSingle.objectResponse);
        callbackCommande(produitPV, context);
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics()
          .sendEmail("Recharge findProduitRechargeForPV", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void callbackCommande(ProduitPV produitPV, BuildContext context) {
    Commande commande = Commande();
    commande.clients.length == 0
        ? commande.clients.add(dataDto.clientBPrice)
        : null;
    commande.idPointVente = dataDto.caisse.idPointVente;
    commande.fPaye = 0;
    commande.idSession = dataDto.session.idSession;
    commande.montant = double.parse(dataDto.montant.text);
    CommandeDto commandeDto = CommandeDto();
    commandeDto.commande = commande;
    commandeDto.idsession = dataDto.session.idSession;
    CommandeDetails commandeDetailNew = CommandeDetails();
    commandeDetailNew.prix = double.parse(dataDto.montant.text);
    commandeDetailNew.prixSansRemise = double.parse(dataDto.montant.text);
    commandeDetailNew.quantite = 1;
    commandeDetailNew.idProduit = produitPV.produitpointvente.idProduit;
    commandeDetailNew.designation = produitPV.designation;
    commandeDetailNew.recharge = true;
    setState(() {
      this.dataDto.commandeDetails.add(commandeDetailNew);
      this.dataDto.total = dataDto.remise.text == ""
          ? double.parse(dataDto.montant.text).toString()
          : ((double.parse(dataDto.montant.text) / 100) *
                  (100 - double.parse(dataDto.remise.text.replaceAll("%", ""))))
              .toString();
    });

    //print("Recharge Count: " + this.dataDto.commandeDetails.length.toString());
    if (this.dataDto.commandeDetails.length > 0) {
      this.dataDto.isRecharge = true;
      this.dataDto.rendu = double.parse(this.dataDto.total);
      this.dataDto.montant.text = "";
      this.dataDto.remise.text = "";

      dataDto.setCurrentPage(Payement(dataDto), "Payement");
    }
  }

  void callbackNumbers(String number, BuildContext context) {
    if (number == "C") {
      setState(() {
        dataDto.montant.text = "";
        dataDto.remise.text = "";
      });
      return;
    }
    setState(() {
      dataDto.montant.text += number;
      dataDto.remise.text = listRemise
                  .where((element) =>
                      double.parse(dataDto.montant.text) >= element.min &&
                      double.parse(dataDto.montant.text) <= element.max)
                  .toList()
                  .length >
              0
          ? listRemise
                  .where((element) =>
                      double.parse(dataDto.montant.text) >= element.min &&
                      double.parse(dataDto.montant.text) <= element.max)
                  .toList()[0]
                  .valeurRemise
                  .floor()
                  .toString() +
              "%"
          : "";

      current = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Stack(
      fit: StackFit.loose,
      children: [
        Column(
          children: <Widget>[
            SizedBox(
              height: 16,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Recharge Solde Client",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: width > height ? width / 50 : width / 30),
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            SingleChildScrollView(
              padding: EdgeInsets.only(left: 24, right: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      Expanded(
                          child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: Container(
                                alignment: Alignment.centerLeft,
                                height: width > height
                                    ? SizeClacule().calcule(context, 15)
                                    : SizeClacule().calcule(context, 7.5),
                                padding: EdgeInsets.only(left: 16, right: 16),
                                child: Text(
                                  dataDto.clientBPrice != null
                                      ? dataDto.clientBPrice.nom != null
                                          ? dataDto.clientBPrice.nom +
                                              " " +
                                              dataDto.clientBPrice.prenom
                                          : ""
                                      : "Veuillez choisir votre client",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: width / 100),
                                ),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1, color: Color(0xff707070)),
                                ),
                              )),
                            ],
                          )
                        ],
                      )),
                      SizedBox(
                        width: 8,
                      ),
                      SizedBox(
                        height: width > height
                            ? SizeClacule().calcule(context, 15)
                            : SizeClacule().calcule(context, 7.5),
                        child: FlatButton(
                          onPressed: () {
                            setState(() {
                              dataDto.currentPage = "Recharge";
                            });
                            dataDto.setCurrentPage(Clients(dataDto), "");
                          },
                          child: Text(
                            "Selectionner un client",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                                fontSize: width / 100),
                          ),
                          color: Color(0xffCF5FFE),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        height: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        width: width > height
                                            ? SizeClacule().calcule(context, 8)
                                            : SizeClacule().calcule(context, 4),
                                        child: SizedBox(
                                          height: width > height
                                              ? SizeClacule()
                                                  .calcule(context, 15)
                                              : SizeClacule()
                                                  .calcule(context, 7.5),
                                          width: width > height
                                              ? SizeClacule()
                                                  .calcule(context, 8)
                                              : SizeClacule()
                                                  .calcule(context, 4),
                                          child: FlatButton(
                                              color: current == 10
                                                  ? Colors.purpleAccent
                                                  : Colors.deepPurpleAccent,
                                              padding: EdgeInsets.all(0),
                                              onPressed: () {
                                                setState(() {
                                                  dataDto.montant.text = dataDto
                                                              .montant.text !=
                                                          ""
                                                      ? (double.parse(dataDto
                                                                  .montant
                                                                  .text) +
                                                              10)
                                                          .toStringAsFixed(
                                                              int.parse(dataDto
                                                                  .pointVente
                                                                  .chiffrevirgule))
                                                      : "10";
                                                });
                                              },
                                              child: Text(
                                                '10 DT',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: width / 100,
                                                    color: Colors.white),
                                              )),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        height: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        width: width > height
                                            ? SizeClacule().calcule(context, 8)
                                            : SizeClacule().calcule(context, 4),
                                        child: SizedBox(
                                          height: width > height
                                              ? SizeClacule()
                                                  .calcule(context, 15)
                                              : SizeClacule()
                                                  .calcule(context, 7.5),
                                          width: width > height
                                              ? SizeClacule()
                                                  .calcule(context, 8)
                                              : SizeClacule()
                                                  .calcule(context, 4),
                                          child: FlatButton(
                                              color: current == 20
                                                  ? Colors.purpleAccent
                                                  : Colors.deepPurpleAccent,
                                              padding: EdgeInsets.all(0),
                                              onPressed: () {
                                                setState(() {
                                                  dataDto.montant.text = dataDto
                                                              .montant.text !=
                                                          ""
                                                      ? (double.parse(dataDto
                                                                  .montant
                                                                  .text) +
                                                              20)
                                                          .toStringAsFixed(
                                                              int.parse(dataDto
                                                                  .pointVente
                                                                  .chiffrevirgule))
                                                      : "20";
                                                });
                                              },
                                              child: Text(
                                                '20 DT',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: width / 100,
                                                    color: Colors.white),
                                              )),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        height: width > height
                                            ? SizeClacule().calcule(context, 15)
                                            : SizeClacule()
                                                .calcule(context, 7.5),
                                        width: width > height
                                            ? SizeClacule().calcule(context, 8)
                                            : SizeClacule().calcule(context, 4),
                                        child: SizedBox(
                                          height: width > height
                                              ? SizeClacule()
                                                  .calcule(context, 15)
                                              : SizeClacule()
                                                  .calcule(context, 7.5),
                                          width: width > height
                                              ? SizeClacule()
                                                  .calcule(context, 8)
                                              : SizeClacule()
                                                  .calcule(context, 4),
                                          child: FlatButton(
                                              color: current == 50
                                                  ? Colors.purpleAccent
                                                  : Colors.deepPurpleAccent,
                                              padding: EdgeInsets.all(0),
                                              onPressed: () {
                                                setState(() {
                                                  dataDto.montant.text = dataDto
                                                              .montant.text !=
                                                          ""
                                                      ? (double.parse(dataDto
                                                                  .montant
                                                                  .text) +
                                                              50)
                                                          .toStringAsFixed(
                                                              int.parse(dataDto
                                                                  .pointVente
                                                                  .chiffrevirgule))
                                                      : "50";
                                                });
                                              },
                                              child: Text(
                                                '50 DT',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: width / 100,
                                                    color: Colors.white),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      )),
                    ],
                  ),
                  SizedBox(
                    height: 48,
                  ),
                  Row(children: [
                    Container(
                      width: width / 5,
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(0),
                            height: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            width: width / 5,
                            child: TextField(
                                onTap: () {
                                  setState(() {
                                    dataDto.focused = 1;
                                  });
                                  // dataDto.callbackKeyboard(true);
                                },
                                cursorColor: Colors.black,
                                controller: dataDto.montant,
                                style: TextStyle(
                                  fontSize: width / 100,
                                ),
                                keyboardType: TextInputType.number,
                                onChanged: ((string) => {
                                      setState(() {
                                        dataDto.remise.text = listRemise
                                                    .where((element) =>
                                                        double.parse(dataDto.montant.text) >=
                                                            element.min &&
                                                        double.parse(dataDto.montant.text) <=
                                                            element.max)
                                                    .toList()
                                                    .length >
                                                0
                                            ? listRemise
                                                    .where((element) =>
                                                        double.parse(dataDto
                                                                .montant
                                                                .text) >=
                                                            element.min &&
                                                        double.parse(
                                                                dataDto.montant.text) <=
                                                            element.max)
                                                    .toList()[0]
                                                    .valeurRemise
                                                    .floor()
                                                    .toString() +
                                                "%"
                                            : "";

                                        current = 0;
                                      })
                                    }),
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.transparent,
                                  hintText: "Montant",
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.transparent),
                                      borderRadius: BorderRadius.circular(0)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.transparent),
                                      borderRadius: BorderRadius.circular(0)),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.transparent),
                                      borderRadius: BorderRadius.circular(0)),
                                )),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey),
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(0),
                            height: width > height
                                ? SizeClacule().calcule(context, 15)
                                : SizeClacule().calcule(context, 7.5),
                            width: width / 5,
                            child: TextField(
                                enabled: false,
                                cursorColor: Colors.black,
                                controller: dataDto.remise,
                                style: TextStyle(
                                  fontSize: width / 100,
                                ),
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.transparent,
                                    hintText: "Remise",
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                        borderRadius: BorderRadius.circular(0)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                        borderRadius: BorderRadius.circular(0)),
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                        borderRadius: BorderRadius.circular(0)),
                                    disabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                        borderRadius:
                                            BorderRadius.circular(0)))),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 24,
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        height: height * 0.24,
                        padding: EdgeInsets.only(top: 16, left: 16),
                        alignment: Alignment.centerLeft,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Remise",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff707070)),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Expanded(
                                child: ListView.builder(
                                    padding: EdgeInsets.all(0),
                                    itemCount: this.listRemise.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors
                                                  .grey, // set border color
                                              width: 1.0),
                                        ),
                                        margin:
                                            EdgeInsets.only(right: 16, top: 8),
                                        padding: EdgeInsets.all(8),
                                        child: Row(
                                          children: [
                                            Text("De ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: width / 100)),
                                            Text(
                                              this
                                                  .listRemise[index]
                                                  .min
                                                  .floor()
                                                  .toString(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: width / 100),
                                            ),
                                            Text(" jusqu'a ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: width / 100)),
                                            Text(
                                              this
                                                  .listRemise[index]
                                                  .max
                                                  .floor()
                                                  .toString(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: width / 100),
                                            ),
                                            Text(
                                                " vous bénéficiez d'une remise de ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: width / 100)),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 4,
                                                  bottom: 4,
                                                  left: 8,
                                                  right: 8),
                                              decoration: BoxDecoration(
                                                color: Color((math.Random()
                                                                .nextDouble() *
                                                            0xFFFFFF)
                                                        .toInt())
                                                    .withOpacity(1.0),
                                                // set border width
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20.0)),
                                              ),
                                              child: Text(
                                                  this
                                                          .listRemise[index]
                                                          .valeurRemise
                                                          .floor()
                                                          .toString() +
                                                      "%",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: width / 100)),
                                            )
                                          ],
                                        ),
                                      );
                                    })),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      color: Colors.transparent,
                      width: width * 0.25,
                      height: width * 0.18,
                      child: NumbersLogin(callbackNumbers),
                    ),
                  ]),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: width / 5,
                        height: width > height
                            ? SizeClacule().calcule(context, 15)
                            : SizeClacule().calcule(context, 7.5),
                        child: FlatButton(
                          onPressed: () {
                            findProduitRechargeForPV();
                          },
                          child: Text(
                            "Payer",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                                fontSize: width / 100),
                          ),
                          color: Color(0xff32C533),
                        ),
                      ),
                      SizedBox(
                        width: 24,
                      ),
                      SizedBox(
                        width: width / 5,
                        height: width > height
                            ? SizeClacule().calcule(context, 15)
                            : SizeClacule().calcule(context, 7.5),
                        child: FlatButton(
                          onPressed: () {
                            setState(() {
                              dataDto.montant.text = "";
                              dataDto.remise.text = "";
                              dataDto.searchController.text = "";
                              dataDto.textController.text = "";
                              dataDto.clientBPrice = null;
                              current = 0;
                            });
                            ;
                          },
                          child: Text(
                            "Réinitialiser",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                                fontSize: width / 100),
                          ),
                          color: Colors.red,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
        search != ""
            ? Positioned(
                child: Container(
                  color: Colors.white,
                  child: Container(
                      child: SingleChildScrollView(
                          scrollDirection:
                              width > height ? Axis.vertical : Axis.horizontal,
                          child: DataTable(
                            columns: [
                              DataColumn(
                                label: Text('Nom'),
                              ),
                              DataColumn(label: Text('Email')),
                              DataColumn(label: Text('Téléphone')),
                              DataColumn(label: Text('Solde')),
                            ],
                            rows: dataDto.listClientFiltred
                                .where((c) =>
                                    c.prenom != null &&
                                        c.prenom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nom != null &&
                                        c.nom
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.nTel != null &&
                                        c.nTel
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.email != null &&
                                        c.email
                                            .toLowerCase()
                                            .contains(search.toLowerCase()) ||
                                    c.qrCodePartn != null &&
                                        c.qrCodePartn
                                            .toLowerCase()
                                            .contains(search.toLowerCase()))
                                .toList()
                                .map(
                                  (client) => DataRow(cells: [
                                    DataCell(
                                      Text(
                                        client.nom + " " + client.prenom,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.clear();
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.email != null
                                            ? client.email
                                            : "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.clear();
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.nTel,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.clear();
                                        });
                                      },
                                    ),
                                    DataCell(
                                      Text(
                                        client.soldePartn.toStringAsFixed(3) +
                                            " DT",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onTap: () {
                                        dataDto
                                            .searchClient("Rechercher Client");
                                        setState(() {
                                          dataDto.hideKeyboard = true;
                                          dataDto.clientBPrice = client;
                                          dataDto.searchController.clear();
                                        });
                                      },
                                    ),
                                  ]),
                                )
                                .toList(),
                          ))),
                ),
                top: 0,
                left: 0,
                right: 0,
              )
            : SizedBox()
      ],
    );
  }
}
