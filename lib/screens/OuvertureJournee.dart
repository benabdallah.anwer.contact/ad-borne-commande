import 'dart:convert';

import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/pos/api/devise_end_point_api.dart';
import 'package:ad_caisse/pos/api/journee_end_point_api.dart';
import 'package:ad_caisse/pos/api/session_end_point_api.dart';
import 'package:ad_caisse/pos/model/AutreSessionRes.dart';
import 'package:ad_caisse/pos/model/ClotureJourneeDto.dart';
import 'package:ad_caisse/pos/model/ClotureJourneeRes.dart';
import 'package:ad_caisse/pos/model/billet_monnaie.dart';
import 'package:ad_caisse/pos/model/journee.dart';
import 'package:ad_caisse/pos/model/session.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/screens/CommandesCuisine.dart';
import 'package:ad_caisse/screens/Dashboard.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';

import 'Billet.dart';
import 'Login_Screen.dart';

class OuvertureJournee extends StatefulWidget {
  final DataDto dataDto;

  OuvertureJournee(this.dataDto);

  @override
  OuvertureJourneeState createState() => OuvertureJourneeState(this.dataDto);
}

class OuvertureJourneeState extends State<OuvertureJournee> {
  final DataDto dataDto;
  var deviseEndPointApi = DeviseEndPointApi();
  bool _loading = false;
  JourneeEndPointApi journeeEndPointApi = JourneeEndPointApi();

  OuvertureJourneeState(this.dataDto);

  void createNewJournee() async {
    try {
      Journee journee = Journee();
      journee.idPointVente = dataDto.caisse.idPointVente;
      journee.dateOuverture = DateTime.now();
      journee.fCloture = 0;
      ResponseSingle responseSingle =
          await journeeEndPointApi.createjourneeUsingPOST(journee);
      Journee journeeRet = Journee.fromJson(responseSingle.objectResponse);
      dataDto.journee = journeeRet;
      if (responseSingle.objectResponse != null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => LoginPage()),
        );
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics().sendEmail("Billet createNewJournee", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }



  void findAllBilletMonnaie(BuildContext context) async {
    try {
      ResponseList responseList = await deviseEndPointApi
          .findAllByFDefautUsingGET(1, dataDto.caisse.idPointVente);
      print("findAllBilletMonnaie: " + responseList.result.toString());
      List<BilletMonnaie> list = [];
      if (responseList.result == 3 && responseList.objectResponse != null) {
        list = List.from(responseList.objectResponse)
            .map((e) => BilletMonnaie.fromJson(e))
            .toList();
      } else {
        BilletMonnaie billetMonnaie = new BilletMonnaie();
        billetMonnaie.valeur = 5;
        billetMonnaie.designation = "5 DT";
        billetMonnaie.fbillet = 1;
        list.add(billetMonnaie);
        BilletMonnaie billetMonnaie10 = new BilletMonnaie();
        billetMonnaie10.valeur = 10;
        billetMonnaie10.designation = "10 DT";
        billetMonnaie10.fbillet = 1;
        list.add(billetMonnaie10);
        BilletMonnaie billetMonnaie20 = new BilletMonnaie();
        billetMonnaie20.valeur = 20;
        billetMonnaie20.designation = "20 DT";
        billetMonnaie20.fbillet = 1;
        list.add(billetMonnaie20);
        BilletMonnaie billetMonnaie50 = new BilletMonnaie();
        billetMonnaie50.valeur = 50;
        billetMonnaie50.designation = "50 DT";
        billetMonnaie50.fbillet = 1;
        list.add(billetMonnaie50);
        BilletMonnaie billetMonnaie5 = new BilletMonnaie();
        billetMonnaie5.valeur = 5;
        billetMonnaie5.designation = "5 DT";
        billetMonnaie5.fbillet = 0;
        list.add(billetMonnaie5);
        BilletMonnaie billetMonnaie2 = new BilletMonnaie();
        billetMonnaie2.valeur = 2;
        billetMonnaie2.designation = "2 DT";
        billetMonnaie2.fbillet = 0;
        list.add(billetMonnaie2);
        BilletMonnaie billetMonnaie1 = new BilletMonnaie();
        billetMonnaie1.valeur = 1;
        billetMonnaie1.designation = "1 DT";
        billetMonnaie1.fbillet = 0;
        list.add(billetMonnaie1);
        BilletMonnaie billetMonnaie05 = new BilletMonnaie();
        billetMonnaie05.valeur = 0.5;
        billetMonnaie05.designation = "0.5 DT";
        billetMonnaie05.fbillet = 0;
        list.add(billetMonnaie05);
        BilletMonnaie billetMonnaie02 = new BilletMonnaie();
        billetMonnaie02.valeur = 0.2;
        billetMonnaie02.designation = "0.2 DT";
        billetMonnaie02.fbillet = 0;
        list.add(billetMonnaie02);
        BilletMonnaie billetMonnaie01 = new BilletMonnaie();
        billetMonnaie01.valeur = 0.1;
        billetMonnaie01.designation = "0.1 DT";
        billetMonnaie01.fbillet = 0;
        list.add(billetMonnaie01);
        BilletMonnaie billetMonnaie005 = new BilletMonnaie();
        billetMonnaie005.valeur = 0.05;
        billetMonnaie005.designation = "0.05 DT";
        billetMonnaie005.fbillet = 0;
        list.add(billetMonnaie005);
      }

      ((dataDto.utilisateurType.code ==
          'CAISSIER' ||
          dataDto.utilisateurType.code ==
              'PROP' ||
          dataDto.utilisateurType.code ==
              'GERANT')) ? Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => Billet(dataDto, list)),
      ):creatNewSession();
    } catch (err) {
      Crashlytics()
          .sendEmail("OuvertureJournee findAllBilletMonnaie", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  clotureJournee() async {
    try {
      setState(() {
        _loading = true;

      });
      ClotureJourneeDto clotureJourneeDto = ClotureJourneeDto(
          idUser: dataDto.utilisateur.idUtilisateur,
          idJournee: dataDto.journee.idJournee,
          dateCloture: DateTime.now());
      ResponseSingle responseSingle =
          await journeeEndPointApi.clotureJourneeUsingPOST(clotureJourneeDto);
      if (responseSingle.result == 1) {
        ClotureJourneeRes clotureJourneeRes =
            ClotureJourneeRes.fromJson(responseSingle.objectResponse);
        showClotureJourneeDialog(clotureJourneeRes, null);

      } else if (responseSingle.result == -1) {
        AutreSessionRes autreSessionRes =
            AutreSessionRes.fromJson(responseSingle.objectResponse);
        showClotureJourneeDialog(null, autreSessionRes);
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
      setState(() {
        _loading = false;

      });
    } catch (err) {
      setState(() {
        _loading = false;

      });
      Crashlytics().sendEmail("Setting clotureJournee", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> showClotureJourneeDialog(
      ClotureJourneeRes colture, AutreSessionRes autreSessionRes) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height;
        return AlertDialog(
          backgroundColor: Color(0xffEEEEEE),
          contentPadding: EdgeInsets.all(0),
          scrollable: true,
          content: Container(
            height: height * 0.6,
            width: width * 0.6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: colture != null ? Colors.green : Colors.red,
                  child: Text(
                    dataDto.utilisateur.prenom + " " + dataDto.utilisateur.nom,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  width: width * 0.6,
                  padding: EdgeInsets.all(24),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: colture != null
                      ? Text(
                          "C.A globale: " +
                              colture.chiffreAffaireGlobaleJournee
                                  .toStringAsFixed(int.parse(
                                      dataDto.pointVente.chiffrevirgule != null
                                          ? dataDto.pointVente.chiffrevirgule
                                          : "3")) +
                              " DT",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      : Text(
                          "Impossible de clôturer la journèe car les sessions suivantes sont encore actives: ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                  width: width * 0.6,
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                colture != null
                    ? Container(
                        child: Text(
                          "Montant par moyen de paiement",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        width: width * 0.6,
                        margin: EdgeInsets.only(bottom: 8, left: 16),
                      )
                    : SizedBox(),
                colture != null
                    ? Expanded(
                        child: GridView.count(
                        padding: EdgeInsets.only(top: 8, left: 16),
                        crossAxisCount: width > height ? 4 : 2,
                        mainAxisSpacing: 8.0,
                        crossAxisSpacing: 8.0,
                        childAspectRatio: 2,
                        children: colture.montantParModeReglParJournee
                            .map(
                              (item) => SizedBox(
                                child: Container(
                                    alignment: Alignment.center,
                                    color: Color(0xffE7D7EE),
                                    padding: EdgeInsets.all(0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(item.fnum == 1
                                            ? item.nombreReg.toString() +
                                                " " +
                                                item.designation
                                            : item.designation),
                                        SizedBox(
                                          height: 16,
                                        ),
                                        Text(item.totale.toStringAsFixed(
                                                int.parse(dataDto.pointVente
                                                    .chiffrevirgule)) +
                                            " DT")
                                      ],
                                    )),
                              ),
                            )
                            .toList(),
                      ))
                    : Expanded(
                        child: GridView.count(
                        padding: EdgeInsets.only(top: 8, left: 16),
                        crossAxisCount: width > height ? 4 : 2,
                        mainAxisSpacing: 8.0,
                        crossAxisSpacing: 8.0,
                        childAspectRatio: 2,
                        children: autreSessionRes.autreSession
                            .map(
                              (item) => SizedBox(
                                child: Container(
                                    alignment: Alignment.center,
                                    color: Color(0xffE7D7EE),
                                    padding: EdgeInsets.all(0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(item.utilisateur.prenom +
                                            " " +
                                            item.utilisateur.nom),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text("Ouverture"),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text(DateFormat("dd-MM-yyyy HH:mm")
                                            .format(item.dateDebut))
                                      ],
                                    )),
                              ),
                            )
                            .toList(),
                      ))
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.purpleAccent,
              child: Text(
                'Quiter',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                if (colture != null) {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }

  void showConfirmDialog() {
    if (dataDto.journee == null && dataDto.utilisateurType.fGerant == 1) {
      findAllBilletMonnaie(context);
      return;
    }
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text('Etes-vous sûr de vouloir Clôturer la journée ?'),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Non', style: TextStyle(color: Colors.white)),
              elevation: 0,
              color: Color(0xffCF0606),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'Oui',
                style: TextStyle(color: Colors.white),
              ),
              elevation: 0,
              color: Color(0xFF32C533),
              onPressed: () {
                Navigator.of(context).pop();
                clotureJournee();
              },
            ),
          ],
        );
      },
    );
  }

  void creatNewSession() async {
    if (dataDto.journee == null) {
      AlertNotif().alert(context, 'Aucune journée ouverte');
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
      return;
    }
    try {
      Session session = Session();
      session.idJournee = dataDto.journee.idJournee;
      session.idCaisse = dataDto.caisse.idCaisse;
      session.idUtilisateur = dataDto.utilisateur.idUtilisateur;
      session.dateDebut = DateTime.now();
      session.fFerme = 0;
      session.montantOuverture = 0;
      print("SessionDto: " + jsonEncode(session.toJson()));
      SessionEndPointApi sessionEndPointApi = SessionEndPointApi();
      ResponseSingle responseSingle =
          await sessionEndPointApi.createsessionUsingPOST(session);
      Session sessionRet = Session.fromJson(responseSingle.objectResponse);
      dataDto.session = sessionRet;
      if (responseSingle.objectResponse != null) {
         (dataDto.utilisateurType.code
            .toLowerCase()
            .contains('cuisine')) ?    Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => CommandesCuisine(dataDto)),
        ) :   Navigator.pushReplacement(
           context,
           MaterialPageRoute(builder: (context) => Dashboard(dataDto)),
         );
      } else {
        AlertNotif().alert(context, responseSingle.errorDescription);
      }
    } catch (err) {
      Crashlytics().sendEmail("Billet creatNewSession", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
              flex: 1,
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: dataDto.utilisateurType.fGerant == 1
                            ? height / 9
                            : height / 20),
                    color: Color(0xff352C60),
                    padding: EdgeInsets.only(top: 16),
                    child: Center(
                        child: Column(
                      children: [
                        Icon(
                          Icons.person_pin,
                          color: Colors.white,
                          size: width > height ? width / 16 : width / 4,
                        ),
                        FlatButton(
                            onPressed: () {},
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(),
                                ),
                                Text(
                                  "Bienvenue",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: width > height
                                          ? width / 50
                                          : width / 25),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  dataDto.utilisateur.nom +
                                      " " +
                                      dataDto.utilisateur.prenom,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w300,
                                      fontSize: width > height
                                          ? width / 50
                                          : width / 25),
                                  textAlign: TextAlign.center,
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                              ],
                            ))
                      ],
                    )),
                  ),
                  Positioned(
                      bottom: 0,
                      right: width / 4,
                      child: Column(
                        children: [
                          _loading
                              ? Container(
                            margin: EdgeInsets.all(16),
                            child: CircularProgressIndicator(),
                            alignment: Alignment.center,
                          ):
                          dataDto.utilisateurType.fGerant == 1
                              ? Container(
                                  height: height / 10,
                                  width: width / 2,
                                  child: RaisedButton(
                                      elevation: 0,
                                      color: Color(0xff32C533),
                                      shape: new RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(
                                            width / 10),
                                      ),
                                      onPressed: () => {showConfirmDialog()},
                                      padding: EdgeInsets.all(0),
                                      child: Text(
                                        dataDto.journee != null
                                            ? "Clôture de la Journée"
                                            : "Ouverture de la Journée et session",
                                        style: TextStyle(
                                            fontSize: width > height
                                                ? width / 60
                                                : width / 40,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.white),
                                        textAlign: TextAlign.center,
                                      )),
                                )
                              : SizedBox(),
                          SizedBox(
                            height: dataDto.utilisateurType.fGerant == 1
                                ? height / 30
                                : 0,
                          ),
                          Container(
                            height: height / 10,
                            width: width / 2,
                            child:   _loading
                                ? Container(
                              margin: EdgeInsets.all(16),
                              child: CircularProgressIndicator(),
                              alignment: Alignment.center,
                            ): RaisedButton(
                                elevation: 0,
                                color: Color(0xff32C533),
                                shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(width / 10),
                                ),
                                onPressed: () {
                                  if (dataDto.utilisateurType.code
                                      .toLowerCase()
                                      .contains('cuisine')) {
                                    creatNewSession();
                                    return;
                                  }




                                  dataDto.utilisateurType.fGerant == 1 &&
                                          dataDto.journee == null
                                      ? createNewJournee()
                                      :  findAllBilletMonnaie(context);
                                },
                                padding: EdgeInsets.all(0),
                                child:
                                Text(
                                  dataDto.journee != null
                                      ? "Ouverture de votre session"
                                      : "Ouverture de la Journée",
                                  style: TextStyle(
                                      fontSize: width > height
                                          ? width / 60
                                          : width / 40,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                  textAlign: TextAlign.center,
                                )),
                          )
                        ],
                      )),
                  Positioned(
                      bottom: height / 20,
                      right: 8,
                      child: Container(
                        height: height / 20,
                        width: width / 6,
                        child: FlatButton(
                            onPressed: () => {},
                            padding: EdgeInsets.all(0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Icon(
                                  Icons.access_time,
                                  color: Colors.white,
                                  size:
                                      width > height ? width / 64 : width / 32,
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  DateFormat("dd-MM-yyyy")
                                      .format(DateTime.now()),
                                  style: TextStyle(
                                      fontSize: width > height
                                          ? width / 150
                                          : width / 75,
                                      fontWeight: FontWeight.w300,
                                      color: Colors.white),
                                  textAlign: TextAlign.end,
                                )
                              ],
                            )),
                      ))
                ],
              )),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.white,
              margin: EdgeInsets.all(height / 6),
              child: Image.asset(
                'assets/logo/logo.png',
                fit: BoxFit.contain,
              ),
            ),
          )
        ],
      ),
    );
  }
}
