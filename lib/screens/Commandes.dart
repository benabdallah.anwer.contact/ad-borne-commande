import 'dart:convert';

import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/dto/CommandeLivreur.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/dto/StompMessage.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/ordering/model/commande.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/Animator.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/screens/Transactions.dart';
import 'package:ad_caisse/transaction/api/commande_end_point_api.dart';
import 'package:ad_caisse/transaction/model/CommandeDetailsDto.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/transaction/model/commande.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:ad_caisse/utils/SizeCalcule.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class Commandes extends StatefulWidget {
  final DataDto dataDto;

  Commandes(this.dataDto);

  @override
  CommandesState createState() => CommandesState(this.dataDto);
}

class CommandesState extends State<Commandes> {
  DataDto dataDto;
  String search = "";
  var commandeEndPointApi = CommandeEndPointApi();
  final utilisateurEndPointApi = UtilisateurEndPointApi();
  TextEditingController codeAnnuleeController = new TextEditingController();
  bool actionAnnule;
  String codeAnnule;
  Commande commandeTemp = Commande();
  CommandesState(this.dataDto);

  @override
  void initState() {
    super.initState();
    getActionAnnulee();
    dataDto.searchClient("Rechercher Commande");
    dataDto.textController.addListener(txtListener);
    dataDto.searchController.addListener(_printLatestValue);
  }

  Future<void> getActionAnnulee() async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();

    bool actionA = prefs.getBool('action_Annulee');
    String codeA = prefs.getString("codeActionAnnulee");

    if (actionA != null) {
      setState(() {
        actionAnnule = actionA;
      });
    }

    if (codeA.isNotEmpty) {
      setState(() {
        codeAnnule = codeA;
      });
    }
  }

  void showCodeActionAnnulee(CommandesDtoTransaction item) {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Veuillez saisir le Code de validation ',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                SizedBox(height: 16.0),
                TextField(
                    obscureText: true,
                    cursorColor: Color(0xff352C60),
                    controller: codeAnnuleeController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xfff5f5f5),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Code Action",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    )),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Annuler', style: TextStyle(color: Colors.white)),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xffCF0606),
              onPressed: () {
                codeAnnuleeController.clear();
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text(
                'Valider',
                style: TextStyle(color: Colors.white),
              ),
              elevation: 0,
              padding: EdgeInsets.all(16),
              color: Color(0xFF32C533),
              onPressed: () {
                if (codeAnnule.toLowerCase() ==
                    codeAnnuleeController.text.toLowerCase()) {
                  codeAnnuleeController.clear();
                  Navigator.of(context).pop();
                  alert(item);
                }
              },
            ),
          ],
        );
      },
    );
  }

  txtListener() {
    setState(() {
      dataDto.searchController.text = dataDto.textController.text;
    });
  }

  _printLatestValue() {
    setState(() {
      dataDto.textController.text = dataDto.searchController.text;
      dataDto.searchController.selection = TextSelection.fromPosition(
          TextPosition(offset: dataDto.searchController.text.length));
      search = dataDto.searchController.text;
    });
  }

  @override
  void dispose() {
    dataDto.textController.removeListener(txtListener);
    dataDto.searchController.removeListener(_printLatestValue);
    setState(() {
      dataDto.commandeDelete = [];
    });
    super.dispose();
  }

  void deleteCommande(commandesDtoTransaction) async {
    try {
      bool connected = await Utils().checkInternet();
      String url = ServerUrl.url;
      if (!connected) {
        url = ServerUrl.urlSynchro;
      }
      var response = await http.get(Uri.parse(url +
          '/bp-api-transaction/v1/AnnulerCommande/' +
          commandesDtoTransaction.commande.idCommande));
      if (response != null && response.statusCode == 200) {
        debugPrint('deleteCommande: ' + response.body);
        ResponseSingle responseSingle =
            ResponseSingle.fromJson(json.decode(response.body));
        if (responseSingle.result != 1) {
          AlertNotif().alert(context, responseSingle.errorDescription);
        } else {
          await getCommandes();
        }
      }
    } catch (err) {
      Crashlytics().sendEmail("Commandes deleteCommande", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void getCommandes() async {
    try {
      ResponseList responseList =
          await commandeEndPointApi.gettransactionbyjourneeandfpayeUsingGET(
              dataDto.journee.idJournee, 0);
      List<CommandesDtoTransaction> list = [];
      if (responseList.result == 1 && responseList.objectResponse != null) {
        list = List.from(responseList.objectResponse)
            .map((e) => CommandesDtoTransaction.fromJson(e))
            .toList()
            .where((element) => element.commandeDetails?.isNotEmpty)
            .toList();
      }
      setState(() {
        dataDto.listCommande = list;
      });
    } catch (err) {
      Crashlytics().sendEmail("Commandes getCommandes", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  void setCurrentCommnde(CommandeTransaction commande,
      List<CommandeDetailsDto> commandeDetails, BuildContext context) {
    dataDto.commandeDetails = [];
    List<CommandeDetails> list = [];
    commandeDetails.forEach((element) {
      CommandeDetails commandeDetails = CommandeDetails();
      commandeDetails.prix = element.commandeDetails.prix;
      //commandeDetails.ingredients=element.commandeDetails.ingredient;
      commandeDetails.idProduit = element.commandeDetails.idProduit;
      commandeDetails.quantite = element.commandeDetails.quantite;
      commandeDetails.idCommande = element.commandeDetails.idCommande;
      commandeDetails.idDetailComm = element.commandeDetails.idDetailComm;
      commandeDetails.isAnnule = element.commandeDetails.isAnnule;
      commandeDetails.designation = element.commandeDetails.designation;
      commandeDetails.employe = element.commandeDetails.employe;
      list.add(commandeDetails);
    });
    dataDto.clientBPrice = commande.clients.length > 0
        ? ClientBPrice.fromJson(commande.clients[0].toJson())
        : null;
    dataDto.commande = commande;
    dataDto.commandeDetails = list;
    dataDto.callbackCommande(null, context);
    dataDto.rendu = null;
    dataDto.payementList = [];
    if (dataDto.pointVente.fGestionTable == 1 && commande.idTable != null) {
      TableCaisse tableCaisse = new TableCaisse();
      tableCaisse.commande = commande;
      tableCaisse.idPointVente = dataDto.pointVente.idPointVente;
      tableCaisse.commandeDetails = list;
      tableCaisse.numTable = commande.numTable;
      tableCaisse.idTable = commande.idTable;
      setState(() {
        dataDto.tableCaisse = tableCaisse;
      });
    }
    dataDto.setCurrentPage(CommandeScreen(dataDto), "CommandeScreen");
  }

  void setCurrentDetailsCommande(CommandeTransaction commande,
      List<CommandeDetailsDto> commandeDetails, BuildContext context) {
    dataDto.commandeDetails = [];
    dataDto.commandeDelete = [];
    List<CommandeDetails> list = [];
    commandeDetails.forEach((element) {
      CommandeDetails commandeDetails = CommandeDetails();
      commandeDetails.prix = element.commandeDetails.prix;
      commandeDetails.idProduit = element.commandeDetails.idProduit;
      commandeDetails.quantite = element.commandeDetails.quantite;
      commandeDetails.idCommande = element.commandeDetails.idCommande;
      commandeDetails.idDetailComm = element.commandeDetails.idDetailComm;
      commandeDetails.isAnnule = element.commandeDetails.isAnnule;
      commandeDetails.designation = element.commandeDetails.designation;
      commandeDetails.employe = element.commandeDetails.employe;
      list.add(commandeDetails);
    });
    dataDto.commandeDelete = list;
    setState(() {
      commandeTemp.numCommande = commande.numCommande;
    });
  }

  Future<void> affectLivreurCommande(
      CommandesDtoTransaction commande, Utilisateur user) async {
    CommandeLivreur cl = new CommandeLivreur(
        dateAffect: DateTime.now(),
        idCommande: commande.commande.idCommande,
        idUtilisateur: user.idUtilisateur);
    ResponseSingle responseSingle =
        await commandeEndPointApi.affectCommandelivreur(cl);
    if (responseSingle.result == 1) {
      this._showToast(context);
    }
    /*  String url = ServerUrl.url + '/bp-api-admin/v1/addNotification';
    StompMessage message = StompMessage();
    message.idPointVente=commande.commande.idPointVente;
    message.idMessageCommande=commande.commande.idMessageCommande;
    message.idClientPartenaire=commande.commande.clients[0].idClientPartenaire;
    message.idPartenaire=dataDto.pointVente.idPartenaire;
    message.body="test test";
    final res = await http.post(url,
        body: jsonEncode(message),
        headers: {"Content-Type": "application/json"});
    if (res.statusCode >= 400) {
      debugPrint(res.statusCode.toString() + ' ' + res.body);
    } else if (res.body != null) {
      debugPrint(jsonDecode(res.body));
    }*/
  }

  void _showToast(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: Colors.green,
        content: const Text('Affecté avec succés'),
      ),
    );
  }

  Future<void> _showDialogLivreur(CommandesDtoTransaction commande) async {
    try {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      String title = commande.commande.nomPrenomLivreur != null
          ? "Cette commande est déjà affectée à " +
              commande.commande.nomPrenomLivreur +
              " !"
          : "Veuillez sélectionner un livreur";
      List<Utilisateur> services = [];
      ResponseList responseList = await utilisateurEndPointApi
          .findAllByCodeTypeUtilisateurAndIdPointVenteUsingGET(
              dataDto.pointVente.idPointVente, "LIVREUR");
      if (responseList.result == 1) {
        services = responseList.objectResponse
            .map((e) => Utilisateur.fromJson(e))
            .toList();
      }
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xffEEEEEE),
            title: Text(
              title,
              textAlign: TextAlign.center,
            ),
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            content: Container(
              height: height * 0.60,
              width: width * 0.5,
              padding: EdgeInsets.all(16),
              child: GridView.count(
                padding: EdgeInsets.only(top: 8),
                crossAxisCount: width > height ? 4 : 2,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                childAspectRatio: 2,
                children: services
                    .map(
                      (item) => SizedBox(
                        child: WidgetANimator(FlatButton(
                            color: Color(0xffE7D7EE),
                            padding: EdgeInsets.all(0),
                            onPressed: () => {
                                  // produit.employe = item,
                                  // this
                                  //     .dataDto
                                  //     .callbackCommande(produit, context),
                                  // setState(() {
                                  //   selectedUser = item;
                                  //  }),
                                  affectLivreurCommande(commande, item),
                                  Navigator.pop(context)
                                },
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      item.prenom + " " + item.nom,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: width > height
                                              ? width / 80
                                              : width / 35,
                                          color: Color(0xff352C60)),
                                    ),
                                  ],
                                )
                              ],
                            ))),
                      ),
                    )
                    .toList(),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                child: Text('Annuler'),
                onPressed: () {
                  //    this.dataDto.callbackCommande(produit, context);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (err) {
      Crashlytics().sendEmail("CommandeScreen _showMyDialog", err.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
    }
  }

  Future<void> printNote(
      CommandesDtoTransaction commandesDtoTransaction) async {
    debugPrint('print note ');
    await Utils().printService(context, commandesDtoTransaction, dataDto);
  }

  Future<void> alert(CommandesDtoTransaction commandesDtoTransaction) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  'Êtes-vous sûr de vouloir supprimer cette commande ?',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('ANNULER'),
              padding: EdgeInsets.only(left: 16, right: 16),
              elevation: 0,
              color: Colors.black26,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text('OK'),
              elevation: 0,
              color: Colors.green,
              onPressed: () {
                deleteCommande(commandesDtoTransaction);
                setCurrentDetailsCommande(commandesDtoTransaction.commande,
                    commandesDtoTransaction.commandeDetails, context);
                Utils().cuisine(
                    context, dataDto.commandeDetails, dataDto, commandeTemp);

                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Commandes :",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: width > height ? width / 50 : width / 30),
            ),
            Expanded(
              child: Container(),
            ),
            SizedBox(
              height: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              width: width > height
                  ? SizeClacule().calcule(context, 15)
                  : SizeClacule().calcule(context, 7.5),
              child: FlatButton(
                  color: Color(0xff32C533),
                  onPressed: () => {
                        dataDto.setCurrentPage(
                            CommandeScreen(dataDto), "CommandeScreen")
                      },
                  padding: EdgeInsets.all(0),
                  child: Image.asset(
                    'assets/back/back.png',
                    color: Colors.white,
                    fit: BoxFit.contain,
                    height: width > height ? width / 40 : width / 20,
                    width: width > height ? width / 40 : width / 20,
                  )),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    child: Text(
                      "N°",
                      style: TextStyle(
                        fontSize: SizeClacule().calcule(context, 60),
                      ),
                    ),
                    padding: EdgeInsets.only(left: 8),
                    width: 85,
                  ),
                  Expanded(
                    child: Text(
                      "Date",
                      style: TextStyle(
                        fontSize: SizeClacule().calcule(context, 60),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "Montant",
                      style: TextStyle(
                        fontSize: SizeClacule().calcule(context, 60),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      "Détail commande",
                      style: TextStyle(
                        fontSize: SizeClacule().calcule(context, 60),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "Clients",
                      style: TextStyle(
                        fontSize: SizeClacule().calcule(context, 60),
                      ),
                    ),
                  ),
                  dataDto.pointVente?.fGestionTable == 1
                      ? Container(
                          width: 100,
                          child: Text(
                            "N° Table",
                            style: TextStyle(
                              fontSize: SizeClacule().calcule(context, 70),
                            ),
                          ))
                      : SizedBox(
                          width: 0,
                        ),
                  Expanded(
                    child: Text(
                      "Actions",
                      style: TextStyle(
                        fontSize: SizeClacule().calcule(context, 60),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: (!(dataDto.caisseType.code == 'CENTRALE' &&
                          (dataDto.utilisateurType.code == 'CAISSIER' ||
                              dataDto.utilisateurType.code == 'PROP' ||
                              dataDto.utilisateurType.code == 'GERANT')))
                      ?  Column(
                    children: [
                      for (var item in dataDto.listCommande.reversed
                          .where((c) =>
                      (c.commande.numCommande
                          .toString()
                          .toLowerCase()
                          .contains(search.toLowerCase()) ||
                          c.commande.nomPrenomLivreur
                              .toString()
                              .toLowerCase()
                              .contains(search.toLowerCase()) ||
                          c.commande.idCommande
                              .toString()
                              .toLowerCase()
                              .contains(search.toLowerCase()) ||
                          c.commande.numTable
                              .toString()
                              .toLowerCase()
                              .contains(search.toLowerCase()) ||
                          c.commandeDetails
                              .where((element) => element
                              .commandeDetails.designation
                              .toLowerCase()
                              .contains(
                              search.toLowerCase()))
                              .toList()
                              .length >
                              0)

                          ///Filtrage by id session
                          &&
                          c.commande.idSession.toString() ==
                              (dataDto.session.idSession))
                          .toList())
                        Column(
                          children: [
                            FlatButton(
                              child: Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 8),
                                    width: 80,
                                    child: Text(
                                      item.commande.numCommande
                                          .toString(),
                                      style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 60),
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      DateFormat("HH:mm\ndd-MM-yyyy ")
                                          .format(
                                          item.commande.dateCreation),
                                      style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 80),
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      item.commande.montant
                                          .toStringAsFixed(3) +
                                          " DT",
                                      style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 60),
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      children: [
                                        for (var items
                                        in item.commandeDetails)
                                          Column(
                                            children: [
                                              Text(
                                                items.commandeDetails
                                                    .quantite
                                                    .toString() +
                                                    " " +
                                                    items.commandeDetails
                                                        .designation
                                                        .toString() /*+
                                                    " " +
                                                    items.commandeDetails.prix
                                                        .toStringAsFixed(3) +
                                                    " DT"*/
                                                ,
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        70),
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                              SizedBox(
                                                height: 8,
                                              )
                                            ],
                                          )
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      children: [
                                        for (var items
                                        in item.commande.clients)
                                          Column(
                                            children: [
                                              Text(
                                                items.nom != null
                                                    ? items.prenom +
                                                    " " +
                                                    items.nom
                                                    : "",
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        80),
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                            ],
                                          ),
                                        Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [
                                              Text(
                                                item.commande
                                                    .typeCommande !=
                                                    null
                                                    ? " - " +
                                                    item.commande
                                                        .typeCommande
                                                        .toString()
                                                    : "",
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        90),
                                                    color:
                                                    Colors.deepPurple,
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                              item.commande.statut != null
                                                  ? Text(
                                                item.commande.statut
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        90),
                                                    color: item.commande
                                                        .statut
                                                        .toString() ==
                                                        "livrée"
                                                        ? Colors
                                                        .green
                                                        : Colors
                                                        .orange,
                                                    fontWeight:
                                                    FontWeight
                                                        .w500),
                                              )
                                                  : Text(''),
                                              if (item.commande
                                                  .nomPrenomLivreur !=
                                                  null)
                                                Text(
                                                  "Par " +
                                                      item.commande
                                                          .nomPrenomLivreur,
                                                  style: TextStyle(
                                                      fontSize:
                                                      SizeClacule()
                                                          .calcule(
                                                          context,
                                                          90),
                                                      fontWeight:
                                                      FontWeight
                                                          .w500),
                                                ),
                                            ]),
                                      ],
                                    ),
                                  ),
                                  dataDto.pointVente.fGestionTable == 1
                                      ? Container(
                                      alignment: Alignment.center,
                                      width: 70,
                                      child: Text(
                                        item.commande.numTable != null
                                            ? item.commande.numTable
                                            .toString()
                                            : "",
                                        style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 60),
                                        ),
                                      ))
                                      : SizedBox(
                                    width: 0,
                                  ),
                                  item.commande.typeCommande != null &&
                                      item.commande.typeCommande ==
                                          "livraison" &&
                                      dataDto.caisseType.code ==
                                          'CENTRALE' &&
                                      (dataDto.utilisateurType.code ==
                                          'CAISSIER' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'PROP' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'GERANT')
                                      ? Container(
                                    margin:
                                    EdgeInsets.only(right: 4),
                                    child: IconButton(
                                      onPressed: () {
                                        _showDialogLivreur(item);
                                      },
                                      iconSize: 20,
                                      color: Colors.deepPurple,
                                      icon: Icon(Icons
                                          .account_box_outlined),
                                    ),
                                  )
                                      : SizedBox(
                                    width: 40,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 4),
                                    child: IconButton(
                                      onPressed: () {
                                        printNote(item);
                                      },
                                      iconSize: 20,
                                      color: Colors.deepPurple,
                                      icon: Icon(
                                          Icons.sticky_note_2_rounded),
                                    ),
                                  ),
                                  dataDto.caisseType.code == 'CENTRALE' &&
                                      (dataDto.utilisateurType.code ==
                                          'CAISSIER' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'PROP' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'GERANT')
                                      ? Container(
                                    margin:
                                    EdgeInsets.only(right: 4),
                                    child: IconButton(
                                      onPressed: () {
                                        (dataDto.caisseType.code ==
                                            'CENTRALE' &&
                                            dataDto.utilisateurType
                                                .code ==
                                                'CAISSIER' &&
                                            actionAnnule ==
                                                true)
                                            ? showCodeActionAnnulee(
                                            item)
                                            : alert(item);
                                      },
                                      color: Colors.red,
                                      icon: Icon(
                                          Icons.delete_outline),
                                    ),
                                  )
                                      : SizedBox(),
                                ],
                              ),
                              color: Color(0xffE7D7EE),
                              padding: EdgeInsets.only(
                                top: 16,
                                bottom: 16,
                              ),
                              onPressed: () {
                                setCurrentCommnde(item.commande,
                                    item.commandeDetails, context);
                              },
                              onLongPress: () {
                                alert(item);
                              },
                            ),
                            SizedBox(
                              height: 8,
                            )
                          ],
                        )
                    ],
                  )
                      : Column(
                    children: [
                      for (var item in dataDto.listCommande.reversed
                          .where((c) => (c.commande.numCommande
                          .toString()
                          .toLowerCase()
                          .contains(search.toLowerCase()) ||
                          c.commande.nomPrenomLivreur
                              .toString()
                              .toLowerCase()
                              .contains(search.toLowerCase()) ||
                          c.commande.idCommande
                              .toString()
                              .toLowerCase()
                              .contains(search.toLowerCase()) ||
                          c.commande.numTable
                              .toString()
                              .toLowerCase()
                              .contains(search.toLowerCase()) ||
                          c.commandeDetails
                              .where((element) => element
                              .commandeDetails.designation
                              .toLowerCase()
                              .contains(search.toLowerCase()))
                              .toList()
                              .length >
                              0))
                          .toList())
                        Column(
                          children: [
                            FlatButton(
                              child: Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 8),
                                    width: 80,
                                    child: Text(
                                      item.commande.numCommande
                                          .toString(),
                                      style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 60),
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      DateFormat("HH:mm\ndd-MM-yyyy ")
                                          .format(
                                          item.commande.dateCreation),
                                      style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 80),
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      item.commande.montant
                                          .toStringAsFixed(3) +
                                          " DT",
                                      style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 60),
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      children: [
                                        for (var items
                                        in item.commandeDetails)
                                          Column(
                                            children: [
                                              Text(
                                                items.commandeDetails
                                                    .quantite
                                                    .toString() +
                                                    " " +
                                                    items.commandeDetails
                                                        .designation
                                                        .toString() /*+
                                                    " " +
                                                    items.commandeDetails.prix
                                                        .toStringAsFixed(3) +
                                                    " DT"*/
                                                ,
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        70),
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                              SizedBox(
                                                height: 8,
                                              )
                                            ],
                                          )
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      children: [
                                        for (var items
                                        in item.commande.clients)
                                          Column(
                                            children: [
                                              Text(
                                                items.nom != null
                                                    ? items.prenom +
                                                    " " +
                                                    items.nom
                                                    : "",
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        80),
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                            ],
                                          ),
                                        Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [
                                              Text(
                                                item.commande
                                                    .typeCommande !=
                                                    null
                                                    ? " - " +
                                                    item.commande
                                                        .typeCommande
                                                        .toString()
                                                    : "",
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        90),
                                                    color:
                                                    Colors.deepPurple,
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                              item.commande.statut != null
                                                  ? Text(
                                                item.commande.statut
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize:
                                                    SizeClacule()
                                                        .calcule(
                                                        context,
                                                        90),
                                                    color: item.commande
                                                        .statut
                                                        .toString() ==
                                                        "livrée"
                                                        ? Colors
                                                        .green
                                                        : Colors
                                                        .orange,
                                                    fontWeight:
                                                    FontWeight
                                                        .w500),
                                              )
                                                  : Text(''),
                                              if (item.commande
                                                  .nomPrenomLivreur !=
                                                  null)
                                                Text(
                                                  "Par " +
                                                      item.commande
                                                          .nomPrenomLivreur,
                                                  style: TextStyle(
                                                      fontSize:
                                                      SizeClacule()
                                                          .calcule(
                                                          context,
                                                          90),
                                                      fontWeight:
                                                      FontWeight
                                                          .w500),
                                                ),
                                            ]),
                                      ],
                                    ),
                                  ),
                                  dataDto.pointVente.fGestionTable == 1
                                      ? Container(
                                      alignment: Alignment.center,
                                      width: 70,
                                      child: Text(
                                        item.commande.numTable != null
                                            ? item.commande.numTable
                                            .toString()
                                            : "",
                                        style: TextStyle(
                                          fontSize: SizeClacule()
                                              .calcule(context, 60),
                                        ),
                                      ))
                                      : SizedBox(
                                    width: 0,
                                  ),
                                  item.commande.typeCommande != null &&
                                      item.commande.typeCommande ==
                                          "livraison" &&
                                      dataDto.caisseType.code ==
                                          'CENTRALE' &&
                                      (dataDto.utilisateurType.code ==
                                          'CAISSIER' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'PROP' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'GERANT')
                                      ? Container(
                                    margin:
                                    EdgeInsets.only(right: 4),
                                    child: IconButton(
                                      onPressed: () {
                                        _showDialogLivreur(item);
                                      },
                                      iconSize: 20,
                                      color: Colors.deepPurple,
                                      icon: Icon(Icons
                                          .account_box_outlined),
                                    ),
                                  )
                                      : SizedBox(
                                    width: 40,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 4),
                                    child: IconButton(
                                      onPressed: () {
                                        printNote(item);
                                      },
                                      iconSize: 20,
                                      color: Colors.deepPurple,
                                      icon: Icon(
                                          Icons.sticky_note_2_rounded),
                                    ),
                                  ),
                                  dataDto.caisseType.code == 'CENTRALE' &&
                                      (dataDto.utilisateurType.code ==
                                          'CAISSIER' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'PROP' ||
                                          dataDto.utilisateurType
                                              .code ==
                                              'GERANT')
                                      ? Container(
                                    margin:
                                    EdgeInsets.only(right: 4),
                                    child: IconButton(
                                      onPressed: () {
                                        (dataDto.caisseType.code ==
                                            'CENTRALE' &&
                                            dataDto.utilisateurType
                                                .code ==
                                                'CAISSIER' &&
                                            actionAnnule ==
                                                true)
                                            ? showCodeActionAnnulee(
                                            item)
                                            : alert(item);
                                      },
                                      color: Colors.red,
                                      icon: Icon(
                                          Icons.delete_outline),
                                    ),
                                  )
                                      : SizedBox(),
                                ],
                              ),
                              color: Color(0xffE7D7EE),
                              padding: EdgeInsets.only(
                                top: 16,
                                bottom: 16,
                              ),
                              onPressed: () {
                                setCurrentCommnde(item.commande,
                                    item.commandeDetails, context);
                              },
                              onLongPress: () {
                                alert(item);
                              },
                            ),
                            SizedBox(
                              height: 8,
                            )
                          ],
                        )
                    ],
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
