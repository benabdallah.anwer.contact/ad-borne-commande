import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/transaction/model/commande_details.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class CommandeDetailsEndPointApi {
  final ApiClient apiClient;

  CommandeDetailsEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer une CommandeDetails
  ///
  /// Retourner la CommandeDetails créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;CommandeDetails est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;commande id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; le produit n&#39;existe pas  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; Commande n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createCommandeDetailsUsingPOST(
      CommandeDetailsTransaction commandeDetails) async {
    Object postBody = commandeDetails;

    // verify required params are set
    if (commandeDetails == null) {
      throw new ApiException(400, "Missing required param: commandeDetails");
    }

    // create path and map variables
    String path = "/v1/CreateCommandeDetails".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une CommandeDetails
  ///
  /// Retourner une valeur boolean qui indique si la CommandeDetails a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;CommandeDetails n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteCommandeDetailsUsingDELETE(
      String idCommandeDetails) async {
    Object postBody = null;

    // verify required params are set
    if (idCommandeDetails == null) {
      throw new ApiException(400, "Missing required param: idCommandeDetails");
    }

    // create path and map variables
    String path = "/v1/DeleteCommandeDetails/{idCommandeDetails}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idCommandeDetails" + "}", idCommandeDetails.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des CommandeDetailss selon l&#39;IdTicket  envoyer
  ///
  /// Retourner la list des CommandeDetailss selon l&#39;IdTicket envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List CommandeDetails n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List CommandeDetails est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllByIdTicketUsingGET(String idTicket) async {
    Object postBody = null;

    // verify required params are set
    if (idTicket == null) {
      throw new ApiException(400, "Missing required param: idTicket");
    }

    // create path and map variables
    String path = "/v1/findAllByIdTicket/{idTicket}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idTicket" + "}", idTicket.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Afficher la CommandeDetails selon l&#39;idCommandeDetails envoyer
  ///
  /// Retourner la CommandeDetails selon l&#39;idCommandeDetails envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; CommandeDetails exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;CommandeDetails n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdCommandeDetailsUsingGET(
      String idCommandeDetails) async {
    Object postBody = null;

    // verify required params are set
    if (idCommandeDetails == null) {
      throw new ApiException(400, "Missing required param: idCommandeDetails");
    }

    // create path and map variables
    String path = "/v1/findByIdCommandeDetails/{idCommandeDetails}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idCommandeDetails" + "}", idCommandeDetails.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier une CommandeDetails
  ///
  /// Retourner la CommandeDetails modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;CommandeDetails est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;commande id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; CommandeDetails n&#39;exist pas  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; le produit n&#39;existe pas  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; Commande n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateCommandeDetailsUsingPUT(
      CommandeDetailsTransaction commandeDetails) async {
    Object postBody = commandeDetails;

    // verify required params are set
    if (commandeDetails == null) {
      throw new ApiException(400, "Missing required param: commandeDetails");
    }

    // create path and map variables
    String path = "/v1/UpdateCommandeDetails".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
