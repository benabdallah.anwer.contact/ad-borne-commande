import 'package:ad_caisse/dto/CommandeLivreur.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/transaction/model/commande.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class CommandeEndPointApi {
  final ApiClient apiClient;

  CommandeEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// Afficher la list des Commandes selon l&#39;idjournee et fpaye  envoyer
  ///
  /// Retourner la list des Commandes selon l&#39;idjournee et fpaye envoyer   input fpaye&#x3D;1 &#x3D;&gt; payée     input fpaye&#x3D;0 &#x3D;&gt; non payée    &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Commande n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Commande est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findNTopbydidpointventeandIdclientpartenaireUsingGET(
      String idpv, String idclient, int size) async {
    Object postBody = null;

    // verify required params are set
    if (idpv == null) {
      throw new ApiException(400, "Missing required param: idpv");
    }
    if (idclient == null) {
      throw new ApiException(400, "Missing required param: idclient");
    }
    if (size == null) {
      throw new ApiException(400, "Missing required param: size");
    }

    // create path and map variables
    String path =
        "/v1/findNTopbydidpointventeandIdclientpartenaire/{idpv}/{idclient}/{size}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idpv" + "}", idpv.toString())
            .replaceAll("{" + "idclient" + "}", idclient.toString())
            .replaceAll("{" + "size" + "}", size.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// créer une Commande
  ///
  /// Retourner la Commande créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Commande est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; session n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createCommandeUsingPOST(CommandeTransaction commande) async {
    Object postBody = commande;

    // verify required params are set
    if (commande == null) {
      throw new ApiException(400, "Missing required param: commande");
    }

    // create path and map variables
    String path = "/v1/CreateCommande".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une Commande
  ///
  /// affecter livreur commande
  Future<ResponseSingle> affectCommandelivreur(CommandeLivreur commande) async {
    Object postBody = commande;

    // verify required params are set
    if (commande == null) {
      throw new ApiException(400, "Missing required param: commande");
    }

    // create path and map variables
    String path = "/v1/affectCommandelivreur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
    contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
      as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Retourner une valeur boolean qui indique si la Commande a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;Commande n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteCommandeUsingDELETE(String idCommande) async {
    Object postBody = null;

    // verify required params are set
    if (idCommande == null) {
      throw new ApiException(400, "Missing required param: idCommande");
    }

    // create path and map variables
    String path = "/v1/DeleteCommande/{idCommande}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idCommande" + "}", idCommande.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des Commandes selon l&#39;IdPointVente  envoyer
  ///
  /// Retourner la list des Commandes selon l&#39;IdPointVente envoyer   input order&#x3D;1 &#x3D;&gt; order decroissant     input order&#x3D;2 &#x3D;&gt; order croissant    &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Commande n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Commande est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPointVenteUsingGET(
      String idPointVente, int order) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }
    if (order == null) {
      throw new ApiException(400, "Missing required param: order");
    }

    // create path and map variables
    String path = "/v1/findAllByIdPointVente/{idPointVente}/{order}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString())
        .replaceAll("{" + "order" + "}", order.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la Commande selon l&#39;idCommande envoyer
  ///
  /// Retourner la Commande selon l&#39;idCommande envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Commande exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;Commande n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdCommandeUsingGET(String idCommande) async {
    Object postBody = null;

    // verify required params are set
    if (idCommande == null) {
      throw new ApiException(400, "Missing required param: idCommande");
    }

    // create path and map variables
    String path = "/v1/findByIdCommande/{idCommande}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idCommande" + "}", idCommande.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des Commandes selon l&#39;idjournee  envoyer
  ///
  /// Retourner la list des Commandes selon l&#39;idjournee envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Commande n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Commande est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> gettransactionbyjourneeUsingGET(String idjournee) async {
    Object postBody = null;

    // verify required params are set
    if (idjournee == null) {
      throw new ApiException(400, "Missing required param: idjournee");
    }

    // create path and map variables
    String path = "/v1/Gettransactionbyjournee/{idjournee}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idjournee" + "}", idjournee.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des Commandes selon l&#39;idjournee et idcaisse  envoyer
  ///
  /// Retourner la list des Commandes selon l&#39;idjournee et idcaisse  envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Commande n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Commande est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un ou plusieurs parametres envoyer sont null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;caisse n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> gettransactionbyjourneeUsingGET1(
      String idjournee, String idcaisse) async {
    Object postBody = null;

    // verify required params are set
    if (idjournee == null) {
      throw new ApiException(400, "Missing required param: idjournee");
    }
    if (idcaisse == null) {
      throw new ApiException(400, "Missing required param: idcaisse");
    }

    // create path and map variables
    String path =
        "/v1/Gettransactionbyjourneeandidcaisse/{idjournee}/{idcaisse}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idjournee" + "}", idjournee.toString())
            .replaceAll("{" + "idcaisse" + "}", idcaisse.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des Commandes selon l&#39;idjournee et fpaye  envoyer
  ///
  /// Retourner la list des Commandes selon l&#39;idjournee et fpaye envoyer   input fpaye&#x3D;1 &#x3D;&gt; payée     input fpaye&#x3D;0 &#x3D;&gt; non payée    &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Commande n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Commande est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> gettransactionbyjourneeandfpayeUsingGET(
      String idjournee, int fpaye) async {
    Object postBody = null;

    // verify required params are set
    if (idjournee == null) {
      throw new ApiException(400, "Missing required param: idjournee");
    }
    if (fpaye == null) {
      throw new ApiException(400, "Missing required param: fpaye");
    }

    // create path and map variables
    String path = "/v1/Gettransactionbyjourneeandfpaye/{idjournee}/{fpaye}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idjournee" + "}", idjournee.toString())
        .replaceAll("{" + "fpaye" + "}", fpaye.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Modifier une Commande
  ///
  /// Retourner la Commande modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Commande est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; Commande n&#39;exist pas  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; session n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateCommandeUsingPUT(CommandeTransaction commande) async {
    Object postBody = commande;

    // verify required params are set
    if (commande == null) {
      throw new ApiException(400, "Missing required param: commande");
    }

    // create path and map variables
    String path = "/v1/UpdateCommande".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
