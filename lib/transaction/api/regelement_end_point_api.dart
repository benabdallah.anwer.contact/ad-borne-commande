import 'package:ad_caisse/transaction/model/reglement.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class RegelementEndPointApi {
  final ApiClient apiClient;

  RegelementEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer un Reglement
  ///
  /// Retourner la Reglement créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Reglement est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;client n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; Commande n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createReglementUsingPOST(Reglement reglement) async {
    Object postBody = reglement;

    // verify required params are set
    if (reglement == null) {
      throw new ApiException(400, "Missing required param: reglement");
    }

    // create path and map variables
    String path = "/v1/CreateReglement".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer un Reglement
  ///
  /// Retourner un valeur boolean qui indique si la Reglement a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;Reglement n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteReglementUsingDELETE(String idReglement) async {
    Object postBody = null;

    // verify required params are set
    if (idReglement == null) {
      throw new ApiException(400, "Missing required param: idReglement");
    }

    // create path and map variables
    String path = "/v1/DeleteReglement/{idReglement}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idReglement" + "}", idReglement.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des Commandes selon l&#39;IdPointVente  envoyer
  ///
  /// Retourner la list des Commandes selon l&#39;IdPointVente envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Commande n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Commande est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPointVenteUsingGET1(String idCommande) async {
    Object postBody = null;

    // verify required params are set
    if (idCommande == null) {
      throw new ApiException(400, "Missing required param: idCommande");
    }

    // create path and map variables
    String path = "/v1/findAllRegelementByIdTicket/{idCommande}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idCommande" + "}", idCommande.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la Reglement selon l&#39;idReglement envoyer
  ///
  /// Retourner la Reglement selon l&#39;idReglement envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Reglement exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;Reglement n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdReglementUsingGET(String idReglement) async {
    Object postBody = null;

    // verify required params are set
    if (idReglement == null) {
      throw new ApiException(400, "Missing required param: idReglement");
    }

    // create path and map variables
    String path = "/v1/findByIdReglement/{idReglement}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idReglement" + "}", idReglement.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier un Reglement
  ///
  /// Retourner la Reglement modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Reglement est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;client n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; Commande n&#39;exist pas  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; Reglement n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateReglementUsingPUT(Reglement reglement) async {
    Object postBody = reglement;

    // verify required params are set
    if (reglement == null) {
      throw new ApiException(400, "Missing required param: reglement");
    }

    // create path and map variables
    String path = "/v1/UpdateReglement".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
