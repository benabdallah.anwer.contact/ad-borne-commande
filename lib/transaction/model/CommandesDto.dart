import 'package:ad_caisse/transaction/model/commande.dart';

import 'CommandeDetailsDto.dart';

class CommandesDtoTransaction {
  CommandeTransaction commande = CommandeTransaction();
  List<CommandeDetailsDto> commandeDetails = [];

  CommandesDtoTransaction();

  CommandesDtoTransaction.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    commande = CommandeTransaction.fromJson(json['commande']);
    commandeDetails = List.from(json['commandeDetails'])
        .map((e) => CommandeDetailsDto.fromJson(e))
        .toList();
  }
}
