
class Reglement {
  
  DateTime datePourEncaissement = null;
  

  DateTime dateReg = null;
  

  String idClientPartenaire = null;
  

  String idCommande = null;
  

  String idModReg = null;
  

  String idReglement = null;
  

  int isRejete = null;
  

  double montant = null;
  

  int num = null;
  

  String numReg = null;
  
  Reglement();

  @override
  String toString() {
    return 'Reglement[datePourEncaissement=$datePourEncaissement, dateReg=$dateReg, idClientPartenaire=$idClientPartenaire, idCommande=$idCommande, idModReg=$idModReg, idReglement=$idReglement, isRejete=$isRejete, montant=$montant, num=$num, numReg=$numReg, ]';
  }

  Reglement.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    datePourEncaissement = json['datePourEncaissement'] == null ? null : DateTime.parse(json['datePourEncaissement']);
    dateReg = json['dateReg'] == null ? null : DateTime.parse(json['dateReg']);
    idClientPartenaire =
        json['idClientPartenaire']
    ;
    idCommande =
        json['idCommande']
    ;
    idModReg =
        json['idModReg']
    ;
    idReglement =
        json['idReglement']
    ;
    isRejete =
        json['isRejete']
    ;
    montant =
        json['montant']
    ;
    num =
        json['num']
    ;
    numReg =
        json['numReg']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'datePourEncaissement': datePourEncaissement == null ? '' : datePourEncaissement.toIso8601String(),
      'dateReg': dateReg == null ? '' : dateReg.toIso8601String(),
      'idClientPartenaire': idClientPartenaire,
      'idCommande': idCommande,
      'idModReg': idModReg,
      'idReglement': idReglement,
      'isRejete': isRejete,
      'montant': montant,
      'num': num,
      'numReg': numReg
     };
  }

  static List<Reglement> listFromJson(List<dynamic> json) {
    return json == null ? new List<Reglement>() : json.map((value) => new Reglement.fromJson(value)).toList();
  }

  static Map<String, Reglement> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Reglement>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new Reglement.fromJson(value));
    }
    return map;
  }
}

