class ClientTransaction {
  String cin = null;

  DateTime dateCreation = null;

  DateTime dateNaissance = null;

  String email = null;

  int fActive = null;

  String genre = null;

  String idClient = null;

  String nTel = null;

  String nom = null;

  String prenom = null;

  String qrCodeBprice = null;

  double soldeBprice = null;

  double soldePartn;
  String idClientPartenaire;
  String idPartenaire;
  int isconnected;
  String qrCodePartn;
  String tokenNotification;
  String adress = null;

  ClientTransaction();

  @override
  String toString() {
    return 'Client[cin=$cin, dateCreation=$dateCreation, dateNaissance=$dateNaissance, email=$email, fActive=$fActive, genre=$genre, idClient=$idClient, nTel=$nTel, nom=$nom, prenom=$prenom, qrCodeBprice=$qrCodeBprice, soldeBprice=$soldeBprice,adress=$adress ]';
  }

  ClientTransaction.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    cin = json['cin'];
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    dateNaissance = json['dateNaissance'] == null || json['dateNaissance'] == ''
        ? null
        : DateTime.parse(json['dateNaissance']);
    email = json['email'];
    fActive = json['fActive'];
    genre = json['genre'];
    idClient = json['idClient'];
    nTel = json['nTel'];
    nom = json['nom'];
    prenom = json['prenom'];
    qrCodeBprice = json['qrCodeBprice'];
    soldeBprice = json['soldeBprice'];
    soldePartn = json['soldePartn'];
    idClientPartenaire = json['idClientPartenaire'];
    isconnected = json['isconnected'];
    qrCodePartn = json['qrCodePartn'];
    tokenNotification = json['tokenNotification'];
    idPartenaire = json['idPartenaire'];
    adress = json['adress'];
  }

  Map<String, dynamic> toJson() {
    return {
      'cin': cin,
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'dateNaissance':
          dateNaissance == null ? '' : dateNaissance.toIso8601String(),
      'email': email,
      'fActive': fActive,
      'genre': genre,
      'idClient': idClient,
      'nTel': nTel,
      'nom': nom,
      'prenom': prenom,
      'qrCodeBprice': qrCodeBprice,
      'soldeBprice': soldeBprice,
      'adress' : adress
    };
  }

  static List<ClientTransaction> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ClientTransaction>()
        : json.map((value) => new ClientTransaction.fromJson(value)).toList();
  }

  static Map<String, ClientTransaction> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ClientTransaction>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ClientTransaction.fromJson(value));
    }
    return map;
  }
}
