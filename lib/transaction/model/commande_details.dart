import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/product/model/produit.dart';

class CommandeDetailsTransaction {

  String idCommande ;

  String idDetailComm ;

  String idProduit ;

  int isAnnule;

  double prix ;

  double quantite ;

  String designation ;

  Utilisateur employe;

  double remise ;
  Produit produit;
  String ingredient;
  double prixSansRemise ;

  CommandeDetailsTransaction();

  @override
  String toString() {
    return 'CommandeDetails[idCommande=$idCommande, idDetailComm=$idDetailComm, idProduit=$idProduit, isAnnule=$isAnnule, prix=$prix, quantite=$quantite, ]';
  }

  CommandeDetailsTransaction.fromJson(Map<String, dynamic> json) {

    produit =
        json['produit'] != null ? new Produit.fromJson(json['produit']) : null;
    idCommande = json['idCommande'];
    ingredient = json['ingredient'];
    idDetailComm = json['idDetailComm'];
    idProduit = json['idProduit'];
    isAnnule = json['isAnnule'];
    prix = json['prix'];
    quantite = json['quantite'];
    designation = json['designation'];
    remise = json['remise'];
    prixSansRemise = json['prixSansRemise'];
    employe = json['employe'] != null
        ? new Utilisateur.fromJson(json['employe'])
        : null;
  }

  Map<String, dynamic> toJson() {
    return {
      'idCommande': idCommande,
      'idDetailComm': idDetailComm,
      'idProduit': idProduit,
      'isAnnule': isAnnule,
      'prix': prix,
      'prixSansRemise': prixSansRemise,
      'quantite': quantite,
    };
  }

  static List<CommandeDetailsTransaction> listFromJson(List<dynamic> json) {
    return json == null
        // ignore: deprecated_member_use
        ? new List<CommandeDetailsTransaction>()
        : json
            .map((value) => new CommandeDetailsTransaction.fromJson(value))
            .toList();
  }

  static Map<String, CommandeDetailsTransaction> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CommandeDetailsTransaction>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new CommandeDetailsTransaction.fromJson(value));
    }
    return map;
  }
}
