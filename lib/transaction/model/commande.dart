import 'client.dart';

class CommandeTransaction {
  List<ClientTransaction> clients = [];

  DateTime dateCreation = null;

  DateTime dateGeneration = null;

  int fPaye = null;

  String idCommande = null;

  String idPointVente = null;

  String idSession = null;

  int isForClient = null;

  double montant = null;

  int numCommande = null;

  double reste = null;

  String idTable;
  int numTable;

  String typeCommande;

  String idMessageCommande;

  String statut;

  String adresse;
  String idLivreur;
  String nomPrenomLivreur;
  CommandeTransaction();

  @override
  String toString() {
    return 'CommandeTransaction[clients=$clients, dateCreation=$dateCreation, dateGeneration=$dateGeneration, fPaye=$fPaye, idCommande=$idCommande, idPointVente=$idPointVente, idSession=$idSession, isForClient=$isForClient, montant=$montant, numCommande=$numCommande, reste=$reste, adresse=$adresse]';
  }

  CommandeTransaction.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    clients = ClientTransaction.listFromJson(json['clients']);
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    dateGeneration = json['dateGeneration'] == null
        ? null
        : DateTime.parse(json['dateGeneration']);
    fPaye = json['fPaye'];
    typeCommande = json['typeCommande'];
    idMessageCommande = json['idMessageCommande'];
    idCommande = json['idCommande'];
    idPointVente = json['idPointVente'];
    idSession = json['idSession'];
    isForClient = json['isForClient'];
    montant = json['montant'];
    numCommande = json['numCommande'];
    reste = json['reste'];
    idTable = json['idTable'];
    numTable = json['numTable'];
    statut = json['statut'];
    adresse = json['adresse'];
    idLivreur = json['idLivreur'];
    nomPrenomLivreur = json['nomPrenomLivreur'];
  }

  Map<String, dynamic> toJson() {
    return {
      'clients': clients,
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'dateGeneration':
          dateGeneration == null ? '' : dateGeneration.toIso8601String(),
      'fPaye': fPaye,
      'idCommande': idCommande,
      'idPointVente': idPointVente,
      'idSession': idSession,
      'isForClient': isForClient,
      'montant': montant,
      'numCommande': numCommande,
      'reste': reste,
      'idTable': idTable,
      'numTable': numTable,
      'typeCommande': typeCommande,
      'idMessageCommande': idMessageCommande,
      'statut' : statut,
      'adresse' : adresse,
      'idLivreur':idLivreur,
     'nomPrenomLivreur':nomPrenomLivreur
    };
  }

  static List<CommandeTransaction> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<CommandeTransaction>()
        : json.map((value) => new CommandeTransaction.fromJson(value)).toList();
  }

  static Map<String, CommandeTransaction> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CommandeTransaction>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new CommandeTransaction.fromJson(value));
    }
    return map;
  }
}
