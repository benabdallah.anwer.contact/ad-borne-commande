import 'package:ad_caisse/dto/DataDto.dart';
import 'package:flutter/material.dart';

import 'commande_details.dart';

class CommandeDetailsDto {

  String nomProduit;
  CommandeDetailsTransaction commandeDetails;

  CommandeDetailsDto();

   CommandeDetailsDto.fromJson(Map<String, dynamic> json) {
    nomProduit = json['nomProduit'];
    commandeDetails =
        CommandeDetailsTransaction.fromJson(json["commandeDetails"]!=null?json["commandeDetails"]:json);

  }
}
