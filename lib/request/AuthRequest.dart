import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad_caisse/admin/api/utilisateur_end_point_api.dart';
import 'package:ad_caisse/admin/api/utilisateur_type_end_point_api.dart';
import 'package:ad_caisse/admin/model/login_dto.dart';
import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/admin/model/utilisateur_type.dart';
import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/dto/UpdateDto.dart';
import 'package:ad_caisse/pos/api/caisse_type_end_point_api.dart';
import 'package:ad_caisse/pos/api/point_vente_end_point_api.dart';
import 'package:ad_caisse/pos/model/caisse.dart';
import 'package:ad_caisse/pos/model/caisse_type.dart';
import 'package:ad_caisse/pos/model/journee.dart';
import 'package:ad_caisse/pos/model/point_vente.dart';
import 'package:ad_caisse/pos/model/session.dart';
import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens//Dashboard.dart';
import 'package:ad_caisse/screens/CommandesCuisine.dart';
import 'package:ad_caisse/screens/OuvertureJournee.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:serial_number/serial_number.dart';

import 'ResponseList.dart';
import 'ResponseSingle.dart';

class AuthRequest {
  var serveur = ServerUrl.url;

  var pointVenteEndPointApi = PointVenteEndPointApi();
  var caisseTypeEndPointApi = CaisseTypeEndPointApi();

  Future<List<Session>> getConnectedSessions(BuildContext context) async {
    try {
      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        return [];
      }
      String ref = await getRef();
      List<Session> list = await reqConnectedSessions(context, ref);
      if (list != null) {
        return list;
      } else {
        // if (kReleaseMode) {
        //   ref = "adlac";
        // } else {
        //   // ref = "1008cc07";
        //   ref = "B595079C1CD300696681";
        // }
        list = await reqConnectedSessions(context, ref);
        return list != null ? list : [];
      }
    } catch (err) {
      debugPrint('getConnectedSessions ' + err.toString());
      Crashlytics()
          .sendEmail("AuthRequest getConnectedSessions", err.toString());
      return [];
    }
  }

  Future<List<Session>> reqConnectedSessions(
      BuildContext context, String ref) async {
    try {
      List<Session> sessions;

      var url =
          serveur + "/bp-api-pos/v1/InitialeListSessionActif/" + ref.trim();

      var urlSynch = ServerUrl.urlSynchro +
          "/bp-api-pos/v1/InitialeListSessionActif/" +
          ref.trim();

      final response = await http.get(Uri.parse(url)).timeout(
        Duration(seconds: 3),
        onTimeout: () {
          // time has run out, do what you wanted to do
          return null;
        },
      );
      if (response != null && response.statusCode == 200) {
        ResponseList responseList =
            ResponseList.fromJson(json.decode(response.body));
        if (responseList.result == 1 && responseList.objectResponse != null) {
          sessions = Session.listFromJson(responseList.objectResponse);
        } else {
          return null;
        }
      } else {
        return null;
      }
      return sessions;
    } catch (err) {
      debugPrint('reqConnectedSessions ' + err.toString());
      Crashlytics()
          .sendEmail("AuthRequest getConnectedSessions", err.toString());
      return [];
    }
  }

  Future<Journee> getJournee(BuildContext context) async {
    bool isConnected = await Utils().checkInternet();
    if (!isConnected) {
      return null;
    }
    String ref = await getRef();
    Journee journeeSn = await journeeRequest(context, ref);
    if (journeeSn != null) {
      return journeeSn;
    } else {
      // if (kReleaseMode) {
      //   ref = "adlac";
      // } else {
      //   //ref = "1008cc07";
      //   ref = "B595079C1CD300696681";
      // }
      journeeSn = await journeeRequest(context, ref);
      return journeeSn;
    }
  }

  Future<Journee> journeeRequest(BuildContext context, String ref) async {
    try {
      var url =
          serveur + "/bp-api-pos/v1/InitialeCheckIfDayIsOpen/" + ref.trim();
      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        url = ServerUrl.urlSynchro +
            "/bp-api-pos/v1/InitialeCheckIfDayIsOpen/" +
            ref.trim();
      }
      debugPrint('Current Server $url');
      final response = await http.get(Uri.parse(url)).timeout(
        Duration(seconds: 3),
        onTimeout: () {
          // time has run out, do what you wanted to do
          return null;
        },
      );
      if (response != null && response.statusCode == 200) {
        ResponseSingle responseList =
            ResponseSingle.fromJson(json.decode(response.body));
        if (responseList.result == 1 && responseList.objectResponse != null) {
          return Journee.fromJson(responseList.objectResponse);
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (err) {
      debugPrint('journeeRequest ' + err.toString());
      Crashlytics().sendEmail("AuthRequest journeeRequest", err.toString());
      return null;
    }
  }

  Future<String> getRef() async {
    try {
      if (Platform.isAndroid) {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        return androidInfo.androidId;
      }

      if (Platform.isIOS) {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        IosDeviceInfo info = await deviceInfo.iosInfo;
        return info.identifierForVendor;
      }
      if (Platform.isWindows) {
        return await SerialNumber.firstHddSerialNumber;
      }
      return '';
    } catch (err) {
      return '';
    }
  }

  Future<bool> fetchLogin(
      BuildContext context, String login, String password) async {
    try {
      UtilisateurEndPointApi api = UtilisateurEndPointApi();
      if (login.isEmpty || password.isEmpty) {
        AlertNotif().alert(
            context, "Veuillez saisir votre identifiant et mot de passe");
        return false;
      }
      String ref;


      switch (ref) {
        case "1010":
          ref = "admarsa";
          break;
        case "0000":
          ref = "admarsa";
          break;
        case "9696":
          ref = "admarsa";
          break;
        case "5555":
          ref = "adlac";
          break;
        case "4554":
          ref = "adlac";
          break;
        case "1212":
          ref = "adlac";
          break;
        case "Mehdi1":
          {
            ref = "F5430797168703499845";
          }
          break;

        case "zied":
          {
            ref = "9C0D0797160E03504161";
          }
          break;


        default:
          {
            ref = "adlac";
          }
          break;
      }


    // ref = await getRef();
      LoginDto dto = LoginDto();
      dto.login = login;
      dto.password = password;
      dto.refrance = ref.trim();
      dto.typeconnection = "1";
      ResponseSingle res = await api.loginUsingPOST(dto);

      if (res.result == 1 && res.objectResponse != null) {
        var user = Utilisateur.fromJson(res.objectResponse);
        UtilisateurType type;
        UtilisateurTypeEndPointApi utilisateurTypeEndPointApi =
            UtilisateurTypeEndPointApi();
        ResponseSingle responseSingle = await utilisateurTypeEndPointApi
            .findByIdTypeUtilisateurUsingGET(user.idTypeUtilisateur);
        debugPrint(responseSingle.objectResponse.toString());
        if (responseSingle.result == 1) {
          type = UtilisateurType.fromJson(responseSingle.objectResponse);
        }
        return findByreference(context, user, type);
      } else {
        AlertNotif().alert(context, res.errorDescription);

        if (res.result == -10) {
          Crashlytics().sendEmail("Tentative de connexion suspecte",
              "Login: " + login + " SN: " + ref.trim());
        }
        return false;
      }
    } catch (err) {
      Crashlytics().sendEmail("AuthRequest fetchLogin", err.toString());
      AlertNotif().alert(context,
          'Une erreur s\'est produite, veuillez réessayer ultérieurement');
      return false;
    }
  }

  Future<UpdateDto> getLastUpdate(String version, String os) async {
    try {
      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        return null;
      }

      if (version.isEmpty ||
          os.isEmpty ||
          ServerUrl.url.isEmpty ||
          ServerUrl.url.contains("bpapi")) {
        return null;
      }
      var url =
          'https://dev.twinsdigitallabs.tech/update/v1/update?app_version='+
              version +
              '&os=windows';
      final response = await http
          .post(Uri.parse(url), headers: {"Content-Type": "application/json"}, body: null)
          .timeout(
        Duration(seconds: 3),
        onTimeout: () {
          // time has run out, do what you wanted to do
          return null;
        },
      );
      if (response == null) {
        // AlertNotif().alert(context, "Désolé, une erreur est survenue.");
        return null;
      }
      if (response != null && response.statusCode == 200) {
        var res = UpdateDto.fromJson(json.decode(response.body));
        return res;
      } else {
        //  AlertNotif().alert(context, "Désolé, une erreur est survenue.");
        return null;
      }
    } catch (error) {
      // AlertNotif().alert(context, error.toString());
      Crashlytics().sendEmail("AuthRequest getLastUpdate", error.toString());
      return null;
    }
  }

  Future<bool> checkIfSessionIsOpen(
      BuildContext context,
      Journee journee,
      Utilisateur utilisateur,
      Caisse caisse,
      PointVente pointVente,
      UtilisateurType type) async {
    try {
      var url = serveur +
          "/bp-api-pos/v1/checkIfSessionIsOpen/" +
          journee.idJournee +
          "/" +
          utilisateur.idUtilisateur +
          "/" +
          caisse.idCaisse;
      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        url = ServerUrl.urlSynchro +
            "/bp-api-pos/v1/checkIfSessionIsOpen/" +
            journee.idJournee +
            "/" +
            utilisateur.idUtilisateur +
            "/" +
            caisse.idCaisse;
      }
      debugPrint('Current Server $url');
      final response = await http.get(Uri.parse(url));
      if (response != null && response.statusCode == 200) {
        var res = ResponseSingle.fromJson(json.decode(response.body));
        if (res.result == 1 && res.objectResponse != null) {
          var session = Session.fromJson(res.objectResponse);

          Function function;
          DataDto dataDto = DataDto(utilisateur, caisse, journee, session,
              function, function, function, [], []);
          dataDto.utilisateurType = type;
          dataDto.pointVente = pointVente;
          dataDto.listClient = [];
          dataDto.listClientFiltred = [];
          CaisseType caisseType;
          ResponseSingle responseSingleCT= await caisseTypeEndPointApi.findByIdCaisseTypeUsingGET(caisse.idTypeCaisse);
          if(responseSingleCT.result== 1){
            caisseType=CaisseType.fromJson(responseSingleCT.objectResponse);
             dataDto.caisseType=caisseType;
          }
          if (type.code.toLowerCase().contains('cuisine')) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => CommandesCuisine(dataDto)),
            );
            return true;
          }

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => Dashboard(dataDto)),
          );
          return true;
        } else {
          Function function;
          DataDto dataDto = DataDto(utilisateur, caisse, journee, null,
              function, function, function, [], []);
          dataDto.pointVente = pointVente;
          dataDto.utilisateurType = type;
          dataDto.listClient = [];
          dataDto.listClientFiltred = [];
          CaisseType caisseType;
          ResponseSingle responseSingleCT= await caisseTypeEndPointApi.findByIdCaisseTypeUsingGET(caisse.idTypeCaisse);
          if(responseSingleCT.result== 1){
            caisseType=CaisseType.fromJson(responseSingleCT.objectResponse);
            dataDto.caisseType=caisseType;
          }
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => OuvertureJournee(dataDto)),
          );
          return false;
        }
      } else {
        AlertNotif().alert(context,
            "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
        return false;
      }
    } catch (error) {
      debugPrint("checkIfSessionIsOpen Error: " + error.toString());
      Crashlytics()
          .sendEmail("AuthRequest checkIfSessionIsOpen", error.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      return false;
    }
  }

  Future<bool> checkIfDayIsOpen(BuildContext context, Caisse caisse,
      Utilisateur user, PointVente pointVente, UtilisateurType type) async {
    try {
      var url =
          serveur + "/bp-api-pos/v1/CheckIfDayIsOpen/" + caisse.idPointVente;
      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        url = ServerUrl.urlSynchro +
            "/bp-api-pos/v1/CheckIfDayIsOpen/" +
            caisse.idPointVente;
      }
      debugPrint('Current Server $url');
      final response = await http.get(Uri.parse(url));
      if (response != null && response.statusCode == 200) {
        var res = ResponseSingle.fromJson(json.decode(response.body));
        if (res.result == 1 && res.objectResponse != null) {
          var journee = Journee.fromJson(res.objectResponse);
          return checkIfSessionIsOpen(
              context, journee, user, caisse, pointVente, type);
        } else {
          if ((res.result == -4 && type != null && type.fGerant != 1)||(res.objectResponse == null&& type.fGerant != 1)) {
            AlertNotif().alert(context,
                "Aucune journée n'est ouverte Veuillez contacter le gérant.");
            return false;
          }
          Function function;

          DataDto dataDto = DataDto(
              user, caisse, null, null, function, function, function, [], []);
          dataDto.utilisateurType = type;
          dataDto.listClient = [];
          dataDto.listClientFiltred = [];
          CaisseType caisseType;
          ResponseSingle responseSingleCT= await caisseTypeEndPointApi.findByIdCaisseTypeUsingGET(caisse.idTypeCaisse);
          if(responseSingleCT.result== 1){
            caisseType=CaisseType.fromJson(responseSingleCT.objectResponse);
            dataDto.caisseType=caisseType;
          }
          dataDto.pointVente = pointVente;
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => OuvertureJournee(dataDto)),
          );
          return true;
        }
      } else {
        AlertNotif().alert(context,
            "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
        return false;
      }
    } catch (error) {
      Crashlytics().sendEmail("AuthRequest checkIfDayIsOpen", error.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      return false;
    }
  }

  Future<bool> findByreference(
      BuildContext context, Utilisateur user, UtilisateurType type) async {
    try {
      String ref = "";
 switch (user.login) {
        case "1010":
          ref = "admarsa";
          break;
   case "0000":
     ref = "admarsa";
     break;
   case "9696":
     ref = "admarsa";
     break;
        case "5555":
          ref = "adlac";
          break;
        case "4554":
          ref = "adlac";
          break;
        case "1212":
          ref = "adlac";
          break;
      case "Mehdi1":
        {
          ref = "F5430797168703499845";
        }
        break;

     case "zied":
       {
         ref = "9C0D0797160E03504161";
       }
       break;


     default:
       {
         ref = "adlac";
       }
       break;
    }
/*    ref = await getRef();
        Crashlytics().sendEmail("Serial Number " + ref.trim(), " refrenece");*/
        var url = serveur + "/bp-api-pos/v1/findByreference/" + ref.trim();

      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        url = ServerUrl.urlSynchro +
            "/bp-api-pos/v1/findByreference/" +
            ref.trim();
      }
      debugPrint('Current Server $url');
      final response = await http.get(Uri.parse(url)).timeout(
        Duration(seconds: 3),
        onTimeout: () {
          // time has run out, do what you wanted to do
          return null;
        },
      );

      if (response == null) {
        AlertNotif().alert(context,
            "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
        return false;
      }
      if (response.statusCode == 200) {
        var res = ResponseSingle.fromJson(json.decode(response.body));
        if (res.result == 1 && res.objectResponse != null) {
          var caisse = Caisse.fromJson(res.objectResponse);
          ResponseSingle responseSingle = await pointVenteEndPointApi
              .findByIdPointVenteUsingGET(caisse.idPointVente);
          PointVente pointVente = PointVente();


          if (responseSingle.result == 1) {
            pointVente = PointVente.fromJson(responseSingle.objectResponse);
            debugPrint('Point De Vente: ' + pointVente.idPointVente);
          }
          return checkIfDayIsOpen(context, caisse, user, pointVente, type);
        } else {
          debugPrint(res.errorDescription);
          Crashlytics()
              .sendEmail("Serial Number " + ref.trim(), res.errorDescription);
          AlertNotif().alert(context, res.errorDescription);
          return false;
        }
      } else {
        AlertNotif().alert(context,
            "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
        return false;
      }
    } catch (error) {
      debugPrint("findByreference Error: " + error.toString());
      Crashlytics().sendEmail("AuthRequest findByreference", error.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      return false;
    }
  }
}
