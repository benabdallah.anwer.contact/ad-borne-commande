import 'package:ad_caisse/pos/model/caisse.dart';
import 'package:ad_caisse/product/api/categorie_end_point_api.dart';
import 'package:ad_caisse/product/model/categorie_article.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:flutter/material.dart';

import 'Crashlytics.dart';

class CategoryList {
  final api_category = new CategorieEndPointApi();

  Future<List<CategorieArticle>> getCategoryList(Caisse caisse) async {
    try {
      ResponseList list = await api_category
          .findAllCategorieArticleByIdPVUsingGET(caisse.idPointVente);
      List<CategorieArticle> listCat =
          list != null && list.objectResponse != null
              ? list.objectResponse
                  .map((i) => CategorieArticle.fromJson(i))
                  .toList()
              : [];
      return listCat;
    } catch (error) {
      debugPrint('$error');
      Crashlytics().sendEmail("CategoryList getCategoryList", error.toString());
      return [];
    }
  }
}
