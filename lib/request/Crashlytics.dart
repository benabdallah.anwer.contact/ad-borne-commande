import 'dart:io';

import 'package:ad_caisse/request/Utils.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:serial_number/serial_number.dart';

class Crashlytics {
  Future<bool> sendEmail(String classs, String error) async {
    try {
      bool isConnected = await Utils().checkInternet();
      if (!isConnected) {
        return false;
      }
      String ref = await getRef();

      String username = 'adcaissebugs22@gmail.com';
      String password = 'TDLadcaisse';

/*      final smtpServer = SmtpServer("ssl0.ovh.net",
          username: username, password: password, ssl: true, port: 465);*/
            // ignore: deprecated_member_use
            final smtpServer = gmail(username,  password);
      final message = Message()
        ..from = Address(username, 'BPrice Crashlytics')
        ..recipients.add('adcaissebugs@gmail.com')
        ..ccRecipients.addAll(
            ['adcaissebugs@gmail.com','anwerxxy@gmail.com'])
        ..subject =
            'Crash BPrice : ${DateFormat("dd-MM-yyyy HH:mm:ss").format(DateTime.now())}'
        ..text = ref + '\n' + classs + ' : ' + '\n' + error;
      bool ok;
      try {
        await send(message, smtpServer);
        ok = true;
      } on MailerException catch (e) {
        debugPrint('Message not sent. ' + e.message);
        ok = false;
      }
      return ok;
    } catch (err) {
      debugPrint(err.toString());
    }
  }

  Future<String> getRef() async {
    try {
      if (Platform.isAndroid) {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        return androidInfo.androidId;
      }

      if (Platform.isIOS) {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        IosDeviceInfo info = await deviceInfo.iosInfo;
        return info.identifierForVendor;
      }
      if (Platform.isWindows) {
        return await SerialNumber.firstHddSerialNumber;
      }
      return '';
    } catch (err) {
      return '';
    }
  }
}
