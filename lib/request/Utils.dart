import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/ordering/model/commande.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/pos/api/caisse_end_point_api.dart';
import 'package:ad_caisse/pos/model/ClotureJourneeRes.dart';
import 'package:ad_caisse/pos/model/PreColture.dart';
import 'package:ad_caisse/pos/model/FamilleDetailsCommande.dart';
import 'package:ad_caisse/pos/model/CatgorieDetailsCommandeDTO.dart';
import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/transaction/model/CommandeDetailsDto.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:ad_caisse/utils/PrintingTicket.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:pdf/widgets.dart' as pw;

import 'Crashlytics.dart';

class Utils {
  var caisseEndPointApi = CaisseEndPointApi();
  Future<bool> checkInternet() async {
    try {
      if (ServerUrl.urlSynchro == null) {
        debugPrint('checkInternet urlSynchro null');
        DataDto.isConnected = true;
        return true;
      }

      if (ServerUrl.urlSynchro.isEmpty) {
        debugPrint('checkInternet urlSynchro isEmpty');
        DataDto.isConnected = true;
        return true;
      }

      if (!ServerUrl.urlSynchro.contains('localhost')) {
        debugPrint('checkInternet urlSynchro not correct');
        DataDto.isConnected = true;
        return true;
      }

      if (kIsWeb) {
        debugPrint('checkInternet web');
        DataDto.isConnected = true;
        return true;
      }
      final response = await http.get(Uri.parse(ServerUrl.url)).timeout(
        Duration(seconds: 5),
        onTimeout: () {
          return null;
        },
      );
      if (response == null) {
        debugPrint('checkInternet false : response null');
        DataDto.isConnected = false;
        return false;
      }
      if (response.statusCode != 200) {
        debugPrint(
            'checkInternet false & status: ' + response.statusCode.toString());
        DataDto.isConnected = false;
        return false;
      }
      if (!DataDto.isConnected) {
        await http
            .get(
            Uri.parse('http://localhost:8080/bp-api-synchronise/bp-api-synchro/v1/fromLocalToRemote'))
            .timeout(
          Duration(seconds: 3),
          onTimeout: () {
            return null;
          },
        );
      }
      debugPrint('checkInternet true Without synchronise');
      DataDto.isConnected = true;
      return true;
    } catch (error) {
      debugPrint('checkInternet false: $error');
      DataDto.isConnected = false;
      return false;
    }
  }

  static Future<bool> checkInternetInterval() async {
    try {
      final response = await http.get(Uri.parse(ServerUrl.url)).timeout(
        Duration(seconds: 1),
        onTimeout: () {
          return null;
        },
      );
      if (response == null) {
        return false;
      }
      if (response.statusCode != 200) {
        return false;
      }
      return true;
    } catch (error) {
      debugPrint('checkInternet false: $error');
      return false;
    }
  }

  Future<bool> printService(BuildContext context,
      CommandesDtoTransaction commandeDto, DataDto dataDto) async {

    //await showInSplitScreen( commandeDto) ;

    LocalStorageInterface prefs = await LocalStorage.getInstance();
    String printerip = prefs.getString('principalPrinter');
    bool notif = prefs.getBool('notif') ?? false;
    bool isImpIng = prefs.getBool('imp_Ing') ?? false;
    String formatPapierSaved = prefs.getString('formatPapier');
    int numTicket = prefs.getInt('numTicket');
    String marginSaved = prefs.getString('margin_printer');
    int margin = marginSaved != null ? int.parse(marginSaved) : 0;
    String fontSizeSaved = prefs.getString('fontSize');
    double fontSize = fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
    String formatPapier = formatPapierSaved != null ? formatPapierSaved : '80';
    String codeW = prefs.getString('codeW')??'';
    bool numTicketFromLocal = prefs.getBool("num_ticket_from_local") ?? false ;
    bool isDesImpTicket = prefs.getBool("des_imp_ticket")?? false ;

    try {
      var dataBold = await rootBundle.load("assets/Montserrat-Bold.ttf");
      var bold = Font.ttf(dataBold);
      var dataMedium = await rootBundle.load("assets/Montserrat-Medium.ttf");
      var medium = Font.ttf(dataMedium);
      var dataRegular = await rootBundle.load("assets/Montserrat-Regular.ttf");
      var regular = Font.ttf(dataRegular);
     if (!kIsWeb && (printerip == null || printerip == "" || isDesImpTicket==true )) {
        return null;
      }
      final pdf = pw.Document();
      List<List<dynamic>> list = [];
      for (CommandeDetailsDto element in commandeDto.commandeDetails) {
        String s = element.commandeDetails.quantite.toString().endsWith('.0')
            ? element.commandeDetails.quantite.ceil().toString()
            : element.commandeDetails.quantite.toString();
        list.add([
          s,
          element.commandeDetails.designation,
          (element.commandeDetails.quantite * element.commandeDetails.prix)
              .toStringAsFixed(3)
        ]);
      }
      if ((commandeDto.commande.clients != null &&
          commandeDto.commande.clients.length > 0) || (dataDto.commande!=null&&dataDto.commande.clients!=null&&dataDto.commande.clients.length > 0) ) {
        pdf.addPage(pw.Page(
            margin: pw.EdgeInsets.all(margin.ceilToDouble()),
            pageFormat: formatPapier.contains('80')
                ? PdfPageFormat.roll80
                : PdfPageFormat.roll57,
            build: (pw.Context context) {
              return pw.Column(children: [
                pw.Center(
                  child: pw.Column(children: [
                    pw.Text(dataDto.pointVente.designation,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.Text(dataDto.pointVente.adresse,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.SizedBox(height: 4),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Text((numTicketFromLocal==true&&numTicket!=null)?'N° '+numTicket.toString():'N° '+(dataDto.commande!=null? dataDto.commande.numCommande.toString():commandeDto.commande.numCommande.toString()),
                        style: pw.TextStyle(
                            font: regular, fontSize: fontSize + 2)),
                    dataDto.commande!=null&&dataDto.commande.numTable!=null?
                    pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur ' +(dataDto.commande.numTable.toString()):'Table ' + (dataDto.commande.numTable.toString()),
                        style: pw.TextStyle(
                            font: regular, fontSize: fontSize + 2))  : pw.SizedBox(),
                    pw.SizedBox(height: 4),
                    pw.Text(
                        "Date : " +
                            DateFormat("dd-MM-yyyy HH:mm")
                                .format(DateTime.now()),
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
/*                    pw.Text(
                        "Caissier : " + dataDto.utilisateur.nom+" "+dataDto.utilisateur.prenom,
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),*/
                  ]),
                ),
                pw.SizedBox(height: 12),
                for (CommandeDetailsDto element in commandeDto.commandeDetails)
                  pw.Column(children: [
                    pw.Row(

                        children: [
                          pw.Text(
                              element.commandeDetails.quantite
                                  .toString()
                                  .endsWith('.0')
                                  ? element.commandeDetails.quantite
                                  .ceil()
                                  .toString() +
                                  " " +
                                  (   element.commandeDetails.designation.length<30?element.commandeDetails.designation: element.commandeDetails.designation.replaceAllMapped(RegExp(r".{30}"),
                                          (match) => "${match.group(0)}\n") +"        " +(element.commandeDetails.quantite *
                                      element.commandeDetails.prix)
                                      .toStringAsFixed(3))
                                  : element.commandeDetails.quantite.toString() +
                                  " " +
                                  element.commandeDetails.designation.replaceAllMapped(RegExp(r".{10}"),
                                          (match) => "${match.group(0)}\n"),
                              style: pw.TextStyle(font: regular, fontSize: fontSize)),
                          if( element.commandeDetails.designation.length<30)
                            pw.Expanded(
                                child: pw.Container(
                                  alignment: pw.Alignment.centerRight,
                                  child: pw.Text(
                                      (element.commandeDetails.quantite *
                                          element.commandeDetails.prix)
                                          .toStringAsFixed(3),
                                      style:
                                      pw.TextStyle(font: medium, fontSize: fontSize)),
                                ))
                        ]),
                    pw.Row(

                        children: [
                          pw.Expanded(
                              child: pw.Container(
                                alignment: pw.Alignment.centerRight,
                                child: pw.Text(
                                    ( element.commandeDetails.remise!=null&& element.commandeDetails.remise!=0?"P.init "+
                                        element.commandeDetails.prixSansRemise.toStringAsFixed(3)+' ---------- Remise '+element.commandeDetails.remise.toString()
                                        +'%':""),
                                    style:
                                    pw.TextStyle(font: medium, fontSize: fontSize-2)),
                              ))


                        ]
                    ),
                  if (isImpIng==true && isImpIng != null)
                    element.commandeDetails.ingredient!=null?   pw.Row(children: [
                      pw.Text(element.commandeDetails.ingredient.replaceAllMapped(
                          RegExp(r".{30}"),
                              (match) => "${match.group(0)}\n"),
                          textAlign: pw.TextAlign.left,
                          style: pw.TextStyle(
                              fontWeight: FontWeight.normal,
                              // color: PdfColors.grey800,
                              fontSize: fontSize/1.5)),
                    ]):pw.SizedBox(),
                  ]),

                pw.SizedBox(height: 8),
                pw.Row(children: [
                  pw.Text("",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Expanded(
                      child: pw.Container(
                          padding: pw.EdgeInsets.only(top: 4, bottom: 4),
                          alignment: pw.Alignment.centerLeft,
                          child: pw.Column(children: [
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.Text(
                                "TOTAL " +
                                    commandeDto.commande.montant
                                        .toStringAsFixed(3) +
                                    ' DT',
                                style: pw.TextStyle(
                                    font: bold, fontSize: fontSize + 2)),
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.SizedBox(height: 5),
                            pw.Text(
                                ( dataDto.commande!=null&& dataDto.commande.clients!=null ?  dataDto.commande.clients[0].prenom +
                                    " " +
                                    dataDto.commande.clients[0].nom : commandeDto.commande.clients[0].prenom +
                                    " " +
                                    commandeDto.commande.clients[0].nom),
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.Text(
                                "N° Tel : " +
                                    (  dataDto.commande!=null&& dataDto.commande.clients!=null?dataDto.commande.clients[0].nTel:commandeDto.commande.clients[0].nTel),
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                          ])))
                ]),
                pw.Row(children: [
                  pw.Text( (dataDto.commande!=null?(
                  dataDto.commande.adresse != null
                      ? "@ Livraison : \n " +
                      dataDto.commande.adresse.replaceAllMapped(
                          RegExp(r".{21}"),
                              (match) => "${match.group(0)}\n")
                      : dataDto.commande.clients[0].adress != null
                      ? "@ Livraison : \n " +
                      dataDto.commande.clients[0].adress
                          .replaceAllMapped(RegExp(r".{21}"),
                              (match) => "${match.group(0)}\n")
                      : " "):(
                      commandeDto.commande.adresse != null
                          ? "@ Livraison : \n " +
                          commandeDto.commande.adresse.replaceAllMapped(
                              RegExp(r".{21}"),
                                  (match) => "${match.group(0)}\n")
                          : commandeDto.commande.clients[0].adress != null
                          ? "@ Livraison : \n " +
                          commandeDto.commande.clients[0].adress
                              .replaceAllMapped(RegExp(r".{21}"),
                                  (match) => "${match.group(0)}\n")
                          : " ")),
                      style: pw.TextStyle(font: regular, fontSize: fontSize))
                ]),
                pw.SizedBox(height: 12),codeW!=null&&codeW!=""?
              pw.Center(
              child: pw.Column(children: [
                pw.Text("Wifi :"+codeW,
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
              pw.Text("******************************",
              style: pw.TextStyle(font: regular, fontSize: fontSize)),
              pw.Text("Merci De Votre Visite,",
              style: pw.TextStyle(font: regular, fontSize: fontSize)),
              pw.Text("À bientôt !",
              style: pw.TextStyle(font: regular, fontSize: fontSize)),
              pw.SizedBox(height: 4),
              pw.Text("******************************",
              style: pw.TextStyle(font: regular, fontSize: fontSize)),
              ]))
                    :
                pw.Center(
                    child: pw.Column(children: [
                  pw.Text("******************************",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Text("Merci De Votre Visite,",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Text("À bientôt !",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text("******************************",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                ])),
              ]); // Center
            }));
      } else {
        pdf.addPage(pw.Page(
            margin: pw.EdgeInsets.all(margin.ceilToDouble()),
            pageFormat: formatPapier.contains('80')
                ? PdfPageFormat.roll80
                : PdfPageFormat.roll57,
            build: (pw.Context context) {
              return pw.Column(children: [
                pw.Center(
                  child: pw.Column(children: [
                    pw.Text(dataDto.pointVente.designation,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.Text(dataDto.pointVente.adresse,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.SizedBox(height: 4),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                   pw.Text((numTicketFromLocal==true&&numTicket!=null)?'N° '+numTicket.toString():'N° '+(dataDto.commande!=null? dataDto.commande.numCommande.toString():commandeDto.commande.numCommande.toString()),
                       style: pw.TextStyle(
                           font: regular, fontSize: fontSize + 2)),

                    dataDto.tableCaisse!=null? pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur N°' +(dataDto.tableCaisse.numTable.toString()):'Table ' + (dataDto.tableCaisse.numTable.toString()),

                        style: pw.TextStyle(
                            font: regular, fontSize: fontSize + 2))  : pw.SizedBox(),
                    pw.SizedBox(height: 4),

                    pw.Text(
                        "Date : " +
                            DateFormat("dd-MM-yyyy HH:mm")
                                .format(DateTime.now()),
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
/*                    pw.Text(
                        "Caissier : " + dataDto.utilisateur.nom+" "+dataDto.utilisateur.prenom,
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),*/
                  ]),
                ),
                pw.SizedBox(height: 12),
                for (CommandeDetailsDto element in commandeDto.commandeDetails)
                  pw.Column(children: [
                  pw.Row(

                      children: [
                    pw.Text(
                        element.commandeDetails.quantite
                                .toString()
                                .endsWith('.0')
                            ? element.commandeDetails.quantite
                                    .ceil()
                                    .toString() +
                                " " +
              (   element.commandeDetails.designation.length<30?element.commandeDetails.designation: element.commandeDetails.designation.replaceAllMapped(RegExp(r".{30}"),
                      (match) => "${match.group(0)}\n") +"        " +(element.commandeDetails.quantite *
                  element.commandeDetails.prix)
                  .toStringAsFixed(3))
                            : element.commandeDetails.quantite.toString() +
                                " " +
                                element.commandeDetails.designation.replaceAllMapped(RegExp(r".{10}"),
                                        (match) => "${match.group(0)}\n"),
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
              if( element.commandeDetails.designation.length<30)
                    pw.Expanded(
                        child: pw.Container(
                      alignment: pw.Alignment.centerRight,
                      child: pw.Text(
                          (element.commandeDetails.quantite *
                                  element.commandeDetails.prix)
                              .toStringAsFixed(3),
                          style:
                              pw.TextStyle(font: medium, fontSize: fontSize)),
                    ))
                  ]),
              pw.Row(

              children: [
                pw.Expanded(
                    child: pw.Container(
                      alignment: pw.Alignment.centerRight,
                      child: pw.Text(
                          ( element.commandeDetails.remise!=null&& element.commandeDetails.remise!=0?"P.init "+
                              element.commandeDetails.prixSansRemise.toStringAsFixed(3)+' ---------- Remise '+element.commandeDetails.remise.toString()
                              +'%':""),
                          style:
                          pw.TextStyle(font: medium, fontSize: fontSize-2)),
                    ))


              ]
              ),
                    if (isImpIng==true && isImpIng != null)
                      element.commandeDetails.ingredient!=null?  pw.Row(children: [
                      pw.Text(element.commandeDetails.ingredient.replaceAllMapped(
                          RegExp(r".{30}"),
                              (match) => "${match.group(0)}\n"),
                          textAlign: pw.TextAlign.left,
                          style: pw.TextStyle(
                              fontWeight: FontWeight.normal,
                              // color: PdfColors.grey800,
                              fontSize:fontSize/1.5)),
                    ]):pw.SizedBox(),
                  ]),
                pw.SizedBox(height: 8),
                pw.Row(children: [
                  pw.Text("",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Expanded(
                      child: pw.Container(
                          padding: pw.EdgeInsets.only(top: 4, bottom: 4),
                          alignment: pw.Alignment.center,
                          child: pw.Column(children: [
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.Text(
                                "TOTAL " +
                                    commandeDto.commande.montant
                                        .toStringAsFixed(3) +
                                    ' DT',
                                style: pw.TextStyle(
                                    font: bold, fontSize: fontSize + 2)),
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.SizedBox(height: 5),
                          ])))
                ]),
                pw.SizedBox(height: 12),
                pw.SizedBox(height: 12),codeW!=null&&codeW!=""?
                pw.Center(
                    child: pw.Column(children: [
                      pw.Text("Wifi :"+codeW,
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("Merci De Votre Visite,",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("À bientôt !",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.SizedBox(height: 4),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    ]))
                    :
                pw.Center(
                    child: pw.Column(children: [
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("Merci De Votre Visite,",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("À bientôt !",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.SizedBox(height: 4),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    ])),
              ]); // Center
            }));
      }

      PrintingTicket().printTicket(pdf, printerip, context, notif);
    } catch (err) {
      Crashlytics().sendEmail("Utils printService", err.toString());
      if (notif) {
        AlertNotif().alert(
            context, "Une erreur est survenue lors de l'impression du ticket");
      }
      return false;
    }
    if (numTicketFromLocal){
      var x= numTicket+1;
      prefs.setInt("numTicket", x);
    }

    return true;
  }
  Future<bool> printService1(BuildContext context,
      CommandesDtoTransaction commandeDto, DataDto dataDto) async {

    LocalStorageInterface prefs = await LocalStorage.getInstance();
    String printerip = prefs.getString('principalPrinter');
    bool notif = prefs.getBool('notif');
    bool isImpIng = prefs.getBool('imp_Ing');
    String formatPapierSaved = prefs.getString('formatPapier');
    int numTicket = prefs.getInt('numTicket');
    String marginSaved = prefs.getString('margin_printer');
    int margin = marginSaved != null ? int.parse(marginSaved) : 0;
    String fontSizeSaved = prefs.getString('fontSize');
    double fontSize = fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
    String formatPapier = formatPapierSaved != null ? formatPapierSaved : '80';
    String codeW = prefs.getString('codeW');
    bool numTicketFromLocal = prefs.getBool("num_ticket_from_local");

    try {
      var dataBold = await rootBundle.load("assets/Montserrat-Bold.ttf");
      var bold = Font.ttf(dataBold);
      var dataMedium = await rootBundle.load("assets/Montserrat-Medium.ttf");
      var medium = Font.ttf(dataMedium);
      var dataRegular = await rootBundle.load("assets/Montserrat-Regular.ttf");
      var regular = Font.ttf(dataRegular);
      if (!kIsWeb && (printerip == null || printerip == "" )) {
        return null;
      }
      final pdf = pw.Document();
      List<List<dynamic>> list = [];
      for (CommandeDetailsDto element in commandeDto.commandeDetails) {
        String s = element.commandeDetails.quantite.toString().endsWith('.0')
            ? element.commandeDetails.quantite.ceil().toString()
            : element.commandeDetails.quantite.toString();
        list.add([
          s,
          element.commandeDetails.designation,
          (element.commandeDetails.quantite * element.commandeDetails.prix)
              .toStringAsFixed(3)
        ]);
      }
      if ((commandeDto.commande.clients != null &&
          commandeDto.commande.clients.length > 0) || (dataDto.commande!=null&&dataDto.commande.clients!=null&&dataDto.commande.clients.length > 0) ) {
        pdf.addPage(pw.Page(
            margin: pw.EdgeInsets.all(margin.ceilToDouble()),
            pageFormat: formatPapier.contains('80')
                ? PdfPageFormat.roll80
                : PdfPageFormat.roll57,
            build: (pw.Context context) {
              return pw.Column(children: [
                pw.Center(
                  child: pw.Column(children: [
                    pw.Text(dataDto.pointVente.designation,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.Text(dataDto.pointVente.adresse,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.SizedBox(height: 4),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text((numTicketFromLocal==true&&numTicket!=null)?'N° '+numTicket.toString():'N° '+(dataDto.commande!=null? dataDto.commande.numCommande.toString():commandeDto.commande.numCommande.toString()),
                        style: pw.TextStyle(
                            font: regular, fontSize: fontSize + 2)),
                    dataDto.commande!=null&&dataDto.commande.numTable!=null?
                    pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur ' +(dataDto.commande.numTable.toString()):'Table ' + (dataDto.commande.numTable.toString()),
                        style: pw.TextStyle(
                            font: regular, fontSize: fontSize + 2))  : pw.SizedBox(),
                    pw.SizedBox(height: 4),
                    pw.Text(
                        "Date : " +
                            DateFormat("dd-MM-yyyy HH:mm")
                                .format(DateTime.now()),
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
/*                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text(
                        "Caissier : " + dataDto.utilisateur.nom+" "+dataDto.utilisateur.prenom,
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),*/
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  ]),
                ),
                pw.SizedBox(height: 12),
                for (CommandeDetailsDto element in commandeDto.commandeDetails)
                  pw.Column(children: [
                    pw.Row(

                        children: [
                          pw.Text(
                              element.commandeDetails.quantite
                                  .toString()
                                  .endsWith('.0')
                                  ? element.commandeDetails.quantite
                                  .ceil()
                                  .toString() +
                                  " " +
                                  (   element.commandeDetails.designation.length<30?element.commandeDetails.designation: element.commandeDetails.designation.replaceAllMapped(RegExp(r".{30}"),
                                          (match) => "${match.group(0)}\n") +"        " +(element.commandeDetails.quantite *
                                      element.commandeDetails.prix)
                                      .toStringAsFixed(3))
                                  : element.commandeDetails.quantite.toString() +
                                  " " +
                                  element.commandeDetails.designation.replaceAllMapped(RegExp(r".{10}"),
                                          (match) => "${match.group(0)}\n"),
                              style: pw.TextStyle(font: regular, fontSize: fontSize)),
                          if( element.commandeDetails.designation.length<30)
                            pw.Expanded(
                                child: pw.Container(
                                  alignment: pw.Alignment.centerRight,
                                  child: pw.Text(
                                      (element.commandeDetails.quantite *
                                          element.commandeDetails.prix)
                                          .toStringAsFixed(3),
                                      style:
                                      pw.TextStyle(font: medium, fontSize: fontSize)),
                                ))
                        ]),
                    pw.Row(

                        children: [
                          pw.Expanded(
                              child: pw.Container(
                                alignment: pw.Alignment.centerRight,
                                child: pw.Text(
                                    ( element.commandeDetails.remise!=null&& element.commandeDetails.remise!=0?"P.init "+
                                        element.commandeDetails.prixSansRemise.toStringAsFixed(3)+' ---------- Remise '+element.commandeDetails.remise.toString()
                                        +'%':""),
                                    style:
                                    pw.TextStyle(font: medium, fontSize: fontSize-2)),
                              ))


                        ]
                    ),
                    if (isImpIng==true && isImpIng != null)
                      element.commandeDetails.ingredient!=null?   pw.Row(children: [
                        pw.Text(element.commandeDetails.ingredient.replaceAllMapped(
                            RegExp(r".{30}"),
                                (match) => "${match.group(0)}\n"),
                            textAlign: pw.TextAlign.left,
                            style: pw.TextStyle(
                                fontWeight: FontWeight.normal,
                                // color: PdfColors.grey800,
                                fontSize: fontSize/1.5)),
                      ]):pw.SizedBox(),
                  ]),

                pw.SizedBox(height: 8),
                pw.Row(children: [
                  pw.Text("",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Expanded(
                      child: pw.Container(
                          padding: pw.EdgeInsets.only(top: 4, bottom: 4),
                          alignment: pw.Alignment.centerLeft,
                          child: pw.Column(children: [
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.Text(
                                "TOTAL " +
                                    commandeDto.commande.montant
                                        .toStringAsFixed(3) +
                                    ' DT',
                                style: pw.TextStyle(
                                    font: bold, fontSize: fontSize + 2)),
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.SizedBox(height: 5),
                            pw.Text(
                                ( dataDto.commande!=null&& dataDto.commande.clients!=null ?  dataDto.commande.clients[0].prenom +
                                    " " +
                                    dataDto.commande.clients[0].nom : commandeDto.commande.clients[0].prenom +
                                    " " +
                                    commandeDto.commande.clients[0].nom),
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.Text(
                                "N° Tel : " +
                                    (  dataDto.commande!=null&& dataDto.commande.clients!=null?dataDto.commande.clients[0].nTel:commandeDto.commande.clients[0].nTel),
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                          ])))
                ]),
                pw.Row(children: [
                  pw.Text( (dataDto.commande!=null?(
                      dataDto.commande.adresse != null
                          ? "@ Livraison : \n " +
                          dataDto.commande.adresse.replaceAllMapped(
                              RegExp(r".{21}"),
                                  (match) => "${match.group(0)}\n")
                          : dataDto.commande.clients[0].adress != null
                          ? "@ Livraison : \n " +
                          dataDto.commande.clients[0].adress
                              .replaceAllMapped(RegExp(r".{21}"),
                                  (match) => "${match.group(0)}\n")
                          : " "):(
                      commandeDto.commande.adresse != null
                          ? "@ Livraison : \n " +
                          commandeDto.commande.adresse.replaceAllMapped(
                              RegExp(r".{21}"),
                                  (match) => "${match.group(0)}\n")
                          : commandeDto.commande.clients[0].adress != null
                          ? "@ Livraison : \n " +
                          commandeDto.commande.clients[0].adress
                              .replaceAllMapped(RegExp(r".{21}"),
                                  (match) => "${match.group(0)}\n")
                          : " ")),
                      style: pw.TextStyle(font: regular, fontSize: fontSize))
                ]),
                pw.SizedBox(height: 12),codeW!=null&&codeW!=""?
                pw.Center(
                    child: pw.Column(children: [
                      pw.Text("Wifi :"+codeW,
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("Merci De Votre Visite,",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("À bientôt !",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.SizedBox(height: 4),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    ]))
                    :
                pw.Center(
                    child: pw.Column(children: [
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("Merci De Votre Visite,",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("À bientôt !",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.SizedBox(height: 4),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    ])),
              ]); // Center
            }));
      } else {
        pdf.addPage(pw.Page(
            margin: pw.EdgeInsets.all(margin.ceilToDouble()),
            pageFormat: formatPapier.contains('80')
                ? PdfPageFormat.roll80
                : PdfPageFormat.roll57,
            build: (pw.Context context) {
              return pw.Column(children: [
                pw.Center(
                  child: pw.Column(children: [
                    pw.Text(dataDto.pointVente.designation,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.Text(dataDto.pointVente.adresse,
                        style: pw.TextStyle(font: bold, fontSize: fontSize)),
                    pw.SizedBox(height: 4),
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text((numTicketFromLocal==true&&numTicket!=null)?'N° '+numTicket.toString():'N° '+(dataDto.commande!=null? dataDto.commande.numCommande.toString():commandeDto.commande.numCommande.toString()),
                        style: pw.TextStyle(
                            font: regular, fontSize: fontSize + 2)),

                    dataDto.tableCaisse!=null? pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur N°' +(dataDto.tableCaisse.numTable.toString()):'Table ' + (dataDto.tableCaisse.numTable.toString()),

                        style: pw.TextStyle(
                            font: regular, fontSize: fontSize + 2))  : pw.SizedBox(),
                    pw.SizedBox(height: 4),

                    pw.Text(
                        "Date : " +
                            DateFormat("dd-MM-yyyy HH:mm")
                                .format(DateTime.now()),
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
 /*                   pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    pw.Text(
                        "Caissier : " + dataDto.utilisateur.nom+" "+dataDto.utilisateur.prenom,
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),*/
                    pw.Text(' ----------------------- ',
                        style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  ]),
                ),
                pw.SizedBox(height: 12),
                for (CommandeDetailsDto element in commandeDto.commandeDetails)
                  pw.Column(children: [
                    pw.Row(

                        children: [
                          pw.Text(
                              element.commandeDetails.quantite
                                  .toString()
                                  .endsWith('.0')
                                  ? element.commandeDetails.quantite
                                  .ceil()
                                  .toString() +
                                  " " +
                                  (   element.commandeDetails.designation.length<30?element.commandeDetails.designation: element.commandeDetails.designation.replaceAllMapped(RegExp(r".{30}"),
                                          (match) => "${match.group(0)}\n") +"        " +(element.commandeDetails.quantite *
                                      element.commandeDetails.prix)
                                      .toStringAsFixed(3))
                                  : element.commandeDetails.quantite.toString() +
                                  " " +
                                  element.commandeDetails.designation.replaceAllMapped(RegExp(r".{10}"),
                                          (match) => "${match.group(0)}\n"),
                              style: pw.TextStyle(font: regular, fontSize: fontSize)),
                          if( element.commandeDetails.designation.length<30)
                            pw.Expanded(
                                child: pw.Container(
                                  alignment: pw.Alignment.centerRight,
                                  child: pw.Text(
                                      (element.commandeDetails.quantite *
                                          element.commandeDetails.prix)
                                          .toStringAsFixed(3),
                                      style:
                                      pw.TextStyle(font: medium, fontSize: fontSize)),
                                ))
                        ]),
                    pw.Row(

                        children: [
                          pw.Expanded(
                              child: pw.Container(
                                alignment: pw.Alignment.centerRight,
                                child: pw.Text(
                                    ( element.commandeDetails.remise!=null&& element.commandeDetails.remise!=0?"P.init "+
                                        element.commandeDetails.prixSansRemise.toStringAsFixed(3)+' ---------- Remise '+element.commandeDetails.remise.toString()
                                        +'%':""),
                                    style:
                                    pw.TextStyle(font: medium, fontSize: fontSize-2)),
                              ))


                        ]
                    ),
                    if (isImpIng==true && isImpIng != null)
                      element.commandeDetails.ingredient!=null?  pw.Row(children: [
                        pw.Text(element.commandeDetails.ingredient.replaceAllMapped(
                            RegExp(r".{30}"),
                                (match) => "${match.group(0)}\n"),
                            textAlign: pw.TextAlign.left,
                            style: pw.TextStyle(
                                fontWeight: FontWeight.normal,
                                // color: PdfColors.grey800,
                                fontSize:fontSize/1.5)),
                      ]):pw.SizedBox(),
                  ]),
                pw.SizedBox(height: 8),
                pw.Row(children: [
                  pw.Text("",
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Expanded(
                      child: pw.Container(
                          padding: pw.EdgeInsets.only(top: 4, bottom: 4),
                          alignment: pw.Alignment.center,
                          child: pw.Column(children: [
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.Text(
                                "TOTAL " +
                                    commandeDto.commande.montant
                                        .toStringAsFixed(3) +
                                    ' DT',
                                style: pw.TextStyle(
                                    font: bold, fontSize: fontSize + 2)),
                            pw.Text(' ----------------------- ',
                                style: pw.TextStyle(
                                    font: regular, fontSize: fontSize)),
                            pw.SizedBox(height: 5),
                          ])))
                ]),
                pw.SizedBox(height: 12),
                pw.SizedBox(height: 12),codeW!=null&&codeW!=""?
                pw.Center(
                    child: pw.Column(children: [
                      pw.Text("Wifi :"+codeW,
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("Merci De Votre Visite,",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("À bientôt !",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.SizedBox(height: 4),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    ]))
                    :
                pw.Center(
                    child: pw.Column(children: [
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("Merci De Votre Visite,",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.Text("À bientôt !",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                      pw.SizedBox(height: 4),
                      pw.Text("******************************",
                          style: pw.TextStyle(font: regular, fontSize: fontSize)),
                    ])),
              ]); // Center
            }));
      }

      PrintingTicket().printTicket(pdf, printerip, context, notif);
    } catch (err) {
      Crashlytics().sendEmail("Utils printService", err.toString());
      if (notif) {
        AlertNotif().alert(
            context, "Une erreur est survenue lors de l'impression du ticket");
      }
      return false;
    }
    if (numTicketFromLocal){
      var x= numTicket+1;
      prefs.setInt("numTicket", x);
    }

    return true;
  }

  Future<bool> cuisine(BuildContext context, List<CommandeDetails> listCommande,
      DataDto dataDto, Commande commandeRes) async {
    if (listCommande.length == 0 && dataDto.commandeDelete.length == 0) {
      return null;
    }
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    bool notif = prefs.getBool('notif');
    String printercuisineip = prefs.getString('principalCuisine');
    String printerip = prefs.getString('principalPrinter');
    String printerAux1 = prefs.getString("printerAux1");
    String printerAux2 = prefs.getString("printerAux2");
    String formatPapierSaved = prefs.getString('formatPapier');
    String fontSizeSaved = prefs.getString('fontSize');
    double fontSize = fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
    String formatPapier = formatPapierSaved != null ? formatPapierSaved : '80';
    String marginSaved = prefs.getString('margin_printer');
    int margin = marginSaved != null ? int.parse(marginSaved) : 0;
    if (!kIsWeb && (printercuisineip == null || printercuisineip.isEmpty)) {
      return null;
    }
    if (dataDto.pointVente.fImpresCateg!=null&&dataDto.pointVente.fImpresCateg == 1 ) {
      ResponseList responseList =
          await caisseEndPointApi.listImprByCategorie(listCommande);

      if (responseList.result == 1) {
        List<FamilleDetailsCommande> familles = [];

        familles = responseList.objectResponse
            .map((e) => FamilleDetailsCommande.fromJson(e))
            .toList();

        print(familles);

        familles.forEach((element) {

          final ticket = pw.Document();
          ticket.addPage(pw.Page(
              margin: pw.EdgeInsets.all(margin.ceilToDouble()),
              pageFormat: formatPapier.contains('80')
                  ? PdfPageFormat.roll80
                  : PdfPageFormat.roll57,
              build: (pw.Context context) {
                return pw.Column(children: [
                  pw.Center(
                    child: pw.Column(children: [
                      pw.Text(dataDto.pointVente.designation,
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.Text("Ticket Cuisine",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.Text("-----------------------",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.SizedBox(height: 5),
                      pw.Text(commandeRes != null&&commandeRes.typeCommande!=null?commandeRes.typeCommande.toString().toUpperCase():"-",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize+3)),
                      pw.Text("-----------------------",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.SizedBox(height: 10),
                      pw.Text(
                          commandeRes != null
                              ? "N° " + (commandeRes.numCommande!=null?commandeRes.numCommande.toString():(dataDto.lastTicketNumber).toString())
                              : "",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.normal, fontSize: fontSize)),
                   commandeRes!=null&&   commandeRes.numTable!=null? pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur ' + (commandeRes.numTable.toString()):'Table ' + (commandeRes.numTable.toString()),
                          style: pw.TextStyle(fontWeight: FontWeight.bold,
                               fontSize: fontSize + 3))  : pw.SizedBox(),
                      (commandeRes==null||   commandeRes.numTable==null)&&     dataDto.tableCaisse!=null? pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur N°' +(dataDto.tableCaisse.numTable.toString()):'Table ' + (dataDto.tableCaisse.numTable.toString()),

                          style: pw.TextStyle(
                               fontSize: fontSize + 3))  : pw.SizedBox(),
                      pw.SizedBox(height:10),
                      pw.Text(DateFormat("dd-MM-yyyy HH:mm").format(DateTime.now()),
                          style: pw.TextStyle(
                              fontWeight: FontWeight.normal, fontSize: fontSize)),

                    ]),
                  ),
                  pw.SizedBox(height: 15),
                  pw.Text(element.details.length > 0 ? element.nomCateg : '',
                      style: pw.TextStyle(
                          fontWeight: FontWeight.bold, fontSize: fontSize+2)),
               //   pw.SizedBox(height: listCommande.length > 0 ? 6 : 0),
                  pw.SizedBox(height: 15),
                  for (CatgorieDetailsCommandeDTO c in element.details)
                    pw.Column(children: [
                      pw.Row(children: [
                        pw.Text(
                            c.commandeDetails.quantite.toString().endsWith('.0')
                                ? c.commandeDetails.quantite.ceil().toString() +
                                ' ' +
                             //   c.commandeDetails.mesure +
                                "  " +
                                c.commandeDetails.designation.replaceAllMapped(
                                    RegExp(r".{30}"),
                                        (match) => "${match.group(0)}\n")
                                : c.commandeDetails.quantite.toString() +
                                ' ' +
                              //  c.commandeDetails.mesure +
                                "  " +
                                c.commandeDetails.designation.replaceAllMapped(
                                    RegExp(r".{30}"),
                                        (match) => "${match.group(0)}\n"),
                            style: pw.TextStyle(
                                fontWeight: FontWeight.normal, fontSize: fontSize+1)),

                      ]),
                      c.commandeDetails.ingredients!=null ?  pw.Row(children: [
                        pw.Text(c.commandeDetails.ingredients.replaceAllMapped(
                            RegExp(r".{30}"),
                                (match) => "${match.group(0)}\n"),
                            textAlign: pw.TextAlign.left,
                            style: pw.TextStyle(
                                fontWeight: FontWeight.normal,
                               // color: PdfColors.grey800,
                                fontSize: fontSize+1)),
                      ]): pw.SizedBox(),
                      pw.SizedBox(height: 10),
                    ]),
                  pw.SizedBox(height: 35),
                  pw.Text("************************",
                      style: pw.TextStyle(
                          fontWeight: FontWeight.bold, fontSize: fontSize)),
                  pw.SizedBox(height: 10),
                  // pw.Text(dataDto.commandeDelete.length > 0 ? "Plats Annulés" : "",
                  //     style: pw.TextStyle(
                  //         fontWeight: FontWeight.normal, fontSize: fontSize)),
                  // pw.SizedBox(height: dataDto.commandeDelete.length > 0 ? 6 : 0),
                  // for (CommandeDetails element in dataDto.commandeDelete)
                  //   pw.Row(children: [
                  //     pw.Text(
                  //         element.quantite.toString().endsWith('.0')
                  //             ? element.quantite.ceil().toString() +
                  //             ' ' +
                  //             element.mesure +
                  //             " " +
                  //             element.designation
                  //             : element.quantite.toString() +
                  //             ' ' +
                  //             element.mesure +
                  //             " " +
                  //             element.designation,
                  //         style: pw.TextStyle(
                  //             fontWeight: FontWeight.normal, fontSize: fontSize)),
                  //   ]),
                ]); // Center
              }));
          try {
            if(ServerUrl.familleone!=null&&printerip!=null&&ServerUrl.familleone.contains(element.nomCateg)){
              PrintingTicket().printTicket(ticket, printerip, context, notif);
            }
            if(ServerUrl.familletwo!=null&&printercuisineip!=null&&ServerUrl.familletwo.contains(element.nomCateg)){

                PrintingTicket().printTicket(
                    ticket, printercuisineip, context, notif);
              }
            if(ServerUrl.famillePrinterAux1!=null&&printerAux1!=null&&ServerUrl.famillePrinterAux1.contains(element.nomCateg)){

              PrintingTicket().printTicket(
                  ticket, printerAux1, context, notif);
            }
            if(ServerUrl.famillePrinterAux2!=null&&printerAux2!=null&&ServerUrl.famillePrinterAux2.contains(element.nomCateg)){

              PrintingTicket().printTicket(
                  ticket, printerAux2, context, notif);
            }
          } catch (err) {
            Crashlytics().sendEmail("Utils cuisine", err.toString());
            if (notif) {
              AlertNotif().alert(context,
                  "Une erreur est survenue lors de l'impression du ticket");
            }
            return false;
          }

        });
      }
    } else {
      final ticket = pw.Document();
      ticket.addPage(pw.Page(
          margin: pw.EdgeInsets.all(margin.ceilToDouble()),
          pageFormat: formatPapier.contains('80')
              ? PdfPageFormat.roll80
              : PdfPageFormat.roll57,
          build: (pw.Context context) {
            return pw.Column(children: [
              pw.Center(
                child: pw.Column(children: [
                  pw.Text(dataDto.pointVente.designation,
                      style: pw.TextStyle(
                          fontWeight: FontWeight.bold, fontSize: fontSize)),
                  pw.Text(dataDto.pointVente.adresse,
                      style: pw.TextStyle(
                          fontWeight: FontWeight.bold, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text(
                      commandeRes != null
                          ? "N° " + commandeRes.numCommande.toString()
                          : "",
                      style: pw.TextStyle(
                          fontWeight: FontWeight.normal, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text(DateFormat("dd-MM-yyyy HH:mm").format(DateTime.now()),
                      style: pw.TextStyle(
                          fontWeight: FontWeight.normal, fontSize: fontSize)),
                ]),
              ),
              pw.SizedBox(height: 12),
              pw.Text(listCommande.length > 0 ? "Nouveaux Plats" : '',
                  style: pw.TextStyle(
                      fontWeight: FontWeight.normal, fontSize: fontSize)),
              pw.SizedBox(height: listCommande.length > 0 ? 6 : 0),
              for (CommandeDetails element in listCommande)
                pw.Column(children: [
                  pw.Row(children: [
                    pw.Text(
                        element.quantite.toString().endsWith('.0')
                            ? element.quantite.ceil().toString() +
                                ' ' +
                                element.mesure +
                                " " +
                                element.designation.replaceAllMapped(
                                    RegExp(r".{30}"),
                                        (match) => "${match.group(0)}\n")
                            : element.quantite.toString() +
                                ' ' +
                                element.mesure +
                                " " +
                                element.designation.replaceAllMapped(
                                    RegExp(r".{30}"),
                                        (match) => "${match.group(0)}\n"),
                        style: pw.TextStyle(
                            fontWeight: FontWeight.normal, fontSize: fontSize)),
                  ]),
                  pw.Row(children: [
                    pw.Text(element.ingredients,
                        textAlign: pw.TextAlign.left,
                        style: pw.TextStyle(
                            fontWeight: FontWeight.normal,
                            color: PdfColors.grey800,
                            fontSize: fontSize)),
                  ])
                ]),
              pw.SizedBox(height: 12),
              pw.Text(dataDto.commandeDelete.length > 0 ? "Plats Annulés" : "",
                  style: pw.TextStyle(
                      fontWeight: FontWeight.normal, fontSize: fontSize)),
              pw.SizedBox(height: dataDto.commandeDelete.length > 0 ? 6 : 0),
              for (CommandeDetails element in dataDto.commandeDelete)
                pw.Row(children: [
                  pw.Text(
                      element.quantite.toString().endsWith('.0')
                          ? element.quantite.ceil().toString() +
                              ' ' +
                              element.mesure +
                              " " +
                              element.designation.replaceAllMapped(
                                  RegExp(r".{30}"),
                                      (match) => "${match.group(0)}\n")
                          : element.quantite.toString() +
                              ' ' +
                              element.mesure +
                              " " +
                              element.designation.replaceAllMapped(
                                  RegExp(r".{30}"),
                                      (match) => "${match.group(0)}\n"),
                      style: pw.TextStyle(
                          fontWeight: FontWeight.normal, fontSize: fontSize)),
                ]),
            ]); // Center
          }));
      try {
        PrintingTicket().printTicket(ticket, printercuisineip, context, notif);
      } catch (err) {
        Crashlytics().sendEmail("Utils cuisine", err.toString());
        if (notif) {
          AlertNotif().alert(context,
              "Une erreur est survenue lors de l'impression du ticket");
        }
        return false;
      }
    }
    return true;
  }

  Future<bool> cuisineCommandeAnnulee(BuildContext context, List<CommandeDetails> listCommandeAnnulee,
      DataDto dataDto) async {
    if (listCommandeAnnulee.length == 0 && dataDto.commandeDelete.length == 0) {
      return null;
    }
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    bool notif = prefs.getBool('notif');
    String printercuisineip = prefs.getString('principalCuisine');
    String printerip = prefs.getString('principalPrinter');
    String printerAux1 = prefs.getString("printerAux1");
    String printerAux2 = prefs.getString("printerAux2");
    String formatPapierSaved = prefs.getString('formatPapier');
    String fontSizeSaved = prefs.getString('fontSize');
    double fontSize = fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
    String formatPapier = formatPapierSaved != null ? formatPapierSaved : '80';
    String marginSaved = prefs.getString('margin_printer');
    int margin = marginSaved != null ? int.parse(marginSaved) : 0;
    if (!kIsWeb && (printercuisineip == null || printercuisineip.isEmpty)) {
      return null;
    }
    if (dataDto.pointVente.fImpresCateg!=null&&dataDto.pointVente.fImpresCateg == 1 ) {
      ResponseList responseList =
      await caisseEndPointApi.listImprByCategorie(listCommandeAnnulee);

      if (responseList.result == 1) {
        List<FamilleDetailsCommande> familles = [];

        familles = responseList.objectResponse
            .map((e) => FamilleDetailsCommande.fromJson(e))
            .toList();

        familles.forEach((element) {

          final ticket = pw.Document();
          ticket.addPage(pw.Page(
              margin: pw.EdgeInsets.all(margin.ceilToDouble()),
              pageFormat: formatPapier.contains('80')
                  ? PdfPageFormat.roll80
                  : PdfPageFormat.roll57,
              build: (pw.Context context) {
                return pw.Column(children: [
                  pw.Center(
                    child: pw.Column(children: [
                      pw.Text(dataDto.pointVente.designation,
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.Text("Ticket Cuisine",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.Text("-----------------------",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.SizedBox(height: 5),
                      pw.Text(dataDto.commande != null&&dataDto.commande.typeCommande!=null?dataDto.commande.typeCommande.toString().toUpperCase():"-",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize+3)),
                      pw.Text("-----------------------",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.bold, fontSize: fontSize)),
                      pw.SizedBox(height: 10),
                      pw.Text(
                          dataDto.commande != null
                              ? "N° " + (dataDto.commande.numCommande!=null?dataDto.commande.numCommande.toString():(dataDto.lastTicketNumber).toString())
                              : "",
                          style: pw.TextStyle(
                              fontWeight: FontWeight.normal, fontSize: fontSize)),
                      dataDto.commande!=null&&    dataDto.commande.numTable!=null? pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur ' + (dataDto.commande.numTable.toString()):'Table ' + (dataDto.commande.numTable.toString()),
                          style: pw.TextStyle(fontWeight: FontWeight.bold,
                              fontSize: fontSize + 3))  : pw.SizedBox(),
                      (dataDto.commande.idTable==null||   dataDto.commande.numTable==null)&&     dataDto.tableCaisse!=null? pw.Text(dataDto.pointVente.fGestionTable!=null&&dataDto.pointVente.fGestionTable==2?'Bipeur N°' +(dataDto.tableCaisse.numTable.toString()):'Table ' + (dataDto.tableCaisse.numTable.toString()),

                          style: pw.TextStyle(
                              fontSize: fontSize + 3))  : pw.SizedBox(),
                      pw.SizedBox(height:10),
                      pw.Text(DateFormat("dd-MM-yyyy HH:mm").format(DateTime.now()),
                          style: pw.TextStyle(
                              fontWeight: FontWeight.normal, fontSize: fontSize)),
                    ]),
                  ),
                  pw.SizedBox(height: 15),
                  pw.Text(element.details.length > 0 ? element.nomCateg : '',
                      style: pw.TextStyle(
                          fontWeight: FontWeight.bold, fontSize: fontSize+2)),

                  pw.Text("Commandes Annulèes",
                      style: pw.TextStyle(
                          fontWeight: FontWeight.bold, fontSize: fontSize)),

                  pw.SizedBox(height: 15),





                  for (CommandeDetails element in dataDto.commandeDelete)
                    pw.Row(children: [
                      pw.Text(
                          element.quantite.toString().endsWith('.0')
                              ? element.quantite.ceil().toString() +
                              ' ' +
                              element.mesure +
                              " " +
                              element.designation.replaceAllMapped(
                                  RegExp(r".{30}"),
                                      (match) => "${match.group(0)}\n")
                              : element.quantite.toString() +
                              ' ' +
                              element.mesure +
                              " " +
                              element.designation.replaceAllMapped(
                                  RegExp(r".{30}"),
                                      (match) => "${match.group(0)}\n"),
                          style: pw.TextStyle(
                              fontWeight: FontWeight.normal, fontSize: fontSize)),
                    ]),



                ]); // Center
              }));
          try {
            if(ServerUrl.familleone!=null&&printerip!=null&&ServerUrl.familleone.contains(element.nomCateg)){
              PrintingTicket().printTicket(ticket, printerip, context, notif);
            }
            if(ServerUrl.familletwo!=null&&printercuisineip!=null&&ServerUrl.familletwo.contains(element.nomCateg)){

              PrintingTicket().printTicket(
                  ticket, printercuisineip, context, notif);
            }
            if(ServerUrl.famillePrinterAux1!=null&&printerAux1!=null&&ServerUrl.famillePrinterAux1.contains(element.nomCateg)){

              PrintingTicket().printTicket(
                  ticket, printerAux1, context, notif);
            }
            if(ServerUrl.famillePrinterAux2!=null&&printerAux2!=null&&ServerUrl.famillePrinterAux2.contains(element.nomCateg)){

              PrintingTicket().printTicket(
                  ticket, printerAux2, context, notif);
            }
          } catch (err) {
            Crashlytics().sendEmail("Utils cuisine", err.toString());
            if (notif) {
              AlertNotif().alert(context,
                  "Une erreur est survenue lors de l'impression du ticket");
            }
            return false;
          }

        });
      }
    }
    return true;
  }

  Future<bool> printSessionCloture(
      BuildContext context, PreColture colture, DataDto dataDto) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    String printerip = prefs.getString('principalPrinter');
    bool notif = prefs.getBool('notif');
    try {
      var dataBold = await rootBundle.load("assets/Montserrat-Bold.ttf");
      var bold = Font.ttf(dataBold);
      var dataMedium = await rootBundle.load("assets/Montserrat-Medium.ttf");
      var medium = Font.ttf(dataMedium);
      var dataRegular = await rootBundle.load("assets/Montserrat-Regular.ttf");
      var regular = Font.ttf(dataRegular);
      String formatPapierSaved = prefs.getString('formatPapier');
      String fontSizeSaved = prefs.getString('fontSize');
      double fontSize =
          fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
      String formatPapier =
          formatPapierSaved != null ? formatPapierSaved : '80';
      String marginSaved = prefs.getString('margin_printer');
      int margin = marginSaved != null ? int.parse(marginSaved) : 0;
      if (!kIsWeb && (printerip == null || printerip == "")) {
        return null;
      }
      final pdf = pw.Document();
      pdf.addPage(pw.Page(
          margin: pw.EdgeInsets.all(margin.ceilToDouble()),
          pageFormat: formatPapier.contains('80')
              ? PdfPageFormat.roll80
              : PdfPageFormat.roll57,
          build: (pw.Context context) {
            return pw.Column(children: [
              pw.Center(
                child: pw.Column(children: [
                  pw.Text(dataDto.pointVente.designation,
                      style: pw.TextStyle(font: bold, fontSize: fontSize)),
                  pw.Text(dataDto.pointVente.adresse,
                      style: pw.TextStyle(font: bold, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text(DateFormat("dd-MM-yyyy HH:mm").format(DateTime.now()),
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text("Votre session a été cloturée avec succès",
                      style: pw.TextStyle(font: regular, fontSize: fontSize),
                      textAlign: TextAlign.center),
                ]),
              ),
              pw.SizedBox(height: 12),
              pw.Text(
                  dataDto.utilisateur.prenom + " " + dataDto.utilisateur.nom,
                  style: pw.TextStyle(font: regular, fontSize: fontSize)),
              pw.SizedBox(height: 12),
              pw.Text(
                  colture.montantParModeRegl.length > 0
                      ? "Montant par moyen de paiement"
                      : "",
                  style: pw.TextStyle(font: regular, fontSize: fontSize),
                  textAlign: TextAlign.center),
              pw.SizedBox(height: 4),
              for (MontantParModeRegl element in colture.montantParModeRegl)
                pw.Row(children: [
                  pw.Text(element.designation,
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Expanded(
                      child: pw.Container(
                    alignment: pw.Alignment.centerRight,
                    child: pw.Text(
                        element.totale.toStringAsFixed(int.parse(
                                dataDto.pointVente.chiffrevirgule != null
                                    ? dataDto.pointVente.chiffrevirgule
                                    : "3")) +
                            " DT",
                        style: pw.TextStyle(font: medium, fontSize: fontSize)),
                  ))
                ]),
              pw.SizedBox(height: 12),
              pw.Center(
                  child: pw.Column(children: [
                pw.Text("********************",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.Text("À bientot",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.SizedBox(height: 4),
                pw.Text("********************",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
              ])),
            ]); // Center
          }));
      PrintingTicket().printTicket(pdf, printerip, context, notif);
    } catch (err) {
      if (notif) {
        AlertNotif().alert(
            context, "Une erreur est survenue lors de l'impression du ticket");
      }
      return false;
    }
    return true;
  }

  Future<bool> printJourneeCloture(
      BuildContext context, ClotureJourneeRes colture, DataDto dataDto) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    String printerip = prefs.getString('principalPrinter');
    bool notif = prefs.getBool('notif');
    String formatPapierSaved = prefs.getString('formatPapier');
    String fontSizeSaved = prefs.getString('fontSize');
    double fontSize = fontSizeSaved != null ? double.parse(fontSizeSaved) : 13;
    String formatPapier = formatPapierSaved != null ? formatPapierSaved : '80';
    String marginSaved = prefs.getString('margin_printer');
    int margin = marginSaved != null ? int.parse(marginSaved) : 0;
    try {
      var dataBold = await rootBundle.load("assets/Montserrat-Bold.ttf");
      var bold = Font.ttf(dataBold);
      var dataMedium = await rootBundle.load("assets/Montserrat-Medium.ttf");
      var medium = Font.ttf(dataMedium);
      var dataRegular = await rootBundle.load("assets/Montserrat-Regular.ttf");
      var regular = Font.ttf(dataRegular);
      if (!kIsWeb && (printerip == null || printerip == "")) {
        return null;
      }
      final pdf = pw.Document();
      pdf.addPage(pw.Page(
          margin: pw.EdgeInsets.all(margin.ceilToDouble()),
          pageFormat: formatPapier.contains('80')
              ? PdfPageFormat.roll80
              : PdfPageFormat.roll57,
          build: (pw.Context context) {
            return pw.Column(children: [
              pw.Center(
                child: pw.Column(children: [
                  pw.Text(dataDto.pointVente.designation,
                      style: pw.TextStyle(font: bold, fontSize: fontSize)),
                  pw.Text(dataDto.pointVente.adresse,
                      style: pw.TextStyle(font: bold, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text(DateFormat("dd-MM-yyyy HH:mm").format(DateTime.now()),
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.SizedBox(height: 4),
                  pw.Text("Votre journée a été cloturée avec succès",
                      style: pw.TextStyle(font: regular, fontSize: fontSize),
                      textAlign: TextAlign.center),
                ]),
              ),
              pw.SizedBox(height: 12),
              pw.Text(
                  dataDto.utilisateur.prenom + " " + dataDto.utilisateur.nom,
                  style: pw.TextStyle(font: regular, fontSize: fontSize)),
              pw.SizedBox(height: 12),
              pw.Text(
                  colture.montantParModeReglParJournee.length > 0
                      ? "Montant par moyen de paiement"
                      : "",
                  style: pw.TextStyle(font: regular, fontSize: fontSize),
                  textAlign: TextAlign.center),
              pw.SizedBox(height: 4),
              for (MontantParModeRegl element
                  in colture.montantParModeReglParJournee)
                pw.Row(children: [
                  pw.Text(element.designation,
                      style: pw.TextStyle(font: regular, fontSize: fontSize)),
                  pw.Expanded(
                      child: pw.Container(
                    alignment: pw.Alignment.centerRight,
                    child: pw.Text(
                        element.totale.toStringAsFixed(int.parse(
                                dataDto.pointVente.chiffrevirgule != null
                                    ? dataDto.pointVente.chiffrevirgule
                                    : "3")) +
                            " DT",
                        style: pw.TextStyle(font: medium, fontSize: fontSize)),
                  ))
                ]),
              pw.SizedBox(height: 12),
              pw.Center(
                  child: pw.Column(children: [
                pw.Text("********************",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.Text("À bientot",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
                pw.SizedBox(height: 4),
                pw.Text("********************",
                    style: pw.TextStyle(font: regular, fontSize: fontSize)),
              ])),
            ]); // Center
          }));
      PrintingTicket().printTicket(pdf, printerip, context, notif);
    } catch (err) {
      if (notif) {
        AlertNotif().alert(
            context, "Une erreur est survenue lors de l'impression du ticket");
      }
      return false;
    }
    return true;
  }
}
