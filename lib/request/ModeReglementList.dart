import 'package:ad_caisse/pos/api/mode_reglement_end_point_api.dart';
import 'package:ad_caisse/pos/model/caisse.dart';
import 'package:ad_caisse/pos/model/mode_reglement.dart';

import 'Crashlytics.dart';
import 'ResponseList.dart';

class ModeReglementList {
  Future<List<ModeReglement>> getModeReglementList(Caisse caisse) async {
    try {
      var modeReglementEndPointApi = ModeReglementEndPointApi();
      var idPointVente = caisse.idPointVente;
      ResponseList modeReglement = await modeReglementEndPointApi
          .findAllByIdPointVenteUsingGET(idPointVente: idPointVente);
      List<ModeReglement> list = [];
      if (modeReglement.result == 1) {
        list = await List<ModeReglement>.from(
            modeReglement.objectResponse.map((e) => ModeReglement.fromJson(e)));
      }
      return list;
    } catch (error) {
      Crashlytics()
          .sendEmail("ModeReglementList getModeReglement", error.toString());
      return [];
    }
  }
}
