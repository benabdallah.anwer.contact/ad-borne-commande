import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/pos/api/journee_end_point_api.dart';
import 'package:ad_caisse/pos/api/session_end_point_api.dart';
import 'package:ad_caisse/pos/model/ClotureJourneeDto.dart';
import 'package:ad_caisse/pos/model/PreColture.dart';
import 'package:ad_caisse/pos/model/cloture_session_dto.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/material.dart';

import 'Crashlytics.dart';
import 'ResponseSingle.dart';

class ClotureRequest {
  var sessionEndPointApi = SessionEndPointApi();
  var journeeEndPointApi = JourneeEndPointApi();

  Future<PreColture> getPreColture(DataDto dataDto) async {
    ResponseSingle responseSingle =
        await sessionEndPointApi.preColtureUsingGET(dataDto.session.idSession);
    PreColture cloture = PreColture.fromJson(responseSingle.objectResponse);
    return cloture;
  }

  Future<ResponseSingle> cloture(DataDto dataDto, PreColture pre,
      double montantCloture, BuildContext context) async {
    try {
      ClotureSessionDTO clotureSessionDTO = ClotureSessionDTO();
      clotureSessionDTO.idSession = dataDto.session.idSession;
      clotureSessionDTO.dateCloture = DateTime.now();
      clotureSessionDTO.chiffreAffaireSession = pre.chiffreAffaire!=null?pre.chiffreAffaire:0.0;
      clotureSessionDTO.montantCloture = montantCloture!=null?montantCloture:0.0;
      clotureSessionDTO.montantTiroirSysteme = pre.montantTirroir!=null?pre.montantTirroir:0.0;
      return await sessionEndPointApi
          .cloturesessionUsingPOST(clotureSessionDTO);
    } catch (error) {
      Crashlytics().sendEmail("ClotureRequest cloture", error.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      return null;
    }
  }

  Future<ResponseSingle> clotureJournee(
      BuildContext context, DataDto dataDto) async {
    try {
      ClotureJourneeDto clotureJourneeDto = ClotureJourneeDto(
          dateCloture: DateTime.now(),
          idJournee: dataDto.journee.idJournee,
          idUser: dataDto.utilisateur.idUtilisateur);
      return await journeeEndPointApi
          .clotureJourneeUsingPOST(clotureJourneeDto);

    } catch (error) {
      Crashlytics()
          .sendEmail("ClotureRequest clotureJournee", error.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      return null;
    }
  }
}
