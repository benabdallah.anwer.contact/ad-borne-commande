import 'package:ad_caisse/dto/DataDto.dart';
import 'package:ad_caisse/loyality/api/fid_cred_end_point_api.dart';
import 'package:ad_caisse/loyality/model/commande_partner_dto.dart';
import 'package:ad_caisse/ordering/api/ordering_end_point_api.dart';
import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/ordering/model/commande.dart';
import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/ordering/model/commande_dto.dart';
import 'package:ad_caisse/payement/api/payment_end_point_api.dart';
import 'package:ad_caisse/payement/model/reglement_dto.dart';
import 'package:ad_caisse/payement/model/reglm_details_dto.dart';
import 'package:ad_caisse/pos/api/mode_reglement_end_point_api.dart';
import 'package:ad_caisse/pos/model/mode_reglement.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/screens/CommandeScreen.dart';
import 'package:ad_caisse/transaction/model/CommandeDetailsDto.dart';
import 'package:ad_caisse/transaction/model/CommandesDto.dart';
import 'package:ad_caisse/transaction/model/client.dart';
import 'package:ad_caisse/transaction/model/commande.dart';
import 'package:ad_caisse/transaction/model/commande_details.dart';
import 'package:ad_caisse/utils/AlertNotif.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/cupertino.dart';

import 'Crashlytics.dart';
import 'ResponseSingle.dart';

class CommandeRequest {
  var orderingEndPointApi = OrderingEndPointApi();
  var modeReglementEndPointApi = ModeReglementEndPointApi();
  var paymentEndPointApi = PaymentEndPointApi();
  var fidCredEndPointApi = FidCredEndPointApi();

  Future<ResponseSingle> createCommande(
      int paye, DataDto dataDto, BuildContext context) async {
    try {
      if (dataDto.lastTicketNumber != null) {
        debugPrint('lastTicketNumber: ' + dataDto.lastTicketNumber.toString());
      }
      List<CommandeDetails> listCommande = [];
      dataDto.commandeDetails.forEach((element) {
        if (element.idDetailComm == null || element.idDetailComm == "") {
          listCommande.add(element);
        }
      });
      Commande commandeRes;
      ResponseSingle responseSingle;
      if (listCommande.length > 0) {
        Commande commande = Commande();
        commande.numCommande = dataDto.lastTicketNumber;
        dataDto.clientBPrice != null && commande.clients.length == 0
            ? commande.clients.add(dataDto.clientBPrice)
            : null;
        commande.idCommande =
            dataDto.commande != null ? dataDto.commande.idCommande : null;
        commande.idPointVente = dataDto.caisse.idPointVente;
        commande.fPaye = paye;
        commande.idTable =
            dataDto.tableCaisse != null ? dataDto.tableCaisse.idTable : null;
        commande.idSession = dataDto.session.idSession;
        commande.montant = double.parse(dataDto.total);
        CommandeDto commandeDto = CommandeDto();
        commandeDto.commande = commande;
        commandeDto.idsession = dataDto.session.idSession;
        commandeDto.details = listCommande;
        responseSingle = await orderingEndPointApi
            .createCommandeAndDetailsCommandeUsingPOST(commandeDto);
        if (responseSingle.result != 1) {
          return responseSingle;
        }

        commandeRes = Commande.fromJson(responseSingle.objectResponse);

        dataDto.lastTicketNumber = commandeDto.commande?.numCommande;
        LocalStorageInterface prefs = await LocalStorage.getInstance();
        prefs.setInt("lastTicketNumber", commandeDto.commande?.numCommande);
      }

      if (paye == 1) {
        if (commandeRes == null) {
          commandeRes = Commande();
          commandeRes.numCommande = dataDto.lastTicketNumber;
          commandeRes.idCommande = dataDto.commande.idCommande;
          commandeRes.idTable =
              dataDto.tableCaisse != null ? dataDto.tableCaisse.idTable : null;
          commandeRes.numCommande = dataDto.commande.numCommande;
          commandeRes.montant = dataDto.commande.montant;
          commandeRes.dateCreation = dataDto.commande.dateCreation;
          commandeRes.idSession = dataDto.commande.idSession;
          commandeRes.idPointVente = dataDto.commande.idPointVente;
          commandeRes.reste = dataDto.commande.reste;

        }
        dataDto.payementList.forEach((element) {
          element.idCommande = commandeRes.idCommande;
        });
        ReglementDto reglementDto = ReglementDto();
        reglementDto.idPointVente = dataDto.caisse.idPointVente;
        reglementDto.dateTransaction = new DateTime.now();
        reglementDto.idCommande = commandeRes.idCommande;
        reglementDto.reglements = dataDto.payementList;
        if(dataDto.commande!=null && (dataDto.commande.idSession.toString()!= dataDto.session.idSession.toString())) {
          reglementDto.idSessionServer =  dataDto.session
              .idSession.toString() ;
        }
        ResponseSingle response =
            await paymentEndPointApi.checkAndSavePaymentUsingPOST(reglementDto);
        CommandesDtoTransaction commandesDtoTransaction =
            CommandesDtoTransaction();
        commandesDtoTransaction.commande.idCommande = commandeRes.idCommande;
        commandesDtoTransaction.commande.montant = commandeRes.montant;
        commandesDtoTransaction.commande.numCommande = commandeRes.numCommande;
        commandesDtoTransaction.commande.dateCreation =
            commandeRes.dateCreation;
        commandeRes.clients.forEach((element) {
          commandesDtoTransaction.commande.clients
              .add(ClientTransaction.fromJson(element.toJson()));
        });
        if(dataDto.commande!=null&& dataDto.commande.clients!=null)
          commandesDtoTransaction.commande.clients=dataDto.commande.clients;
        commandesDtoTransaction.commandeDetails = [];
        dataDto.commandeDetails.forEach((element) {
          CommandeDetailsDto commandeDetailsDto = CommandeDetailsDto();
          commandeDetailsDto.nomProduit = element.designation;
          commandeDetailsDto.commandeDetails =
              CommandeDetailsTransaction.fromJson(element.toJson());
          commandesDtoTransaction.commandeDetails.add(commandeDetailsDto);
        });
        if (response.result == 1) {
          CommandePartnerDto commandePartnerDto = CommandePartnerDto();
          commandePartnerDto.idPartenaire = dataDto.utilisateur.idPartenaire;
          commandePartnerDto.idCommande = commandeRes.idCommande;
          fidCredEndPointApi.checkAndSavePaymentUsingPOST(commandePartnerDto);
          // await Utils().openDrawer(context);
          await Utils().printService(context, commandesDtoTransaction, dataDto);
          if (dataDto.pointVente.fImprimCuisine == 1 ) {
            await Utils().cuisine(context, listCommande, dataDto,
                commandeRes);
          }


        }
        return response;
      }
      return responseSingle;
    } catch (error) {
      Crashlytics()
          .sendEmail("CommandeRequest createCommande", error.toString());
      AlertNotif().alert(context,
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion");
      // return null;
    }
  }

  Future<ResponseSingle> cancelOrderDetails(String idDetails) async {
    try {
      return await orderingEndPointApi.cancelOrderDetailsUsingPOST(idDetails);
    } catch (error) {
      Crashlytics()
          .sendEmail("CommandeRequest cancelOrderDetails", error.toString());
      return null;
    }
  }

  Future<ResponseSingle> createCommandeEsp(
      int paye, DataDto dataDto, BuildContext context) async {
    try {
      if (dataDto.lastTicketNumber != null) {
        debugPrint('lastTicketNumber: ' + dataDto.lastTicketNumber.toString());
      }
      List<CommandeDetails> listCommande = [];
      if (dataDto.commandeDetails.length > 0) {
        for (CommandeDetails element in dataDto.commandeDetails) {
          if (element.idDetailComm == null || element.idDetailComm.isEmpty || element.isModifier==1) {
            listCommande.add(element);
          }
        }
      }
      Commande commandeRes;

      ResponseSingle responseSingle;
      if (listCommande.length > 0) {
        Commande commande = Commande();
        commande.typeCommande = dataDto.commandeType;
        commande.idMessageCommande = dataDto.stompMessage != null
            ? dataDto.stompMessage.idMessageCommande
            : null;
        commande.adresse = dataDto.stompMessage != null
            ? dataDto.stompMessage.body
            : null;
        commande.numCommande = dataDto.lastTicketNumber;
        dataDto.clientBPrice != null && commande.clients.length == 0
            ? commande.clients.add(dataDto.clientBPrice)
            : null;

        if (dataDto.tableCaisse != null) {
          commande.idCommande = dataDto.tableCaisse.commande != null
              ? dataDto.tableCaisse.commande.idCommande
              : null;
          commande.idTable = dataDto.tableCaisse.idTable;
        } else {
          commande.idCommande =
          dataDto.commande != null ? dataDto.commande.idCommande : null;
        }
        commande.idPointVente = dataDto.caisse.idPointVente;
        commande.fPaye = paye;
        commande.idSession = dataDto.session.idSession;
        commande.montant = double.parse(dataDto.total);
        CommandeDto commandeDto = CommandeDto();
        commandeDto.commande = commande;
        commandeDto.idsession = dataDto.session.idSession;
        commandeDto.details = listCommande;

        responseSingle = await orderingEndPointApi
            .createCommandeAndDetailsCommandeUsingPOST(commandeDto);
        if (responseSingle.result != 1) {
          return responseSingle;
        } else if (paye != 1) {
          //AlertNotif().alert(context, "Commande enregistré");


        }
        commandeRes = Commande.fromJson(responseSingle.objectResponse);
        commandeRes.idMessageCommande = dataDto.stompMessage != null
            ? dataDto.stompMessage.idMessageCommande
            : null;
        commandeRes.adresse = dataDto.stompMessage != null
            ? dataDto.stompMessage.body
            : null;
        commandeRes.typeCommande = dataDto.commandeType;
        dataDto.lastTicketNumber = commandeRes.numCommande;
        LocalStorageInterface prefs = await LocalStorage.getInstance();
        prefs.setInt("lastTicketNumber", commandeRes.numCommande);
        if(dataDto.commandeType=="livraison") {
          CommandesDtoTransaction commandesDtoTransaction =
          CommandesDtoTransaction();
          commandeRes.idMessageCommande = dataDto.stompMessage != null
              ? dataDto.stompMessage.idMessageCommande
              : null;
          CommandeTransaction commandeTransaction = CommandeTransaction();
          commandeTransaction.idMessageCommande = dataDto.stompMessage != null
              ? dataDto.stompMessage.idMessageCommande
              : null;
          commandeTransaction.adresse = dataDto.stompMessage != null
              ? dataDto.stompMessage.body
              : null;
          commandeTransaction.typeCommande = dataDto.commandeType;
          commandeTransaction.idCommande = commandeRes.idCommande;
          commandeTransaction.montant = double.parse(dataDto.total);
          commandeTransaction.dateCreation = commandeRes.dateCreation;
          commandeTransaction.numCommande = commandeRes.numCommande;
          commandeTransaction.clients = commandeRes.clients
              .map((e) => ClientTransaction.fromJson(e.toJson()))
              .toList();
          commandesDtoTransaction.commande = commandeTransaction;
          commandesDtoTransaction.commandeDetails = [];
          dataDto.commandeDetails.forEach((element) {
            CommandeDetailsDto commandeDetailsDto = CommandeDetailsDto();
            commandeDetailsDto.nomProduit = element.designation;
            commandeDetailsDto.commandeDetails =
                CommandeDetailsTransaction.fromJson(element.toJson());
            commandesDtoTransaction.commandeDetails.add(commandeDetailsDto);
          });
          await Utils().printService(context, commandesDtoTransaction, dataDto);
          // a enlever juste pour zied
          // await Utils().printService(context, commandesDtoTransaction, dataDto);
        }
      }
      if (paye == 1) {
        if (commandeRes == null) {
          commandeRes = Commande();
          commandeRes.idMessageCommande = dataDto.stompMessage != null
              ? dataDto.stompMessage.idMessageCommande
              : null;
          commandeRes.typeCommande = dataDto.commandeType;
          commandeRes.idCommande = dataDto.commande.idCommande;
          commandeRes.idTable =
          dataDto.tableCaisse != null ? dataDto.tableCaisse.idTable : null;
          commandeRes.numCommande = dataDto.commande.numCommande;
          commandeRes.montant = dataDto.commande.montant;
          commandeRes.dateCreation = dataDto.commande.dateCreation;
          commandeRes.idSession = dataDto.commande.idSession;
          commandeRes.idPointVente = dataDto.commande.idPointVente;
          commandeRes.reste = dataDto.commande.reste;
          commandeRes.numCommande = dataDto.lastTicketNumber;
        }

        ModeReglement reglement =
        dataDto.modeReg.where((i) => i.fdefault == 1).toList()[0];
        List<ReglmDetailsDto> list = [];
        ReglmDetailsDto reglmDetailsDto = ReglmDetailsDto();
        reglmDetailsDto.idCommande = commandeRes.idCommande;
        reglmDetailsDto.idClientPartenaire = dataDto.clientBPrice != null
            ? dataDto.clientBPrice.idClientPartenaire
            : null;
        reglmDetailsDto.montant = commandeRes.montant;
        reglmDetailsDto.datePourEncaissement = new DateTime.now();
        reglmDetailsDto.dateReg = new DateTime.now();
        reglmDetailsDto.idModReg = reglement.idModeReglement;
        list.add(reglmDetailsDto);
        ReglementDto reglementDto = ReglementDto();
        reglementDto.idPointVente = dataDto.caisse.idPointVente;
        reglementDto.dateTransaction = new DateTime.now();
        reglementDto.idCommande = commandeRes.idCommande;
        reglementDto.reglements = list;
        if(dataDto.commande!=null && (dataDto.commande.idSession.toString()!= dataDto.session.idSession.toString())) {
          reglementDto.idSessionServer =  dataDto.session
              .idSession.toString() ;
        }
        ResponseSingle response =
        await paymentEndPointApi.checkAndSavePaymentUsingPOST(reglementDto);
        CommandesDtoTransaction commandesDtoTransaction =
        CommandesDtoTransaction();
        commandeRes.idMessageCommande = dataDto.stompMessage != null
            ? dataDto.stompMessage.idMessageCommande
            : null;
        CommandeTransaction commandeTransaction = CommandeTransaction();
        commandeTransaction.idMessageCommande = dataDto.stompMessage != null
            ? dataDto.stompMessage.idMessageCommande
            : null;
        commandeTransaction.typeCommande = dataDto.commandeType;
        commandeTransaction.idCommande = commandeRes.idCommande;
        commandeTransaction.montant = double.parse(dataDto.total);
        commandeTransaction.dateCreation = commandeRes.dateCreation;
        commandeTransaction.numCommande = commandeRes.numCommande;
        commandeTransaction.clients = commandeRes.clients
            .map((e) => ClientTransaction.fromJson(e.toJson()))
            .toList();
        commandesDtoTransaction.commande = commandeTransaction;
        commandesDtoTransaction.commandeDetails = [];
        dataDto.commandeDetails.forEach((element) {
          CommandeDetailsDto commandeDetailsDto = CommandeDetailsDto();
          commandeDetailsDto.nomProduit = element.designation;
          commandeDetailsDto.commandeDetails =
              CommandeDetailsTransaction.fromJson(element.toJson());
          commandesDtoTransaction.commandeDetails.add(commandeDetailsDto);
        });
        if (response.result == 1) {
          await Utils().printService(context, commandesDtoTransaction, dataDto);
          CommandePartnerDto commandePartnerDto = CommandePartnerDto();
          commandePartnerDto.idPartenaire = dataDto.utilisateur.idPartenaire;
          commandePartnerDto.idCommande = commandeRes.idCommande;
          fidCredEndPointApi.checkAndSavePaymentUsingPOST(commandePartnerDto);
        }
        return response;
      }
      return responseSingle;
    } catch (err) {
      Crashlytics()
          .sendEmail("CommandeRequest createCommandeEsp", err.toString());
      return ResponseSingle(
          result: -10,
          errorDescription:
          "Connexion perdue avec le serveur\nVeuillez vérifier votre connexion",
          objectResponse: null);
    }
  }
}

