import 'dart:convert';

import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

import 'api_exception.dart';
import 'auth/api_key_auth.dart';
import 'auth/authentication.dart';
import 'auth/oauth.dart';
import 'model/reservation_dto.dart';

class QueryParam {
  String name;
  String value;

  QueryParam(this.name, this.value);
}

class ApiClient {
  String basePath;
  var client = http.Client();

  Map<String, String> _defaultHeaderMap = {};
  Map<String, Authentication> _authentications = {};

  final _RegList = new RegExp(r'^List<(.*)>$');
  final _RegMap = new RegExp(r'^Map<String,(.*)>$');

  ApiClient() {
    // Setup authentications (key: authentication name, value: authentication).
    this.basePath = ServerUrl.url + "/bp-api-reservation";
    _authentications['Bearer'] = new ApiKeyAuth("header", "Authorization");
  }

  void addDefaultHeader(String key, String value) {
    _defaultHeaderMap[key] = value;
  }

  dynamic _deserialize(dynamic value, String targetType) {
    try {
      switch (targetType) {
        case 'String':
          return '$value';
        case 'int':
          return value is int ? value : int.parse('$value');
        case 'bool':
          return value is bool ? value : '$value'.toLowerCase() == 'true';
        case 'double':
          return value is double ? value : double.parse('$value');
        case 'ReservationDto':
          return new ReservationDto.fromJson(value);
        case 'ObjectSingle':
          return new ResponseSingle.fromJson(value);
        case 'Object':
          return new ResponseList.fromJson(value);
        default:
          {
            Match match;
            if (value is List &&
                (match = _RegList.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return value.map((v) => _deserialize(v, newTargetType)).toList();
            } else if (value is Map &&
                (match = _RegMap.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return new Map.fromIterables(value.keys,
                  value.values.map((v) => _deserialize(v, newTargetType)));
            }
          }
      }
    } catch (e, stack) {
      throw new ApiException.withInner(
          500, 'Exception during deserialization.', e, stack);
    }
    throw new ApiException(
        500, 'Could not find a suitable class for deserialization');
  }

  dynamic deserialize(String jsonVal, String targetType) {
    // Remove all spaces.  Necessary for reg expressions as well.
    targetType = targetType.replaceAll(' ', '');

    if (targetType == 'String') return jsonVal;

    var decodedJson = json.decode(jsonVal);
    return _deserialize(decodedJson, targetType);
  }

  String serialize(Object obj) {
    String serialized = '';
    if (obj == null) {
      serialized = '';
    } else {
      serialized = json.encode(obj);
    }
    return serialized;
  }

  // We don't use a Map<String, String> for queryParams.
  // If collectionFormat is 'multi' a key might appear multiple times.
  Future<Response> invokeAPI(
      String path,
      String method,
      Iterable<QueryParam> queryParams,
      Object body,
      Map<String, String> headerParams,
      Map<String, String> formParams,
      String contentType,
      List<String> authNames) async {
    _updateParamsForAuth(authNames, queryParams, headerParams);

    var ps = queryParams
        .where((p) => p.value != null)
        .map((p) => '${p.name}=${p.value}');
    String queryString = ps.isNotEmpty ? '?' + ps.join('&') : '';

    String url = basePath + path + queryString;

    bool isConnected = await Utils().checkInternet();
    if (!isConnected) {
      url = ServerUrl.urlSynchro + "/bp-api-reservation" + path + queryString;
    }
    debugPrint('Current Server $url');
    headerParams.addAll(_defaultHeaderMap);
    headerParams['Content-Type'] = contentType;

    if (body is MultipartRequest) {
      var request = new MultipartRequest(method, Uri.parse(url));
      request.fields.addAll(body.fields);
      request.files.addAll(body.files);
      request.headers.addAll(body.headers);
      request.headers.addAll(headerParams);
      var response = await client.send(request);
      return Response.fromStream(response);
    } else {
      var msgBody = contentType == "application/x-www-form-urlencoded"
          ? formParams
          : serialize(body);
      final errorRet = Response(
          "{\"result\": -5,\"errorDescription\": \"Connexion perdue avec le serveur\\nVeuillez vérifier votre connexion\"}",
          200);
      switch (method) {
        case "POST":
          return client
              .post(Uri.parse(url), headers: headerParams, body: msgBody)
              .timeout(Duration(seconds: 20), onTimeout: () {
            return errorRet;
          });
        case "PUT":
          return client
              .put(Uri.parse(url), headers: headerParams, body: msgBody)
              .timeout(Duration(seconds: 5), onTimeout: () {
            return errorRet;
          });
        case "DELETE":
          return client
              .delete(Uri.parse(url), headers: headerParams)
              .timeout(Duration(seconds: 5), onTimeout: () {
            return errorRet;
          });
        case "PATCH":
          return client
              .patch(Uri.parse(url), headers: headerParams, body: msgBody)
              .timeout(Duration(seconds: 5), onTimeout: () {
            return errorRet;
          });
        default:
          return client
              .get(Uri.parse(url), headers: headerParams)
              .timeout(Duration(seconds: 20), onTimeout: () {
            return errorRet;
          });
      }
    }
  }

  /// Update query and header parameters based on authentication settings.
  /// @param authNames The authentications to apply
  void _updateParamsForAuth(List<String> authNames,
      List<QueryParam> queryParams, Map<String, String> headerParams) {
    authNames.forEach((authName) {
      Authentication auth = _authentications[authName];
      if (auth == null)
        throw new ArgumentError("Authentication undefined: " + authName);
      auth.applyToParams(queryParams, headerParams);
    });
  }

  void setAccessToken(String accessToken) {
    _authentications.forEach((key, auth) {
      if (auth is OAuth) {
        auth.setAccessToken(accessToken);
      }
    });
  }
}
