import 'package:ad_caisse/product/model/produit.dart';

class ProduitAndQte {
  String idProduit = null;

  double quantite = null;

  String designation;

  double prix;
  Produit produit;

  ProduitAndQte({this.quantite, this.idProduit, this.designation});

  @override
  String toString() {
    return 'ProduitAndQte[idProduit=$idProduit, quantite=$quantite, ]';
  }

  ProduitAndQte.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idProduit = json['idProduit'];
    quantite = json['quantite'];
    designation = json['designation'];
  }

  Map<String, dynamic> toJson() {
    return {
      'idProduit': idProduit,
      'quantite': quantite,
      'designation': designation
    };
  }

  static List<ProduitAndQte> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ProduitAndQte>()
        : json.map((value) => new ProduitAndQte.fromJson(value)).toList();
  }

  static Map<String, ProduitAndQte> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ProduitAndQte>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ProduitAndQte.fromJson(value));
    }
    return map;
  }
}
