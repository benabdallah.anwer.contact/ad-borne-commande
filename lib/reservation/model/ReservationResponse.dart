import 'ListReservations.dart';
import 'VReservationTable.dart';

class ReservationResponse {
  String nomClient;
  String prenomClient;
  String telClient;
  VReservationTable vReservationTable;
  //List<ListReservations> listReservations;

  ReservationResponse(
      {this.nomClient,
      this.prenomClient,
      this.telClient});

  ReservationResponse.fromJson(Map<String, dynamic> json) {
    nomClient = json['nomClient'];
    prenomClient = json['prenomClient'];
    telClient = json['telClient'];
    vReservationTable = json['vReservationTable'] != null
        ? new VReservationTable.fromJson(json['vReservationTable'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nomClient'] = this.nomClient;
    data['prenomClient'] = this.prenomClient;
    data['telClient'] = this.telClient;
    if (this.vReservationTable != null) {
      data['vReservationTable'] = this.vReservationTable.toJson();
    }
    return data;
  }
}
