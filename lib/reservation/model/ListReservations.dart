import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/reservation/model/produit_and_qte.dart';

class ListReservations {
  List<ProduitAndQte> produitsQtes = [];
  String idReservation;
  String idConsommateur;
  String idTable;
  String idSession;
  DateTime dateReseravation;
  int nbrPers;
  int fTraite;
  String src;
  String idEmployeeService;
  String idPointVente;
  String idproduitPointVente;
  DateTime dateFinReseravation;
  TableCaisse tableCaisse;

  ListReservations(
      {this.idReservation,
      this.idConsommateur,
      this.idTable,
      this.idSession,
      this.dateReseravation,
      this.nbrPers,
      this.fTraite,
      this.src,
      this.idEmployeeService,
      this.idPointVente,
      this.idproduitPointVente,
      this.dateFinReseravation});

  ListReservations.fromJson(Map<String, dynamic> json) {
    idReservation = json['idReservation'];
    idConsommateur = json['idConsommateur'];
    idTable = json['idTable'];
    idSession = json['idSession'];
    dateReseravation = json['dateReseravation'] == null
        ? null
        : DateTime.parse(json['dateReseravation']);
    nbrPers = json['nbrPers'];
    fTraite = json['fTraite'];
    src = json['src'];
    idEmployeeService = json['idEmployeeService'];
    idPointVente = json['idPointVente'];
    idproduitPointVente = json['idproduitPointVente'];
    dateFinReseravation = json['dateFinReseravation'] == null
        ? null
        : DateTime.parse(json['dateFinReseravation']);
    tableCaisse = json['tableCaisse'] != null
        ? new TableCaisse.fromJson(json['tableCaisse'])
        : null;
    produitsQtes = ProduitAndQte.listFromJson(json['produitsQtes']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idReservation'] = this.idReservation;
    data['idConsommateur'] = this.idConsommateur;
    data['idTable'] = this.idTable;
    data['idSession'] = this.idSession;
    data['dateReseravation'] = this.dateReseravation;
    data['nbrPers'] = this.nbrPers;
    data['fTraite'] = this.fTraite;
    data['src'] = this.src;
    data['idEmployeeService'] = this.idEmployeeService;
    data['idPointVente'] = this.idPointVente;
    data['idproduitPointVente'] = this.idproduitPointVente;
    data['dateFinReseravation'] = this.dateFinReseravation;
    if (this.produitsQtes != null) {
      data['produitsQtes'] = this.produitsQtes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
