import 'package:ad_caisse/admin/model/utilisateur.dart';
import 'package:ad_caisse/pos/model/table_caisse.dart';
import 'package:ad_caisse/reservation/model/produit_and_qte.dart';

class VReservationTable {
  String idReservation;
  String idConsommateur;
  String idTable;
  String idSession;
  DateTime dateReseravation;
  int nbrPers;
  int fTraite;
  String src;
  String idEmployeeService;
  String idPointVente;
  List<ProduitAndQte> produitsQtes = [];
  DateTime dateFinReseravation;
  TableCaisse tableCaisse;
  Utilisateur employee;
  String nom;
  String prenom;
  String nTel;
  String commentaire;

  VReservationTable(
      {this.idReservation,
      this.idConsommateur,
      this.idTable,
      this.idSession,
      this.dateReseravation,
      this.nbrPers,
      this.fTraite,
      this.src,
      this.idEmployeeService,
      this.idPointVente,
      this.produitsQtes,
      this.dateFinReseravation,
      this.tableCaisse,
      this.employee});

  VReservationTable.fromJson(Map<String, dynamic> json) {
    nom = json['nom'];
    prenom = json['prenom'];
    commentaire = json['commentaire'];
    nTel = json['nTel'];
    idReservation = json['idReservation'];
    idConsommateur = json['idConsommateur'];
    idTable = json['idTable'];
    idSession = json['idSession'];
    dateReseravation = json['dateReseravation'] != null
        ? DateTime.parse(json['dateReseravation'])
        : null;
    nbrPers = json['nbrPers'];
    fTraite = json['fTraite'];
    src = json['src'];
    idEmployeeService = json['idEmployeeService'];
    idPointVente = json['idPointVente'];
    if (json['produitsQtes'] != null) {
      produitsQtes = new List<ProduitAndQte>();
      json['produitsQtes'].forEach((v) {
        produitsQtes.add(new ProduitAndQte.fromJson(v));
      });
    }
    dateFinReseravation = json['dateFinReseravation'] != null
        ? DateTime.parse(json['dateFinReseravation'])
        : null;
    tableCaisse = json['tableCaisse'] != null
        ? new TableCaisse.fromJson(json['tableCaisse'])
        : null;
    employee = json['employee'] != null
        ? Utilisateur.fromJson(json['employee'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nom'] = this.nom;
    data['commentaire'] = this.commentaire;
    data['prenom'] = this.prenom;
    data['nTel'] = this.nTel;
    data['idReservation'] = this.idReservation;
    data['idConsommateur'] = this.idConsommateur;
    data['idTable'] = this.idTable;
    data['idSession'] = this.idSession;
    data['dateReseravation'] = this.dateReseravation;
    data['nbrPers'] = this.nbrPers;
    data['fTraite'] = this.fTraite;
    data['src'] = this.src;
    data['idEmployeeService'] = this.idEmployeeService;
    data['idPointVente'] = this.idPointVente;
    if (this.produitsQtes != null) {
      data['produitsQtes'] = this.produitsQtes.map((v) => v.toJson()).toList();
    }
    data['dateFinReseravation'] = this.dateFinReseravation;
    if (this.tableCaisse != null) {
      data['tableCaisse'] = this.tableCaisse.toJson();
    }
    data['employee'] = this.employee != null ? this.employee.toJson() : null;
    return data;
  }
}
