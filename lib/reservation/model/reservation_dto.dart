import 'package:ad_caisse/reservation/model/produit_and_qte.dart';

class ReservationDto {
  String calledfrom;

  String commentaire = null;

  DateTime dateFinReseravation = null;

  DateTime dateReseravation = null;

  int fTraite = null;

  String idConsommateur = null;

  String idEmployeeService = null;

  String idPointVente = null;

  String idReservation = null;

  String idSession = null;

  String idTable = null;

  int nbrPers = null;

  List<ProduitAndQte> produitsQtes = [];

  String src = null;

  String nom;
  String prenom;
  String nTel;

  ReservationDto();

  @override
  String toString() {
    return 'ReservationDto[commentaire=$commentaire, dateFinReseravation=$dateFinReseravation, dateReseravation=$dateReseravation, fTraite=$fTraite, idConsommateur=$idConsommateur, idEmployeeService=$idEmployeeService, idPointVente=$idPointVente, idReservation=$idReservation, idSession=$idSession, idTable=$idTable, nbrPers=$nbrPers, produitsQtes=$produitsQtes, src=$src, ]';
  }

  ReservationDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    calledfrom = json['calledfrom'];
    nom = json['nom'];
    prenom = json['prenom'];
    nTel = json['nTel'];
    commentaire = json['commentaire'];
    dateFinReseravation = json['dateFinReseravation'] == null
        ? null
        : DateTime.parse(json['dateFinReseravation']).toUtc();
    dateReseravation = json['dateReseravation'] == null
        ? null
        : DateTime.parse(json['dateReseravation']).toUtc();
    fTraite = json['fTraite'];
    idConsommateur = json['idConsommateur'];
    idEmployeeService = json['idEmployeeService'];
    idPointVente = json['idPointVente'];
    idReservation = json['idReservation'];
    idSession = json['idSession'];
    idTable = json['idTable'];
    nbrPers = json['nbrPers'];
    produitsQtes = ProduitAndQte.listFromJson(json['produitsQtes']);
    src = json['src'];
  }

  Map<String, dynamic> toJson() {
    return {
      'calledfrom': calledfrom,
      'nom': nom,
      'prenom': prenom,
      'nTel': nTel,
      'commentaire': commentaire,
      'dateFinReseravation': dateFinReseravation == null
          ? ''
          : dateFinReseravation.toIso8601String(),
      'dateReseravation':
          dateReseravation == null ? '' : dateReseravation.toIso8601String(),
      'fTraite': fTraite,
      'idConsommateur': idConsommateur,
      'idEmployeeService': idEmployeeService,
      'idPointVente': idPointVente,
      'idReservation': idReservation,
      'idSession': idSession,
      'idTable': idTable,
      'nbrPers': nbrPers,
      'produitsQtes': produitsQtes,
      'src': src
    };
  }

  static List<ReservationDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ReservationDto>()
        : json.map((value) => new ReservationDto.fromJson(value)).toList();
  }

  static Map<String, ReservationDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ReservationDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ReservationDto.fromJson(value));
    }
    return map;
  }
}
