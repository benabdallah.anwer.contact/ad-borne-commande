import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/reservation/model/reservation_dto.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class ReservationEndPointApi {
  final ApiClient apiClient;

  ReservationEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// Créer une nouvelle réservation
  ///
  /// Retourner La réservation créé. (SRC : 0 --&gt; caisse, 1--&gt; Mobile App)  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes : Fstatut : 0--&gt; crée&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Client est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> createReservationUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/createReservation".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Rechercher Les réservation par Client et par point de vente et par date
  ///
  /// passer les paramètres suivants :  String idConsommateur,String idPointVente , Date dateReservation.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Liste de réservation remplie  et retournée &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Liste vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findReservationByClientAndPointVenteAndDateUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;

    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/findReservationByClientAndPointVenteAndDate"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Rechercher Les réservation par Client et par point de vente
  ///
  /// passer les paramètres suivants :  String idConsommateur,String idPointVente .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Liste de réservation remplie  et retournée &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Liste vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findReservationByClientAndPointVenteUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;

    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/findReservationByClientAndPointVente"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Rechercher Les réservation par date, par point de vente et par produit
  ///
  /// passer les paramètres suivants :  Date dateReservation , String idPointVente, String idProduct .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Liste de réservation remplie  et retournée &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Liste vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D;-3 :&lt;/b&gt;Exception  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList>
      findReservationByDateAndIdPointVenteAndIdProductUsingPOST(
          ReservationDto reservationDto) async {
    Object postBody = reservationDto;

    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/findReservationByDateAndIdPointVenteAndProduct"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Rechercher Les réservation par date et par point de vente
  ///
  /// passer les paramètres suivants :  Date dateReservation , String idPointVente .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Liste de réservation remplie  et retournée &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Liste vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D;-3 :&lt;/b&gt;Exception  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findReservationByDateAndIdPointVenteUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;

    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/findReservationByDateAndIdPointVente"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Rechercher Les réservation par source et par point de vente
  ///
  /// passer les paramètres suivants :  String source,String idPointVente .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Liste de réservation remplie  et retournée &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Liste vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findReservationBySrcAndIdPointVenteUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;

    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/findReservationBySrcAndIdPointVente"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Rechercher Les réservation par Status et par point de vente
  ///
  /// passer les paramètres suivants :  short status : 0 : crée / 1 : traitée / -1 : annulée ,String idPointVente .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;Liste de réservation remplie  et retournée &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Liste vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findReservationByStatusAndIdPointVenteUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;
    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/findReservationByStatusAndIdPointVente"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// BTraiter ou annuler une réservation
  ///
  /// Retourner La réservation mise à jour, 1 --&gt; traité , -1 --&gt; annulée  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Réservation Mise à jour avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object Réservation est vide  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> treatOrCancelReservationUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;

    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/treatOrCancelReservation".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Mise à jour d&#39;une réservation
  ///
  /// Mettre à jour la réservation : passer l&#39;objet :reservationDto et renseigner éventuellement (la date, la table, le nbr Personne, l&#39;id employee, le statut, la dateFinReservation) , l&#39;Id reservation est obligatoire  .  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Mise à jour avec succès&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Réservation inexistant  &lt;b&gt;result &#x3D;-1 :&lt;/b&gt;un paramètre est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> updateReservationUsingPOST(
      ReservationDto reservationDto) async {
    Object postBody = reservationDto;
    // verify required params are set
    if (reservationDto == null) {
      throw new ApiException(400, "Missing required param: reservationDto");
    }

    // create path and map variables
    String path = "/v1/updateReservation".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }
}
