import 'package:ad_caisse/loyality/model/commande_partner_dto.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class FidCredEndPointApi {
  final ApiClient apiClient;

  FidCredEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer et verifier les reglements concernant  une transaction de paiement
  ///
  /// Retourner le code 1 si tous les conditions sont remplis.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Remboursement point de fidélité effectué avec succés&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;La commande n&#39;existe pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt;La commande n&#39;est pas encore payée  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; Aucun réglement n&#39;a été effecuté sur cette commande vous ne pouvez pas bénificier des points de Fid  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; vous ne pouvez pas bénificier des points de Fid car tous les reglements sont effectuées par la carte Fid  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; Aucun client est identifié pour cette transaction  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt;client n&#39;exist pas  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt;client n&#39;est plus actif  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt;Pas de point de fidélité pour cette commande  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> checkAndSavePaymentUsingPOST(CommandePartnerDto cmd) async {
    Object postBody = cmd;

    // verify required params are set
    if (cmd == null) {
      throw new ApiException(400, "Missing required param: cmd");
    }

    // create path and map variables
    String path = "/v1/checkAndSavePayment".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      // print("FidCheckAndSavePayment: " + response.body);
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
