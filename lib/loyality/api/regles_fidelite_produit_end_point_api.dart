import 'package:ad_caisse/loyality/model/regles_fidelite_produit.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class ReglesFideliteProduitEndPointApi {
  final ApiClient apiClient;

  ReglesFideliteProduitEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer une ReglesFideliteProduit
  ///
  /// Retourner la ReglesFideliteProduit créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;ReglesFideliteProduit est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createReglesFideliteProduitUsingPOST(
      ReglesFideliteProduit reglesFideliteProduit) async {
    Object postBody = reglesFideliteProduit;

    // verify required params are set
    if (reglesFideliteProduit == null) {
      throw new ApiException(
          400, "Missing required param: reglesFideliteProduit");
    }

    // create path and map variables
    String path =
        "/v1/CreateReglesFideliteProduit".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une ReglesFideliteProduit
  ///
  /// Retourner une valeur boolean qui indique si la ReglesFideliteProduit a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;ReglesFideliteProduit n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteReglesFideliteProduitUsingDELETE(
      String idReglesFideliteProduit) async {
    Object postBody = null;

    // verify required params are set
    if (idReglesFideliteProduit == null) {
      throw new ApiException(
          400, "Missing required param: idReglesFideliteProduit");
    }

    // create path and map variables
    String path = "/v1/DeleteReglesFideliteProduit/{idReglesFideliteProduit}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idReglesFideliteProduit" + "}",
            idReglesFideliteProduit.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher une liste ReglesFideliteProduit selon l&#39;idPartenaire envoyer
  ///
  /// Retourner une liste ReglesFideliteProduit selon l&#39;idPartenaire envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List ReglesFideliteProduit n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List ReglesFideliteProduit est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPartenaireUsingGET(String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path =
        "/v1/findAllReglesFideliteProduitByIdPartenaire/{idPartenaire}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la ReglesFideliteProduit selon l&#39;idReglesFideliteProduit envoyer
  ///
  /// Retourner la ReglesFideliteProduit selon l&#39;idReglesFideliteProduit envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List ReglesFideliteProduit n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List ReglesFideliteProduit est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPointVenteIsNullUsingGET(String idProduit) async {
    Object postBody = null;

    // verify required params are set
    if (idProduit == null) {
      throw new ApiException(400, "Missing required param: idProduit");
    }

    // create path and map variables
    String path = "/v1/findAllReglesFideliteProduitByIdProduit/{idProduit}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idProduit" + "}", idProduit.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la ReglesFideliteProduit selon l&#39;idReglesFideliteProduit envoyer
  ///
  /// Retourner la ReglesFideliteProduit selon l&#39;idReglesFideliteProduit envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; ReglesFideliteProduit exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;ReglesFideliteProduit n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdReglesFideliteProduitUsingGET(
      String idReglesFideliteProduit) async {
    Object postBody = null;

    // verify required params are set
    if (idReglesFideliteProduit == null) {
      throw new ApiException(
          400, "Missing required param: idReglesFideliteProduit");
    }

    // create path and map variables
    String path = "/v1/findByIdReglesFideliteProduit/{idReglesFideliteProduit}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idReglesFideliteProduit" + "}",
            idReglesFideliteProduit.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
