import 'package:ad_caisse/loyality/model/regle_utilisation_fidelite.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class RegleUtilisationFideliteEndPointApi {
  final ApiClient apiClient;

  RegleUtilisationFideliteEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer une RegleUtilisationFidelite
  ///
  /// Retourner la RegleUtilisationFidelite créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;RegleUtilisationFidelite est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createRegleUtilisationFideliteUsingPOST(
      RegleUtilisationFidelite regleUtilisationFidelite) async {
    Object postBody = regleUtilisationFidelite;

    // verify required params are set
    if (regleUtilisationFidelite == null) {
      throw new ApiException(
          400, "Missing required param: regleUtilisationFidelite");
    }

    // create path and map variables
    String path =
        "/v1/CreateRegleUtilisationFidelite".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une RegleUtilisationFidelite
  ///
  /// Retourner une valeur boolean qui indique si la RegleUtilisationFidelite a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;RegleUtilisationFidelite n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteRegleUtilisationFideliteUsingDELETE(
      String idRegleUtilisationFidelite) async {
    Object postBody = null;

    // verify required params are set
    if (idRegleUtilisationFidelite == null) {
      throw new ApiException(
          400, "Missing required param: idRegleUtilisationFidelite");
    }

    // create path and map variables
    String path =
        "/v1/DeleteRegleUtilisationFidelite/{idRegleUtilisationFidelite}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idRegleUtilisationFidelite" + "}",
                idRegleUtilisationFidelite.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des RegleUtilisationFidelites selon l&#39;IdPointVente envoyer
  ///
  /// Retourner la list des RegleUtilisationFidelites selon l&#39;IdPointVente envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List RegleUtilisationFidelite n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List RegleUtilisationFidelite est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un ou plusieurs parametres envoyer sont null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllRegleUtilisationByIdPointVenteUsingGET(
      String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/findAllRegleUtilisationByIdPointVente/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la RegleUtilisationFidelite selon l&#39;idRegleUtilisationFidelite envoyer
  ///
  /// Retourner la RegleUtilisationFidelite selon l&#39;idRegleUtilisationFidelite envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; RegleUtilisationFidelite exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;RegleUtilisationFidelite n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdRegleUtilisationFideliteUsingGET(
      String idRegleUtilisationFidelite) async {
    Object postBody = null;

    // verify required params are set
    if (idRegleUtilisationFidelite == null) {
      throw new ApiException(
          400, "Missing required param: idRegleUtilisationFidelite");
    }

    // create path and map variables
    String path =
        "/v1/findByIdRegleUtilisationFidelite/{idRegleUtilisationFidelite}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idRegleUtilisationFidelite" + "}",
                idRegleUtilisationFidelite.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier une RegleUtilisationFidelite
  ///
  /// Retourner la RegleUtilisationFidelite modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;RegleUtilisationFidelite est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; RegleUtilisationFidelite n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateRegleUtilisationFideliteUsingPUT(
      RegleUtilisationFidelite regleUtilisationFidelite) async {
    Object postBody = regleUtilisationFidelite;

    // verify required params are set
    if (regleUtilisationFidelite == null) {
      throw new ApiException(
          400, "Missing required param: regleUtilisationFidelite");
    }

    // create path and map variables
    String path =
        "/v1/UpdateRegleUtilisationFidelite".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
