
class JoursRegle {
  
  String code = null;
  

  String debut = null;
  

  String designation = null;
  

  int fAllDay = null;
  

  String fin = null;
  
  JoursRegle();

  @override
  String toString() {
    return 'JoursRegle[code=$code, debut=$debut, designation=$designation, fAllDay=$fAllDay, fin=$fin, ]';
  }

  JoursRegle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    code =
        json['code']
    ;
    debut =
        json['debut']
    ;
    designation =
        json['designation']
    ;
    fAllDay =
        json['fAllDay']
    ;
    fin =
        json['fin']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'debut': debut,
      'designation': designation,
      'fAllDay': fAllDay,
      'fin': fin
     };
  }

  static List<JoursRegle> listFromJson(List<dynamic> json) {
    return json == null ? new List<JoursRegle>() : json.map((value) => new JoursRegle.fromJson(value)).toList();
  }

  static Map<String, JoursRegle> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, JoursRegle>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new JoursRegle.fromJson(value));
    }
    return map;
  }
}

