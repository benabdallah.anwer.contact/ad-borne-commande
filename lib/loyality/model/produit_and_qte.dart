
class ProduitAndQte {
  
  String idProduit = null;
  

  int quantite = null;
  
  ProduitAndQte();

  @override
  String toString() {
    return 'ProduitAndQte[idProduit=$idProduit, quantite=$quantite, ]';
  }

  ProduitAndQte.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idProduit =
        json['idProduit']
    ;
    quantite =
        json['quantite']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'idProduit': idProduit,
      'quantite': quantite
     };
  }

  static List<ProduitAndQte> listFromJson(List<dynamic> json) {
    return json == null ? new List<ProduitAndQte>() : json.map((value) => new ProduitAndQte.fromJson(value)).toList();
  }

  static Map<String, ProduitAndQte> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ProduitAndQte>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new ProduitAndQte.fromJson(value));
    }
    return map;
  }
}

