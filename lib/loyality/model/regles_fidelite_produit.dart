import 'package:ad_caisse/loyality/model/point_vente.dart';
import 'package:ad_caisse/loyality/model/produit_and_qte.dart';

import 'jours_regle.dart';

class ReglesFideliteProduit {
  DateTime dateDebut = null;

  DateTime dateFin = null;

  int fAutorisGerant = null;

  int fMoinsCherGratuit = null;

  int fPourcentage = null;

  String idPack = null;

  String idPartenaireBprice = null;

  String idProduit = null;

  String idRegle = null;

  int isActif = null;

  List<JoursRegle> joursUtilisation = [];

  int palMontantMin = null;

  int palMontatntMax = null;

  List<PointVente> pointsVentes = [];

  List<ProduitAndQte> produitsCombines = [];

  int quantite = null;

  String typeFid = null;

  double valeur = null;

  ReglesFideliteProduit();

  @override
  String toString() {
    return 'ReglesFideliteProduit[dateDebut=$dateDebut, dateFin=$dateFin, fAutorisGerant=$fAutorisGerant, fMoinsCherGratuit=$fMoinsCherGratuit, fPourcentage=$fPourcentage, idPack=$idPack, idPartenaireBprice=$idPartenaireBprice, idProduit=$idProduit, idRegle=$idRegle, isActif=$isActif, joursUtilisation=$joursUtilisation, palMontantMin=$palMontantMin, palMontatntMax=$palMontatntMax, pointsVentes=$pointsVentes, produitsCombines=$produitsCombines, quantite=$quantite, typeFid=$typeFid, valeur=$valeur, ]';
  }

  ReglesFideliteProduit.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateDebut =
        json['dateDebut'] == null ? null : DateTime.parse(json['dateDebut']);
    dateFin = json['dateFin'] == null ? null : DateTime.parse(json['dateFin']);
    fAutorisGerant = json['fAutorisGerant'];
    fMoinsCherGratuit = json['fMoinsCherGratuit'];
    fPourcentage = json['fPourcentage'];
    idPack = json['idPack'];
    idPartenaireBprice = json['idPartenaireBprice'];
    idProduit = json['idProduit'];
    idRegle = json['idRegle'];
    isActif = json['isActif'];
    joursUtilisation = JoursRegle.listFromJson(json['joursUtilisation']);
    palMontantMin = json['palMontantMin'];
    palMontatntMax = json['palMontatntMax'];
    pointsVentes = PointVente.listFromJson(json['pointsVentes']);
    produitsCombines = ProduitAndQte.listFromJson(json['produitsCombines']);
    quantite = json['quantite'];
    typeFid = json['typeFid'];
    valeur = json['valeur'];
  }

  Map<String, dynamic> toJson() {
    return {
      'dateDebut': dateDebut == null ? '' : dateDebut.toIso8601String(),
      'dateFin': dateFin == null ? '' : dateFin.toIso8601String(),
      'fAutorisGerant': fAutorisGerant,
      'fMoinsCherGratuit': fMoinsCherGratuit,
      'fPourcentage': fPourcentage,
      'idPack': idPack,
      'idPartenaireBprice': idPartenaireBprice,
      'idProduit': idProduit,
      'idRegle': idRegle,
      'isActif': isActif,
      'joursUtilisation': joursUtilisation,
      'palMontantMin': palMontantMin,
      'palMontatntMax': palMontatntMax,
      'pointsVentes': pointsVentes,
      'produitsCombines': produitsCombines,
      'quantite': quantite,
      'typeFid': typeFid,
      'valeur': valeur
    };
  }

  static List<ReglesFideliteProduit> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ReglesFideliteProduit>()
        : json
            .map((value) => new ReglesFideliteProduit.fromJson(value))
            .toList();
  }

  static Map<String, ReglesFideliteProduit> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ReglesFideliteProduit>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ReglesFideliteProduit.fromJson(value));
    }
    return map;
  }
}
