
import 'jours_regle.dart';

class RegleUtilisationFidelite {
  
  String idPointVente = null;
  

  String idRegleUtilisationFidelite = null;
  

  int isActive = null;
  

  List<JoursRegle> joursUtilisation = [];
  

  int minPresent = null;
  

  double minSolde = null;
  
  RegleUtilisationFidelite();

  @override
  String toString() {
    return 'RegleUtilisationFidelite[idPointVente=$idPointVente, idRegleUtilisationFidelite=$idRegleUtilisationFidelite, isActive=$isActive, joursUtilisation=$joursUtilisation, minPresent=$minPresent, minSolde=$minSolde, ]';
  }

  RegleUtilisationFidelite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idPointVente =
        json['idPointVente']
    ;
    idRegleUtilisationFidelite =
        json['idRegleUtilisationFidelite']
    ;
    isActive =
        json['isActive']
    ;
    joursUtilisation =
      JoursRegle.listFromJson(json['joursUtilisation'])
;
    minPresent =
        json['minPresent']
    ;
    minSolde =
        json['minSolde']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'idPointVente': idPointVente,
      'idRegleUtilisationFidelite': idRegleUtilisationFidelite,
      'isActive': isActive,
      'joursUtilisation': joursUtilisation,
      'minPresent': minPresent,
      'minSolde': minSolde
     };
  }

  static List<RegleUtilisationFidelite> listFromJson(List<dynamic> json) {
    return json == null ? new List<RegleUtilisationFidelite>() : json.map((value) => new RegleUtilisationFidelite.fromJson(value)).toList();
  }

  static Map<String, RegleUtilisationFidelite> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, RegleUtilisationFidelite>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new RegleUtilisationFidelite.fromJson(value));
    }
    return map;
  }
}

