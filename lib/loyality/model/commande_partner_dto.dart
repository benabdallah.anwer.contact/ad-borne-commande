
class CommandePartnerDto {
  
  String idCommande = null;
  

  String idPartenaire = null;
  
  CommandePartnerDto();

  @override
  String toString() {
    return 'CommandePartnerDto[idCommande=$idCommande, idPartenaire=$idPartenaire, ]';
  }

  CommandePartnerDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idCommande =
        json['idCommande']
    ;
    idPartenaire =
        json['idPartenaire']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'idCommande': idCommande,
      'idPartenaire': idPartenaire
     };
  }

  static List<CommandePartnerDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<CommandePartnerDto>() : json.map((value) => new CommandePartnerDto.fromJson(value)).toList();
  }

  static Map<String, CommandePartnerDto> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CommandePartnerDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new CommandePartnerDto.fromJson(value));
    }
    return map;
  }
}

