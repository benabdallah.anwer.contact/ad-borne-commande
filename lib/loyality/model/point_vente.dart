import 'mode_reglement.dart';

class PointVente {
  String adresse = null;

  String chiffrevirgule = null;

  int coordX = null;

  int coordY = null;

  DateTime dateCreation = null;

  String defaultouverture = null;

  String designation = null;

  String differenceautorise = null;

  String enteteTicket = null;

  int fActif = null;

  int fClavierVirtuel = null;

  int fControlCaisse = null;

  int fDetailMontant = null;

  int fEntetePied = null;

  int fGestionTable = null;

  int fImprimCuisine = null;

  int fImprimeAvP = null;

  int fPartageAdition = null;

  int fPhotoCateg = null;

  int fReImprim = null;

  int faffectcollab = null;

  int fdetectPack = null;

  int fprixttc = null;

  String idPartenaire = null;

  String idPointVente = null;

  List<ModeReglement> modeReglements = [];

  String piedTicket = null;

  PointVente();

  @override
  String toString() {
    return 'PointVente[adresse=$adresse, chiffrevirgule=$chiffrevirgule, coordX=$coordX, coordY=$coordY, dateCreation=$dateCreation, defaultouverture=$defaultouverture, designation=$designation, differenceautorise=$differenceautorise, enteteTicket=$enteteTicket, fActif=$fActif, fClavierVirtuel=$fClavierVirtuel, fControlCaisse=$fControlCaisse, fDetailMontant=$fDetailMontant, fEntetePied=$fEntetePied, fGestionTable=$fGestionTable, fImprimCuisine=$fImprimCuisine, fImprimeAvP=$fImprimeAvP, fPartageAdition=$fPartageAdition, fPhotoCateg=$fPhotoCateg, fReImprim=$fReImprim, faffectcollab=$faffectcollab, fdetectPack=$fdetectPack, fprixttc=$fprixttc, idPartenaire=$idPartenaire, idPointVente=$idPointVente, modeReglements=$modeReglements, piedTicket=$piedTicket, ]';
  }

  PointVente.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    adresse = json['adresse'];
    chiffrevirgule = json['chiffrevirgule'];
    coordX = json['coordX'];
    coordY = json['coordY'];
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    defaultouverture = json['defaultouverture'];
    designation = json['designation'];
    differenceautorise = json['differenceautorise'];
    enteteTicket = json['enteteTicket'];
    fActif = json['fActif'];
    fClavierVirtuel = json['fClavierVirtuel'];
    fControlCaisse = json['fControlCaisse'];
    fDetailMontant = json['fDetailMontant'];
    fEntetePied = json['fEntetePied'];
    fGestionTable = json['fGestionTable'];
    fImprimCuisine = json['fImprimCuisine'];
    fImprimeAvP = json['fImprimeAvP'];
    fPartageAdition = json['fPartageAdition'];
    fPhotoCateg = json['fPhotoCateg'];
    fReImprim = json['fReImprim'];
    faffectcollab = json['faffectcollab'];
    fdetectPack = json['fdetectPack'];
    fprixttc = json['fprixttc'];
    idPartenaire = json['idPartenaire'];
    idPointVente = json['idPointVente'];
    modeReglements = ModeReglement.listFromJson(json['modeReglements']);
    piedTicket = json['piedTicket'];
  }

  Map<String, dynamic> toJson() {
    return {
      'adresse': adresse,
      'chiffrevirgule': chiffrevirgule,
      'coordX': coordX,
      'coordY': coordY,
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'defaultouverture': defaultouverture,
      'designation': designation,
      'differenceautorise': differenceautorise,
      'enteteTicket': enteteTicket,
      'fActif': fActif,
      'fClavierVirtuel': fClavierVirtuel,
      'fControlCaisse': fControlCaisse,
      'fDetailMontant': fDetailMontant,
      'fEntetePied': fEntetePied,
      'fGestionTable': fGestionTable,
      'fImprimCuisine': fImprimCuisine,
      'fImprimeAvP': fImprimeAvP,
      'fPartageAdition': fPartageAdition,
      'fPhotoCateg': fPhotoCateg,
      'fReImprim': fReImprim,
      'faffectcollab': faffectcollab,
      'fdetectPack': fdetectPack,
      'fprixttc': fprixttc,
      'idPartenaire': idPartenaire,
      'idPointVente': idPointVente,
      'modeReglements': modeReglements,
      'piedTicket': piedTicket
    };
  }

  static List<PointVente> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<PointVente>()
        : json.map((value) => new PointVente.fromJson(value)).toList();
  }

  static Map<String, PointVente> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, PointVente>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new PointVente.fromJson(value));
    }
    return map;
  }
}
