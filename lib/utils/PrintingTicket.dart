import 'package:ad_caisse/request/Crashlytics.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:printing/printing.dart';

import 'AlertNotif.dart';

class PrintingTicket {
  void printTicket(
      Document doc, String printer, BuildContext context, bool notif) async {
    try {
      if (kIsWeb) {
        await Printing.layoutPdf(
            onLayout: (PdfPageFormat format) async => doc.save());
        return;
      }
      List<Printer> filtred =
          ServerUrl.list.where((element) => element.name == printer).toList();
      if (filtred.length == 0) {
        return;
      }
      Printer my_printer = filtred[0];


        await Printing.directPrintPdf(
            onLayout: (PdfPageFormat format) async => doc.save(),
            printer: my_printer);


    } catch (err) {
      debugPrint("Printing... " + err.toString());
      Crashlytics().sendEmail("PrintingTicket printTicket", err.toString());
      if (notif) {
        AlertNotif().alert(
            context, "Une erreur est survenue lors de l'impression du ticket");
      }
    }
  }
}
