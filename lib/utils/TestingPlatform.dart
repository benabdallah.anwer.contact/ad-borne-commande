
import 'dart:io';

class TestingPlatform{
  bool android() {
    try {
      return Platform.isAndroid;
    } catch (err) {
      return false;
    }
  }
  bool windows() {
    try {
      return Platform.isWindows;
    } catch (err) {
      return false;
    }
  }
}