import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:printing/printing.dart';

class ServerUrl {
  static String url = "";
  static String urlSynchro = "";
  static bool fid = true;
  static List<Printer> list = [];
  // String urlProd = "https://prod.twinsdigitallabs.tech/zull-serveur";
  // String urlDev = "https://dev.twinsdigitallabs.tech";

static List<String> familleone=[];
  static List<String> familletwo=[];
  static List<String> famillePrinterAux1=[];
  static List<String> famillePrinterAux2=[];

static void refreshFamille()async{
  LocalStorageInterface prefs = await LocalStorage.getInstance();
  String familleOne = prefs.getString('familleprincipalPrinter');
  if(familleOne != null) {
    familleOne.trim();
    String tr=  familleOne.replaceAll(' ', '');
    ServerUrl.familleone=  tr.split(",");
  }
  String familleCuisine =   prefs.getString('famillecuisinePrinter');
  if(familleCuisine != null) {
    familleCuisine.trim();
    String tr=  familleCuisine.replaceAll(' ', '');
    ServerUrl.familletwo=  tr.split(",");
  }
  String famillePrinterAux1 =   prefs.getString('famillePrinterAux1');
  if(famillePrinterAux1 != null) {
    familleCuisine.trim();
    String tr=  famillePrinterAux1.replaceAll(' ', '');
    ServerUrl.famillePrinterAux1=  tr.split(",");
  }
  String famillePrinterAux2 =   prefs.getString('famillePrinterAux2');
  if(famillePrinterAux2 != null) {
    familleCuisine.trim();
    String tr=  famillePrinterAux2.replaceAll(' ', '');
    ServerUrl.famillePrinterAux2=  tr.split(",");
  }

}
}
