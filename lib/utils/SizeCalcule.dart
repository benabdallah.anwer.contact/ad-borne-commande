import 'package:flutter/cupertino.dart';

class SizeClacule{
  double calcule(BuildContext context,double percent){
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double d = percent * 1.4;
    return width>1000 ? width/d:width / percent;
  }
}