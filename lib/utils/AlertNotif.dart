import 'package:flutter/material.dart';

class AlertNotif {
  Future<void> alert(BuildContext context, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Image.asset('assets/logo/logo.png',
                    width: 50, height: 50, fit: BoxFit.scaleDown),
                SizedBox(height: 16.0),
                Text(
                  message != null ? message : '',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('OK'),
              elevation: 0,
              color: new Color(0xFFCF5FFE),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
