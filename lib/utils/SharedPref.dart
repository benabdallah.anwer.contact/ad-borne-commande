import 'package:cross_local_storage/cross_local_storage.dart';
import 'dart:convert';

class SharedPref {
  Future<String> read(String key) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    String value = prefs.getString(key);
    print(key + ": " + value);
    return value;
  }

  save(String key, value) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    prefs.setString(key, json.encode(value));
  }

  remove(String key) async {
    LocalStorageInterface prefs = await LocalStorage.getInstance();
    prefs.remove(key);
  }
}
