import 'package:http/http.dart';

import '../api_client.dart';
import '../api.dart';
import '../api_exception.dart';

class FileEndPointApi {
  final ApiClient apiClient;

  FileEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  ///  upload file
  ///
  /// upload file.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt;file uploaded successfully &lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;upload failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> handleFileUploadUsingPOST(MultipartFile file) async {
    Object postBody = null;

    // verify required params are set
    if (file == null) {
      throw new ApiException(400, "Missing required param: file");
    }

    // create path and map variables
    String path = "/v1/upload".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["multipart/form-data"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (file != null) {
        hasFields = true;
        mp.fields['file'] = file.field;
        mp.files.add(file);
      }

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
