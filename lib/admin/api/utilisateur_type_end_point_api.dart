import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/utilisateur_type.dart';

class UtilisateurTypeEndPointApi {
  final ApiClient apiClient;

  UtilisateurTypeEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer plusieurs Type utilisateur
  ///
  /// Retourner les Types utilisateurs crées.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createManyTypeUtilisateurUsingPOST(
      List<UtilisateurType> utilisateurTypes) async {
    Object postBody = utilisateurTypes;

    // verify required params are set
    if (utilisateurTypes == null) {
      throw new ApiException(400, "Missing required param: utilisateurTypes");
    }

    // create path and map variables
    String path =
        "/v1/CreateManyTypeUtilisateur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// créer un Type  utilisateur
  ///
  /// Retourner le Type utilisateur créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createTypeUtilisateurUsingPOST(
      UtilisateurType utilisateurType) async {
    Object postBody = utilisateurType;

    // verify required params are set
    if (utilisateurType == null) {
      throw new ApiException(400, "Missing required param: utilisateurType");
    }

    // create path and map variables
    String path = "/v1/CreateTypeUtilisateur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer un Type utilisateur
  ///
  /// Retourner une valeur boolean qui indique si le Type utilisateur a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;TypeUtilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteTypeUtilisateurUsingDELETE(
      String idUtilisateurType) async {
    Object postBody = null;

    // verify required params are set
    if (idUtilisateurType == null) {
      throw new ApiException(400, "Missing required param: idUtilisateurType");
    }

    // create path and map variables
    String path = "/v1/DeleteTypeUtilisateur/{idUtilisateurType}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idUtilisateurType" + "}", idUtilisateurType.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs par FActif
  ///
  /// Retourner la liste de tous les Types utilisateurs par FActif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFActifUsingGET3(int factif) async {
    Object postBody = null;

    // verify required params are set
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllTypeUtilisateurByFActif/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs
  ///
  /// Retourner la liste de tous les Types utilisateurs.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFActifUsingGET4() async {
    Object postBody = null;

    // verify required params are set

    // create path and map variables
    String path = "/v1/findAllTypeUtilisateur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs par FBackoffice et FActif
  ///
  /// Retourner la liste de tous les Types utilisateurs par FBackoffice et FActif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;UList TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFBackofficeAndFActifUsingGET(
      int fBackoffice, int factif) async {
    Object postBody = null;

    // verify required params are set
    if (fBackoffice == null) {
      throw new ApiException(400, "Missing required param: fBackoffice");
    }
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllByFBackofficeAndFActif/{fBackoffice}/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "fBackoffice" + "}", fBackoffice.toString())
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs par FBackoffice
  ///
  /// Retourner la liste de tous les Types utilisateurs par FBackoffice.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;UList TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFBackofficeUsingGET(int fBackoffice) async {
    Object postBody = null;

    // verify required params are set
    if (fBackoffice == null) {
      throw new ApiException(400, "Missing required param: fBackoffice");
    }

    // create path and map variables
    String path = "/v1/findAllByFBackoffice/{fBackoffice}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "fBackoffice" + "}", fBackoffice.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs par FCaisse et FActif
  ///
  /// Retourner la liste de tous les Types utilisateurs par FCaisse et FActif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;UList TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFCaisseAndFActifUsingGET(
      String fCaisse, int factif) async {
    Object postBody = null;

    // verify required params are set
    if (fCaisse == null) {
      throw new ApiException(400, "Missing required param: fCaisse");
    }
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllByFCaisseAndFActif/{fCaisse}/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "fCaisse" + "}", fCaisse.toString())
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs par FCaisse
  ///
  /// Retourner la liste de tous les Types utilisateurs par FCaisse.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;UList TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFCaisseUsingGET(String fCaisse) async {
    Object postBody = null;

    // verify required params are set
    if (fCaisse == null) {
      throw new ApiException(400, "Missing required param: fCaisse");
    }

    // create path and map variables
    String path = "/v1/findAllByFCaisse/{fCaisse}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "fCaisse" + "}", fCaisse.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs par FGerant et FActif
  ///
  /// Retourner la liste de tous les Types utilisateurs par FGerant et FActif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;UList TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFGerantAndFActifUsingGET(
      int fGerant, int factif) async {
    Object postBody = null;

    // verify required params are set
    if (fGerant == null) {
      throw new ApiException(400, "Missing required param: fGerant");
    }
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllByFGerantAndFActif/{fGerant}/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "fGerant" + "}", fGerant.toString())
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Types utilisateurs par FGerant
  ///
  /// Retourner la liste de tous les Types utilisateurs par FGerant.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List TypeUtilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;UList TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFGerantUsingGET(int fGerant) async {
    Object postBody = null;

    // verify required params are set
    if (fGerant == null) {
      throw new ApiException(400, "Missing required param: fGerant");
    }

    // create path and map variables
    String path = "/v1/findAllByFGerant/{fGerant}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "fGerant" + "}", fGerant.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher le type utilisateur selon l&#39;idTypeUtilisateur envoyer
  ///
  /// Retourner le type utilisateur selon l&#39;idTypeUtilisateur envoyer.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; TypeUtilisateur exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;TypeUtilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> findByIdTypeUtilisateurUsingGET(
      String idTypeUtilisateur) async {
    Object postBody = null;

    // verify required params are set
    if (idTypeUtilisateur == null) {
      throw new ApiException(400, "Missing required param: idTypeUtilisateur");
    }

    // create path and map variables
    String path = "/v1/findByIdTypeUtilisateur/{idTypeUtilisateur}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idTypeUtilisateur" + "}", idTypeUtilisateur.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);
    ApiClient client = ApiClient();
    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      // debugPrint("findByIdTypeUtilisateur: "+response.body);
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Modifier un Type utilisateur
  ///
  /// Retourner le type utilisateur modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;TypeUtilisateur est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;TypeUtilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateTypeUtilisateurUsingPUT(
      UtilisateurType utilisateurType) async {
    Object postBody = utilisateurType;

    // verify required params are set
    if (utilisateurType == null) {
      throw new ApiException(400, "Missing required param: utilisateurType");
    }

    // create path and map variables
    String path = "/v1/UpdateTypeUtilisateur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
