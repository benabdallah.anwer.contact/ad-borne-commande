import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/employee_reservation_dto.dart';
import '../model/login_dto.dart';
import '../model/reset_password_dto.dart';
import '../model/utilisateur.dart';

class UtilisateurEndPointApi {
  final ApiClient apiClient;

  UtilisateurEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer plusieurs utilisateurs
  ///
  /// Retourner la liste des utilisateurs créés.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createManyUtilisateurUsingPOST(
      List<Utilisateur> utilisateurs) async {
    Object postBody = utilisateurs;

    // verify required params are set
    if (utilisateurs == null) {
      throw new ApiException(400, "Missing required param: utilisateurs");
    }

    // create path and map variables
    String path = "/v1/CreateManyUtilisateur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// créer un  utilisateur
  ///
  /// Retourner l&#39;utilisateur créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Utilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;TypeUtilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; login est null  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; login est utilisée  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; password est null  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt; type utilisateur est null  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt; partenaire n&#39;exist pas  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; profil n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createUtilisateurUsingPOST(Utilisateur utilisateur) async {
    Object postBody = utilisateur;

    // verify required params are set
    if (utilisateur == null) {
      throw new ApiException(400, "Missing required param: utilisateur");
    }

    // create path and map variables
    String path = "/v1/CreateUtilisateur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer un utilisateur
  ///
  /// Retourner une valeur boolean qui indique si l&#39;utilisateur a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;Utilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteUtilistaeurUsingDELETE(String idUtilisateur) async {
    Object postBody = null;

    // verify required params are set
    if (idUtilisateur == null) {
      throw new ApiException(400, "Missing required param: idUtilisateur");
    }

    // create path and map variables
    String path = "/v1/DeleteUtilisateur/{idUtilisateur}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idUtilisateur" + "}", idUtilisateur.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// changer mot de passe
  ///
  /// le mote de passe a ete changé  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; veuillez vérifier votre ancien mot de passe&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt; Utilisateur n&#39;exist pas&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt; Tun ou plusieurs parametres envoyer sont null&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;l&#39;object envoyer est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> editpaasworUsingPOST(ResetPasswordDto resetPasswordDto) async {
    Object postBody = resetPasswordDto;

    // verify required params are set
    if (resetPasswordDto == null) {
      throw new ApiException(400, "Missing required param: resetPasswordDto");
    }

    // create path and map variables
    String path = "/v1/Editpaaswor".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les utilisateurs par FActif
  ///
  /// Retourner la liste de tous les utilisateurs par FActif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFActifUsingGET2(int factif) async {
    Object postBody = null;

    // verify required params are set
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllByFActif/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les utilisateurs par IdPartenaireBrpice et FActif
  ///
  /// Retourner la liste de tous les utilisateurs par IdPartenaireBrpice et FActif  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPartenaireBrpiceAndFActifUsingGET(
      String idPartenaireBrpice, int factif) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaireBrpice == null) {
      throw new ApiException(400, "Missing required param: idPartenaireBrpice");
    }
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path =
        "/v1/findAllByIdPartenaireBrpiceAndFActif/{idPartenaireBrpice}/{factif}"
            .replaceAll("{format}", "json")
            .replaceAll(
                "{" + "idPartenaireBrpice" + "}", idPartenaireBrpice.toString())
            .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les utilisateurs par IdPartenaireBrpice
  ///
  /// Retourner la liste de tous les utilisateurs par IdPartenaireBrpice.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPartenaireBrpiceUsingGET(
      String idPartenaireBrpice) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaireBrpice == null) {
      throw new ApiException(400, "Missing required param: idPartenaireBrpice");
    }

    // create path and map variables
    String path = "/v1/findAllByIdPartenaireBrpice/{idPartenaireBrpice}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idPartenaireBrpice" + "}", idPartenaireBrpice.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les utilisateurs par IdPartenaire est null
  ///
  /// Retourner la liste de tous les utilisateurs par IdPartenaire est null.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdPartenaireIsNullUsingGET() async {
    Object postBody = null;

    // verify required params are set

    // create path and map variables
    String path =
        "/v1/findAllByIdPartenaireIsNull".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les utilisateurs par idPointVente
  ///
  /// Retourner la liste de tous les utilisateurs par idPointVente   &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseList> findAllByIdPointVenteUsingGET(
      String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/findAllByIdPointVente/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les utilisateurs par IdtypeUtilisateur et FActif
  ///
  Future<ResponseList> findAllByCodeTypeUtilisateurAndIdPointVenteUsingGET(
      String idPointVente , String code) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null||code == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/findAllByCodeTypeUtilisateurAndIdPointVente/{idPointVente}/{code}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString())
        .replaceAll("{" + "code" + "}", code.toString())
    ;

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
    contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as ResponseList;
    } else {
      return null;
    }
  }
  /// Retourner la liste de tous les utilisateurs par IdtypeUtilisateur et FActif  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdtypeUtilisateurAndFActifUsingGET(
      String idtypeUtilisateur, int factif) async {
    Object postBody = null;

    // verify required params are set
    if (idtypeUtilisateur == null) {
      throw new ApiException(400, "Missing required param: idtypeUtilisateur");
    }
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path =
        "/v1/findAllByIdtypeUtilisateurAndFActif/{idtypeUtilisateur}/{factif}"
            .replaceAll("{format}", "json")
            .replaceAll(
                "{" + "idtypeUtilisateur" + "}", idtypeUtilisateur.toString())
            .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les utilisateurs par IdtypeUtilisateur
  ///
  /// Retourner la liste de tous les utilisateurs par IdtypeUtilisateur.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdtypeUtilisateurUsingGET(
      String idtypeUtilisateur) async {
    Object postBody = null;

    // verify required params are set
    if (idtypeUtilisateur == null) {
      throw new ApiException(400, "Missing required param: idtypeUtilisateur");
    }

    // create path and map variables
    String path = "/v1/findAllByIdtypeUtilisateur/{idtypeUtilisateur}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idtypeUtilisateur" + "}", idtypeUtilisateur.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la list des TableCaisses selon l&#39;IdPointVente  envoyer
  ///
  /// Retourner la list des TableCaisses selon l&#39;IdPointVente  envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Utilisateur est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAlltableUsingPOST(
      EmployeeReservationDto employeeReservationDto) async {
    Object postBody = employeeReservationDto;

    // verify required params are set
    if (employeeReservationDto == null) {
      throw new ApiException(
          400, "Missing required param: employeeReservationDto");
    }

    // create path and map variables
    String path =
        "/v1/findfreeemplyeebyreservation".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher l&#39;utilisateur selon l&#39;idUtilisateur envoyer
  ///
  /// Retourner l&#39;utilisateur selon l&#39;idUtilisateur envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Utilisateur n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;Utilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findUtilisateurByIdUtilisateurUsingGET(
      String idUtilisateur) async {
    Object postBody = null;

    // verify required params are set
    if (idUtilisateur == null) {
      throw new ApiException(400, "Missing required param: idUtilisateur");
    }

    // create path and map variables
    String path = "/v1/findUtilisateurByIdUtilisateur/{idUtilisateur}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idUtilisateur" + "}", idUtilisateur.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
///id card
  ///
  Future<Object> findUtilisateurByIdCardUsingGET(
      String idCard) async {
    Object postBody = null;

    // verify required params are set
    if (idCard == null) {
      throw new ApiException(400, "Missing required param: idUtilisateur");
    }

    // create path and map variables
    String path = "/v1/findUtilisateurByIdCard/{idCard}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idCard" + "}", idCard.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
    contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await
    apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
      as ResponseSingle;
    } else {
      return null;
    }
  }
  /// authentification
  ///
  /// authentification   typeconnetion&#x3D;1 -&gt; une connexion à une caisse  typeconnetion&#x3D;2 -&gt; une connexion pour le backoffice &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; login avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; TypeUtilisateur est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt; veuillez verifier votre nom utilisateur&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt; TypeUtilisateur n&#39;exist pas&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;utilisateur non autorisé&lt;/b&gt;   &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;type de connection est null&lt;/b&gt;   &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;login ou password sont vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -7 :&lt;/b&gt;utilisateur non actif&lt;/b&gt;   &lt;b&gt;result &#x3D; -8 :&lt;/b&gt;veuillez vérifier votre mot de passe&lt;/b&gt;   &lt;b&gt;result &#x3D; -9 :&lt;/b&gt;le point de vente n&#39;exist pas&lt;/b&gt;   &lt;b&gt;result &#x3D; -10 :&lt;/b&gt;la caisse n&#39;exist pas&lt;/b&gt;   &lt;b&gt;result &#x3D; -11 :&lt;/b&gt;la referance est null&lt;/b&gt;   &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> loginUsingPOST(LoginDto dtoLogin) async {
    Object postBody = dtoLogin;

    // verify required params are set
    if (dtoLogin == null) {
      throw new ApiException(400, "Missing required param: dtoLogin");
    }

    // create path and map variables
    String path = "/v1/Authentification".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Modifier un utilisateur
  ///
  /// Retourner l&#39;utilisateur modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Utilisateur est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;Utilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;TypeUtilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; login est null  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; login est utilisée  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; password est null  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt; type utilisateur est null  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt; partenaire n&#39;exist pas  &lt;b&gt;result &#x3D; -9 :&lt;/b&gt; profil n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateUtilisateurUsingPUT(Utilisateur utilisateur) async {
    Object postBody = utilisateur;

    // verify required params are set
    if (utilisateur == null) {
      throw new ApiException(400, "Missing required param: utilisateur");
    }

    // create path and map variables
    String path = "/v1/UpdateUtilisateur".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
