
class EmployeeReservationDto {
  
  DateTime datereservation = null;
  

  String idPointVente = null;
  
  EmployeeReservationDto();

  @override
  String toString() {
    return 'EmployeeReservationDto[datereservation=$datereservation, idPointVente=$idPointVente, ]';
  }

  EmployeeReservationDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    datereservation = json['datereservation'] == null ? null : DateTime.parse(json['datereservation']);
    idPointVente =
        json['idPointVente']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'datereservation': datereservation == null ? '' : datereservation.toIso8601String(),
      'idPointVente': idPointVente
     };
  }

  static List<EmployeeReservationDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<EmployeeReservationDto>() : json.map((value) => new EmployeeReservationDto.fromJson(value)).toList();
  }

  static Map<String, EmployeeReservationDto> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, EmployeeReservationDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new EmployeeReservationDto.fromJson(value));
    }
    return map;
  }
}

