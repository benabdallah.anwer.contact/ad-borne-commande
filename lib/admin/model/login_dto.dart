
class LoginDto {
  
  String login = null;
  

  String password = null;
  

  String refrance = null;
  

  String typeconnection = null;
  
  LoginDto();

  @override
  String toString() {
    return 'LoginDto[login=$login, password=$password, refrance=$refrance, typeconnection=$typeconnection, ]';
  }

  LoginDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    login =
        json['login']
    ;
    password =
        json['password']
    ;
    refrance =
        json['refrance']
    ;
    typeconnection =
        json['typeconnection']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'login': login,
      'password': password,
      'refrance': refrance,
      'typeconnection': typeconnection
     };
  }

  static List<LoginDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<LoginDto>() : json.map((value) => new LoginDto.fromJson(value)).toList();
  }

  static Map<String, LoginDto> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, LoginDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new LoginDto.fromJson(value));
    }
    return map;
  }
}

