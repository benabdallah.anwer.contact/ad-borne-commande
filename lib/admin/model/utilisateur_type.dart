
class UtilisateurType {
  
  String code = null;
  

  String designation = null;
  

  int fActif = null;
  

  int fBackoffice = null;
  

  int fBprice = null;
  

  String fCaisse = null;
  

  int fGerant = null;
  

  String idTypeUtilisateur = null;
  
  UtilisateurType();

  @override
  String toString() {
    return 'UtilisateurType[code=$code, designation=$designation, fActif=$fActif, fBackoffice=$fBackoffice, fBprice=$fBprice, fCaisse=$fCaisse, fGerant=$fGerant, idTypeUtilisateur=$idTypeUtilisateur, ]';
  }

  UtilisateurType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    code =
        json['code']
    ;
    designation =
        json['designation']
    ;
    fActif =
        json['fActif']
    ;
    fBackoffice =
        json['fBackoffice']
    ;
    fBprice =
        json['fBprice']
    ;
    fCaisse =
        json['fCaisse']
    ;
    fGerant =
        json['fGerant']
    ;
    idTypeUtilisateur =
        json['idTypeUtilisateur']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'designation': designation,
      'fActif': fActif,
      'fBackoffice': fBackoffice,
      'fBprice': fBprice,
      'fCaisse': fCaisse,
      'fGerant': fGerant,
      'idTypeUtilisateur': idTypeUtilisateur
     };
  }

  static List<UtilisateurType> listFromJson(List<dynamic> json) {
    return json == null ? new List<UtilisateurType>() : json.map((value) => new UtilisateurType.fromJson(value)).toList();
  }

  static Map<String, UtilisateurType> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, UtilisateurType>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new UtilisateurType.fromJson(value));
    }
    return map;
  }
}

