
class CardDto {

  String login = null;


  String password = null;


  CardDto();

  @override
  String toString() {
    return 'CardDto[login=$login, password=$password,  ]';
  }

  CardDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    login =
    json['login']
    ;
    password =
    json['password']
    ;

  }

  Map<String, dynamic> toJson() {
    return {
      'login': login,
      'password': password,

    };
  }

  static List<CardDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<CardDto>() : json.map((value) => new CardDto.fromJson(value)).toList();
  }

  static Map<String, CardDto> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CardDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new CardDto.fromJson(value));
    }
    return map;
  }
}

