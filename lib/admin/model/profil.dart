
class Profil {
  
  DateTime dateCreation = null;
  

  String designation = null;
  

  int fActif = null;
  

  String idProfil = null;
  
  Profil();

  @override
  String toString() {
    return 'Profil[dateCreation=$dateCreation, designation=$designation, fActif=$fActif, idProfil=$idProfil, ]';
  }

  Profil.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateCreation = json['dateCreation'] == null ? null : DateTime.parse(json['dateCreation']);
    designation =
        json['designation']
    ;
    fActif =
        json['fActif']
    ;
    idProfil =
        json['idProfil']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'dateCreation': dateCreation == null ? '' : dateCreation.toIso8601String(),
      'designation': designation,
      'fActif': fActif,
      'idProfil': idProfil
     };
  }

  static List<Profil> listFromJson(List<dynamic> json) {
    return json == null ? new List<Profil>() : json.map((value) => new Profil.fromJson(value)).toList();
  }

  static Map<String, Profil> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Profil>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new Profil.fromJson(value));
    }
    return map;
  }
}

