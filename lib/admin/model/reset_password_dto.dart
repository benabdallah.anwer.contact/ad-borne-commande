
class ResetPasswordDto {
  
  String iduser = null;
  

  String oldpassword = null;
  

  String password = null;
  
  ResetPasswordDto();

  @override
  String toString() {
    return 'ResetPasswordDto[iduser=$iduser, oldpassword=$oldpassword, password=$password, ]';
  }

  ResetPasswordDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    iduser =
        json['iduser']
    ;
    oldpassword =
        json['oldpassword']
    ;
    password =
        json['password']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'iduser': iduser,
      'oldpassword': oldpassword,
      'password': password
     };
  }

  static List<ResetPasswordDto> listFromJson(List<dynamic> json) {
    return json == null ? new List<ResetPasswordDto>() : json.map((value) => new ResetPasswordDto.fromJson(value)).toList();
  }

  static Map<String, ResetPasswordDto> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ResetPasswordDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new ResetPasswordDto.fromJson(value));
    }
    return map;
  }
}

