class MontantOuverture {
  String id = null;

  String idSession = null;

  int qteBillet = null;

  double valeur = null;

  MontantOuverture();

  @override
  String toString() {
    return 'MontantOuverture[id=$id, idSession=$idSession, qteBillet=$qteBillet, valeur=$valeur, ]';
  }

  MontantOuverture.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    idSession = json['idSession'];
    qteBillet = json['qteBillet'];
    valeur = json['valeur'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'idSession': idSession,
      'qteBillet': qteBillet,
      'valeur': valeur
    };
  }

  static List<MontantOuverture> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<MontantOuverture>()
        : json.map((value) => new MontantOuverture.fromJson(value)).toList();
  }

  static Map<String, MontantOuverture> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, MontantOuverture>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new MontantOuverture.fromJson(value));
    }
    return map;
  }
}
