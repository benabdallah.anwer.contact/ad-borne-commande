class Journee {
  int chiffreAffaire = null;

  DateTime dateFermeture = null;

  DateTime dateOuverture = null;

  int fCloture = null;

  String idJournee = null;

  String idPointVente = null;

  int nbrJour = null;

  Journee();

  @override
  String toString() {
    return 'Journee[chiffreAffaire=$chiffreAffaire, dateFermeture=$dateFermeture, dateOuverture=$dateOuverture, fCloture=$fCloture, idJournee=$idJournee, idPointVente=$idPointVente, nbrJour=$nbrJour, ]';
  }

  Journee.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    chiffreAffaire = json['chiffreAffaire'];
    dateFermeture = json['dateFermeture'] == null
        ? null
        : DateTime.parse(json['dateFermeture']);
    dateOuverture = json['dateOuverture'] == null
        ? null
        : DateTime.parse(json['dateOuverture']);
    fCloture = json['fCloture'];
    idJournee = json['idJournee'];
    idPointVente = json['idPointVente'];
    nbrJour = json['nbrJour'];
  }

  Map<String, dynamic> toJson() {
    return {
      'chiffreAffaire': chiffreAffaire,
      'dateFermeture':
          dateFermeture == null ? '' : dateFermeture.toIso8601String(),
      'dateOuverture':
          dateOuverture == null ? '' : dateOuverture.toIso8601String(),
      'fCloture': fCloture,
      'idJournee': idJournee,
      'idPointVente': idPointVente,
      'nbrJour': nbrJour
    };
  }

  static List<Journee> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Journee>()
        : json.map((value) => new Journee.fromJson(value)).toList();
  }

  static Map<String, Journee> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Journee>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Journee.fromJson(value));
    }
    return map;
  }
}
