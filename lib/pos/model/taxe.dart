class Taxe {
  int fActif = null;

  int fDefault = null;

  String idPointVente = null;

  String idTaxe = null;

  String porteeTaxe = null;

  String typeTaxe = null;

  double valeur = null;

  Taxe();

  @override
  String toString() {
    return 'Taxe[fActif=$fActif, fDefault=$fDefault, idPointVente=$idPointVente, idTaxe=$idTaxe, porteeTaxe=$porteeTaxe, typeTaxe=$typeTaxe, valeur=$valeur, ]';
  }

  Taxe.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    fActif = json['fActif'];
    fDefault = json['fDefault'];
    idPointVente = json['idPointVente'];
    idTaxe = json['idTaxe'];
    porteeTaxe = json['porteeTaxe'];
    typeTaxe = json['typeTaxe'];
    valeur = json['valeur'];
  }

  Map<String, dynamic> toJson() {
    return {
      'fActif': fActif,
      'fDefault': fDefault,
      'idPointVente': idPointVente,
      'idTaxe': idTaxe,
      'porteeTaxe': porteeTaxe,
      'typeTaxe': typeTaxe,
      'valeur': valeur
    };
  }

  static List<Taxe> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Taxe>()
        : json.map((value) => new Taxe.fromJson(value)).toList();
  }

  static Map<String, Taxe> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Taxe>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Taxe.fromJson(value));
    }
    return map;
  }
}
