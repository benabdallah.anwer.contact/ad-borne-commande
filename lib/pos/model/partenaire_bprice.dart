class PartenaireBprice {
  String abbreviation = null;

  String adresse = null;

  DateTime dateCreation = null;

  String devise = null;

  int fActif = null;

  String idChart = null;

  String idClientType = null;

  String idPartenaire = null;

  String idSector = null;

  String idVille = null;

  String logo = null;

  String matriculeFiscale = null;

  String nTel = null;

  String raisonSociale = null;

  String siteWeb = null;

  PartenaireBprice();

  @override
  String toString() {
    return 'PartenaireBprice[abbreviation=$abbreviation, adresse=$adresse, dateCreation=$dateCreation, devise=$devise, fActif=$fActif, idChart=$idChart, idClientType=$idClientType, idPartenaire=$idPartenaire, idSector=$idSector, idVille=$idVille, logo=$logo, matriculeFiscale=$matriculeFiscale, nTel=$nTel, raisonSociale=$raisonSociale, siteWeb=$siteWeb, ]';
  }

  PartenaireBprice.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    abbreviation = json['abbreviation'];
    adresse = json['adresse'];
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    devise = json['devise'];
    fActif = json['fActif'];
    idChart = json['idChart'];
    idClientType = json['idClientType'];
    idPartenaire = json['idPartenaire'];
    idSector = json['idSector'];
    idVille = json['idVille'];
    logo = json['logo'];
    matriculeFiscale = json['matriculeFiscale'];
    nTel = json['nTel'];
    raisonSociale = json['raisonSociale'];
    siteWeb = json['siteWeb'];
  }

  Map<String, dynamic> toJson() {
    return {
      'abbreviation': abbreviation,
      'adresse': adresse,
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'devise': devise,
      'fActif': fActif,
      'idChart': idChart,
      'idClientType': idClientType,
      'idPartenaire': idPartenaire,
      'idSector': idSector,
      'idVille': idVille,
      'logo': logo,
      'matriculeFiscale': matriculeFiscale,
      'nTel': nTel,
      'raisonSociale': raisonSociale,
      'siteWeb': siteWeb
    };
  }

  static List<PartenaireBprice> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<PartenaireBprice>()
        : json.map((value) => new PartenaireBprice.fromJson(value)).toList();
  }

  static Map<String, PartenaireBprice> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, PartenaireBprice>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new PartenaireBprice.fromJson(value));
    }
    return map;
  }
}
