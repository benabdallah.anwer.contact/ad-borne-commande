class CaisseType {
  String code = null;

  String designation = null;

  String idTypeCaisse = null;

  CaisseType();

  @override
  String toString() {
    return 'CaisseType[code=$code, designation=$designation, idTypeCaisse=$idTypeCaisse, ]';
  }

  CaisseType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    code = json['code'];
    designation = json['designation'];
    idTypeCaisse = json['idTypeCaisse'];
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'designation': designation,
      'idTypeCaisse': idTypeCaisse
    };
  }

  static List<CaisseType> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<CaisseType>()
        : json.map((value) => new CaisseType.fromJson(value)).toList();
  }

  static Map<String, CaisseType> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CaisseType>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new CaisseType.fromJson(value));
    }
    return map;
  }
}
