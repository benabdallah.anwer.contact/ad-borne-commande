class TableReservationDto {
  DateTime datereservation = null;

  String idPointVente = null;

  TableReservationDto();

  @override
  String toString() {
    return 'TableReservationDto[datereservation=$datereservation, idPointVente=$idPointVente, ]';
  }

  TableReservationDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    datereservation = json['datereservation'] == null
        ? null
        : DateTime.parse(json['datereservation']);
    idPointVente = json['idPointVente'];
  }

  Map<String, dynamic> toJson() {
    return {
      'datereservation': datereservation == null
          ? ''
          : datereservation.toIso8601String(),
      'idPointVente': idPointVente
    };
  }

  static List<TableReservationDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<TableReservationDto>()
        : json.map((value) => new TableReservationDto.fromJson(value)).toList();
  }

  static Map<String, TableReservationDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, TableReservationDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new TableReservationDto.fromJson(value));
    }
    return map;
  }
}
