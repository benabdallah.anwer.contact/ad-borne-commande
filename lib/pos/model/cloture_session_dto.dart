
class ClotureSessionDTO {
  
  double chiffreAffaireSession = null;
  

  DateTime dateCloture = null;
  

  String idSession = null;
  

  double montantCloture = null;
  

  double montantTiroirSysteme = null;
  
  ClotureSessionDTO();

  @override
  String toString() {
    return 'ClotureSessionDTO[chiffreAffaireSession=$chiffreAffaireSession, dateCloture=$dateCloture, idSession=$idSession, montantCloture=$montantCloture, montantTiroirSysteme=$montantTiroirSysteme, ]';
  }

  ClotureSessionDTO.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    chiffreAffaireSession =
        json['chiffreAffaireSession']
    ;
    dateCloture = json['dateCloture'] == null ? null : DateTime.parse(json['dateCloture']);
    idSession =
        json['idSession']
    ;
    montantCloture =
        json['montantCloture']
    ;
    montantTiroirSysteme =
        json['montantTiroirSysteme']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'chiffreAffaireSession': chiffreAffaireSession,
      'dateCloture': dateCloture == null ? '' : dateCloture.toIso8601String(),
      'idSession': idSession,
      'montantCloture': montantCloture,
      'montantTiroirSysteme': montantTiroirSysteme
     };
  }

  static List<ClotureSessionDTO> listFromJson(List<dynamic> json) {
    return json == null ? new List<ClotureSessionDTO>() : json.map((value) => new ClotureSessionDTO.fromJson(value)).toList();
  }

  static Map<String, ClotureSessionDTO> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ClotureSessionDTO>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new ClotureSessionDTO.fromJson(value));
    }
    return map;
  }
}

