class Caisse {
  DateTime dateCreation = null;

  int fActif = null;

  String idCaisse = null;

  String idPointVente = null;

  String idTypeCaisse = null;

  String idrelatedCaisse = null;

  int numCaisse = null;

  String reference = null;

  Caisse();

  @override
  String toString() {
    return 'Caisse[dateCreation=$dateCreation, fActif=$fActif, idCaisse=$idCaisse, idPointVente=$idPointVente, idTypeCaisse=$idTypeCaisse, idrelatedCaisse=$idrelatedCaisse, numCaisse=$numCaisse, reference=$reference, ]';
  }

  Caisse.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateCreation = json['dateCreation'] == null
        ? null
        : DateTime.parse(json['dateCreation']);
    fActif = json['fActif'];
    idCaisse = json['idCaisse'];
    idPointVente = json['idPointVente'];
    idTypeCaisse = json['idTypeCaisse'];
    idrelatedCaisse = json['idrelatedCaisse'];
    numCaisse = json['numCaisse'];
    reference = json['reference'];
  }

  Map<String, dynamic> toJson() {
    return {
      'dateCreation':
          dateCreation == null ? '' : dateCreation.toIso8601String(),
      'fActif': fActif,
      'idCaisse': idCaisse,
      'idPointVente': idPointVente,
      'idTypeCaisse': idTypeCaisse,
      'idrelatedCaisse': idrelatedCaisse,
      'numCaisse': numCaisse,
      'reference': reference
    };
  }

  static List<Caisse> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Caisse>()
        : json.map((value) => new Caisse.fromJson(value)).toList();
  }

  static Map<String, Caisse> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Caisse>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Caisse.fromJson(value));
    }
    return map;
  }
}
