class ModeReglement {
  String code = null;

  String designation = null;

  int fdate = null;

  int fdefault = null;

  int ffidelite = null;

  int fnum = null;

  int fvalidation = null;

  String idModeReglement = null;

  String idPointVente = null;

  int isactif = null;

  ModeReglement();

  @override
  String toString() {
    return 'ModeReglement[code=$code, designation=$designation, fdate=$fdate, fdefault=$fdefault, ffidelite=$ffidelite, fnum=$fnum, fvalidation=$fvalidation, idModeReglement=$idModeReglement, idPointVente=$idPointVente, isactif=$isactif, ]';
  }

  ModeReglement.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    code = json['code'];
    designation = json['designation'];
    fdate = json['fdate'];
    fdefault = json['fdefault'];
    ffidelite = json['ffidelite'];
    fnum = json['fnum'];
    fvalidation = json['fvalidation'];
    idModeReglement = json['idModeReglement'];
    idPointVente = json['idPointVente'];
    isactif = json['isactif'];
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'designation': designation,
      'fdate': fdate,
      'fdefault': fdefault,
      'ffidelite': ffidelite,
      'fnum': fnum,
      'fvalidation': fvalidation,
      'idModeReglement': idModeReglement,
      'idPointVente': idPointVente,
      'isactif': isactif
    };
  }

  static List<ModeReglement> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<ModeReglement>()
        : json.map((value) => new ModeReglement.fromJson(value)).toList();
  }

  static Map<String, ModeReglement> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, ModeReglement>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new ModeReglement.fromJson(value));
    }
    return map;
  }
}
