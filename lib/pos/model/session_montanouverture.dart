import 'montant_ouverture.dart';

class SessionMontanouverture {
  String idsession = null;

  List<MontantOuverture> montantOuvertureList = [];

  SessionMontanouverture();

  @override
  String toString() {
    return 'SessionMontanouverture[idsession=$idsession, montantOuvertureList=$montantOuvertureList, ]';
  }

  SessionMontanouverture.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idsession = json['idsession'];
    montantOuvertureList =
        MontantOuverture.listFromJson(json['montantOuvertureList']);
  }

  Map<String, dynamic> toJson() {
    return {
      'idsession': idsession,
      'montantOuvertureList': montantOuvertureList
    };
  }

  static List<SessionMontanouverture> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<SessionMontanouverture>()
        : json
            .map((value) => new SessionMontanouverture.fromJson(value))
            .toList();
  }

  static Map<String, SessionMontanouverture> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, SessionMontanouverture>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new SessionMontanouverture.fromJson(value));
    }
    return map;
  }
}
