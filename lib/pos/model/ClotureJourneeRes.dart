import 'package:ad_caisse/ordering/model/client.dart';

import 'PreColture.dart';

class ClotureJourneeRes {
  String idJournee;
  double chiffreAffaireGlobaleJournee;
  double sommeMontantOuvertureParJournee;
  int nombreTotaleCommandesParJournee;
  double sommeMontantClotureCalculeSystemParJournee;
  double sommeMontantClotureSaisieParJournee;
  int nombreClientPassager;
  List<MontantParModeRegl> montantParModeReglParJournee;
  List<ClientBPrice> dejaClient;

  ClotureJourneeRes(
      {this.idJournee,
      this.chiffreAffaireGlobaleJournee,
      this.sommeMontantOuvertureParJournee,
      this.nombreTotaleCommandesParJournee,
      this.sommeMontantClotureCalculeSystemParJournee,
      this.sommeMontantClotureSaisieParJournee,
      this.nombreClientPassager,
      this.montantParModeReglParJournee,
      this.dejaClient});

  ClotureJourneeRes.fromJson(Map<String, dynamic> json) {
    idJournee = json['idJournee'];
    chiffreAffaireGlobaleJournee = json['chiffreAffaireGlobaleJournee'];
    sommeMontantOuvertureParJournee = json['sommeMontantOuvertureParJournee'];
    nombreTotaleCommandesParJournee = json['nombreTotaleCommandesParJournee'];
    sommeMontantClotureCalculeSystemParJournee =
        json['sommeMontantClotureCalculeSystemParJournee'];
    sommeMontantClotureSaisieParJournee =
        json['sommeMontantClotureSaisieParJournee'];
    nombreClientPassager = json['nombreClientPassager'];
    if (json['montantParModeReglParJournee'] != null) {
      montantParModeReglParJournee = new List<MontantParModeRegl>();
      json['montantParModeReglParJournee'].forEach((v) {
        montantParModeReglParJournee.add(new MontantParModeRegl.fromJson(v));
      });
    }
    if (json['dejaClient'] != null) {
      dejaClient = new List<ClientBPrice>();
      json['dejaClient'].forEach((v) {
        dejaClient.add(new ClientBPrice.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idJournee'] = this.idJournee;
    data['chiffreAffaireGlobaleJournee'] = this.chiffreAffaireGlobaleJournee;
    data['sommeMontantOuvertureParJournee'] =
        this.sommeMontantOuvertureParJournee;
    data['nombreTotaleCommandesParJournee'] =
        this.nombreTotaleCommandesParJournee;
    data['sommeMontantClotureCalculeSystemParJournee'] =
        this.sommeMontantClotureCalculeSystemParJournee;
    data['sommeMontantClotureSaisieParJournee'] =
        this.sommeMontantClotureSaisieParJournee;
    data['nombreClientPassager'] = this.nombreClientPassager;

    if (this.montantParModeReglParJournee != null) {
      data['montantParModeReglParJournee'] =
          this.montantParModeReglParJournee.map((v) => v.toJson()).toList();
    }
    if (this.dejaClient != null) {
      data['dejaClient'] = this.dejaClient.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
