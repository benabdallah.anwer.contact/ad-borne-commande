class BilletMonnaie {
  String designation = null;

  int fbillet = null;

  String idBilletMonnaie = null;

  String image = null;

  double valeur = null;

  int qte = 0;

  BilletMonnaie();

  @override
  String toString() {
    return 'BilletMonnaie[designation=$designation, fbillet=$fbillet, idBilletMonnaie=$idBilletMonnaie, image=$image, valeur=$valeur, ]';
  }

  BilletMonnaie.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    designation = json['designation'];
    fbillet = json['fbillet'];
    idBilletMonnaie = json['idBilletMonnaie'];
    image = json['image'];
    valeur = json['valeur'];
  }

  Map<String, dynamic> toJson() {
    return {
      'designation': designation,
      'fbillet': fbillet,
      'idBilletMonnaie': idBilletMonnaie,
      'image': image,
      'valeur': valeur
    };
  }

  static List<BilletMonnaie> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<BilletMonnaie>()
        : json.map((value) => new BilletMonnaie.fromJson(value)).toList();
  }

  static Map<String, BilletMonnaie> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, BilletMonnaie>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new BilletMonnaie.fromJson(value));
    }
    return map;
  }
}
