import 'CatgorieDetailsCommandeDTO.dart';
class FamilleDetailsCommande {
  String nomCateg;
  List<CatgorieDetailsCommandeDTO> details;

  FamilleDetailsCommande({this.nomCateg, this.details});

  FamilleDetailsCommande.fromJson(Map<String, dynamic> json) {
    nomCateg = json['nomCateg'];
    if (json['details'] != null) {
      details = new List<CatgorieDetailsCommandeDTO>();
      json['details'].forEach((v) {
        details.add(new CatgorieDetailsCommandeDTO.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nomCateg'] = this.nomCateg;
    if (this.details != null) {
      data['details'] = this.details.map((v) => v.toJson()).toList();
    }
    return data;
  }
}