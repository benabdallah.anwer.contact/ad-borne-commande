class OperationType {

  String designation = null;


  int fActif = null;


  String idTypeOperation = null;

  OperationType();

  @override
  String toString() {
    return 'OperationType[designation=$designation, fActif=$fActif, idTypeOperation=$idTypeOperation, ]';
  }

  OperationType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    designation =
    json['designation']
    ;
    fActif =
    json['fActif']
    ;
    idTypeOperation =
    json['idTypeOperation']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'designation': designation,
      'fActif': fActif,
      'idTypeOperation': idTypeOperation
    };
  }

  static List<OperationType> listFromJson(List<dynamic> json) {
    return json == null ? new List<OperationType>() : json.map((value) => new OperationType.fromJson(value)).toList();
  }

  static Map<String, OperationType> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, OperationType>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new OperationType.fromJson(value));
    }
    return map;
  }
}

