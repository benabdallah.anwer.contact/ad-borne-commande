class ClotureJourneeDto {
  DateTime dateCloture;
  String idJournee;
  String idUser;

  ClotureJourneeDto({this.dateCloture, this.idJournee, this.idUser});

  ClotureJourneeDto.fromJson(Map<String, dynamic> json) {
    dateCloture = json['dateCloture'] != null
        ? DateTime.parse(json['dateCloture'])
        : null;
    idJournee = json['idJournee'];
    idUser = json['idUser'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dateCloture'] = this.dateCloture.toIso8601String();
    data['idJournee'] = this.idJournee;
    data['idUser'] = this.idUser;
    return data;
  }
}
