import 'package:ad_caisse/product/model/produit.dart';

class DetailCommandeTable {
  String idDetailComm;
  Produit produit;
  String idCommande;
  int quantite;

  DetailCommandeTable(
      {this.idDetailComm, this.produit, this.idCommande, this.quantite});

  DetailCommandeTable.fromJson(Map<String, dynamic> json) {
    idDetailComm = json['idDetailComm'];
    produit =
        json['produit'] != null ? new Produit.fromJson(json['produit']) : null;
    idCommande = json['idCommande'];
    quantite = json['quantite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idDetailComm'] = this.idDetailComm;
    if (this.produit != null) {
      data['produit'] = this.produit.toJson();
    }
    data['idCommande'] = this.idCommande;
    data['quantite'] = this.quantite;
    return data;
  }
}
