import 'package:ad_caisse/pos/model/session.dart';

class AutreSessionRes {
  List<Session> autreSession;

  AutreSessionRes({this.autreSession});

  AutreSessionRes.fromJson(Map<String, dynamic> json) {
    if (json['autreSession'] != null) {
      autreSession = new List<Session>();
      json['autreSession'].forEach((v) {
        autreSession.add(new Session.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.autreSession != null) {
      data['autreSession'] = this.autreSession.map((v) => v.toJson()).toList();
    }
    return data;
  }
}