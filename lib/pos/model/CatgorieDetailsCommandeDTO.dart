import 'package:ad_caisse/ordering/model/commande_details.dart';

class CatgorieDetailsCommandeDTO {
  String nomCateg;
  CommandeDetails commandeDetails;

  CatgorieDetailsCommandeDTO({this.nomCateg, this.commandeDetails});

  CatgorieDetailsCommandeDTO.fromJson(Map<String, dynamic> json) {
    nomCateg = json['nomCateg'];
    commandeDetails = json['commandeDetails'] != null
        ? new CommandeDetails.fromJson(json['commandeDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nomCateg'] = this.nomCateg;
    if (this.commandeDetails != null) {
      data['commandeDetails'] = this.commandeDetails.toJson();
    }
    return data;
  }
}