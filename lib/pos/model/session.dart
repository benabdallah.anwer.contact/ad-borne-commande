import 'package:ad_caisse/admin/model/utilisateur.dart';

class Session {
  DateTime dateDebut = null;

  DateTime dateFin = null;

  int fFerme = null;

  String idCaisse = null;

  String idJournee = null;

  String idSession = null;

  String idUtilisateur = null;

  double montantOuverture = null;

  Utilisateur utilisateur;

  Session();

  @override
  String toString() {
    return 'Session[dateDebut=$dateDebut, dateFin=$dateFin, fFerme=$fFerme, idCaisse=$idCaisse, idJournee=$idJournee, idSession=$idSession, idUtilisateur=$idUtilisateur, montantOuverture=$montantOuverture, ]';
  }

  Session.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateDebut =
        json['dateDebut'] == null ? null : DateTime.parse(json['dateDebut']);
    dateFin = json['dateFin'] == null ? null : DateTime.parse(json['dateFin']);
    fFerme = json['fFerme'];
    idCaisse = json['idCaisse'];
    idJournee = json['idJournee'];
    idSession = json['idSession'];
    idUtilisateur = json['idUtilisateur'];
    montantOuverture = json['montantOuverture'];
    utilisateur = json['utilisateur'] != null
        ? new Utilisateur.fromJson(json['utilisateur'])
        : null;
  }

  Map<String, dynamic> toJson() {
    return {
      'dateDebut': dateDebut == null ? '' : dateDebut.toIso8601String(),
      'dateFin': dateFin == null ? '' : dateFin.toIso8601String(),
      'fFerme': fFerme,
      'idCaisse': idCaisse,
      'idJournee': idJournee,
      'idSession': idSession,
      'idUtilisateur': idUtilisateur,
      'montantOuverture': montantOuverture,
      "utilisateur": utilisateur != null ? utilisateur.toJson() : null
    };
  }

  static List<Session> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Session>()
        : json.map((value) => new Session.fromJson(value)).toList();
  }

  static Map<String, Session> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Session>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Session.fromJson(value));
    }
    return map;
  }
}
