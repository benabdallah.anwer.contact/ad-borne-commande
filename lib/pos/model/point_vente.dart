import 'mode_reglement.dart';

class PointVente {
  String idPointVente = "";
  String idPartenaire = "";
  String designation = "";
  int fActif = 0;
  DateTime dateCreation;
  String adresse = "";
  int coordX;
  int coordY;
  int fGestionTable = 0;
  int fPhotoCateg = 0;
  int fClavierVirtuel = 0;
  int fImprimCuisine = 0;
  int fReImprim = 0;
  int fPartageAdition = 0;
  int fEntetePied = 0;
  String enteteTicket = "";
  String piedTicket = "";
  int fDetailMontant = 0;
  int fAffectEmployetoservice = 0;
  int fAutoriserRecharge = 0;
  String fReservation = "";
  int fImprimeAvP = 0;
  int fControlCaisse = 0;
  List<ModeReglement> modeReglements = [];
  String defaultouverture = '';
  String differenceautorise = '';
  int fprixttc = 0;
  int faffectcollab = 0;
  int fdetectPack = 0;
  String chiffrevirgule = '3';
  int fImpresCateg=0;
  int fDispCA=0;

  PointVente();

  @override
  String toString() {
    return 'PointVente[adresse=$adresse, chiffrevirgule=$chiffrevirgule, coordX=$coordX, coordY=$coordY, dateCreation=$dateCreation, defaultouverture=$defaultouverture, designation=$designation, differenceautorise=$differenceautorise, enteteTicket=$enteteTicket, fActif=$fActif, fClavierVirtuel=$fClavierVirtuel, fControlCaisse=$fControlCaisse, fDetailMontant=$fDetailMontant, fEntetePied=$fEntetePied, fGestionTable=$fGestionTable, fImprimCuisine=$fImprimCuisine, fImprimeAvP=$fImprimeAvP, fPartageAdition=$fPartageAdition, fPhotoCateg=$fPhotoCateg, fReImprim=$fReImprim, faffectcollab=$faffectcollab, fdetectpack=$fdetectPack, fprixttc=$fprixttc, idPartenaire=$idPartenaire, idPointVente=$idPointVente, modeReglements=$modeReglements, piedTicket=$piedTicket,fImpresCateg=$fImpresCateg ]';
  }

  PointVente.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    idPointVente = json['idPointVente'];
    idPartenaire = json['idPartenaire'];
    designation = json['designation'];
    fActif = json['fActif'];
    dateCreation = DateTime.parse(json['dateCreation']);
    adresse = json['adresse'];
    coordX = json['coordX'];
    coordY = json['coordY'];
    fGestionTable = json['fGestionTable'];
    fPhotoCateg = json['fPhotoCateg'];
    fClavierVirtuel = json['fClavierVirtuel'];
    fImprimCuisine = json['fImprimCuisine'];
    fReImprim = json['fReImprim'];
    fPartageAdition = json['fPartageAdition'];
    fEntetePied = json['fEntetePied'];
    enteteTicket = json['enteteTicket'];
    piedTicket = json['piedTicket'];
    fDetailMontant = json['fDetailMontant'];
    fAffectEmployetoservice = json['fAffectEmployetoservice'];
    fAutoriserRecharge = json['fAutoriserRecharge'];
    fReservation = json['fReservation'];
    fImprimeAvP = json['fImprimeAvP'];
    fControlCaisse = json['fControlCaisse'];
    if (json['modeReglements'] != null) {
      modeReglements = new List<ModeReglement>();
      json['modeReglements'].forEach((v) {
        modeReglements.add(new ModeReglement.fromJson(v));
      });
    }
    defaultouverture = json['defaultouverture'];
    differenceautorise = json['differenceautorise'];
    fprixttc = json['fprixttc'];
    faffectcollab = json['faffectcollab'];
    fdetectPack = json['fdetectPack'];
    chiffrevirgule = json['chiffrevirgule'];
    fImpresCateg = json ['fImpresCateg'];
    fDispCA = json ['fDispCA'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idPointVente'] = this.idPointVente;
    data['idPartenaire'] = this.idPartenaire;
    data['designation'] = this.designation;
    data['fActif'] = this.fActif;
    data['dateCreation'] = this.dateCreation;
    data['adresse'] = this.adresse;
    data['coordX'] = this.coordX;
    data['coordY'] = this.coordY;
    data['fGestionTable'] = this.fGestionTable;
    data['fPhotoCateg'] = this.fPhotoCateg;
    data['fClavierVirtuel'] = this.fClavierVirtuel;
    data['fImprimCuisine'] = this.fImprimCuisine;
    data['fReImprim'] = this.fReImprim;
    data['fPartageAdition'] = this.fPartageAdition;
    data['fEntetePied'] = this.fEntetePied;
    data['enteteTicket'] = this.enteteTicket;
    data['piedTicket'] = this.piedTicket;
    data['fDetailMontant'] = this.fDetailMontant;
    data['fAffectEmployetoservice'] = this.fAffectEmployetoservice;
    data['fAutoriserRecharge'] = this.fAutoriserRecharge;
    data['fReservation'] = this.fReservation;
    data['fImprimeAvP'] = this.fImprimeAvP;
    data['fControlCaisse'] = this.fControlCaisse;
    if (this.modeReglements != null) {
      data['modeReglements'] =
          this.modeReglements.map((v) => v.toJson()).toList();
    }
    data['defaultouverture'] = this.defaultouverture;
    data['differenceautorise'] = this.differenceautorise;
    data['fprixttc'] = this.fprixttc;
    data['faffectcollab'] = this.faffectcollab;
    data['fdetectPack'] = this.fdetectPack;
    data['chiffrevirgule'] = this.chiffrevirgule;
    data['fImpresCateg'] = this.fImpresCateg;
    data['fDispCA'] = this.fDispCA;

    return data;
  }

  static List<PointVente> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<PointVente>()
        : json.map((value) => new PointVente.fromJson(value)).toList();
  }

  static Map<String, PointVente> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, PointVente>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new PointVente.fromJson(value));
    }
    return map;
  }
}
