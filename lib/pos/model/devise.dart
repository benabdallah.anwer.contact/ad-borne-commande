import 'billet_monnaie.dart';

class Devise {
  String code = null;

  String designation = null;

  int fDefaut = null;

  String idDevise = null;

  String idPointVente = null;

  List<BilletMonnaie> refMonetique = [];

  double taux = null;

  Devise();

  @override
  String toString() {
    return 'Devise[code=$code, designation=$designation, fDefaut=$fDefaut, idDevise=$idDevise, idPointVente=$idPointVente, refMonetique=$refMonetique, taux=$taux, ]';
  }

  Devise.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    code = json['code'];
    designation = json['designation'];
    fDefaut = json['fDefaut'];
    idDevise = json['idDevise'];
    idPointVente = json['idPointVente'];
    refMonetique = BilletMonnaie.listFromJson(json['refMonetique']);
    taux = json['taux'];
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'designation': designation,
      'fDefaut': fDefaut,
      'idDevise': idDevise,
      'idPointVente': idPointVente,
      'refMonetique': refMonetique,
      'taux': taux
    };
  }

  static List<Devise> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Devise>()
        : json.map((value) => new Devise.fromJson(value)).toList();
  }

  static Map<String, Devise> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Devise>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Devise.fromJson(value));
    }
    return map;
  }
}
