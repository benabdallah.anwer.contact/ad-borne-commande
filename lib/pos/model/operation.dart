class Operation {

  String caiIdCaisse = null;

  double montant;

  String commenatire = null;


  DateTime dateOperation = null;


  String idCaisse = null;


  String idFournisseur = null;


  String idOperation = null;


  String idPointVente = null;


  String idTypeOperation = null;


  String idUtilisateur = null;


  int numCommande = null;


  String statut = null;

  String idEmploye = null;

  Operation();

  @override
  String toString() {
    return 'Operation[caiIdCaisse=$caiIdCaisse, commenatire=$commenatire, dateOperation=$dateOperation, idCaisse=$idCaisse, idFournisseur=$idFournisseur, idOperation=$idOperation, idPointVente=$idPointVente, idTypeOperation=$idTypeOperation, idUtilisateur=$idUtilisateur, numCommande=$numCommande, statut=$statut,idEmploye=$idEmploye ]';
  }

  Operation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    montant =
    json['montant']
    ;
    caiIdCaisse =
    json['caiIdCaisse']
    ;
    commenatire =
    json['commenatire']
    ;
    dateOperation = json['dateOperation'] == null ||json['dateOperation'] =='' ? null : DateTime.parse(json['dateOperation']);
    idCaisse =
    json['idCaisse']
    ;
    idFournisseur =
    json['idFournisseur']
    ;
    idOperation =
    json['idOperation']
    ;
    idPointVente =
    json['idPointVente']
    ;
    idTypeOperation =
    json['idTypeOperation']
    ;
    idUtilisateur =
    json['idUtilisateur']
    ;
    numCommande =
    json['numCommande']
    ;
    statut =
    json['statut']
    ;
    idEmploye =
        json['idEmploye'];
  }

  Map<String, dynamic> toJson() {
    return {
      'montant': montant,
      'caiIdCaisse': caiIdCaisse,
      'commenatire': commenatire,
      'dateOperation': dateOperation == null ? '' : dateOperation.toIso8601String(),
      'idCaisse': idCaisse,
      'idFournisseur': idFournisseur,
      'idOperation': idOperation,
      'idPointVente': idPointVente,
      'idTypeOperation': idTypeOperation,
      'idUtilisateur': idUtilisateur,
      'numCommande': numCommande,
      'statut': statut,
      'idEmploye': idEmploye
    };
  }

  static List<Operation> listFromJson(List<dynamic> json) {
    return json == null ? new List<Operation>() : json.map((value) => new Operation.fromJson(value)).toList();
  }

  static Map<String, Operation> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Operation>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new Operation.fromJson(value));
    }
    return map;
  }
}
