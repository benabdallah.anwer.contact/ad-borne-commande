import 'package:ad_caisse/ordering/model/commande_details.dart';
import 'package:ad_caisse/transaction/model/commande.dart';

class TableCaisse {
  int capaciteTable = null;

  String idPointVente = null;

  String idTable = null;

  int numTable = null;

  String etat;

  String idCommande;

  CommandeTransaction commande;
  List<CommandeDetails> commandeDetails = [];

  TableCaisse();

  @override
  String toString() {
    return 'TableCaisse[capaciteTable=$capaciteTable, idPointVente=$idPointVente, idTable=$idTable, numTable=$numTable, ]';
  }

  TableCaisse.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    capaciteTable = json['capaciteTable'];
    idPointVente = json['idPointVente'];
    idTable = json['idTable'];
    numTable = json['numTable'];
    etat = json['etat'];
    idCommande = json['idCommande'];
    commande = json['commande'] != null
        ? new CommandeTransaction.fromJson(json['commande'])
        : null;
    if (json['commandeDetails'] != null) {
      commandeDetails = new List<CommandeDetails>();
      json['commandeDetails'].forEach((v) {
        commandeDetails.add(new CommandeDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'capaciteTable': capaciteTable,
      'idPointVente': idPointVente,
      'idTable': idTable,
      'numTable': numTable,
      'etat': etat
    };
  }

  static List<TableCaisse> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<TableCaisse>()
        : json.map((value) => new TableCaisse.fromJson(value)).toList();
  }

  static Map<String, TableCaisse> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, TableCaisse>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new TableCaisse.fromJson(value));
    }
    return map;
  }
}
