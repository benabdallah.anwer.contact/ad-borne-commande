import 'package:ad_caisse/ordering/model/client.dart';
import 'package:ad_caisse/transaction/model/commande.dart';

class PreColture {
  String idSession;
  String idUser;
  String nomPrenomUser;
  double chiffreAffaire;
  double montantTirroir;
  double montantOuverture;
  double montantNonEncorePayes;
  int nombreTotalCommande;
  int nombreCommandePayes;
  int nombreCommandeNonPayes;
  int nombreCommandePartiellementPayes;
  int nombreClientPassager;
  List<ClientBPrice> listDejaClient;
  List<CommandeTransaction> commandesNonPayeAndPartielPayee;
  List<MontantParModeRegl> montantParModeRegl;

  PreColture(
      {this.idSession,
      this.idUser,
      this.nomPrenomUser,
      this.chiffreAffaire,
      this.montantTirroir,
      this.montantOuverture,
      this.montantNonEncorePayes,
      this.nombreTotalCommande,
      this.nombreCommandePayes,
      this.nombreCommandeNonPayes,
      this.nombreCommandePartiellementPayes,
      this.montantParModeRegl});

  PreColture.fromJson(Map<String, dynamic> json) {
    idSession = json['idSession'];
    idUser = json['idUser'];
    nomPrenomUser = json['nomPrenomUser'];
    chiffreAffaire = json['chiffreAffaire'];
    montantTirroir = json['montantTirroir'];
    montantOuverture = json['montantOuverture'];
    montantNonEncorePayes = json['montantNonEncorePayes'];
    nombreTotalCommande = json['nombreTotalCommande'];
    nombreCommandePayes = json['nombreCommandePayes'];
    nombreCommandeNonPayes = json['nombreCommandeNonPayes'];
    nombreCommandePartiellementPayes = json['nombreCommandePartiellementPayes'];
    if (json['montantParModeRegl'] != null) {
      montantParModeRegl = new List<MontantParModeRegl>();
      json['montantParModeRegl'].forEach((v) {
        montantParModeRegl.add(new MontantParModeRegl.fromJson(v));
      });
    }
    nombreClientPassager = json['nombreClientPassager'];
    if (json['listDejaClient'] != null) {
      listDejaClient = new List<ClientBPrice>();
      json['listDejaClient'].forEach((v) {
        listDejaClient.add(new ClientBPrice.fromJson(v));
      });
    }
    if (json['commandesNonPayeAndPartielPayee'] != null) {
      commandesNonPayeAndPartielPayee = new List<CommandeTransaction>();
      json['commandesNonPayeAndPartielPayee'].forEach((v) {
        commandesNonPayeAndPartielPayee
            .add(new CommandeTransaction.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idSession'] = this.idSession;
    data['idUser'] = this.idUser;
    data['nomPrenomUser'] = this.nomPrenomUser;
    data['chiffreAffaire'] = this.chiffreAffaire;
    data['montantTirroir'] = this.montantTirroir;
    data['montantOuverture'] = this.montantOuverture;
    data['montantNonEncorePayes'] = this.montantNonEncorePayes;
    data['nombreTotalCommande'] = this.nombreTotalCommande;
    data['nombreCommandePayes'] = this.nombreCommandePayes;
    data['nombreCommandeNonPayes'] = this.nombreCommandeNonPayes;
    data['nombreCommandePartiellementPayes'] =
        this.nombreCommandePartiellementPayes;
    if (this.montantParModeRegl != null) {
      data['montantParModeRegl'] =
          this.montantParModeRegl.map((v) => v.toJson()).toList();
    }
    data['nombreClientPassager'] = this.nombreClientPassager;
    if (this.listDejaClient != null) {
      data['listDejaClient'] =
          this.listDejaClient.map((v) => v.toJson()).toList();
    }
    if (this.commandesNonPayeAndPartielPayee != null) {
      data['commandesNonPayeAndPartielPayee'] =
          this.commandesNonPayeAndPartielPayee.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MontantParModeRegl {
  String idModReg;
  String designation;
  double totale;
  int nombreReg;
  int fnum;
  int ffidelite;

  MontantParModeRegl(
      {this.idModReg,
      this.designation,
      this.totale,
      this.nombreReg,
      this.fnum,
      this.ffidelite});

  MontantParModeRegl.fromJson(Map<String, dynamic> json) {
    idModReg = json['idModReg'];
    designation = json['designation'];
    totale = json['totale'];
    nombreReg = json['nombreReg'];
    fnum = json['fnum'];
    ffidelite = json['ffidelite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idModReg'] = this.idModReg;
    data['designation'] = this.designation;
    data['totale'] = this.totale;
    data['nombreReg'] = this.nombreReg;
    data['fnum'] = this.fnum;
    data['ffidelite'] = this.ffidelite;
    return data;
  }
}
