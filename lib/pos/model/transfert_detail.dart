class TransfertDetail {
  String idTableDes;
  String idTableSrc;
  List<String> ids;

  TransfertDetail();

  TransfertDetail.fromJson(Map<String, dynamic> json) {
    idTableDes = json['idTableDes'];
    idTableSrc = json['idTableSrc'];
    ids = json['ids'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idTableDes'] = this.idTableDes;
    data['idTableSrc'] = this.idTableSrc;
    data['ids'] = this.ids;
    return data;
  }
}