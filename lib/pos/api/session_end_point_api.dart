import 'package:ad_caisse/pos/model/cloture_session_dto.dart';
import 'package:ad_caisse/pos/model/session_montanouverture.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/session.dart';

class SessionEndPointApi {
  final ApiClient apiClient;

  SessionEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// retourne un rapport de precloture de session
  ///
  /// Retourner un rapport complet pour une session donnée  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Rapport traité&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; session n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> preColtureUsingGET(String idSession) async {
    Object postBody = null;

    // verify required params are set
    if (idSession == null) {
      throw new ApiException(400, "Missing required param: idSession");
    }

    // create path and map variables
    String path = "/v1/preClotureSession/{idSession}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idSession" + "}", idSession.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      // print("preClotureSession: " + response.body);
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Cloturer une session
  ///
  /// Retourner la session cloturée.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Cloture effectuée avec succées&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Objet est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt; idsession est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;chiffre d&#39;affaire est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;montant tiroir calculé par le systeme  est vide  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;date de cloture est vide  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;session n&#39;exist pas  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt;montant de la cloture est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> cloturesessionUsingPOST(
      ClotureSessionDTO session) async {
    Object postBody = session;
    // verify required params are set
    if (session == null) {
      throw new ApiException(400, "Missing required param: session");
    }

    // create path and map variables
    String path = "/v1/ClotureSession".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Afficher la session selon l&#39;idsession envoyer
  ///
  /// Retourner la session selon l&#39;idsession envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La session exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;Utilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;un ou plusieurs parametres envoyer sont null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; journee n&#39;exist pas  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; caisse n&#39;exist pas  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;session est fermée  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> checkIfSessionIsOpenUsingGET(
      String idJournee, String idUtilisateur, String idCaisse) async {
    Object postBody = null;

    // verify required params are set
    if (idJournee == null) {
      throw new ApiException(400, "Missing required param: idJournee");
    }
    if (idUtilisateur == null) {
      throw new ApiException(400, "Missing required param: idUtilisateur");
    }
    if (idCaisse == null) {
      throw new ApiException(400, "Missing required param: idCaisse");
    }

    // create path and map variables
    String path =
        "/v1/checkIfSessionIsOpen/{idJournee}/{idUtilisateur}/{idCaisse}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "idJournee" + "}", idJournee.toString())
            .replaceAll("{" + "idUtilisateur" + "}", idUtilisateur.toString())
            .replaceAll("{" + "idCaisse" + "}", idCaisse.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Update le Montant d&#39;Ouverture une session
  ///
  /// Retourner la session avec le nouveau montant.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Modifier&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;l&#39;object envoyer est null  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;session n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> createMontantForCaisseUsingPOST(
      SessionMontanouverture sessionMontanouverture) async {
    Object postBody = sessionMontanouverture;

    // verify required params are set
    if (sessionMontanouverture == null) {
      throw new ApiException(
          400, "Missing required param: sessionMontanouverture");
    }

    // create path and map variables
    String path = "/v1/createMontantForCaisse".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// créer une session
  ///
  /// Retourner la session créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;session est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;journee est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;Utilisateur est vide  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;Utilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;id caisse est null  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt;caisse n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> createsessionUsingPOST(Session session) async {
    Object postBody = session;

    // verify required params are set
    if (session == null) {
      throw new ApiException(400, "Missing required param: session");
    }

    // create path and map variables
    String path = "/v1/CreateSession".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Supprimer une session
  ///
  /// Retourner une valeur boolean qui indique si la session a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;session n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteSessionUsingDELETE(String idSession) async {
    Object postBody = null;

    // verify required params are set
    if (idSession == null) {
      throw new ApiException(400, "Missing required param: idSession");
    }

    // create path and map variables
    String path = "/v1/DeleteSession/{idSession}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idSession" + "}", idSession.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la session selon l&#39;idsession envoyer
  ///
  /// Retourner la session selon l&#39;idsession envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; La session exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;session n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdsessionUsingGET(String idSession) async {
    Object postBody = null;

    // verify required params are set
    if (idSession == null) {
      throw new ApiException(400, "Missing required param: idSession");
    }

    // create path and map variables
    String path = "/v1/findByIdsession/{idSession}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idSession" + "}", idSession.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier une session
  ///
  /// Retourner la session modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;session est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;journee est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;Utilisateur est vide  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;Utilisateur n&#39;exist pas  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;session n&#39;exist pas  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt;caisse n&#39;exist pas  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt;id caisse est null  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updatesessionUsingPUT(Session session) async {
    Object postBody = session;

    // verify required params are set
    if (session == null) {
      throw new ApiException(400, "Missing required param: session");
    }

    // create path and map variables
    String path = "/v1/UpdateSession".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
