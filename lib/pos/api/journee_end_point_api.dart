import 'package:ad_caisse/pos/model/ClotureJourneeDto.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/journee.dart';

class JourneeEndPointApi {
  final ApiClient apiClient;

  JourneeEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// Afficher la journee selon l&#39;idCaisse envoyer
  ///
  /// Retourner la journee selon l&#39;idCaisse envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; journee exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; journee est fermée  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> checkIfDayIsOpenUsingGET(String idPointVente) async {
    Object postBody = null;

    // verify required params are set
    if (idPointVente == null) {
      throw new ApiException(400, "Missing required param: idPointVente");
    }

    // create path and map variables
    String path = "/v1/CheckIfDayIsOpen/{idPointVente}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPointVente" + "}", idPointVente.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// créer une journee
  ///
  /// Retourner la journee créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;journee est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> createjourneeUsingPOST(Journee journee) async {

   LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setInt("numTicket", 1);


    Object postBody = journee;


    // verify required params are set
    if (journee == null) {
      throw new ApiException(400, "Missing required param: journee");
    }

    // create path and map variables
    String path = "/v1/CreateJournee".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      // print("createNewJournee: " + response.body);
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Supprimer une journee
  ///
  /// Retourner une valeur boolean qui indique si la journee a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteJourneeUsingDELETE(String idJournee) async {
    Object postBody = null;

    // verify required params are set
    if (idJournee == null) {
      throw new ApiException(400, "Missing required param: idJournee");
    }

    // create path and map variables
    String path = "/v1/DeleteJournee/{idJournee}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idJournee" + "}", idJournee.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la journee selon l&#39;idJournee envoyer
  ///
  /// Retourner la journee selon l&#39;idJournee envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; journee exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdJourneeUsingGET(String idJournee) async {
    Object postBody = null;

    // verify required params are set
    if (idJournee == null) {
      throw new ApiException(400, "Missing required param: idJournee");
    }

    // create path and map variables
    String path = "/v1/findByIdJournee/{idJournee}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idJournee" + "}", idJournee.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier une journee
  ///
  /// Retourner la journee modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;journee est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; journee n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateJourneeUsingPUT(Journee journee) async {
    Object postBody = journee;

    // verify required params are set
    if (journee == null) {
      throw new ApiException(400, "Missing required param: journee");
    }

    // create path and map variables
    String path = "/v1/UpdateJournee".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }


  /// Cloturer une journee et generer un rapport
  ///
  /// Retourner le rapport de la journée.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; Cloture effectuée avec succées&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Objet est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt; Veuillez cloturer les sessions suivantes pour proceder à la cloture de la journée&lt;/b&gt;   &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id journee  est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;date cloture est vide  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt;journee n&#39;exist pas  &lt;b&gt;result &#x3D; -7 :&lt;/b&gt;montant de la cloture est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> clotureJourneeUsingPOST(
      ClotureJourneeDto journee) async {


    LocalStorageInterface prefs = await LocalStorage.getInstance();
    await prefs.setInt("numTicket", 1);


    Object postBody = journee;
    // print("Cloture DTO: "+jsonEncode(postBody));
    // verify required params are set
    if (journee == null) {
      throw new ApiException(400, "Missing required param: journee");
    }

    // create path and map variables
    String path = "/v1/ClotureJournee".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      //   print("ClotureJournee: " + response.body);
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }
}
