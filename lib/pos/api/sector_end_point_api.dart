import 'package:http/http.dart';

import '../api_client.dart';
import '../api.dart';
import '../api_exception.dart';

class SectorEndPointApi {
  final ApiClient apiClient;

  SectorEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// Afficher la liste de tous les Sectors par FActif
  ///
  /// Retourner la liste de tous les Sectors par FActif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Sector n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Sector est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFActifUsingGET3(int factif) async {
    Object postBody = null;

    // verify required params are set
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllSectorByFActif/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les Sectors
  ///
  /// Retourner la liste de tous les Sectors.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List Sector n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List Sector est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllSectorUsingGET() async {
    Object postBody = null;

    // verify required params are set

    // create path and map variables
    String path = "/v1/findAllSector".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
