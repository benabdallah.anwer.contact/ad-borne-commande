import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:http/http.dart';

import '../api_client.dart';
import '../api.dart';
import '../model/caisse_type.dart';
import '../api_exception.dart';

class CaisseTypeEndPointApi {
  final ApiClient apiClient;

  CaisseTypeEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer une CaisseType
  ///
  /// Retourner la CaisseType créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;le type caisse esi vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le code est déjà utilisé  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; le code est vide  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createCaisseTypeUsingPOST(CaisseType caisseType) async {
    Object postBody = caisseType;

    // verify required params are set
    if (caisseType == null) {
      throw new ApiException(400, "Missing required param: caisseType");
    }

    // create path and map variables
    String path = "/v1/CreateCaisseType".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer une CaisseType
  ///
  /// Retourner une valeur boolean qui indique si la CaisseType a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; Id est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt;le type caisse n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteCaisseTypeUsingDELETE(String idCaisseType) async {
    Object postBody = null;

    // verify required params are set
    if (idCaisseType == null) {
      throw new ApiException(400, "Missing required param: idCaisseType");
    }

    // create path and map variables
    String path = "/v1/DeleteCaisseType/{idCaisseType}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idCaisseType" + "}", idCaisseType.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la CaisseType selon l&#39;idCaisseType envoyer
  ///
  /// Retourner la CaisseType selon l&#39;idCaisseType envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; CaisseType exist&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt;le type caisse n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdCaisseTypeUsingGET(String idCaisseType) async {
    Object postBody = null;

    // verify required params are set
    if (idCaisseType == null) {
      throw new ApiException(400, "Missing required param: idCaisseType");
    }

    // create path and map variables
    String path = "/v1/findByIdCaisseType/{idCaisseType}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idCaisseType" + "}", idCaisseType.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
    contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];
    ApiClient client = ApiClient();
    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient != null
          ? apiClient.deserialize(response.body, 'ObjectSingle')
      as ResponseSingle
          : client.deserialize(response.body, 'ObjectSingle') as ResponseSingle;
    } else {
      return null;
    }
  }

  /// Afficher la list  Caisses
  ///
  /// Retourner la list des Caisses  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; la list des types caisses  est non vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt; la list des types caisses est vide&lt;/b&gt;   &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findallUsingGET1() async {
    Object postBody = null;

    // verify required params are set

    // create path and map variables
    String path = "/v1/findalltypecaisse".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier une CaisseType
  ///
  /// Retourner la CaisseType modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;CaisseType est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;le type caisse est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le code est déjà utilisé  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; le code est vide  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; le type caisse n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updateCaisseTypeUsingPUT(CaisseType caisseType) async {
    Object postBody = caisseType;

    // verify required params are set
    if (caisseType == null) {
      throw new ApiException(400, "Missing required param: caisseType");
    }

    // create path and map variables
    String path = "/v1/UpdateCaisseType".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
