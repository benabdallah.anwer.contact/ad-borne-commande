import 'package:ad_caisse/pos/model/partenaire_bprice.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class PartenaireBpriceEndPointApi {
  final ApiClient apiClient;

  PartenaireBpriceEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// créer plusieurs PartenaireBprices
  ///
  /// Retourner la liste des PartenaireBprices créés.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List PartenaireBprice est vide  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createManyPartenaireBpriceUsingPOST(
      List<PartenaireBprice> partenaireBprice) async {
    Object postBody = partenaireBprice;

    // verify required params are set
    if (partenaireBprice == null) {
      throw new ApiException(400, "Missing required param: partenaireBprice");
    }

    // create path and map variables
    String path =
        "/v1/CreateManyPartenaireBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// créer un PartenaireBprice
  ///
  /// Retourner le PartenaireBprice créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;PartenaireBprice est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;matricule deja utiliser  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; matricule est vide  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; ville n&#39;exist pas  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; chart n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> createPartenaireBpriceUsingPOST(
      PartenaireBprice partenaireBprice) async {
    Object postBody = partenaireBprice;

    // verify required params are set
    if (partenaireBprice == null) {
      throw new ApiException(400, "Missing required param: partenaireBprice");
    }

    // create path and map variables
    String path = "/v1/CreatePartenaireBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Supprimer un PartenaireBprice
  ///
  /// Retourner une valeur boolean qui indique si le PartenaireBprice a été supprimé ou non.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet supprimé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PartenaireBprice n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> deleteUtilistaeurUsingDELETE(String idPartenaireBprice) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaireBprice == null) {
      throw new ApiException(400, "Missing required param: idPartenaireBprice");
    }

    // create path and map variables
    String path = "/v1/DeletePartenaireBprice/{idPartenaireBprice}"
        .replaceAll("{format}", "json")
        .replaceAll(
            "{" + "idPartenaireBprice" + "}", idPartenaireBprice.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'DELETE', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les PartenaireBprices par FActif
  ///
  /// Retourner la liste de tous les PartenaireBprices par FActif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List PartenaireBprice n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List PartenaireBprice est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByFActifUsingGET1(int factif) async {
    Object postBody = null;

    // verify required params are set
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllPartenaireBpriceByFActif/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les PartenaireBprices par idVille et factif
  ///
  /// Retourner la liste de tous les PartenaireBprices par idVille et factif.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List PartenaireBprice n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List PartenaireBprice est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdVilleAndFActifUsingGET(
      String idVille, int factif) async {
    Object postBody = null;

    // verify required params are set
    if (idVille == null) {
      throw new ApiException(400, "Missing required param: idVille");
    }
    if (factif == null) {
      throw new ApiException(400, "Missing required param: factif");
    }

    // create path and map variables
    String path = "/v1/findAllByIdVilleAndFActif/{idVille}/{factif}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idVille" + "}", idVille.toString())
        .replaceAll("{" + "factif" + "}", factif.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher la liste de tous les PartenaireBprices par IdVille
  ///
  /// Retourner la liste de tous les PartenaireBprices par IdVille.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List PartenaireBprice n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;List PartenaireBprice est vide  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;le parametre envoyer est null  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findAllByIdVilleUsingGET(String idVille) async {
    Object postBody = null;

    // verify required params are set
    if (idVille == null) {
      throw new ApiException(400, "Missing required param: idVille");
    }

    // create path and map variables
    String path = "/v1/findAllByIdVille/{idVille}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idVille" + "}", idVille.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Afficher l&#39;PointVente selon le PartenaireBprice envoyer
  ///
  /// Retourner le PointVente selon l&#39;idPartenaireBprice envoyer  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; List PartenaireBprice n&#39;est pas vide&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;Id est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PartenaireBprice n&#39;exist pas  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> findByIdPartenaireUsingGET(String idPartenaire) async {
    Object postBody = null;

    // verify required params are set
    if (idPartenaire == null) {
      throw new ApiException(400, "Missing required param: idPartenaire");
    }

    // create path and map variables
    String path = "/v1/findByIdPartenaire/{idPartenaire}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idPartenaire" + "}", idPartenaire.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Modifier un PartenaireBprice
  ///
  /// Retourner le PartenaireBprice modifié.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet modifié avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;PartenaireBprice est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PartenaireBprice n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;matricule deja utiliser  &lt;b&gt;result &#x3D; -3 :&lt;/b&gt; Query failed  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; matricule est vide  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; ville n&#39;exist pas  &lt;b&gt;result &#x3D; -5 :&lt;/b&gt; chart n&#39;exist pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> updatePartenaireBpriceUsingPUT(
      PartenaireBprice partenaireBprice) async {
    Object postBody = partenaireBprice;

    // verify required params are set
    if (partenaireBprice == null) {
      throw new ApiException(400, "Missing required param: partenaireBprice");
    }

    // create path and map variables
    String path = "/v1/UpdatePartenaireBprice".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'PUT', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
