import 'dart:convert';

import 'package:ad_caisse/request/ResponseList.dart';
import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/request/Utils.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

import './api_exception.dart';
import 'auth/api_key_auth.dart';
import 'auth/authentication.dart';
import 'auth/oauth.dart';
import 'model/billet_monnaie.dart';
import 'model/caisse.dart';
import 'model/caisse_type.dart';
import 'model/devise.dart';
import 'model/journee.dart';
import 'model/mode_reglement.dart';
import 'model/montant_ouverture.dart';
import 'model/operation.dart';
import 'model/operation_type.dart';
import 'model/partenaire_bprice.dart';
import 'model/point_vente.dart';
import 'model/session.dart';
import 'model/session_montanouverture.dart';
import 'model/table_caisse.dart';
import 'model/table_reservation_dto.dart';
import 'model/taxe.dart';

class QueryParam {
  String name;
  String value;

  QueryParam(this.name, this.value);
}

class ApiClient {
  String basePath;
  var client = http.Client();

  Map<String, String> _defaultHeaderMap = {};
  Map<String, Authentication> _authentications = {};

  final _RegList = new RegExp(r'^List<(.*)>$');
  final _RegMap = new RegExp(r'^Map<String,(.*)>$');

  ApiClient() {
    // Setup authentications (key: authentication name, value: authentication).
    _authentications['Bearer'] = new ApiKeyAuth("header", "Authorization");
    this.basePath = ServerUrl.url + "/bp-api-pos";
  }

  void addDefaultHeader(String key, String value) {
    _defaultHeaderMap[key] = value;
  }

  dynamic _deserialize(dynamic value, String targetType) {
    try {
      switch (targetType) {
        case 'String':
          return '$value';
        case 'int':
          return value is int ? value : int.parse('$value');
        case 'bool':
          return value is bool ? value : '$value'.toLowerCase() == 'true';
        case 'double':
          return value is double ? value : double.parse('$value');
        case 'BilletMonnaie':
          return new BilletMonnaie.fromJson(value);
        case 'Caisse':
          return new Caisse.fromJson(value);
        case 'CaisseType':
          return new CaisseType.fromJson(value);
        case 'Devise':
          return new Devise.fromJson(value);
        case 'Journee':
          return new Journee.fromJson(value);
        case 'ModeReglement':
          return new ModeReglement.fromJson(value);
        case 'MontantOuverture':
          return new MontantOuverture.fromJson(value);
        case 'Operation':
          return new Operation.fromJson(value);
        case 'OperationType':
          return new OperationType.fromJson(value);
        case 'PartenaireBprice':
          return new PartenaireBprice.fromJson(value);
        case 'PointVente':
          return new PointVente.fromJson(value);
        case 'Session':
          return new Session.fromJson(value);
        case 'SessionMontanouverture':
          return new SessionMontanouverture.fromJson(value);
        case 'TableCaisse':
          return new TableCaisse.fromJson(value);
        case 'TableReservationDto':
          return new TableReservationDto.fromJson(value);
        case 'Taxe':
          return new Taxe.fromJson(value);
        case 'ObjectSingle':
          return new ResponseSingle.fromJson(value);
        case 'Object':
          return new ResponseList.fromJson(value);
        default:
          {
            Match match;
            if (value is List &&
                (match = _RegList.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return value.map((v) => _deserialize(v, newTargetType)).toList();
            } else if (value is Map &&
                (match = _RegMap.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return new Map.fromIterables(value.keys,
                  value.values.map((v) => _deserialize(v, newTargetType)));
            }
          }
      }
    } catch (e, stack) {
      throw new ApiException.withInner(
          500, 'Exception during deserialization.', e, stack);
    }
    throw new ApiException(
        500, 'Could not find a suitable class for deserialization');
  }

  dynamic deserialize(String jsonVal, String targetType) {
    // Remove all spaces.  Necessary for reg expressions as well.
    targetType = targetType.replaceAll(' ', '');

    if (targetType == 'String') return jsonVal;

    var decodedJson = json.decode(jsonVal);
    return _deserialize(decodedJson, targetType);
  }

  String serialize(Object obj) {
    String serialized = '';
    if (obj == null) {
      serialized = '';
    } else {
      serialized = json.encode(obj);
    }
    return serialized;
  }

  // We don't use a Map<String, String> for queryParams.
  // If collectionFormat is 'multi' a key might appear multiple times.
  Future<Response> invokeAPI(
      String path,
      String method,
      Iterable<QueryParam> queryParams,
      Object body,
      Map<String, String> headerParams,
      Map<String, String> formParams,
      String contentType,
      List<String> authNames) async {
    _updateParamsForAuth(authNames, queryParams, headerParams);

    var ps = queryParams
        .where((p) => p.value != null)
        .map((p) => '${p.name}=${p.value}');
    String queryString = ps.isNotEmpty ? '?' + ps.join('&') : '';

    String url = basePath + path + queryString;

    bool isConnected = await Utils().checkInternet();
    if (!isConnected) {
      url = ServerUrl.urlSynchro + "/bp-api-pos" + path + queryString;
    }
    debugPrint('Current Server $url');
    headerParams.addAll(_defaultHeaderMap);
    headerParams['Content-Type'] = contentType;

    if (body is MultipartRequest) {
      var request = new MultipartRequest(method, Uri.parse(url));
      request.fields.addAll(body.fields);
      request.files.addAll(body.files);
      request.headers.addAll(body.headers);
      request.headers.addAll(headerParams);
      var response = await client.send(request);
      return Response.fromStream(response);
    } else {
      var msgBody = contentType == "application/x-www-form-urlencoded"
          ? formParams
          : serialize(body);
      final errorRet = Response(
          "{\"result\": -5,\"errorDescription\": \"Connexion perdue avec le serveur\\nVeuillez vérifier votre connexion\"}",
          200);
      switch (method) {
        case "POST":
          return client
              .post(Uri.parse(url), headers: headerParams, body: msgBody)
              .timeout(Duration(seconds: 300), onTimeout: () {
            return errorRet;
          });
        case "PUT":
          return client
              .put(Uri.parse(url), headers: headerParams, body: msgBody)
              .timeout(Duration(seconds: 50), onTimeout: () {
            return errorRet;
          });
        case "DELETE":
          return client
              .delete(Uri.parse(url), headers: headerParams)
              .timeout(Duration(seconds: 10), onTimeout: () {
            return errorRet;
          });
        case "PATCH":
          return client
              .patch(Uri.parse(url), headers: headerParams, body: msgBody)
              .timeout(Duration(seconds: 20), onTimeout: () {
            return errorRet;
          });
        default:
          return client
              .get(Uri.parse(url), headers: headerParams)
              .timeout(Duration(seconds: 50), onTimeout: () {
            return errorRet;
          });
      }
    }
  }

  /// Update query and header parameters based on authentication settings.
  /// @param authNames The authentications to apply
  void _updateParamsForAuth(List<String> authNames,
      List<QueryParam> queryParams, Map<String, String> headerParams) {
    authNames.forEach((authName) {
      Authentication auth = _authentications[authName];
      if (auth == null)
        throw new ArgumentError("Authentication undefined: " + authName);
      auth.applyToParams(queryParams, headerParams);
    });
  }

  void setAccessToken(String accessToken) {
    _authentications.forEach((key, auth) {
      if (auth is OAuth) {
        auth.setAccessToken(accessToken);
      }
    });
  }
}
