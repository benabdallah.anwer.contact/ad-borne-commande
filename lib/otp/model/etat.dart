
class Etat {
  
  int code = null;
  

  String message = null;
  
  Etat();

  @override
  String toString() {
    return 'Etat[code=$code, message=$message, ]';
  }

  Etat.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    code =
        json['code']
    ;
    message =
        json['message']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'message': message
     };
  }

  static List<Etat> listFromJson(List<dynamic> json) {
    return json == null ? new List<Etat>() : json.map((value) => new Etat.fromJson(value)).toList();
  }

  static Map<String, Etat> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Etat>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new Etat.fromJson(value));
    }
    return map;
  }
}

