
class OneTimePwd {
  
  DateTime dateCreation = null;
  

  DateTime dateExpiration = null;
  

  String destination = null;
  

  int etat = null;
  

  String id = null;
  

  String idApplication = null;
  

  String originatorSMS = null;
  

  String password = null;
  
  OneTimePwd();

  @override
  String toString() {
    return 'OneTimePwd[dateCreation=$dateCreation, dateExpiration=$dateExpiration, destination=$destination, etat=$etat, id=$id, idApplication=$idApplication, originatorSMS=$originatorSMS, password=$password, ]';
  }

  OneTimePwd.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dateCreation = json['dateCreation'] == null ? null : DateTime.parse(json['dateCreation']);
    dateExpiration = json['dateExpiration'] == null ? null : DateTime.parse(json['dateExpiration']);
    destination =
        json['destination']
    ;
    etat =
        json['etat']
    ;
    id =
        json['id']
    ;
    idApplication =
        json['idApplication']
    ;
    originatorSMS =
        json['originatorSMS']
    ;
    password =
        json['password']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'dateCreation': dateCreation == null ? '' : dateCreation.toIso8601String(),
      'dateExpiration': dateExpiration == null ? '' : dateExpiration.toIso8601String(),
      'destination': destination,
      'etat': etat,
      'id': id,
      'idApplication': idApplication,
      'originatorSMS': originatorSMS,
      'password': password
     };
  }

  static List<OneTimePwd> listFromJson(List<dynamic> json) {
    return json == null ? new List<OneTimePwd>() : json.map((value) => new OneTimePwd.fromJson(value)).toList();
  }

  static Map<String, OneTimePwd> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, OneTimePwd>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new OneTimePwd.fromJson(value));
    }
    return map;
  }
}

