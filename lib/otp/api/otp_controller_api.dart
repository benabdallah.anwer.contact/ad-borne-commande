import 'package:ad_caisse/otp/model/etat.dart';
import 'package:ad_caisse/otp/model/one_time_pwd.dart';
import 'package:http/http.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';

class OtpControllerApi {
  final ApiClient apiClient;

  OtpControllerApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// getCode
  ///
  ///
  Future<OneTimePwd> getCodeUsingGET(
      String idApplication, String destination) async {
    Object postBody = null;

    // verify required params are set
    if (idApplication == null) {
      throw new ApiException(400, "Missing required param: idApplication");
    }
    if (destination == null) {
      throw new ApiException(400, "Missing required param: destination");
    }

    // create path and map variables
    String path = "/otp/generate/{idApplication}/{destination}"
        .replaceAll("{format}", "json")
        .replaceAll("{" + "idApplication" + "}", idApplication.toString())
        .replaceAll("{" + "destination" + "}", destination.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = [];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'OneTimePwd') as OneTimePwd;
    } else {
      return null;
    }
  }

  /// getEtat
  ///
  ///
  Future<Etat> getEtatUsingGET(
      String destination, String idApplication, String code) async {
    Object postBody = null;

    // verify required params are set
    if (destination == null) {
      throw new ApiException(400, "Missing required param: destination");
    }
    if (idApplication == null) {
      throw new ApiException(400, "Missing required param: idApplication");
    }
    if (code == null) {
      throw new ApiException(400, "Missing required param: code");
    }

    // create path and map variables
    String path =
        "/otp/check/idApplication/{idApplication}/destination/{destination}/code/{code}"
            .replaceAll("{format}", "json")
            .replaceAll("{" + "destination" + "}", destination.toString())
            .replaceAll("{" + "idApplication" + "}", idApplication.toString())
            .replaceAll("{" + "code" + "}", code.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = [];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, postBody,
        headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Etat') as Etat;
    } else {
      return null;
    }
  }
}
