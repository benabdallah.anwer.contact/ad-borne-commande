import 'dart:convert';

import 'package:ad_caisse/request/ResponseSingle.dart';
import 'package:ad_caisse/utils/ServerUrl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';

import '../api.dart';
import '../api_client.dart';
import '../api_exception.dart';
import '../model/commande_dto.dart';

class OrderingEndPointApi {
  final ApiClient apiClient;

  OrderingEndPointApi([ApiClient apiClient])
      : apiClient = apiClient ?? defaultApiClient;

  /// Annulation d&#39;un detail de commande qui n&#39;est pas ecnore payée
  ///
  /// Retourner la commande modifie.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet annule avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object detail commande n&#39;existe pas  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;la commande n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> cancelOrderDetailsUsingPOST(String idDetails) async {
    Object postBody = idDetails;

    // verify required params are set
    if (idDetails == null) {
      throw new ApiException(400, "Missing required param: idDetails");
    }

    // create path and map variables
    String path = "/v1/cancelOrderDetails".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}
    var client = new IOClient();
    String url = ServerUrl.url + "/bp-api-ordering" + path;
    var response =
        await client.post(Uri.parse(url), headers: headerParams, body: idDetails);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// créer une Commande avec ses details ainsi les mouvements toute en verifiant les produits(stock)
  ///
  /// Retourner la commande créé.  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; objet créé avec succes&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;l&#39;object commande est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; la liste detail commande est vide  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; la session n&#39;existe pas  &lt;b&gt;result &#x3D; -8 :&lt;/b&gt; Le produit n&#39;existe pas  &lt;b&gt;result &#x3D; -11 :&lt;/b&gt; Stock non disponible  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> createCommandeAndDetailsCommandeUsingPOST(
      CommandeDto commande) async {
    Object postBody = commande;

    if (commande == null) {
      throw new ApiException(400, "Missing required param: commande");
    }

    // create path and map variables
    String path =
        "/v1/createCommandeAndDetailsCommande".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);
    debugPrint(json.encode(postBody));
    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      print(jsonEncode(postBody) + ' ' + response.body);
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// detecter si il au premier temps si il y a un ou plusierus pack(s) (si le point de vente autorisela detecte pack) et a la suite si il y a une ou plusieurs remises dans la commande passée avant l&#39;enregistrement
  ///
  /// Retourner la commande traitée et modifiée si il y une ou plusierus Pack(s)/remise(s).  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; commande traitée&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la commande est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; la liste detail commande est vide  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; la session n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<ResponseSingle> detectPackAndRemiseAndUpdateDetailsCommandUsingPOST(
      CommandeDto commande) async {
    Object postBody = commande;
    // verify required params are set
    if (commande == null) {
      throw new ApiException(400, "Missing required param: commande");
    }

    // create path and map variables
    String path = "/v1/detectPackAndRemiseAndUpdateDetailsCommande"
        .replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'ObjectSingle')
          as ResponseSingle;
    } else {
      return null;
    }
  }

  /// detecter si il y a un pack dans la commande passée avant l&#39;enregistrement
  ///
  /// Retourner la commande traitée et modifiée si il y un ou plusierus pack(s).  &lt;b&gt;result &#x3D; 1 :&lt;/b&gt; commande traitée&lt;/b&gt;   &lt;b&gt;result &#x3D; 0 :&lt;/b&gt;la commande est vide  &lt;b&gt;result &#x3D; -1 :&lt;/b&gt;PointVente n&#39;exist pas  &lt;b&gt;result &#x3D; -2 :&lt;/b&gt;id PointVente est null  &lt;b&gt;result &#x3D; -4 :&lt;/b&gt; la liste detail commande est vide  &lt;b&gt;result &#x3D; -6 :&lt;/b&gt; la session n&#39;existe pas  &lt;b&gt;result &#x3D; 401 :&lt;/b&gt; TOKEN NOT AUTHORIZED  &lt;b&gt;result &#x3D; 402 :&lt;/b&gt; TOKEN MISSING.
  Future<Object> detectPackAndUpdateDetailsCommandUsingPOST(
      CommandeDto commande) async {
    Object postBody = commande;

    // verify required params are set
    if (commande == null) {
      throw new ApiException(400, "Missing required param: commande");
    }

    // create path and map variables
    String path =
        "/v1/detectPackAndUpdateDetailsCommande".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType =
        contentTypes.length > 0 ? contentTypes[0] : "application/json";
    List<String> authNames = ["Bearer"];

    if (contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = new MultipartRequest(null, null);

      if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'POST', queryParams,
        postBody, headerParams, formParams, contentType, authNames);

    if (response.statusCode >= 400) {
      throw new ApiException(response.statusCode, response.body);
    } else if (response.body != null) {
      return apiClient.deserialize(response.body, 'Object') as Object;
    } else {
      return null;
    }
  }
}
