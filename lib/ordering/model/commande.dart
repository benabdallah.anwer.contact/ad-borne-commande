import 'client.dart';

class Commande {
  List<ClientBPrice> clients = [];

  DateTime dateCreation = null;

  DateTime dateGeneration = null;

  int fPaye = null;

  String idCommande = null;

  String idPointVente = null;

  String idSession = null;

  int isForClientBPrice = null;

  double montant = null;

  int numCommande = null;

  double reste = null;

  String idTable;

  String typeCommande;

  String idMessageCommande;

  String adresse;

  int numTable;

  Commande();

  @override
  String toString() {
    return 'Commande[ClientBPrices=$clients, dateCreation=$dateCreation, dateGeneration=$dateGeneration, fPaye=$fPaye, idCommande=$idCommande, idPointVente=$idPointVente, idSession=$idSession, isForClientBPrice=$isForClientBPrice, montant=$montant, numCommande=$numCommande, reste=$reste,adresse=$adresse ]';
  }

  Commande.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    clients = ClientBPrice.listFromJson(json['clients']);
    dateCreation = json['dateCreation'] == null || json['dateCreation'] == ''
        ? null
        : DateTime.parse(json['dateCreation']);
    dateGeneration = json['dateGeneration'] == null || json['dateGeneration'] == ''
        ? null
        : DateTime.parse(json['dateGeneration']);
    fPaye = json['fPaye'];
    idCommande = json['idCommande'];
    idPointVente = json['idPointVente'];
    idSession = json['idSession'];
    isForClientBPrice = json['isForClientBPrice'];
    montant = json['montant'];
    numCommande = json['numCommande'];
    reste = json['reste'];
    idTable = json['idTable'];
    typeCommande = json['typeCommande'];
    idMessageCommande = json['idMessageCommande'];
    adresse = json['adresse'];
    numTable = json['numTable'];
  }

  Map<String, dynamic> toJson() {
    return {
      'clients': clients,
      'dateCreation':
          dateCreation == null ||    dateCreation =='' ? '' : dateCreation.toIso8601String(),
      'dateGeneration': dateGeneration == null || dateGeneration ==''
          ? ''
          : dateGeneration.toIso8601String(),
      'fPaye': fPaye,
      'idCommande': idCommande,
      'idPointVente': idPointVente,
      'idSession': idSession,
      'isForClientBPrice': isForClientBPrice,
      'montant': montant,
      'numCommande': numCommande,
      'reste': reste,
      'idTable': idTable,
      'typeCommande': typeCommande,
      'idMessageCommande': idMessageCommande,
      'adresse':adresse,
      'numTable': numTable,
    };
  }

  static List<Commande> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<Commande>()
        : json.map((value) => new Commande.fromJson(value)).toList();
  }

  static Map<String, Commande> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Commande>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new Commande.fromJson(value));
    }
    return map;
  }
}
