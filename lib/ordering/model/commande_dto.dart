import 'commande.dart';
import 'commande_details.dart';

class CommandeDto {
  Commande commande = null;

  List<CommandeDetails> details = [];

  String idsession = null;

  CommandeDto();

  @override
  String toString() {
    return 'CommandeDto[commande=$commande, details=$details, idsession=$idsession, ]';
  }

  CommandeDto.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    commande = new Commande.fromJson(json['commande']);
    details = CommandeDetails.listFromJson(json['details']);
    idsession = json['idsession'];
  }

  Map<String, dynamic> toJson() {
    return {'commande': commande, 'details': details, 'idsession': idsession};
  }

  static List<CommandeDto> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<CommandeDto>()
        : json.map((value) => new CommandeDto.fromJson(value)).toList();
  }

  static Map<String, CommandeDto> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CommandeDto>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new CommandeDto.fromJson(value));
    }
    return map;
  }
}
