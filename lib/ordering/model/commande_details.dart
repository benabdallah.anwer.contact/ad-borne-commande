import 'package:ad_caisse/admin/model/utilisateur.dart';

class CommandeDetails {
  String designation = null;

  String idCommande = null;

  String idDetailComm = null;

  String idPack = null;

  String idProduit = null;

  int isAnnule = null;

  double prix = null;

  double prixSansRemise = null;

  double quantite = null;

  double remise = null;

  Utilisateur employe;

  bool recharge;

  String mesure = '';

  int isModifier = null;

  String ingredients;

  bool isChecked;

  List<dynamic> idsIngrdComp;

  CommandeDetails();

  @override
  String toString() {
    return 'CommandeDetails[designation=$designation, idCommande=$idCommande, idDetailComm=$idDetailComm, idPack=$idPack, idProduit=$idProduit, isAnnule=$isAnnule, prix=$prix, prixSansRemise=$prixSansRemise, quantite=$quantite, remise=$remise,ingredients=$ingredients ]';
  }

  CommandeDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    ingredients = json['ingredient'];
    designation = json['designation'];
    idCommande = json['idCommande'];
    idDetailComm = json['idDetailComm'];
    idPack = json['idPack'];
    idProduit = json['idProduit'];
    isAnnule = json['isAnnule'];
    prix = json['prix'];
    prixSansRemise = json['prixSansRemise'];
    quantite = json['quantite'];
    remise = json['remise'];
    employe = json['employe'] != null
        ? new Utilisateur.fromJson(json['employe'])
        : null;
     idsIngrdComp = json['idsIngrdComp'];
  }

  Map<String, dynamic> toJson() {
    return {
      'designation': designation,
      'idCommande': idCommande,
      'idDetailComm': idDetailComm,
      'idPack': idPack,
      'idProduit': idProduit,
      'isAnnule': isAnnule,
      'prix': prix,
      'prixSansRemise': prixSansRemise,
      'quantite': quantite,
      'remise': remise,
      'employe': employe != null ? employe.toJson() : null,
      'ingredient': ingredients,
      'idsIngrdComp':idsIngrdComp
    };
  }

  static List<CommandeDetails> listFromJson(List<dynamic> json) {
    return json == null
        ? new List<CommandeDetails>()
        : json.map((value) => new CommandeDetails.fromJson(value)).toList();
  }

  static Map<String, CommandeDetails> mapFromJson(
      Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, CommandeDetails>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) =>
          map[key] = new CommandeDetails.fromJson(value));
    }
    return map;
  }
}
