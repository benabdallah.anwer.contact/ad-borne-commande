# Changelog

## [1.1.1]

* Updated readme

## [1.1.0]

* Renamed plugin to cross_local_storage

## [1.0.1]

* Fixed issue when opening empty file

## [1.0.0]

* Created common interface for wrapper of SharedPreferences on both Mobile (Android/iOS), Web and Desktop (Windows, macOS, Linux)
