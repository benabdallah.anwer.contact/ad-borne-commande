//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <printing/printing_plugin.h>
#include <sentry_flutter/sentry_flutter_plugin.h>
#include <serial_number/serial_number_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  PrintingPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("PrintingPlugin"));
  SentryFlutterPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("SentryFlutterPlugin"));
  SerialNumberPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("SerialNumberPlugin"));
}
